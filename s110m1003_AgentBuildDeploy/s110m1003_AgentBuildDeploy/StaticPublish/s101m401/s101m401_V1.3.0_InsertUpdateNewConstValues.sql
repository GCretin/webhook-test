
--s101m401

USE [s101m401]
GO

INSERT INTO [dbo].[Droit]
           ([nom]
           ,[libelle])
     VALUES
           ('ANNULATION_EVENEMENT_FERME'
           ,'Peut annuler un �v�nement ferm�')
GO

USE [s101m401]
GO

INSERT INTO [dbo].[Droit]
           ([nom]
           ,[libelle])
     VALUES
           ('DUPLICATION_EVENEMENT'
           ,'Peut dupliquer un �v�nement')
GO

USE [s101m401]
GO

INSERT INTO [dbo].[Droit]
           ([nom]
           ,[libelle])
     VALUES
           ('DUPLICATION_EVENEMENT_FERME'
           ,'Peut dupliquer un �v�nement ferm�')
GO

USE [s101m401]
GO

INSERT INTO [dbo].[Droit]
           ([nom]
           ,[libelle])
     VALUES
           ('MODIFICATION_CONSIGNE'
           ,'Peut �diter les consignes d''exploitation')
GO




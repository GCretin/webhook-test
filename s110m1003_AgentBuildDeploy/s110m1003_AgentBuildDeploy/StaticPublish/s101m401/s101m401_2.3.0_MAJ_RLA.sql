﻿/*
Script de déploiement pour s101m401_temp

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "s101m401_temp"
:setvar DefaultFilePrefix "s101m401_temp"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL13.RLAPRODUCTION01\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL13.RLAPRODUCTION01\MSSQL\DATA\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Modification de [dbo].[DiffusionList]...';


GO
ALTER TABLE [dbo].[DiffusionList]
    ADD [isPublic]    INT CONSTRAINT [DF_DiffusionList_isPublic] DEFAULT ((0)) NOT NULL,
        [isDynamique] INT CONSTRAINT [DF_DiffusionList_isDynamique] DEFAULT ((0)) NOT NULL;


GO
PRINT N'Modification de [dbo].[DifusionListReciptient]...';


GO
ALTER TABLE [dbo].[DifusionListReciptient] ALTER COLUMN [email] NVARCHAR (MAX) NULL;

ALTER TABLE [dbo].[DifusionListReciptient] ALTER COLUMN [mobile] NVARCHAR (MAX) NULL;

ALTER TABLE [dbo].[DifusionListReciptient] ALTER COLUMN [nom] NVARCHAR (MAX) NULL;

ALTER TABLE [dbo].[DifusionListReciptient] ALTER COLUMN [prenom] NVARCHAR (MAX) NULL;


GO
PRINT N'Mise à jour terminée.';


GO

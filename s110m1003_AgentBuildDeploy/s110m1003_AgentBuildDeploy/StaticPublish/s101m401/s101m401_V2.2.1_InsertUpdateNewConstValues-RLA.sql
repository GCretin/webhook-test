USE [s101m401]

---------------------Login utilisateur-----------------------


GO
UPDATE [dbo].[Utilisateur]
   SET [loginWindows] = 'f.bouazza' 
 WHERE email = 'f.bouazza@beemotion.eu'
GO
GO
UPDATE [dbo].[Utilisateur]
   SET [loginWindows] = 'f'
 WHERE email = 'f@f.fr'
GO
GO
UPDATE [dbo].[Utilisateur]
   SET [loginWindows] = 'pcs.rla'
 WHERE email = 'pcs.rla@lignesdazur.fr'
GO
GO
UPDATE [dbo].[Utilisateur]
   SET [loginWindows] = 'thomas.casalengo'
 WHERE email = 'thomas.casalengo@lignesdazur.fr'
GO
GO
UPDATE [dbo].[Utilisateur]
   SET [loginWindows] = 'philippe.chatelain'
 WHERE email = 'philippe.chatelain@lignesdazur.fr'
GO
GO
UPDATE [dbo].[Utilisateur]
   SET [loginWindows] = 'chsct.rla'
 WHERE email = 'chsct.rla@lignesdazur.fr'
GO
GO
UPDATE [dbo].[Utilisateur]
   SET [loginWindows] = 'christophe.kaminski'
 WHERE email ='christophe.kaminski@lignesdazur.fr'
GO
GO
UPDATE [dbo].[Utilisateur]
   SET [loginWindows] = 'cyrille.abbad'
 WHERE email ='cyrille.abbad@lignesdazur.fr'
GO
GO
UPDATE [dbo].[Utilisateur]
   SET [loginWindows] = 'mehdi.mastouri'
 WHERE email ='mehdi.mastouri@lignesdazur.fr'
GO
GO
UPDATE [dbo].[Utilisateur]
   SET [loginWindows] = 'stephane.talbot'
 WHERE email ='stephane.talbot@lignesdazur.fr'
GO
GO
UPDATE [dbo].[Utilisateur]
   SET [loginWindows] = 'gregory.dagleian'
 WHERE email ='gregory.dagleian@lignesdazur.fr'
GO
GO
UPDATE [dbo].[Utilisateur]
   SET [loginWindows] = 'jean-marie.leandri'
 WHERE email ='jean-marie.leandri@lignesdazur.fr'
GO
GO
UPDATE [dbo].[Utilisateur]
   SET [loginWindows] = 'alexandre.cristina'
 WHERE email ='alexandre.cristina@lignesdazur.fr'
GO
GO
UPDATE [dbo].[Utilisateur]
   SET [loginWindows] = 'sebastien.doze'
 WHERE email ='sebastien.doze@lignesdazur.fr'
GO
GO
UPDATE [dbo].[Utilisateur]
   SET [loginWindows] = 'evelyne.carbonnier'
 WHERE email ='evelyne.carbonnier@lignesdazur.fr'
GO
GO
UPDATE [dbo].[Utilisateur]
   SET [loginWindows] = 'thierry.bertona'
 WHERE email ='thierry.bertona@lignesdazur.fr'
GO
GO
UPDATE [dbo].[Utilisateur]
   SET [loginWindows] = 'faze�'
 WHERE email ='faze�@lignesdazur.fr'
GO
GO
UPDATE [dbo].[Utilisateur]
   SET [loginWindows] = 'sylvain.scanu'
 WHERE email ='sylvain.scanu@lignesdazur.fr'
GO
GO
UPDATE [dbo].[Utilisateur]
   SET [loginWindows] = 'karim.azizi'
 WHERE email ='karim.azizi@lignesdazur.fr'
GO
GO
UPDATE [dbo].[Utilisateur]
   SET [loginWindows] = 'guillaume.domenichini'
 WHERE email ='guillaume.domenichini@lignesdazur.fr'
GO
GO
UPDATE [dbo].[Utilisateur]
   SET [loginWindows] = 'patrice.lentz'
 WHERE email ='patrice.lentz@lignesdazur.fr'
GO
GO
UPDATE [dbo].[Utilisateur]
   SET [loginWindows] = 'fabien.giudicelli'
 WHERE email ='fabien.giudicelli@lignesdazur.fr'
GO
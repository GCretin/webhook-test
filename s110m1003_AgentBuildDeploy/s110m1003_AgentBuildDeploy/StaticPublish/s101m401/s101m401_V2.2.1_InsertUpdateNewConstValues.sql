USE [s101m401]

---------------------FIL-637 Nouveaux droits -----------------------
GO
IF NOT EXISTS(select * from [Droit] where [nom] = 'RESSOURCE_ACCESS')
INSERT INTO [dbo].[Droit]
           ([nom]
           ,[libelle])
     VALUES
           ('RESSOURCE_ACCESS'
           ,'Peut acc�der aux ressources')
GO

GO
IF NOT EXISTS(select * from [Droit] where [nom] = 'DIFFUSION_MANUEL_EVENEMENT_PUBLIC')
INSERT INTO [dbo].[Droit]
           ([nom]
           ,[libelle])
     VALUES
           ('DIFFUSION_MANUEL_EVENEMENT_PUBLIC'
           ,'Peut diffuser manuellement (listes publiques)')
GO

---------------------FIL-642 Consignes exploitation -----------------------
GO
IF NOT EXISTS(select * from [Droit] where [nom] = 'CONSIGNE_EXPLOITATION_ACCESS')
INSERT INTO [dbo].[Droit]
           ([nom]
           ,[libelle])
     VALUES
           ('CONSIGNE_EXPLOITATION_ACCESS'
           ,'Peut acc�der aux consignes exploitation')
GO
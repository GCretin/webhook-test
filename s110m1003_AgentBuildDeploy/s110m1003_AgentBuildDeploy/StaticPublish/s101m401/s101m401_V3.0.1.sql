USE s101m401


--*******************************************************
--*******************************************************
---------------------G�n�ral 2.2.1-----------------------
--*******************************************************
--*******************************************************

---------------------FIL-637 Nouveaux droits -----------------------
IF NOT EXISTS(select * from [Droit] where [nom] = 'RESSOURCE_ACCESS')
INSERT INTO [dbo].[Droit]
           ([nom]
           ,[libelle])
     VALUES
           ('RESSOURCE_ACCESS'
           ,'Peut acc�der aux ressources')

IF NOT EXISTS(select * from [Droit] where [nom] = 'DIFFUSION_MANUEL_EVENEMENT_PUBLIC')
INSERT INTO [dbo].[Droit]
           ([nom]
           ,[libelle])
     VALUES
           ('DIFFUSION_MANUEL_EVENEMENT_PUBLIC'
           ,'Peut diffuser manuellement (listes publiques)')

---------------------FIL-642 Consignes exploitation -----------------------

IF NOT EXISTS(select * from [Droit] where [nom] = 'CONSIGNE_EXPLOITATION_ACCESS')
INSERT INTO [dbo].[Droit]
           ([nom]
           ,[libelle])
     VALUES
           ('CONSIGNE_EXPLOITATION_ACCESS'
           ,'Peut acc�der aux consignes exploitation')

--*******************************************************
--*******************************************************
---------------------G�n�ral 2.3.0-----------------------
--*******************************************************
--*******************************************************

---------------------FIL-624 Action de suivis modification CTVH -----------------------
IF NOT EXISTS(select * from [Droit] where nom = 'MODIFICATION_EVENEMENT_ACTION_SUIVI')
INSERT INTO [dbo].[Droit]
           ([nom]
           ,[libelle])
     VALUES
           ('MODIFICATION_EVENEMENT_ACTION_SUIVI'
           ,'Peut modifier les actions de suivis des �v�nements')


---------------------------------------------------------------------------------------
UPDATE [dbo].[Droit]
   SET [libelle] = 'Peut dupliquer et annuler un �v�nement'
 WHERE [nom] = 'ANNULATION_EVENEMENT';

 UPDATE [dbo].[Droit]
   SET [libelle] = 'Peut dupliquer et annuler un �v�nement ferm�'
 WHERE [nom] = 'ANNULATION_EVENEMENT_FERME';


--------------------------------------------------------
--------------------------------------------------------
------------        NUMERO DE VERSION        -----------
--------------------------------------------------------
--------------------------------------------------------
UPDATE [dbo].[DBinfos]
   SET [valeur] = '2.3.0'
 WHERE nom = 'version';

 --------------------------------------------------------
--------------------------------------------------------
------------        NUMERO DE VERSION        -----------
--------------------------------------------------------
--------------------------------------------------------
UPDATE [dbo].[DBinfos]
   SET [valeur] = '3.0.1'
 WHERE nom = 'version';


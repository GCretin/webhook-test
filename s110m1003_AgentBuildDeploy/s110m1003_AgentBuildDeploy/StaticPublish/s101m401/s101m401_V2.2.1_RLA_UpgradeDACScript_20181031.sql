/*
Script de déploiement pour s101m401

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "s101m401"
:setvar DefaultFilePrefix "s101m401"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.RLARECETTE\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.RLARECETTE\MSSQL\DATA\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO

IF (SELECT OBJECT_ID('tempdb..#tmpErrors')) IS NOT NULL DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
GO
BEGIN TRANSACTION
GO
PRINT N'Suppression de contrainte sans nom sur [DataSync].[scope_info_dss]...';


GO
ALTER TABLE [DataSync].[scope_info_dss] DROP CONSTRAINT [DF__scope_inf__scope__30F848ED];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de contrainte sans nom sur [DataSync].[scope_info_dss]...';


GO
ALTER TABLE [DataSync].[scope_info_dss] DROP CONSTRAINT [DF__scope_inf__scope__31EC6D26];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [DataSync].[provision_marker_dss]...';


GO
DROP TABLE [DataSync].[provision_marker_dss];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [DataSync].[schema_info_dss]...';


GO
DROP TABLE [DataSync].[schema_info_dss];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [DataSync].[scope_config_dss]...';


GO
DROP TABLE [DataSync].[scope_config_dss];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [DataSync].[scope_info_dss]...';


GO
DROP TABLE [DataSync].[scope_info_dss];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[DifusionListReciptient]...';


GO
ALTER TABLE [dbo].[DifusionListReciptient] ALTER COLUMN [email] NVARCHAR (MAX) NOT NULL;

ALTER TABLE [dbo].[DifusionListReciptient] ALTER COLUMN [mobile] NVARCHAR (MAX) NOT NULL;

ALTER TABLE [dbo].[DifusionListReciptient] ALTER COLUMN [nom] NVARCHAR (MAX) NOT NULL;

ALTER TABLE [dbo].[DifusionListReciptient] ALTER COLUMN [prenom] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[Actualites]...';


GO
CREATE TABLE [dbo].[Actualites] (
    [id_actualites]        INT            IDENTITY (1, 1) NOT NULL,
    [texte]                NVARCHAR (MAX) NOT NULL,
    [id_actualites_reseau] INT            NOT NULL,
    [fontColor]            NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_Actualites] PRIMARY KEY CLUSTERED ([id_actualites] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[UtilisateurAction]...';


GO
CREATE TABLE [dbo].[UtilisateurAction] (
    [id_utilisateurAction]             INT            IDENTITY (1, 1) NOT NULL,
    [id_utilisateurAction_utilisateur] INT            NOT NULL,
    [actionNom]                        NVARCHAR (MAX) NOT NULL,
    [date]                             DATE           NOT NULL,
    [heure]                            TIME (7)       NOT NULL,
    CONSTRAINT [PK_UtilisateurAction] PRIMARY KEY CLUSTERED ([id_utilisateurAction] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[UtilisateurPreference]...';


GO
CREATE TABLE [dbo].[UtilisateurPreference] (
    [id_utilisateurPreference]             INT            IDENTITY (1, 1) NOT NULL,
    [id_utilisateurPreference_utilisateur] INT            NOT NULL,
    [cles]                                 NVARCHAR (MAX) NOT NULL,
    [valeur]                               NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_UtilisateurPreference] PRIMARY KEY CLUSTERED ([id_utilisateurPreference] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_Actualites_Reseau]...';


GO
ALTER TABLE [dbo].[Actualites] WITH NOCHECK
    ADD CONSTRAINT [FK_Actualites_Reseau] FOREIGN KEY ([id_actualites_reseau]) REFERENCES [dbo].[Reseau] ([id_reseau]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_UtilisateurAction_Utilisateur]...';


GO
ALTER TABLE [dbo].[UtilisateurAction] WITH NOCHECK
    ADD CONSTRAINT [FK_UtilisateurAction_Utilisateur] FOREIGN KEY ([id_utilisateurAction_utilisateur]) REFERENCES [dbo].[Utilisateur] ([id_utilisateurs]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_UtilisateurPreference_Utilisateur]...';


GO
ALTER TABLE [dbo].[UtilisateurPreference] WITH NOCHECK
    ADD CONSTRAINT [FK_UtilisateurPreference_Utilisateur] FOREIGN KEY ([id_utilisateurPreference_utilisateur]) REFERENCES [dbo].[Utilisateur] ([id_utilisateurs]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO

IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT N'Succès de la mise à jour de la portion de base de données traitée.'
COMMIT TRANSACTION
END
ELSE PRINT N'Échec de la mise à jour de la portion de base de données traitée.'
GO
DROP TABLE #tmpErrors
GO
PRINT N'Vérification de données existantes par rapport aux nouvelles contraintes';


GO
USE [$(DatabaseName)];


GO
ALTER TABLE [dbo].[Actualites] WITH CHECK CHECK CONSTRAINT [FK_Actualites_Reseau];

ALTER TABLE [dbo].[UtilisateurAction] WITH CHECK CHECK CONSTRAINT [FK_UtilisateurAction_Utilisateur];

ALTER TABLE [dbo].[UtilisateurPreference] WITH CHECK CHECK CONSTRAINT [FK_UtilisateurPreference_Utilisateur];


GO
PRINT N'Mise à jour terminée.';


GO

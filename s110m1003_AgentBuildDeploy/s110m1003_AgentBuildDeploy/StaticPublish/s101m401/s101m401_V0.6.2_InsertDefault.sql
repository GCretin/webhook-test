------------------------------------------------------------------------------------------------------------------------------------
--									0.6.2											 											  --
------------------------------------------------------------------------------------------------------------------------------------

--************************  DBinfos  *******************************

SET IDENTITY_INSERT [dbo].[DBinfos] ON
INSERT INTO [dbo].[DBinfos] ([id_dbinfos], [nom], [valeur]) VALUES (1, N'version', N'0.6.2')
INSERT INTO [dbo].[DBinfos] ([id_dbinfos], [nom], [valeur]) VALUES (3, N'uid', N's101m401')
INSERT INTO [dbo].[DBinfos] ([id_dbinfos], [nom], [valeur]) VALUES (1003, N'env', N'---')
SET IDENTITY_INSERT [dbo].[DBinfos] OFF

--************************  Droit  *******************************

SET IDENTITY_INSERT [dbo].[Droit] ON
INSERT INTO [dbo].[Droit] ([id_droit], [nom], [libelle]) VALUES (2, N'ADMINISTRATION', N'Peux administrer la solution')
INSERT INTO [dbo].[Droit] ([id_droit], [nom], [libelle]) VALUES (3, N'REPORTING_ADMIN', N'Peux configurer des requêtes')
INSERT INTO [dbo].[Droit] ([id_droit], [nom], [libelle]) VALUES (4, N'REPORTING_EXE', N'Peux exécuter des requêtes')
SET IDENTITY_INSERT [dbo].[Droit] OFF
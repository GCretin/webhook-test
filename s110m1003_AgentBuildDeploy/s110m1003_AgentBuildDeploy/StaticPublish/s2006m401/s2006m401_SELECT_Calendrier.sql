use s2006m401
SELECT UniteHoraire.nom as 'Unite',PeriodeHoraire.nom as 'Periode', date,Graphicage.nom as 'Graphique',scenario, Graphicage.id_graphicage
  FROM [Calendrier],Graphicage,PeriodeHoraire,UniteHoraire
  where id_calendrier_graphicage = id_graphicage
  and id_periodeHoraire = id_graphicage_PeriodeHoraire
  and id_periodeHoraire_uniteHoraire = id_uniteHoraire
  order by date desc, Graphicage.nom, scenario
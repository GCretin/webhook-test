use s2006m401;

delete from Calendrier;
delete from CalendrierDateMax;

delete from Passage;
delete from Course;

delete from AffectationMaterielRoulantServiceVoiture;
delete from ServiceVoiture;

delete from AffectationPersonnelServiceAgent;
delete from ServiceAgent;

delete from Habillage;
delete from Graphicage;

delete from PeriodeHoraire;

delete from UniteHoraire;
use s2006m401;

select	/*Calendrier.date,*/
		Graphicage.nom as 'Graphique',Graphicage.scenario,
		ServiceVoiture.nom as 'SV',
		/*ServiceVoiture.IsFusionOrigin,ServiceVoiture.isFusionResult,*/
		ServiceVoiture.jourSemaine,
		ConstTypeDeJour.code,
		Course.code as 'Course',Course.jourSemaine,
		ConstCoureType.nom,
		Parcours.code as 'Parcours',
		Ligne.code as 'Ligne',
		Point.nomCourt,Point.nomLong,
		Passage .heureArr
from Calendrier,Graphicage,ServiceVoiture,ConstTypeDeJour,Course,ConstCoureType,Parcours,Ligne,Passage,Point 
where date = '2018-03-26'
and  Graphicage.id_graphicage = Calendrier.id_calendrier_graphicage
and ServiceVoiture.id_serviceVoiture_graphicage = Graphicage.id_graphicage
and ConstTypeDeJour.id_constTypeDeJour = Graphicage.id_graphicage_typeDeJour
and Course.id_course_serviceVoiture =  ServiceVoiture.id_serviceVoiture
and ConstCoureType.id_courseType =  Course.id_course_courseType
and Parcours.id_parcours = Course.id_course_parcours
and Ligne.id_ligne = Parcours.id_parcours_ligne
and Passage.id_passage_course = Course.id_course
and Point.id_point = Passage.id_passage_point
order by SV,Course,heureArr;

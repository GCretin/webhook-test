USE [s2006m401]
GO
---------------------G�n�ral 2.1.0-----------------------

---------------------FIL-392 Hastus code types de jour-----------------------
GO
INSERT INTO [dbo].[ConstTypeDeJour]([code],[jourSemaine])
     VALUES ('Mer_Ven','35')
GO
GO
INSERT INTO [dbo].[ConstTypeDeJour]([code],[jourSemaine])
     VALUES ('L_Ma_J ','124')
GO
GO
INSERT INTO [dbo].[ConstTypeDeJour]([code],[jourSemaine])
     VALUES ('L_Ma_Me_J','1234')
GO
GO
INSERT INTO [dbo].[ConstTypeDeJour]([code],[jourSemaine])
     VALUES ('Ma_Me_J','234')
GO
GO
UPDATE [dbo].[ConstTypeDeJour] 
		SET [code] = 'L_Ma_J_V'
		WHERE [code] = 'Lun_Mar_Jeu_Ven'
GO
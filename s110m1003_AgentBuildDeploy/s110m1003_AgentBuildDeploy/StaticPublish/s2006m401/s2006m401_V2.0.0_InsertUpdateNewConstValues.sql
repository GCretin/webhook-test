
USE [s2006m401]

GO
delete  from [dbo].[ConstPointType];
GO

USE [s2006m401]
GO
INSERT INTO [dbo].[ConstPointType] ([code]) VALUES ('STATION')
GO
GO
INSERT INTO [dbo].[ConstPointType] ([code]) VALUES ('STATION_PRINCIPALE')
GO
GO
INSERT INTO [dbo].[ConstPointType] ([code]) VALUES ('TERMINUS')
GO
GO
INSERT INTO [dbo].[ConstPointType] ([code]) VALUES ('SLT')
GO
GO
INSERT INTO [dbo].[ConstPointType] ([code]) VALUES ('ROND_POINT')
GO
GO
INSERT INTO [dbo].[ConstPointType] ([code]) VALUES ('DEPOT')
GO
GO
INSERT INTO [dbo].[ConstPointType] ([code]) VALUES ('CARREFOUR')
GO
GO
INSERT INTO [dbo].[ConstPointType] ([code]) VALUES ('LIMITATION_VITESSE')
GO
GO
INSERT INTO [dbo].[ConstPointType] ([code]) VALUES ('POINT_SINGULIER')
GO
GO
INSERT INTO [dbo].[ConstPointType] ([code]) VALUES ('RETOURNEMENT')
GO


--Point type station pour tous
INSERT INTO [dbo].[LnkPointTypePoint] ([id_lnkPointTypePoint_Point],[id_lnkPointTypePoint_constPointType])
     select Point.id_point, ConstPointType.id_constPointType from Point,ConstPointType  where code ='STATION'                                           
           
GO

--Point type retournement pour certaines
INSERT INTO [dbo].[LnkPointTypePoint] ([id_lnkPointTypePoint_Point],[id_lnkPointTypePoint_constPointType])
     select Point.id_point, ConstPointType.id_constPointType from Point,ConstPointType  where code ='RETOURNEMENT'
	 and (nomLong = 'Henri Sappia')                                                   
GO
INSERT INTO [dbo].[LnkPointTypePoint] ([id_lnkPointTypePoint_Point],[id_lnkPointTypePoint_constPointType])
     select Point.id_point, ConstPointType.id_constPointType from Point,ConstPointType  where code ='RETOURNEMENT'
	 and (nomLong = 'Gorbella')                                                   
GO
INSERT INTO [dbo].[LnkPointTypePoint] ([id_lnkPointTypePoint_Point],[id_lnkPointTypePoint_constPointType])
     select Point.id_point, ConstPointType.id_constPointType from Point,ConstPointType  where code ='RETOURNEMENT'
	 and (nomLong = 'Lib�ration')                                                   
GO
INSERT INTO [dbo].[LnkPointTypePoint] ([id_lnkPointTypePoint_Point],[id_lnkPointTypePoint_constPointType])
     select Point.id_point, ConstPointType.id_constPointType from Point,ConstPointType  where code ='RETOURNEMENT'
	 and (nomLong = 'Gare Thiers')                                                   
GO
INSERT INTO [dbo].[LnkPointTypePoint] ([id_lnkPointTypePoint_Point],[id_lnkPointTypePoint_constPointType])
     select Point.id_point, ConstPointType.id_constPointType from Point,ConstPointType  where code ='RETOURNEMENT'
	 and (nomLong = 'Mass�na')                                                   
GO
INSERT INTO [dbo].[LnkPointTypePoint] ([id_lnkPointTypePoint_Point],[id_lnkPointTypePoint_constPointType])
     select Point.id_point, ConstPointType.id_constPointType from Point,ConstPointType  where code ='RETOURNEMENT'
	 and (nomLong = 'Op�ra - Vieille Ville')                                                   
GO
INSERT INTO [dbo].[LnkPointTypePoint] ([id_lnkPointTypePoint_Point],[id_lnkPointTypePoint_constPointType])
     select Point.id_point, ConstPointType.id_constPointType from Point,ConstPointType  where code ='RETOURNEMENT'
	 and (nomLong = 'Garibaldi')                                                   
GO
INSERT INTO [dbo].[LnkPointTypePoint] ([id_lnkPointTypePoint_Point],[id_lnkPointTypePoint_constPointType])
     select Point.id_point, ConstPointType.id_constPointType from Point,ConstPointType  where code ='RETOURNEMENT'
	 and (nomLong = 'Vauban')                                                   
GO
INSERT INTO [dbo].[LnkPointTypePoint] ([id_lnkPointTypePoint_Point],[id_lnkPointTypePoint_constPointType])
     select Point.id_point, ConstPointType.id_constPointType from Point,ConstPointType  where code ='RETOURNEMENT'
	 and (nomLong = 'Pont Michel')                                                   
GO
INSERT INTO [dbo].[LnkPointTypePoint] ([id_lnkPointTypePoint_Point],[id_lnkPointTypePoint_constPointType])
     select Point.id_point, ConstPointType.id_constPointType from Point,ConstPointType  where code ='RETOURNEMENT'
	 and (nomLong = 'H�pital Pasteur')                                                   
GO


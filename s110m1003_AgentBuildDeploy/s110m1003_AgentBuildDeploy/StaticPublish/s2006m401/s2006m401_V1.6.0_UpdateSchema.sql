﻿/*
Script de déploiement pour s2006m401

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "s2006m401"
:setvar DefaultFilePrefix "s2006m401"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.DEVSQLSP\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.DEVSQLSP\MSSQL\DATA\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Modification de [dbo].[Ligne]...';


GO
ALTER TABLE [dbo].[Ligne]
    ADD [id_ligne_constServiceType] INT NULL;


GO
PRINT N'Création de [dbo].[ConstServiceType]...';


GO
CREATE TABLE [dbo].[ConstServiceType] (
    [id_constServiceType] INT            IDENTITY (1, 1) NOT NULL,
    [code]                NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_ConstServiceType] PRIMARY KEY CLUSTERED ([id_constServiceType] ASC)
);


GO
PRINT N'Création de [dbo].[FK_Ligne_ServiceType]...';


GO
ALTER TABLE [dbo].[Ligne] WITH NOCHECK
    ADD CONSTRAINT [FK_Ligne_ServiceType] FOREIGN KEY ([id_ligne_constServiceType]) REFERENCES [dbo].[ConstServiceType] ([id_constServiceType]);


GO
PRINT N'Vérification de données existantes par rapport aux nouvelles contraintes';


GO
USE [$(DatabaseName)];


GO
ALTER TABLE [dbo].[Ligne] WITH CHECK CHECK CONSTRAINT [FK_Ligne_ServiceType];


GO
PRINT N'Mise à jour terminée.';


GO

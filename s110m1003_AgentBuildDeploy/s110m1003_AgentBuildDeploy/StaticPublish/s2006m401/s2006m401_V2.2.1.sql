USE [s2006m401]

---------------------Général 2.2.1 insertion des nouvelles valeurs avec vérification-----------------------

---------------------FIL-392 Hastus code types de jour-----------------------
GO
IF NOT EXISTS(select * from [ConstTypeDeJour] where code = 'Mer_Ven')
BEGIN
INSERT INTO [dbo].[ConstTypeDeJour]([code],[jourSemaine])
     VALUES ('Mer_Ven','35')
END
GO

GO
IF NOT EXISTS(select * from [ConstTypeDeJour] where code = 'L_Ma_J')
BEGIN
INSERT INTO [dbo].[ConstTypeDeJour]([code],[jourSemaine])
     VALUES ('L_Ma_J ','124')
END
GO

GO
IF NOT EXISTS(select * from [ConstTypeDeJour] where code = 'L_Ma_Me_J')
BEGIN
INSERT INTO [dbo].[ConstTypeDeJour]([code],[jourSemaine])
     VALUES ('L_Ma_Me_J','1234')
END
GO

GO
IF NOT EXISTS(select * from [ConstTypeDeJour] where code = 'Ma_Me_J')
BEGIN
INSERT INTO [dbo].[ConstTypeDeJour]([code],[jourSemaine])
     VALUES ('Ma_Me_J','234')
END
GO

GO
UPDATE [dbo].[ConstTypeDeJour] 
		SET [code] = 'L_Ma_J_V'
		WHERE [code] = 'Lun_Mar_Jeu_Ven'
GO

---------------------Description Parcours avec direction -----------------------
GO
IF NOT EXISTS(select * from ConstConfigurationRules where nom = 'DESCRIPTION_PARCOURS_ORIGINE_DESTINATION_DIRECTION')
BEGIN
INSERT INTO [dbo].[ConstConfigurationRules]
           ([nom]
           ,[libelle])
     VALUES
           ('DESCRIPTION_PARCOURS_ORIGINE_DESTINATION_DIRECTION'
           ,'Description des parcours ORIGINE_DESTINATION avec direction')
END
GO
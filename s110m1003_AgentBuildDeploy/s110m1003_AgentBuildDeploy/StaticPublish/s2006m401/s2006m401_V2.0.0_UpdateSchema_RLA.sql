﻿/*
Script de déploiement pour s2006m401

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "s2006m401"
:setvar DefaultFilePrefix "s2006m401"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL14.DEV\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL14.DEV\MSSQL\DATA\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
/*
La colonne [dbo].[Point].[id_point_PointType] est en cours de suppression, des données risquent d'être perdues.
*/

--IF EXISTS (select top 1 1 from [dbo].[Point])
--    RAISERROR (N'Lignes détectées. Arrêt de la mise à jour du schéma en raison d''''un risque de perte de données.', 16, 127) WITH NOWAIT

GO
/*
La table [dbo].[PointType] est en cours de suppression. Le déploiement va s'arrêter si la table contient des données.
*/

--IF EXISTS (select top 1 1 from [dbo].[PointType])
--    RAISERROR (N'Lignes détectées. Arrêt de la mise à jour du schéma en raison d''''un risque de perte de données.', 16, 127) WITH NOWAIT

GO
PRINT N'Suppression de [dbo].[DF_Point_id_point_PointType]...';


GO
ALTER TABLE [dbo].[Point] DROP CONSTRAINT [DF_Point_id_point_PointType];


GO
PRINT N'Suppression de [dbo].[FK_PointType_Point]...';


GO
ALTER TABLE [dbo].[Point] DROP CONSTRAINT [FK_PointType_Point];


GO
PRINT N'Suppression de [dbo].[PointType]...';


GO
DROP TABLE [dbo].[PointType];


GO
PRINT N'Modification de [dbo].[Passage]...';


GO
ALTER TABLE [dbo].[Passage] ALTER COLUMN [heureArr] NVARCHAR (50) NOT NULL;

ALTER TABLE [dbo].[Passage] ALTER COLUMN [heureDep] NVARCHAR (50) NOT NULL;


GO
PRINT N'Modification de [dbo].[Point]...';


GO
ALTER TABLE [dbo].[Point] DROP COLUMN [id_point_PointType];


GO
PRINT N'Modification de [dbo].[ServiceVoiture]...';


GO
ALTER TABLE [dbo].[ServiceVoiture]
    ADD [isFusionResult]                 INT CONSTRAINT [DF_ServiceVoiture_isFusionResult] DEFAULT ((0)) NOT NULL,
        [IsFusionOrigin]                 INT CONSTRAINT [DF_ServiceVoiture_IsFusionOrigin] DEFAULT ((0)) NOT NULL,
        [id_serviceVoiture_fusionResult] INT NULL;


GO
PRINT N'Création de [dbo].[ConstPointType]...';


GO
CREATE TABLE [dbo].[ConstPointType] (
    [id_constPointType] INT        IDENTITY (1, 1) NOT NULL,
    [code]              NCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([id_constPointType] ASC)
);


GO
PRINT N'Création de [dbo].[LnkPointTypePoint]...';


GO
CREATE TABLE [dbo].[LnkPointTypePoint] (
    [id_lnkPointTypePoint]                INT IDENTITY (1, 1) NOT NULL,
    [id_lnkPointTypePoint_Point]          INT NOT NULL,
    [id_lnkPointTypePoint_constPointType] INT NOT NULL,
    CONSTRAINT [PK_LnkPointTypePoint] PRIMARY KEY CLUSTERED ([id_lnkPointTypePoint] ASC)
);


GO
PRINT N'Création de [dbo].[FK_LnkPointTypePoint_Point]...';


GO
ALTER TABLE [dbo].[LnkPointTypePoint] WITH NOCHECK
    ADD CONSTRAINT [FK_LnkPointTypePoint_Point] FOREIGN KEY ([id_lnkPointTypePoint_Point]) REFERENCES [dbo].[Point] ([id_point]);


GO
PRINT N'Création de [dbo].[FK_LnkPointTypePoint_PointType]...';


GO
ALTER TABLE [dbo].[LnkPointTypePoint] WITH NOCHECK
    ADD CONSTRAINT [FK_LnkPointTypePoint_PointType] FOREIGN KEY ([id_lnkPointTypePoint_constPointType]) REFERENCES [dbo].[ConstPointType] ([id_constPointType]);


GO
PRINT N'Création de [dbo].[FK_Passage_Course]...';


GO
ALTER TABLE [dbo].[Passage] WITH NOCHECK
    ADD CONSTRAINT [FK_Passage_Course] FOREIGN KEY ([id_passage_course]) REFERENCES [dbo].[Course] ([id_course]);


GO
PRINT N'Création de [dbo].[FK_Point_Point]...';


GO
ALTER TABLE [dbo].[Point] WITH NOCHECK
    ADD CONSTRAINT [FK_Point_Point] FOREIGN KEY ([id_point]) REFERENCES [dbo].[Point] ([id_point]);


GO
PRINT N'Création de [dbo].[FK_ServiceVoiture_ServiceVoiture]...';


GO
ALTER TABLE [dbo].[ServiceVoiture] WITH NOCHECK
    ADD CONSTRAINT [FK_ServiceVoiture_ServiceVoiture] FOREIGN KEY ([id_serviceVoiture_fusionResult]) REFERENCES [dbo].[ServiceVoiture] ([id_serviceVoiture]);


GO
PRINT N'Vérification de données existantes par rapport aux nouvelles contraintes';


GO
USE [$(DatabaseName)];


GO
ALTER TABLE [dbo].[LnkPointTypePoint] WITH CHECK CHECK CONSTRAINT [FK_LnkPointTypePoint_Point];

ALTER TABLE [dbo].[LnkPointTypePoint] WITH CHECK CHECK CONSTRAINT [FK_LnkPointTypePoint_PointType];

ALTER TABLE [dbo].[Passage] WITH CHECK CHECK CONSTRAINT [FK_Passage_Course];

ALTER TABLE [dbo].[Point] WITH CHECK CHECK CONSTRAINT [FK_Point_Point];

ALTER TABLE [dbo].[ServiceVoiture] WITH CHECK CHECK CONSTRAINT [FK_ServiceVoiture_ServiceVoiture];


GO
PRINT N'Mise à jour terminée.';


GO

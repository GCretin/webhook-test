use s2006m401;

select UniteHoraire.id_uniteHoraire_reseaus101m401,UniteHoraire.id_uniteHoraire, UniteHoraire.nom as 'Unite',
PeriodeHoraire.id_periodeHoraire, PeriodeHoraire.nom as 'P�riode',
Graphicage.id_graphicage,Graphicage.nom as 'Graphicage', Graphicage.description, Graphicage.scenario,
ConstTypeDeJour.code as 'Type Jour',
ServiceVoiture.id_serviceVoiture, ServiceVoiture.nom as 'SV', ServiceVoiture.id_externe, ServiceVoiture.jourSemaine

from UniteHoraire, PeriodeHoraire, Graphicage, ConstTypeDeJour, ServiceVoiture,Course

where UniteHoraire.id_uniteHoraire_reseaus101m401 = 1
and PeriodeHoraire.id_periodeHoraire_uniteHoraire = UniteHoraire.id_uniteHoraire

and Graphicage.id_graphicage_PeriodeHoraire = PeriodeHoraire.id_periodeHoraire
and Graphicage.id_graphicage_typeDeJour = ConstTypeDeJour.id_constTypeDeJour
and ServiceVoiture.id_serviceVoiture_graphicage = Graphicage.id_graphicage
and Course.id_course_serviceVoiture = ServiceVoiture.id_serviceVoiture;

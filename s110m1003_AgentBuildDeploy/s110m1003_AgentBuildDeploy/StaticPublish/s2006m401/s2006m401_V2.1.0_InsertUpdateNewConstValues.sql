USE [s2006m401]
GO
---------------------G�n�ral 2.1.0-----------------------

---------------------FIL-235 Hastus descritpion des parcours-----------------------

INSERT INTO [dbo].[ConstConfigurationRules]
           ([nom]
           ,[libelle])
     VALUES
           ('DESCRIPTION_PARCOURS_DESTINATION_ORIGINE_VIA'
           ,'Description des parcours DESTINATION_ORIGINE_VIA')

INSERT INTO [dbo].[ConstConfigurationRules]
           ([nom]
           ,[libelle])
     VALUES
           ('DESCRIPTION_PARCOURS_ORIGINE_DESTINATION'
           ,'Description des parcours ORIGINE_DESTINATION')

INSERT INTO [dbo].[ConstConfigurationRules]
           ([nom]
           ,[libelle])
     VALUES
           ('DESCRIPTION_PARCOURS_UNIFORMISE_DESTINATION_IDENTIQUE'
           ,'Description des parcours uniformaisation pour les destinations identiques')

INSERT INTO [dbo].[CfgConfigurationRulesGeneral]
           ([id_cfgConfigurationRulesGeneral_ConfigurationRules]
           ,[id_reseaus101m401])
     VALUES
           (1
           ,1)


INSERT INTO [dbo].[ConstConfigurationRules]
           ([nom]
           ,[libelle]
           ,[valueNumerie])
     VALUES
           ('PARCOURS_PROPORTION_MINIMUM'
           ,'Proportion minimum des parcours pour affichage'
           ,5)

INSERT INTO [dbo].[ConstConfigurationRules]
           ([nom]
           ,[libelle]
           ,[valueNumerie])
     VALUES
           ('PARCOURS_NB_APPLICABLE_MINIMUM'
           ,'Nombre minimum des parcours pour affichage'
           ,5)

INSERT INTO [dbo].[ConstConfigurationRules]
           ([nom]
           ,[libelle]
           ,[valueNumerie])
     VALUES
           ('PARCOURS_IMPORTANT_PROPORTION_MINIMUM'
           ,'Proportion minimum des parcours important'
           ,20)


INSERT INTO [dbo].[ConstTransporteur]
           ([code])
     VALUES
           ('INTERNE')

INSERT INTO [dbo].[ConstTransporteur]
           ([code])
     VALUES
           ('SOUS_TRAITANT')


GO

------------------------------------------------------------------------------------------------------------------------------------
--									0.6.2											 											  --
------------------------------------------------------------------------------------------------------------------------------------
USE [s2006m401]
GO
SET IDENTITY_INSERT [dbo].[CoureType] ON 

GO
INSERT [dbo].[CoureType] ([id_courseType], [nom]) VALUES (1, N'Régulière')
GO
INSERT [dbo].[CoureType] ([id_courseType], [nom]) VALUES (2, N'HLP Entrée')
GO
INSERT [dbo].[CoureType] ([id_courseType], [nom]) VALUES (3, N'HLP Sortie')
GO
INSERT [dbo].[CoureType] ([id_courseType], [nom]) VALUES (4, N'HLP Interne')
GO
SET IDENTITY_INSERT [dbo].[CoureType] OFF
GO
SET IDENTITY_INSERT [dbo].[DBinfos] ON 

GO
INSERT [dbo].[DBinfos] ([id_dbinfos], [nom], [valeur]) VALUES (1, N'version', N'0.6.2')
GO
INSERT [dbo].[DBinfos] ([id_dbinfos], [nom], [valeur]) VALUES (2, N'uid', N's2006m401')
GO
INSERT [dbo].[DBinfos] ([id_dbinfos], [nom], [valeur]) VALUES (3, N'env', N'----')
GO
SET IDENTITY_INSERT [dbo].[DBinfos] OFF
GO
SET IDENTITY_INSERT [dbo].[JourType] ON 

GO
INSERT [dbo].[JourType] ([id_jourType], [nom]) VALUES (1, N'Lu')
GO
INSERT [dbo].[JourType] ([id_jourType], [nom]) VALUES (2, N'Ma')
GO
INSERT [dbo].[JourType] ([id_jourType], [nom]) VALUES (3, N'Me')
GO
INSERT [dbo].[JourType] ([id_jourType], [nom]) VALUES (4, N'Je')
GO
INSERT [dbo].[JourType] ([id_jourType], [nom]) VALUES (5, N'Ve')
GO
INSERT [dbo].[JourType] ([id_jourType], [nom]) VALUES (6, N'Sa')
GO
INSERT [dbo].[JourType] ([id_jourType], [nom]) VALUES (7, N'Di')
GO
INSERT [dbo].[JourType] ([id_jourType], [nom]) VALUES (8, N'Férié')
GO
SET IDENTITY_INSERT [dbo].[JourType] OFF
GO
SET IDENTITY_INSERT [dbo].[PersonnelType] ON 

GO
INSERT [dbo].[PersonnelType] ([id_personnelType], [nom]) VALUES (1, N'Conducteur')
GO
SET IDENTITY_INSERT [dbo].[PersonnelType] OFF
GO
SET IDENTITY_INSERT [dbo].[PointType] ON 

GO
INSERT [dbo].[PointType] ([id_pointType], [nom]) VALUES (1, N'Station                                           ')
GO
INSERT [dbo].[PointType] ([id_pointType], [nom]) VALUES (2, N'Station principale                                ')
GO
INSERT [dbo].[PointType] ([id_pointType], [nom]) VALUES (3, N'Terminus                                          ')
GO
INSERT [dbo].[PointType] ([id_pointType], [nom]) VALUES (4, N'Feux                                              ')
GO
INSERT [dbo].[PointType] ([id_pointType], [nom]) VALUES (5, N'Rond point                                        ')
GO
INSERT [dbo].[PointType] ([id_pointType], [nom]) VALUES (6, N'Depot                                             ')
GO
INSERT [dbo].[PointType] ([id_pointType], [nom]) VALUES (7, N'Carrefour                                         ')
GO
INSERT [dbo].[PointType] ([id_pointType], [nom]) VALUES (8, N'Limitation vitesse                                ')
GO
INSERT [dbo].[PointType] ([id_pointType], [nom]) VALUES (9, N'Point singulier                                   ')
GO
INSERT [dbo].[PointType] ([id_pointType], [nom]) VALUES (10, N'Aucun                                             ')
GO
SET IDENTITY_INSERT [dbo].[PointType] OFF
GO
SET IDENTITY_INSERT [dbo].[ServiceType] ON 

GO
INSERT [dbo].[ServiceType] ([id_serviceType], [nom]) VALUES (1, N'Régulier')
GO
INSERT [dbo].[ServiceType] ([id_serviceType], [nom]) VALUES (2, N'Express')
GO
INSERT [dbo].[ServiceType] ([id_serviceType], [nom]) VALUES (3, N'Scolaire')
GO
INSERT [dbo].[ServiceType] ([id_serviceType], [nom]) VALUES (4, N'Provisoire')
GO
INSERT [dbo].[ServiceType] ([id_serviceType], [nom]) VALUES (5, N'Nuit')
GO
INSERT [dbo].[ServiceType] ([id_serviceType], [nom]) VALUES (6, N'Spécial')
GO
SET IDENTITY_INSERT [dbo].[ServiceType] OFF
GO

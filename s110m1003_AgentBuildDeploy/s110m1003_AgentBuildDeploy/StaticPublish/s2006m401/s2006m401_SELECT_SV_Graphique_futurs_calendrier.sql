use s2006m401;

select g.nom,g.scenario, sv.nom,count(c.id_course) as 'nb. course',sv.jourSemaine, sv.id_serviceVoiture,sv.id_externe, g.id_graphicage
from ServiceVoiture sv
left join Graphicage g on g.id_graphicage = sv.id_serviceVoiture_graphicage
left join Course c on c.id_course_serviceVoiture = sv.id_serviceVoiture
where sv.id_serviceVoiture_graphicage in (select g.id_graphicage from Calendrier cal left join Graphicage g on cal.id_calendrier_graphicage = g.id_graphicage where cal.date >= getdate())
group by g.id_graphicage,g.nom,g.scenario,sv.id_serviceVoiture,sv.nom,sv.id_externe,sv.jourSemaine
order by g.id_graphicage,g.nom,g.scenario,sv.nom;

select g.nom,g.scenario, count(sv.id_serviceVoiture) as 'nb. sv', g.id_graphicage
from Graphicage g
left join ServiceVoiture sv on g.id_graphicage = sv.id_serviceVoiture_graphicage
where sv.id_serviceVoiture_graphicage in (select g.id_graphicage from Calendrier cal left join Graphicage g on cal.id_calendrier_graphicage = g.id_graphicage where cal.date >= getdate())
group by g.id_graphicage,g.nom,g.scenario
order by g.nom,g.scenario;





use s2006m401;

GO
--COURSE

CREATE NONCLUSTERED INDEX [_dta_index_Course_7_1013578649__K9_K1_K7_2_3_4_5_6_8_10_11_9987] ON [dbo].[Course]
(
	[id_course_serviceVoiture] ASC,
	[id_course] ASC,
	[id_course_courseType] ASC
)
INCLUDE([heureDeb],[heureDebInt],[heureFin],[heureFinInt],[id_course_parcours],[id_externe],[jourSemaine],[code]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]


GO

CREATE STATISTICS [_dta_stat_1013578649_1_7] ON [dbo].[Course]([id_course], [id_course_courseType])
GO

CREATE STATISTICS [_dta_stat_1013578649_7_9_1] ON [dbo].[Course]([id_course_courseType], [id_course_serviceVoiture], [id_course])
GO

CREATE STATISTICS [_dta_stat_1013578649_7_6] ON [dbo].[Course]([id_course_courseType], [id_course_parcours])

GO

CREATE STATISTICS [_dta_stat_1013578649_9_7_6] ON [dbo].[Course]([id_course_serviceVoiture], [id_course_courseType], [id_course_parcours])

GO

--Passage
CREATE NONCLUSTERED INDEX [_dta_index_Passage_7_1429580131__K7_1_2_3_4_5_6] ON [dbo].[Passage]
(
	[id_passage_course] ASC
)
INCLUDE([id_passage],[heureArr],[heureArrInt],[heureDep],[heureDepInt],[id_passage_point]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

CREATE STATISTICS [_dta_stat_1429580131_7_6] ON [dbo].[Passage]([id_passage_course], [id_passage_point])
GO

--SV
CREATE NONCLUSTERED INDEX [_dta_index_ServiceVoiture_7_1573580644__K5_K1_2_3_4_6_7_8] ON [dbo].[ServiceVoiture]
(
	[id_serviceVoiture_graphicage] ASC,
	[id_serviceVoiture] ASC
)
INCLUDE([nom],[id_externe],[jourSemaine],[isFusionResult]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

CREATE STATISTICS [_dta_stat_1573580644_1_5] ON [dbo].[ServiceVoiture]([id_serviceVoiture], [id_serviceVoiture_graphicage])
GO

--LnkTronconParcours
GO
CREATE STATISTICS [_dta_stat_1301579675_3_2] ON [dbo].[LnkTronconParcours]([id_lnkTronconParcours_Parcours], [id_lnkTronconParcours_Troncon])
GO

--SA
CREATE NONCLUSTERED INDEX [_dta_index_ServiceAgent_7_1541580530__K5_1_2_3_4_6_7_8_9_10_11_12_13] ON [dbo].[ServiceAgent]
(
	[id_serviceAgent_habillage] ASC
)
INCLUDE([id_serviceAgent],[nom],[id_externe],[jourSemaine],[id_pointPriseService],[id_pointDebut],[id_pointFin],[id_pointFinService],[heurePriseService],[heureDebut],[heureFin],[heureFinService]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]




USE s2006m401


--*******************************************************
--*******************************************************
---------------------G�n�ral 2.2.1-----------------------
--*******************************************************
--*******************************************************-

---------------------FIL-392 Hastus code types de jour-----------------------
IF NOT EXISTS(select * from [ConstTypeDeJour] where code = 'Mer_Ven')
BEGIN
INSERT INTO [dbo].[ConstTypeDeJour]([code],[jourSemaine])
     VALUES ('Mer_Ven','35')
END

IF NOT EXISTS(select * from [ConstTypeDeJour] where code = 'L_Ma_J')
BEGIN
INSERT INTO [dbo].[ConstTypeDeJour]([code],[jourSemaine])
     VALUES ('L_Ma_J ','124')
END

IF NOT EXISTS(select * from [ConstTypeDeJour] where code = 'L_Ma_Me_J')
BEGIN
INSERT INTO [dbo].[ConstTypeDeJour]([code],[jourSemaine])
     VALUES ('L_Ma_Me_J','1234')
END

IF NOT EXISTS(select * from [ConstTypeDeJour] where code = 'Ma_Me_J')
BEGIN
INSERT INTO [dbo].[ConstTypeDeJour]([code],[jourSemaine])
     VALUES ('Ma_Me_J','234')
END

UPDATE [dbo].[ConstTypeDeJour] 
		SET [code] = 'L_Ma_J_V'
		WHERE [code] = 'Lun_Mar_Jeu_Ven'

---------------------Description Parcours avec direction -----------------------
IF NOT EXISTS(select * from ConstConfigurationRules where nom = 'DESCRIPTION_PARCOURS_ORIGINE_DESTINATION_DIRECTION')
BEGIN
INSERT INTO [dbo].[ConstConfigurationRules]
           ([nom]
           ,[libelle])
     VALUES
           ('DESCRIPTION_PARCOURS_ORIGINE_DESTINATION_DIRECTION'
           ,'Description des parcours ORIGINE_DESTINATION avec direction')
END


--*******************************************************
--*******************************************************
---------------------G�n�ral 2.3.0-----------------------
--*******************************************************
--*******************************************************

--------------------------------------------------------
--------------------------------------------------------
------------        NUMERO DE VERSION        -----------
--------------------------------------------------------
--------------------------------------------------------
UPDATE [dbo].[DBinfos]
   SET [valeur] = '2.3.0'
 WHERE nom = 'version';


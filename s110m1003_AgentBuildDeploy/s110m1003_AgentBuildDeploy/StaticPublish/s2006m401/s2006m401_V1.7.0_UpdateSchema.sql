﻿/*
Script de déploiement pour s2006m401

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "s2006m401"
:setvar DefaultFilePrefix "s2006m401"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.DEVSQLSP\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.DEVSQLSP\MSSQL\DATA\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
/*
La colonne [dbo].[Passage].[id_passage_course] de la table [dbo].[Passage] doit être ajoutée mais la colonne ne comporte pas de valeur par défaut et n'autorise pas les valeurs NULL. Si la table contient des données, le script ALTER ne fonctionnera pas. Pour éviter ce problème, vous devez ajouter une valeur par défaut à la colonne, la marquer comme autorisant les valeurs Null ou activer la génération de smart-defaults en tant qu'option de déploiement.
*/

IF EXISTS (select top 1 1 from [dbo].[Passage])
    RAISERROR (N'Lignes détectées. Arrêt de la mise à jour du schéma en raison d''''un risque de perte de données.', 16, 127) WITH NOWAIT

GO
/*
La table [dbo].[LnkCoursePassage] est en cours de suppression. Le déploiement va s'arrêter si la table contient des données.
*/

IF EXISTS (select top 1 1 from [dbo].[LnkCoursePassage])
    RAISERROR (N'Lignes détectées. Arrêt de la mise à jour du schéma en raison d''''un risque de perte de données.', 16, 127) WITH NOWAIT

GO
PRINT N'Suppression de [dbo].[FK_LnkCoursePassage_Passage]...';


GO
ALTER TABLE [dbo].[LnkCoursePassage] DROP CONSTRAINT [FK_LnkCoursePassage_Passage];


GO
PRINT N'Suppression de [dbo].[FK_Passage_Point]...';


GO
ALTER TABLE [dbo].[Passage] DROP CONSTRAINT [FK_Passage_Point];


GO
PRINT N'Suppression de [dbo].[FK_LnkCoursePassage_Course]...';


GO
ALTER TABLE [dbo].[LnkCoursePassage] DROP CONSTRAINT [FK_LnkCoursePassage_Course];


GO
PRINT N'Suppression de [dbo].[LnkCoursePassage]...';


GO
DROP TABLE [dbo].[LnkCoursePassage];


GO
PRINT N'Début de la régénération de la table [dbo].[Passage]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Passage] (
    [id_passage]        INT      IDENTITY (1, 1) NOT NULL,
    [heureArr]          TIME (7) NOT NULL,
    [heureArrInt]       INT      NOT NULL,
    [heureDep]          TIME (7) NOT NULL,
    [heureDepInt]       INT      NOT NULL,
    [id_passage_point]  INT      NOT NULL,
    [id_passage_course] INT      NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK__Passage__C49BD3E7ECAAF17A1] PRIMARY KEY CLUSTERED ([id_passage] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Passage])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Passage] ON;
        INSERT INTO [dbo].[tmp_ms_xx_Passage] ([id_passage], [heureArr], [heureArrInt], [heureDep], [heureDepInt], [id_passage_point])
        SELECT   [id_passage],
                 [heureArr],
                 [heureArrInt],
                 [heureDep],
                 [heureDepInt],
                 [id_passage_point]
        FROM     [dbo].[Passage]
        ORDER BY [id_passage] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Passage] OFF;
    END

DROP TABLE [dbo].[Passage];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Passage]', N'Passage';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK__Passage__C49BD3E7ECAAF17A1]', N'PK__Passage__C49BD3E7ECAAF17A', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Création de [dbo].[FK_Passage_Point]...';


GO
ALTER TABLE [dbo].[Passage] WITH NOCHECK
    ADD CONSTRAINT [FK_Passage_Point] FOREIGN KEY ([id_passage_point]) REFERENCES [dbo].[Point] ([id_point]);


GO
PRINT N'Vérification de données existantes par rapport aux nouvelles contraintes';


GO
USE [$(DatabaseName)];


GO
ALTER TABLE [dbo].[Passage] WITH CHECK CHECK CONSTRAINT [FK_Passage_Point];


GO
PRINT N'Mise à jour terminée.';


GO

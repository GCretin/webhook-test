use s2006m401;

delete from LnkTronconParcours;
delete from Troncon;
delete from LnkPointTypePoint;
delete from Point;

delete from Parcours;
delete from Direction;
delete from Via;

delete from LnkLigneGroupeLigne;
delete from Ligne;
delete from LigneGroupe;


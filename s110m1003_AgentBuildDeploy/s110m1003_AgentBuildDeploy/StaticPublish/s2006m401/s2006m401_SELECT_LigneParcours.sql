use s2006m401;

select Ligne.id_ligne_reseau,Ligne.code,Ligne.nom,Parcours.id_parcours,Parcours.code,Parcours.nom,Parcours.deleted,
Direction.id_direction,Direction.code,Direction.libelle,Direction.code_girouette,Troncon.id_troncon,
Troncon.id_pointDep,Troncon.id_pointArr,Point.id_point,Point.nomCourt,Point.nomLong
from ligne, Parcours, Direction, LnkTronconParcours, Troncon
 Left Join dbo.Point ON Troncon.id_pointDep = Point.id_point OR Troncon.id_pointArr = Point.id_point
 where Ligne.id_ligne_reseau = 1
 and Parcours.id_parcours_ligne = Ligne.id_ligne
 and Direction.id_direction = Parcours.id_parcours_direction
 and LnkTronconParcours.id_lnkTronconParcours_Parcours = Parcours.id_parcours
 and Troncon.id_troncon = LnkTronconParcours.id_lnkTronconParcours_Troncon
 order by Ligne.id_ligne, Parcours.id_parcours, Troncon.id_troncon, Troncon.id_pointDep;


USE [s2006m401]

---------------------Général 2.2.1 insertion des nouvelles valeurs avec vérification-----------------------

---------------------Description Parcours avec direction -----------------------
GO
IF NOT EXISTS(select * from ConstConfigurationRules where nom = 'DESCRIPTION_PARCOURS_ORIGINE_DESTINATION_DIRECTION')

INSERT INTO [dbo].[ConstConfigurationRules]
           ([nom]
           ,[libelle])
     VALUES
           ('DESCRIPTION_PARCOURS_ORIGINE_DESTINATION_DIRECTION'
           ,'Description des parcours ORIGINE_DESTINATION avec direction')
GO
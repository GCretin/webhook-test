/*
Script de déploiement pour s2006m401

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "s2006m401"
:setvar DefaultFilePrefix "s2006m401"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.RLARECETTE\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.RLARECETTE\MSSQL\DATA\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO

IF (SELECT OBJECT_ID('tempdb..#tmpErrors')) IS NOT NULL DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
GO
BEGIN TRANSACTION
GO
PRINT N'Suppression de contrainte sans nom sur [DataSync].[scope_info_dss]...';


GO
ALTER TABLE [DataSync].[scope_info_dss] DROP CONSTRAINT [DF__scope_inf__scope__5441852A];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de contrainte sans nom sur [DataSync].[scope_info_dss]...';


GO
ALTER TABLE [DataSync].[scope_info_dss] DROP CONSTRAINT [DF__scope_inf__scope__5535A963];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_LnkPointTypePoint_Point]...';


GO
ALTER TABLE [dbo].[LnkPointTypePoint] DROP CONSTRAINT [FK_LnkPointTypePoint_Point];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_Passage_Point]...';


GO
ALTER TABLE [dbo].[Passage] DROP CONSTRAINT [FK_Passage_Point];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_Point_Point]...';


GO
ALTER TABLE [dbo].[Point] DROP CONSTRAINT [FK_Point_Point];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_Troncon_Point_1]...';


GO
ALTER TABLE [dbo].[Troncon] DROP CONSTRAINT [FK_Troncon_Point_1];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_Troncon_Point_2]...';


GO
ALTER TABLE [dbo].[Troncon] DROP CONSTRAINT [FK_Troncon_Point_2];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [DataSync].[provision_marker_dss]...';


GO
DROP TABLE [DataSync].[provision_marker_dss];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [DataSync].[schema_info_dss]...';


GO
DROP TABLE [DataSync].[schema_info_dss];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [DataSync].[scope_config_dss]...';


GO
DROP TABLE [DataSync].[scope_config_dss];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [DataSync].[scope_info_dss]...';


GO
DROP TABLE [DataSync].[scope_info_dss];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[Ligne]...';


GO
ALTER TABLE [dbo].[Ligne]
    ADD [id_ligne_constTransporteur] INT NULL,
        [isLocked]                   INT CONSTRAINT [DF_Ligne_isLocked] DEFAULT ((0)) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Début de la régénération de la table [dbo].[Point]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Point] (
    [id_point]        INT           IDENTITY (1, 1) NOT NULL,
    [nomCourt]        NVARCHAR (50) NOT NULL,
    [nomLong]         NVARCHAR (50) NULL,
    [longitude]       FLOAT (53)    CONSTRAINT [DF_Point_longitue] DEFAULT ((0)) NOT NULL,
    [latitude]        FLOAT (53)    CONSTRAINT [DF_Point_latitude] DEFAULT ((0)) NOT NULL,
    [id_point_reseau] INT           NOT NULL,
    [deleted]         FLOAT (53)    NULL,
    [isLocked]        INT           CONSTRAINT [DF_Point_isLocked] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_IX_Point1] UNIQUE NONCLUSTERED ([id_point] ASC),
    CONSTRAINT [tmp_ms_xx_constraint_PK__Point__0AD99761B494CF831] PRIMARY KEY CLUSTERED ([id_point] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Point])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Point] ON;
        INSERT INTO [dbo].[tmp_ms_xx_Point] ([id_point], [nomCourt], [nomLong], [id_point_reseau], [deleted])
        SELECT   [id_point],
                 [nomCourt],
                 [nomLong],
                 [id_point_reseau],
                 [deleted]
        FROM     [dbo].[Point]
        ORDER BY [id_point] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Point] OFF;
    END

DROP TABLE [dbo].[Point];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Point]', N'Point';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_IX_Point1]', N'IX_Point', N'OBJECT';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK__Point__0AD99761B494CF831]', N'PK__Point__0AD99761B494CF83', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[ServiceAgent]...';


GO
ALTER TABLE [dbo].[ServiceAgent]
    ADD [id_pointPriseService] INT           NULL,
        [id_pointDebut]        INT           NULL,
        [id_pointFin]          INT           NULL,
        [id_pointFinService]   INT           NULL,
        [heurePriseService]    NVARCHAR (50) NULL,
        [heureDebut]           NVARCHAR (50) NULL,
        [heureFin]             NVARCHAR (50) NULL,
        [heureFinService]      NVARCHAR (50) NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[CfgConfigurationRulesGeneral]...';


GO
CREATE TABLE [dbo].[CfgConfigurationRulesGeneral] (
    [id_cfgConfigurationRulesGeneral]                    INT IDENTITY (1, 1) NOT NULL,
    [id_cfgConfigurationRulesGeneral_ConfigurationRules] INT NOT NULL,
    [id_reseaus101m401]                                  INT NOT NULL,
    CONSTRAINT [PK_CfgConfigurationRulesGeneral] PRIMARY KEY CLUSTERED ([id_cfgConfigurationRulesGeneral] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[ConstConfigurationRules]...';


GO
CREATE TABLE [dbo].[ConstConfigurationRules] (
    [id_configurationRules] INT            IDENTITY (1, 1) NOT NULL,
    [nom]                   NVARCHAR (MAX) NOT NULL,
    [libelle]               NVARCHAR (MAX) NOT NULL,
    [valueNumerie]          INT            NULL,
    CONSTRAINT [PK_ConstConfigurationRules] PRIMARY KEY CLUSTERED ([id_configurationRules] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[ConstTransporteur]...';


GO
CREATE TABLE [dbo].[ConstTransporteur] (
    [id_constTransporteur] INT            IDENTITY (1, 1) NOT NULL,
    [code]                 NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_ConstTransporteur] PRIMARY KEY CLUSTERED ([id_constTransporteur] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_LnkPointTypePoint_Point]...';


GO
ALTER TABLE [dbo].[LnkPointTypePoint] WITH NOCHECK
    ADD CONSTRAINT [FK_LnkPointTypePoint_Point] FOREIGN KEY ([id_lnkPointTypePoint_Point]) REFERENCES [dbo].[Point] ([id_point]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_Passage_Point]...';


GO
ALTER TABLE [dbo].[Passage] WITH NOCHECK
    ADD CONSTRAINT [FK_Passage_Point] FOREIGN KEY ([id_passage_point]) REFERENCES [dbo].[Point] ([id_point]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_Point_Point]...';


GO
ALTER TABLE [dbo].[Point] WITH NOCHECK
    ADD CONSTRAINT [FK_Point_Point] FOREIGN KEY ([id_point]) REFERENCES [dbo].[Point] ([id_point]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_Troncon_Point_1]...';


GO
ALTER TABLE [dbo].[Troncon] WITH NOCHECK
    ADD CONSTRAINT [FK_Troncon_Point_1] FOREIGN KEY ([id_pointDep]) REFERENCES [dbo].[Point] ([id_point]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_Troncon_Point_2]...';


GO
ALTER TABLE [dbo].[Troncon] WITH NOCHECK
    ADD CONSTRAINT [FK_Troncon_Point_2] FOREIGN KEY ([id_pointArr]) REFERENCES [dbo].[Point] ([id_point]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgConfigurationRulesGeneral_ConfigurationRules]...';


GO
ALTER TABLE [dbo].[CfgConfigurationRulesGeneral] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgConfigurationRulesGeneral_ConfigurationRules] FOREIGN KEY ([id_cfgConfigurationRulesGeneral_ConfigurationRules]) REFERENCES [dbo].[ConstConfigurationRules] ([id_configurationRules]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_Ligne_Transporteur]...';


GO
ALTER TABLE [dbo].[Ligne] WITH NOCHECK
    ADD CONSTRAINT [FK_Ligne_Transporteur] FOREIGN KEY ([id_ligne_constTransporteur]) REFERENCES [dbo].[ConstTransporteur] ([id_constTransporteur]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_ServiceAgent_PointDebut]...';


GO
ALTER TABLE [dbo].[ServiceAgent] WITH NOCHECK
    ADD CONSTRAINT [FK_ServiceAgent_PointDebut] FOREIGN KEY ([id_pointDebut]) REFERENCES [dbo].[Point] ([id_point]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_ServiceAgent_PointFin]...';


GO
ALTER TABLE [dbo].[ServiceAgent] WITH NOCHECK
    ADD CONSTRAINT [FK_ServiceAgent_PointFin] FOREIGN KEY ([id_pointFin]) REFERENCES [dbo].[Point] ([id_point]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_ServiceAgent_PointFinService]...';


GO
ALTER TABLE [dbo].[ServiceAgent] WITH NOCHECK
    ADD CONSTRAINT [FK_ServiceAgent_PointFinService] FOREIGN KEY ([id_pointFinService]) REFERENCES [dbo].[Point] ([id_point]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_ServiceAgent_PointPriseService]...';


GO
ALTER TABLE [dbo].[ServiceAgent] WITH NOCHECK
    ADD CONSTRAINT [FK_ServiceAgent_PointPriseService] FOREIGN KEY ([id_pointPriseService]) REFERENCES [dbo].[Point] ([id_point]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO

IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT N'Succès de la mise à jour de la portion de base de données traitée.'
COMMIT TRANSACTION
END
ELSE PRINT N'Échec de la mise à jour de la portion de base de données traitée.'
GO
DROP TABLE #tmpErrors
GO
PRINT N'Vérification de données existantes par rapport aux nouvelles contraintes';


GO
USE [$(DatabaseName)];


GO
ALTER TABLE [dbo].[LnkPointTypePoint] WITH CHECK CHECK CONSTRAINT [FK_LnkPointTypePoint_Point];

ALTER TABLE [dbo].[Passage] WITH CHECK CHECK CONSTRAINT [FK_Passage_Point];

ALTER TABLE [dbo].[Point] WITH NOCHECK CHECK CONSTRAINT [FK_Point_Point];

ALTER TABLE [dbo].[Troncon] WITH CHECK CHECK CONSTRAINT [FK_Troncon_Point_1];

ALTER TABLE [dbo].[Troncon] WITH CHECK CHECK CONSTRAINT [FK_Troncon_Point_2];

ALTER TABLE [dbo].[CfgConfigurationRulesGeneral] WITH CHECK CHECK CONSTRAINT [FK_CfgConfigurationRulesGeneral_ConfigurationRules];

ALTER TABLE [dbo].[Ligne] WITH CHECK CHECK CONSTRAINT [FK_Ligne_Transporteur];

ALTER TABLE [dbo].[ServiceAgent] WITH CHECK CHECK CONSTRAINT [FK_ServiceAgent_PointDebut];

ALTER TABLE [dbo].[ServiceAgent] WITH CHECK CHECK CONSTRAINT [FK_ServiceAgent_PointFin];

ALTER TABLE [dbo].[ServiceAgent] WITH CHECK CHECK CONSTRAINT [FK_ServiceAgent_PointFinService];

ALTER TABLE [dbo].[ServiceAgent] WITH CHECK CHECK CONSTRAINT [FK_ServiceAgent_PointPriseService];


GO
PRINT N'Mise à jour terminée.';


GO

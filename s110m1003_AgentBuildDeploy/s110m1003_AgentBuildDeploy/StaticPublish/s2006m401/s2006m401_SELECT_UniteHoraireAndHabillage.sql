use s2006m401;

select UniteHoraire.id_uniteHoraire_reseaus101m401,UniteHoraire.id_uniteHoraire, UniteHoraire.nom as 'Unite',
PeriodeHoraire.id_periodeHoraire, PeriodeHoraire.nom as 'P�riode',
Habillage.id_habillage,Habillage.nom as 'Habillage', Habillage.description, Habillage.scenario,
ConstTypeDeJour.code as 'Type Jour',
ServiceAgent.id_serviceAgent, ServiceAgent.nom as 'SA', ServiceAgent.id_externe, ServiceAgent.jourSemaine

from UniteHoraire, PeriodeHoraire, Habillage, ConstTypeDeJour, ServiceAgent

where UniteHoraire.id_uniteHoraire_reseaus101m401 = 1
and PeriodeHoraire.id_periodeHoraire_uniteHoraire = UniteHoraire.id_uniteHoraire

and Habillage.id_habillage_periodeHoraire = PeriodeHoraire.id_periodeHoraire
and Habillage.id_habillage_typeDeJour = ConstTypeDeJour.id_constTypeDeJour
and ServiceAgent.id_serviceAgent_habillage = Habillage.id_habillage;
﻿/*
Script de déploiement pour s2002m401

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "s2002m401"
:setvar DefaultFilePrefix "s2002m401"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.RLAPRODUCTION\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.RLAPRODUCTION\MSSQL\DATA\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
/*
La colonne [dbo].[MaterielListe].[id_conducteurs2006m401] est en cours de suppression, des données risquent d'être perdues.

La colonne [dbo].[MaterielListe].[id_lignes2006m401] est en cours de suppression, des données risquent d'être perdues.
*/

--IF EXISTS (select top 1 1 from [dbo].[MaterielListe])
--    RAISERROR (N'Lignes détectées. Arrêt de la mise à jour du schéma en raison d''''un risque de perte de données.', 16, 127) WITH NOWAIT

--GO
/*
La table [dbo].[CfgAleaElementDetailsConsequences] est en cours de suppression. Le déploiement va s'arrêter si la table contient des données.
*/

--IF EXISTS (select top 1 1 from [dbo].[CfgAleaElementDetailsConsequences])
--    RAISERROR (N'Lignes détectées. Arrêt de la mise à jour du schéma en raison d''''un risque de perte de données.', 16, 127) WITH NOWAIT

--GO
PRINT N'Suppression de [dbo].[DF_AleaListe_indentifiant]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [DF_AleaListe_indentifiant];


GO
PRINT N'Suppression de [dbo].[DF_AleaListe_latitude_saisie]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [DF_AleaListe_latitude_saisie];


GO
PRINT N'Suppression de [dbo].[DF_AleaListe_longitude_saisie]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [DF_AleaListe_longitude_saisie];


GO
PRINT N'Suppression de [dbo].[DF_AleaListe_ponderation]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [DF_AleaListe_ponderation];


GO
PRINT N'Suppression de [dbo].[DF_CfgEvenementControlRule_isDefaut]...';


GO
ALTER TABLE [dbo].[CfgEvenementControlRule] DROP CONSTRAINT [DF_CfgEvenementControlRule_isDefaut];


GO
PRINT N'Suppression de [dbo].[DF_CfgEvenementSectionRule_isDefaut]...';


GO
ALTER TABLE [dbo].[CfgEvenementSectionRule] DROP CONSTRAINT [DF_CfgEvenementSectionRule_isDefaut];


GO
PRINT N'Suppression de [dbo].[DF_MaterielListe_id_materielList_constRessourceFamille]...';


GO
ALTER TABLE [dbo].[MaterielListe] DROP CONSTRAINT [DF_MaterielListe_id_materielList_constRessourceFamille];


GO
PRINT N'Suppression de contrainte sans nom sur [dbo].[CfgAleaElementDetailsConsequences]...';


GO
ALTER TABLE [dbo].[CfgAleaElementDetailsConsequences] DROP CONSTRAINT [DF__CfgAleaEl__explo__59C55456];


GO
PRINT N'Suppression de contrainte sans nom sur [dbo].[CfgAleaElementDetailsConsequences]...';


GO
ALTER TABLE [dbo].[CfgAleaElementDetailsConsequences] DROP CONSTRAINT [DF__CfgAleaEl__perso__5AB9788F];


GO
PRINT N'Suppression de contrainte sans nom sur [dbo].[CfgAleaElementDetailsConsequences]...';


GO
ALTER TABLE [dbo].[CfgAleaElementDetailsConsequences] DROP CONSTRAINT [DF__CfgAleaEl__explo__5BAD9CC8];


GO
PRINT N'Suppression de [dbo].[ColumnDefault_258a69d7-0b1c-4258-b59b-e8074d0a8d8a]...';


GO
ALTER TABLE [dbo].[CfgAleaElementDetailsConsequences] DROP CONSTRAINT [ColumnDefault_258a69d7-0b1c-4258-b59b-e8074d0a8d8a];


GO
PRINT N'Suppression de [dbo].[ColumnDefault_7342e0c9-a197-4213-85b7-adffb018c6f6]...';


GO
ALTER TABLE [dbo].[CfgAleaElementDetailsConsequences] DROP CONSTRAINT [ColumnDefault_7342e0c9-a197-4213-85b7-adffb018c6f6];


GO
PRINT N'Suppression de [dbo].[ColumnDefault_a9539cec-5382-4578-be17-c43fed44aba3]...';


GO
ALTER TABLE [dbo].[CfgAleaElementDetailsConsequences] DROP CONSTRAINT [ColumnDefault_a9539cec-5382-4578-be17-c43fed44aba3];


GO
PRINT N'Suppression de [dbo].[ColumnDefault_b26baf77-cfc2-4320-8274-49b7e5d7b422]...';


GO
ALTER TABLE [dbo].[CfgAleaElementDetailsConsequences] DROP CONSTRAINT [ColumnDefault_b26baf77-cfc2-4320-8274-49b7e5d7b422];


GO
PRINT N'Suppression de [dbo].[ColumnDefault_d00d30d1-6654-4d5a-ab01-e73da5ccc5eb]...';


GO
ALTER TABLE [dbo].[CfgAleaElementDetailsConsequences] DROP CONSTRAINT [ColumnDefault_d00d30d1-6654-4d5a-ab01-e73da5ccc5eb];


GO
PRINT N'Suppression de [dbo].[FK_EvenementMaintenance_EvenementListe]...';


GO
ALTER TABLE [dbo].[EvenementMaintenance] DROP CONSTRAINT [FK_EvenementMaintenance_EvenementListe];


GO
PRINT N'Suppression de [dbo].[FK_EvenementSecurite_EvenementListe]...';


GO
ALTER TABLE [dbo].[EvenementSecurite] DROP CONSTRAINT [FK_EvenementSecurite_EvenementListe];


GO
PRINT N'Suppression de [dbo].[FK_AleaHistorique_Evenement]...';


GO
ALTER TABLE [dbo].[AleaHistorique] DROP CONSTRAINT [FK_AleaHistorique_Evenement];


GO
PRINT N'Suppression de [dbo].[FK_AleaListe_Affectation]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [FK_AleaListe_Affectation];


GO
PRINT N'Suppression de [dbo].[FK_AleaListe_AleaEtat]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [FK_AleaListe_AleaEtat];


GO
PRINT N'Suppression de [dbo].[FK_AleaListe_AleaType]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [FK_AleaListe_AleaType];


GO
PRINT N'Suppression de [dbo].[FK_AleaListe_EvenementLieu]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [FK_AleaListe_EvenementLieu];


GO
PRINT N'Suppression de [dbo].[FK_AleaListe_MeteoDetails]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [FK_AleaListe_MeteoDetails];


GO
PRINT N'Suppression de [dbo].[FK_AleaListe_Ressource]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [FK_AleaListe_Ressource];


GO
PRINT N'Suppression de [dbo].[FK_AleaListe_RessourceRemplacement]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [FK_AleaListe_RessourceRemplacement];


GO
PRINT N'Suppression de [dbo].[FK_AleaListAleaHistorique_AleaList]...';


GO
ALTER TABLE [dbo].[LnkAleaListAleaHistorique] DROP CONSTRAINT [FK_AleaListAleaHistorique_AleaList];


GO
PRINT N'Suppression de [dbo].[FK_LnkAleaListeFicheRealise_AleaListe]...';


GO
ALTER TABLE [dbo].[LnkAleaListeFicheRealise] DROP CONSTRAINT [FK_LnkAleaListeFicheRealise_AleaListe];


GO
PRINT N'Suppression de [dbo].[FK_LnkEvenementAlertHistorique_Evenement]...';


GO
ALTER TABLE [dbo].[LnkEvenementAlertHistorique] DROP CONSTRAINT [FK_LnkEvenementAlertHistorique_Evenement];


GO
PRINT N'Suppression de [dbo].[FK_MaterielListeAleaListe_AleaListe]...';


GO
ALTER TABLE [dbo].[LnkMaterielListeAleaListe] DROP CONSTRAINT [FK_MaterielListeAleaListe_AleaListe];


GO
PRINT N'Suppression de [dbo].[FK_QueryTriggerAsynchoneHistorique_Alea]...';


GO
ALTER TABLE [dbo].[QueryTriggerAsynchoneHistorique] DROP CONSTRAINT [FK_QueryTriggerAsynchoneHistorique_Alea];


GO
PRINT N'Suppression de [dbo].[FK_EvenementDommage_AleaListe]...';


GO
ALTER TABLE [dbo].[EvenementDommage] DROP CONSTRAINT [FK_EvenementDommage_AleaListe];


GO
PRINT N'Suppression de [dbo].[FK_EvenementExploitationConseq_EvenementList]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] DROP CONSTRAINT [FK_EvenementExploitationConseq_EvenementList];


GO
PRINT N'Suppression de [dbo].[FK_EvenementFichier_AleaListe]...';


GO
ALTER TABLE [dbo].[EvenementFichier] DROP CONSTRAINT [FK_EvenementFichier_AleaListe];


GO
PRINT N'Suppression de [dbo].[FK_EvenementHistoriqueView_Evenement]...';


GO
ALTER TABLE [dbo].[EvenementHistoriqueView] DROP CONSTRAINT [FK_EvenementHistoriqueView_Evenement];


GO
PRINT N'Suppression de [dbo].[FK_EvenementIntervention_EvenementList]...';


GO
ALTER TABLE [dbo].[EvenementIntervention] DROP CONSTRAINT [FK_EvenementIntervention_EvenementList];


GO
PRINT N'Suppression de [dbo].[FK_CfgEvenementControlRule_Control]...';


GO
ALTER TABLE [dbo].[CfgEvenementControlRule] DROP CONSTRAINT [FK_CfgEvenementControlRule_Control];


GO
PRINT N'Suppression de [dbo].[FK_CfgEvenementControlRule_EvenementFamille]...';


GO
ALTER TABLE [dbo].[CfgEvenementControlRule] DROP CONSTRAINT [FK_CfgEvenementControlRule_EvenementFamille];


GO
PRINT N'Suppression de [dbo].[FK_CfgEvenementControlRule_RessourceFamille]...';


GO
ALTER TABLE [dbo].[CfgEvenementControlRule] DROP CONSTRAINT [FK_CfgEvenementControlRule_RessourceFamille];


GO
PRINT N'Suppression de [dbo].[FK_CfgEvenementControlRule_State]...';


GO
ALTER TABLE [dbo].[CfgEvenementControlRule] DROP CONSTRAINT [FK_CfgEvenementControlRule_State];


GO
PRINT N'Suppression de [dbo].[FK_CfgEvenementSectionRule_EvenementFamille]...';


GO
ALTER TABLE [dbo].[CfgEvenementSectionRule] DROP CONSTRAINT [FK_CfgEvenementSectionRule_EvenementFamille];


GO
PRINT N'Suppression de [dbo].[FK_CfgEvenementSectionRule_RessourceFamille]...';


GO
ALTER TABLE [dbo].[CfgEvenementSectionRule] DROP CONSTRAINT [FK_CfgEvenementSectionRule_RessourceFamille];


GO
PRINT N'Suppression de [dbo].[FK_CfgEvenementSectionRule_Section]...';


GO
ALTER TABLE [dbo].[CfgEvenementSectionRule] DROP CONSTRAINT [FK_CfgEvenementSectionRule_Section];


GO
PRINT N'Suppression de [dbo].[FK_CfgEvenementSectionRule_State]...';


GO
ALTER TABLE [dbo].[CfgEvenementSectionRule] DROP CONSTRAINT [FK_CfgEvenementSectionRule_State];


GO
PRINT N'Suppression de [dbo].[FK_EvenementLieu_MaterielListe]...';


GO
ALTER TABLE [dbo].[EvenementLieu] DROP CONSTRAINT [FK_EvenementLieu_MaterielListe];


GO
PRINT N'Suppression de [dbo].[FK_MaterielListeAleaListe_MaterielListe]...';


GO
ALTER TABLE [dbo].[LnkMaterielListeAleaListe] DROP CONSTRAINT [FK_MaterielListeAleaListe_MaterielListe];


GO
PRINT N'Suppression de [dbo].[FK_LnkMaterielListMaterielEtatHistorique_MaterielList]...';


GO
ALTER TABLE [dbo].[LnkMaterielListMaterielEtatHistorique] DROP CONSTRAINT [FK_LnkMaterielListMaterielEtatHistorique_MaterielList];


GO
PRINT N'Suppression de [dbo].[FK_MaterielListe_ConstRessourceFamille]...';


GO
ALTER TABLE [dbo].[MaterielListe] DROP CONSTRAINT [FK_MaterielListe_ConstRessourceFamille];


GO
PRINT N'Suppression de [dbo].[FK_MaterielListe_MaterielDetail]...';


GO
ALTER TABLE [dbo].[MaterielListe] DROP CONSTRAINT [FK_MaterielListe_MaterielDetail];


GO
PRINT N'Suppression de [dbo].[FK_MaterielListe_MaterielEtat]...';


GO
ALTER TABLE [dbo].[MaterielListe] DROP CONSTRAINT [FK_MaterielListe_MaterielEtat];


GO
PRINT N'Suppression de [dbo].[FK_MaterielListe_MaterielType]...';


GO
ALTER TABLE [dbo].[MaterielListe] DROP CONSTRAINT [FK_MaterielListe_MaterielType];


GO
PRINT N'Suppression de [dbo].[FK_EvenementAffectation_MaterielListe_Agent]...';


GO
ALTER TABLE [dbo].[EvenementAffectation] DROP CONSTRAINT [FK_EvenementAffectation_MaterielListe_Agent];


GO
PRINT N'Suppression de [dbo].[FK_EvenementAffectation_MaterielListe_Vehicule]...';


GO
ALTER TABLE [dbo].[EvenementAffectation] DROP CONSTRAINT [FK_EvenementAffectation_MaterielListe_Vehicule];


GO
PRINT N'Suppression de [dbo].[FK_EvenementDommage_Ressource]...';


GO
ALTER TABLE [dbo].[EvenementDommage] DROP CONSTRAINT [FK_EvenementDommage_Ressource];


GO
PRINT N'Suppression de [dbo].[FK_EvenementExploitationConseq_RessourceHomme]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] DROP CONSTRAINT [FK_EvenementExploitationConseq_RessourceHomme];


GO
PRINT N'Suppression de [dbo].[FK_EvenementExploitationConseq_RessourceVehicule]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] DROP CONSTRAINT [FK_EvenementExploitationConseq_RessourceVehicule];


GO
PRINT N'Suppression de [dbo].[FK_CfgAleaElementPersonneDetail_AleaElement]...';


GO
ALTER TABLE [dbo].[CfgAleaElementDetailsConsequences] DROP CONSTRAINT [FK_CfgAleaElementPersonneDetail_AleaElement];


GO
PRINT N'Suppression de [dbo].[CfgAleaElementDetailsConsequences]...';


GO
DROP TABLE [dbo].[CfgAleaElementDetailsConsequences];


GO
PRINT N'Début de la régénération de la table [dbo].[AleaListe]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_AleaListe] (
    [id_aleaListe]                       INT            IDENTITY (1, 1) NOT NULL,
    [id_aleaListe_aleaEtat]              INT            NOT NULL,
    [id_aleaListe_aleaType]              INT            NOT NULL,
    [timeStamp]                          FLOAT (53)     NOT NULL,
    [dateApparition]                     DATE           NULL,
    [heureApparition]                    TIME (7)       NULL,
    [comentaires]                        NVARCHAR (MAX) NULL,
    [id_aleaListe_meteoDetails]          INT            NULL,
    [indentifiant]                       NVARCHAR (50)  CONSTRAINT [DF_AleaListe_indentifiant] DEFAULT (N'XXXXXX') NOT NULL,
    [id_aleaListe_evenementLieu]         INT            NULL,
    [dateFin]                            DATE           NULL,
    [heureFin]                           TIME (7)       NULL,
    [id_aleaListe_affectation]           INT            NULL,
    [ponderation]                        INT            CONSTRAINT [DF_AleaListe_ponderation] DEFAULT ((0)) NOT NULL,
    [id_aleaListe_ressourceRemplacement] INT            NULL,
    [id_aleaListe_ressource]             INT            NULL,
    [longitude_saisie]                   FLOAT (53)     CONSTRAINT [DF_AleaListe_longitude_saisie] DEFAULT ((0)) NOT NULL,
    [latitude_saisie]                    FLOAT (53)     CONSTRAINT [DF_AleaListe_latitude_saisie] DEFAULT ((0)) NOT NULL,
    [lastModification]                   DATETIME       CONSTRAINT [DF_AleaListe_dateModificationModel] DEFAULT ('2010-01-01 00:00:00') NOT NULL,
    [id_aleaListe_FermetureRaison]       INT            NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK__AleaList__DD8219B1B6F91A411] PRIMARY KEY CLUSTERED ([id_aleaListe] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[AleaListe])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_AleaListe] ON;
        INSERT INTO [dbo].[tmp_ms_xx_AleaListe] ([id_aleaListe], [id_aleaListe_aleaEtat], [id_aleaListe_aleaType], [timeStamp], [dateApparition], [heureApparition], [comentaires], [id_aleaListe_meteoDetails], [indentifiant], [id_aleaListe_evenementLieu], [heureFin], [id_aleaListe_affectation], [ponderation], [id_aleaListe_ressourceRemplacement], [id_aleaListe_ressource], [longitude_saisie], [latitude_saisie])
        SELECT   [id_aleaListe],
                 [id_aleaListe_aleaEtat],
                 [id_aleaListe_aleaType],
                 [timeStamp],
                 [dateApparition],
                 [heureApparition],
                 [comentaires],
                 [id_aleaListe_meteoDetails],
                 [indentifiant],
                 [id_aleaListe_evenementLieu],
                 [heureFin],
                 [id_aleaListe_affectation],
                 [ponderation],
                 [id_aleaListe_ressourceRemplacement],
                 [id_aleaListe_ressource],
                 [longitude_saisie],
                 [latitude_saisie]
        FROM     [dbo].[AleaListe]
        ORDER BY [id_aleaListe] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_AleaListe] OFF;
    END

DROP TABLE [dbo].[AleaListe];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_AleaListe]', N'AleaListe';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK__AleaList__DD8219B1B6F91A411]', N'PK__AleaList__DD8219B1B6F91A41', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Début de la régénération de la table [dbo].[CfgEvenementControlRule]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_CfgEvenementControlRule] (
    [id_cfgEvenementControlRule]                        INT IDENTITY (1, 1) NOT NULL,
    [id_cfgEvenementControlRule_constControl]           INT NOT NULL,
    [id_cfgEvenementControlRule_constState]             INT NOT NULL,
    [id_cfgEvenementControlRule_constRessourceFamille]  INT NULL,
    [id_cfgEvenementControlRule_constEvenermentFamille] INT NULL,
    [id_cfgEvenementControlRule_constTerminal]          INT NULL,
    [id_cfgEvenementControlRule_constModeSaisie]        INT NULL,
    [id_cfgEvenementControlRule_reseaus101m401]         INT NOT NULL,
    [isDefaut]                                          INT CONSTRAINT [DF_CfgEvenementControlRule_isDefaut] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_CfgEvenementControlRule1] PRIMARY KEY CLUSTERED ([id_cfgEvenementControlRule] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[CfgEvenementControlRule])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_CfgEvenementControlRule] ON;
        INSERT INTO [dbo].[tmp_ms_xx_CfgEvenementControlRule] ([id_cfgEvenementControlRule], [id_cfgEvenementControlRule_constControl], [id_cfgEvenementControlRule_constState], [id_cfgEvenementControlRule_constRessourceFamille], [id_cfgEvenementControlRule_constEvenermentFamille], [id_cfgEvenementControlRule_reseaus101m401], [isDefaut])
        SELECT   [id_cfgEvenementControlRule],
                 [id_cfgEvenementControlRule_constControl],
                 [id_cfgEvenementControlRule_constState],
                 [id_cfgEvenementControlRule_constRessourceFamille],
                 [id_cfgEvenementControlRule_constEvenermentFamille],
                 [id_cfgEvenementControlRule_reseaus101m401],
                 [isDefaut]
        FROM     [dbo].[CfgEvenementControlRule]
        ORDER BY [id_cfgEvenementControlRule] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_CfgEvenementControlRule] OFF;
    END

DROP TABLE [dbo].[CfgEvenementControlRule];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_CfgEvenementControlRule]', N'CfgEvenementControlRule';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_CfgEvenementControlRule1]', N'PK_CfgEvenementControlRule', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Création de [dbo].[CfgEvenementControlRule].[IX_CfgEvenementControlRule]...';


GO
CREATE NONCLUSTERED INDEX [IX_CfgEvenementControlRule]
    ON [dbo].[CfgEvenementControlRule]([id_cfgEvenementControlRule] ASC);


GO
PRINT N'Début de la régénération de la table [dbo].[CfgEvenementSectionRule]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_CfgEvenementSectionRule] (
    [id_cfgEvenementSectionRule]                       INT IDENTITY (1, 1) NOT NULL,
    [id_cfgEvenementSectionRule_constSection]          INT NOT NULL,
    [id_cfgEvenementSectionRule_constState]            INT NOT NULL,
    [id_cfgEvenementSectionRule_constRessourceFamille] INT NULL,
    [id_cfgEvenementSectionRule_constEvenementFamille] INT NULL,
    [id_cfgEvenementSectionRule_constTerminal]         INT NULL,
    [id_cfgEvenementSectionRule_constModeSaisie]       INT NULL,
    [id_cfgEvenementSectionRule_reseaus101m401]        INT NOT NULL,
    [isDefaut]                                         INT CONSTRAINT [DF_CfgEvenementSectionRule_isDefaut] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_CfgEvenementSectionRule1] PRIMARY KEY CLUSTERED ([id_cfgEvenementSectionRule] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[CfgEvenementSectionRule])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_CfgEvenementSectionRule] ON;
        INSERT INTO [dbo].[tmp_ms_xx_CfgEvenementSectionRule] ([id_cfgEvenementSectionRule], [id_cfgEvenementSectionRule_constSection], [id_cfgEvenementSectionRule_constState], [id_cfgEvenementSectionRule_constRessourceFamille], [id_cfgEvenementSectionRule_constEvenementFamille], [id_cfgEvenementSectionRule_reseaus101m401], [isDefaut])
        SELECT   [id_cfgEvenementSectionRule],
                 [id_cfgEvenementSectionRule_constSection],
                 [id_cfgEvenementSectionRule_constState],
                 [id_cfgEvenementSectionRule_constRessourceFamille],
                 [id_cfgEvenementSectionRule_constEvenementFamille],
                 [id_cfgEvenementSectionRule_reseaus101m401],
                 [isDefaut]
        FROM     [dbo].[CfgEvenementSectionRule]
        ORDER BY [id_cfgEvenementSectionRule] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_CfgEvenementSectionRule] OFF;
    END

DROP TABLE [dbo].[CfgEvenementSectionRule];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_CfgEvenementSectionRule]', N'CfgEvenementSectionRule';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_CfgEvenementSectionRule1]', N'PK_CfgEvenementSectionRule', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Modification de [dbo].[ConsignesExploitation]...';


GO
ALTER TABLE [dbo].[ConsignesExploitation]
    ADD [isDefaut] INT CONSTRAINT [DF_ConsignesExploitation_isDefaut] DEFAULT ((0)) NULL;


GO
PRINT N'Modification de [dbo].[EvenementAffectation]...';


GO
ALTER TABLE [dbo].[EvenementAffectation]
    ADD [heurePriseServiceReelle]    TIME (7) NULL,
        [heurePriseServiceThéorique] TIME (7) NULL;


GO
PRINT N'Début de la régénération de la table [dbo].[MaterielListe]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_MaterielListe] (
    [id_materielListe]                      INT           IDENTITY (1, 1) NOT NULL,
    [code]                                  NVARCHAR (50) CONSTRAINT [DF_MaterielListe_code] DEFAULT (N'A MAJ') NOT NULL,
    [nomLong]                               NVARCHAR (50) CONSTRAINT [DF_MaterielListe_nomLong] DEFAULT (N'A MAJ') NOT NULL,
    [id_materielroulants2006m401]           INT           NULL,
    [id_materielList_materielDetail]        INT           NULL,
    [id_materielList_materielType]          INT           NOT NULL,
    [id_materielListe_materielCategorie]    INT           CONSTRAINT [DF_MaterielListe_id_materielDetail_materielCategorie] DEFAULT ((10012)) NOT NULL,
    [id_materielList_constRessourceFamille] INT           CONSTRAINT [DF_MaterielListe_id_materielList_constRessourceFamille] DEFAULT ((1)) NOT NULL,
    [id_materielList_materielEtat]          INT           NULL,
    [id_reseau]                             INT           NOT NULL,
    [deleted]                               FLOAT (53)    NULL,
    [lastModification]                      DATETIME      CONSTRAINT [DF_MaterielListe_lastModification] DEFAULT ('2010-01-01 00:00:00') NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK__Materiel__37882C8FC00D19BA1] PRIMARY KEY CLUSTERED ([id_materielListe] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[MaterielListe])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_MaterielListe] ON;
        INSERT INTO [dbo].[tmp_ms_xx_MaterielListe] ([id_materielListe], [id_reseau], [id_materielroulants2006m401], [id_materielList_materielDetail], [id_materielList_materielType], [id_materielList_constRessourceFamille], [deleted], [id_materielList_materielEtat])
        SELECT   [id_materielListe],
                 [id_reseau],
                 [id_materielroulants2006m401],
                 [id_materielList_materielDetail],
                 [id_materielList_materielType],
                 [id_materielList_constRessourceFamille],
                 [deleted],
                 [id_materielList_materielEtat]
        FROM     [dbo].[MaterielListe]
        ORDER BY [id_materielListe] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_MaterielListe] OFF;
    END

DROP TABLE [dbo].[MaterielListe];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_MaterielListe]', N'MaterielListe';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK__Materiel__37882C8FC00D19BA1]', N'PK__Materiel__37882C8FC00D19BA', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Création de [dbo].[CfgConfigurationRules]...';


GO
CREATE TABLE [dbo].[CfgConfigurationRules] (
    [id_cfgConfigurationRules]                        INT IDENTITY (1, 1) NOT NULL,
    [id_cfgConfigurationRules_CfgGeneralMainCourante] INT NOT NULL,
    [id_cfgConfigurationRules_ConfigurationRules]     INT NOT NULL,
    CONSTRAINT [PK_CfgConfigurationRules] PRIMARY KEY CLUSTERED ([id_cfgConfigurationRules] ASC)
);


GO
PRINT N'Création de [dbo].[CfgEvenementFermetureRaisonFamille]...';


GO
CREATE TABLE [dbo].[CfgEvenementFermetureRaisonFamille] (
    [id_cfgEvenementFermetureRaisonFamille]                       INT IDENTITY (1, 1) NOT NULL,
    [id_cfgEvenementFermetureRaisonFamille_constEvenementFamille] INT NOT NULL,
    [id_cfgEvenementFermetureRaisonFamille_fermetureRaison]       INT NOT NULL,
    CONSTRAINT [PK_CfgEvenementFermetureRaisonFamille] PRIMARY KEY CLUSTERED ([id_cfgEvenementFermetureRaisonFamille] ASC)
);


GO
PRINT N'Création de [dbo].[CfgRessourceSousClasse]...';


GO
CREATE TABLE [dbo].[CfgRessourceSousClasse] (
    [id_cfgRessourceSousClasse]                     INT IDENTITY (1, 1) NOT NULL,
    [id_cfgRessourceSousClasse_reseaus101m401]      INT NOT NULL,
    [id_cfgRessourceSousClasse_RessourceSousClasse] INT NOT NULL,
    CONSTRAINT [PK_CfgRessourceSousClasse] PRIMARY KEY CLUSTERED ([id_cfgRessourceSousClasse] ASC)
);


GO
PRINT N'Création de [dbo].[ConstConfigurationRules]...';


GO
CREATE TABLE [dbo].[ConstConfigurationRules] (
    [id_configurationRules] INT           IDENTITY (1, 1) NOT NULL,
    [nom]                   NVARCHAR (50) NOT NULL,
    [libelle]               NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ConfigurationRules] PRIMARY KEY CLUSTERED ([id_configurationRules] ASC)
);


GO
PRINT N'Création de [dbo].[ConstModeSaisie]...';


GO
CREATE TABLE [dbo].[ConstModeSaisie] (
    [id_constModeSaisie] INT           IDENTITY (1, 1) NOT NULL,
    [code]               NVARCHAR (50) NOT NULL,
    [libelle]            NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ConstModeSaisie] PRIMARY KEY CLUSTERED ([id_constModeSaisie] ASC)
);


GO
PRINT N'Création de [dbo].[ConstTerminalSaisie]...';


GO
CREATE TABLE [dbo].[ConstTerminalSaisie] (
    [id_constTerminalSaisie] INT           IDENTITY (1, 1) NOT NULL,
    [code]                   NVARCHAR (50) NOT NULL,
    [libelle]                NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ConstTerminalSaisie] PRIMARY KEY CLUSTERED ([id_constTerminalSaisie] ASC)
);


GO
PRINT N'Création de [dbo].[EvenementFermetureRaison]...';


GO
CREATE TABLE [dbo].[EvenementFermetureRaison] (
    [id_evenementFermetureRaison] INT           IDENTITY (1, 1) NOT NULL,
    [libelle]                     NVARCHAR (50) NOT NULL,
    [isGlobal]                    INT           NOT NULL,
    [automatiqueHeure]            TIME (7)      NULL,
    [automatiqueDuree]            INT           NULL,
    [automatiqueActions]          INT           NULL,
    CONSTRAINT [PK_EvenementFermetureRaison] PRIMARY KEY CLUSTERED ([id_evenementFermetureRaison] ASC)
);


GO
PRINT N'Création de [dbo].[PK_MaterielDetailCategorie]...';


GO
ALTER TABLE [dbo].[MaterielDetailCategorie]
    ADD CONSTRAINT [PK_MaterielDetailCategorie] PRIMARY KEY CLUSTERED ([id_materielDetailCategorie] ASC);


GO
PRINT N'Création de [dbo].[DF_EvenementFermetureRaison_isGlobal]...';


GO
ALTER TABLE [dbo].[EvenementFermetureRaison]
    ADD CONSTRAINT [DF_EvenementFermetureRaison_isGlobal] DEFAULT ((0)) FOR [isGlobal];


GO
PRINT N'Création de [dbo].[FK_EvenementMaintenance_EvenementListe]...';


GO
ALTER TABLE [dbo].[EvenementMaintenance] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementMaintenance_EvenementListe] FOREIGN KEY ([id_evenementMaintenance_evenementListe]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
PRINT N'Création de [dbo].[FK_EvenementSecurite_EvenementListe]...';


GO
ALTER TABLE [dbo].[EvenementSecurite] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementSecurite_EvenementListe] FOREIGN KEY ([id_evenementSecurite_evenmentList]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
PRINT N'Création de [dbo].[FK_AleaHistorique_Evenement]...';


GO
ALTER TABLE [dbo].[AleaHistorique] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaHistorique_Evenement] FOREIGN KEY ([id_aleaHistorique_Evenement]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
PRINT N'Création de [dbo].[FK_AleaListe_Affectation]...';


GO
ALTER TABLE [dbo].[AleaListe] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaListe_Affectation] FOREIGN KEY ([id_aleaListe_affectation]) REFERENCES [dbo].[EvenementAffectation] ([id_evenementAffectation]);


GO
PRINT N'Création de [dbo].[FK_AleaListe_AleaEtat]...';


GO
ALTER TABLE [dbo].[AleaListe] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaListe_AleaEtat] FOREIGN KEY ([id_aleaListe_aleaEtat]) REFERENCES [dbo].[AleaEtat] ([id_aleaEtat]);


GO
PRINT N'Création de [dbo].[FK_AleaListe_AleaType]...';


GO
ALTER TABLE [dbo].[AleaListe] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaListe_AleaType] FOREIGN KEY ([id_aleaListe_aleaType]) REFERENCES [dbo].[AleaType] ([id_aleaType]);


GO
PRINT N'Création de [dbo].[FK_AleaListe_EvenementLieu]...';


GO
ALTER TABLE [dbo].[AleaListe] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaListe_EvenementLieu] FOREIGN KEY ([id_aleaListe_evenementLieu]) REFERENCES [dbo].[EvenementLieu] ([id_evenementLieu]);


GO
PRINT N'Création de [dbo].[FK_AleaListe_MeteoDetails]...';


GO
ALTER TABLE [dbo].[AleaListe] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaListe_MeteoDetails] FOREIGN KEY ([id_aleaListe_meteoDetails]) REFERENCES [dbo].[EvenementMeteo] ([id_evenementMeteo]);


GO
PRINT N'Création de [dbo].[FK_AleaListe_Ressource]...';


GO
ALTER TABLE [dbo].[AleaListe] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaListe_Ressource] FOREIGN KEY ([id_aleaListe_ressource]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
PRINT N'Création de [dbo].[FK_AleaListe_RessourceRemplacement]...';


GO
ALTER TABLE [dbo].[AleaListe] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaListe_RessourceRemplacement] FOREIGN KEY ([id_aleaListe_ressourceRemplacement]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
PRINT N'Création de [dbo].[FK_AleaListAleaHistorique_AleaList]...';


GO
ALTER TABLE [dbo].[LnkAleaListAleaHistorique] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaListAleaHistorique_AleaList] FOREIGN KEY ([id_aleaListeAleaHistorique_aleaList]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
PRINT N'Création de [dbo].[FK_LnkAleaListeFicheRealise_AleaListe]...';


GO
ALTER TABLE [dbo].[LnkAleaListeFicheRealise] WITH NOCHECK
    ADD CONSTRAINT [FK_LnkAleaListeFicheRealise_AleaListe] FOREIGN KEY ([id_lnkAleaListeFicheRealise_aleaListe]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
PRINT N'Création de [dbo].[FK_LnkEvenementAlertHistorique_Evenement]...';


GO
ALTER TABLE [dbo].[LnkEvenementAlertHistorique] WITH NOCHECK
    ADD CONSTRAINT [FK_LnkEvenementAlertHistorique_Evenement] FOREIGN KEY ([id_lnkEvenementAlertHistorique_evenement]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
PRINT N'Création de [dbo].[FK_MaterielListeAleaListe_AleaListe]...';


GO
ALTER TABLE [dbo].[LnkMaterielListeAleaListe] WITH NOCHECK
    ADD CONSTRAINT [FK_MaterielListeAleaListe_AleaListe] FOREIGN KEY ([id_materielListeAleaListe_aleaListe]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
PRINT N'Création de [dbo].[FK_QueryTriggerAsynchoneHistorique_Alea]...';


GO
ALTER TABLE [dbo].[QueryTriggerAsynchoneHistorique] WITH NOCHECK
    ADD CONSTRAINT [FK_QueryTriggerAsynchoneHistorique_Alea] FOREIGN KEY ([id_queryDeclencheurAsynchoneHistorique_alea]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
PRINT N'Création de [dbo].[FK_EvenementDommage_AleaListe]...';


GO
ALTER TABLE [dbo].[EvenementDommage] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementDommage_AleaListe] FOREIGN KEY ([id_evenementDommage_aleaListe]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
PRINT N'Création de [dbo].[FK_EvenementExploitationConseq_EvenementList]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementExploitationConseq_EvenementList] FOREIGN KEY ([id_evenementExploitationConseq_evenementListe]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
PRINT N'Création de [dbo].[FK_EvenementFichier_AleaListe]...';


GO
ALTER TABLE [dbo].[EvenementFichier] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementFichier_AleaListe] FOREIGN KEY ([id_evenementFichier_evenement]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
PRINT N'Création de [dbo].[FK_EvenementHistoriqueView_Evenement]...';


GO
ALTER TABLE [dbo].[EvenementHistoriqueView] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementHistoriqueView_Evenement] FOREIGN KEY ([id_aleaHistorique_Evenement]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
PRINT N'Création de [dbo].[FK_EvenementIntervention_EvenementList]...';


GO
ALTER TABLE [dbo].[EvenementIntervention] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementIntervention_EvenementList] FOREIGN KEY ([id_evenementIntervention_evenementList]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
PRINT N'Création de [dbo].[FK_AleaListe_FermetureRaison]...';


GO
ALTER TABLE [dbo].[AleaListe] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaListe_FermetureRaison] FOREIGN KEY ([id_aleaListe_FermetureRaison]) REFERENCES [dbo].[EvenementFermetureRaison] ([id_evenementFermetureRaison]);


GO
PRINT N'Création de [dbo].[FK_CfgEvenementControlRule_Control]...';


GO
ALTER TABLE [dbo].[CfgEvenementControlRule] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementControlRule_Control] FOREIGN KEY ([id_cfgEvenementControlRule_constControl]) REFERENCES [dbo].[ConstEvenementControl] ([id_constEvenementControl]);


GO
PRINT N'Création de [dbo].[FK_CfgEvenementControlRule_EvenementFamille]...';


GO
ALTER TABLE [dbo].[CfgEvenementControlRule] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementControlRule_EvenementFamille] FOREIGN KEY ([id_cfgEvenementControlRule_constEvenermentFamille]) REFERENCES [dbo].[ConstEvenementFamille] ([id_constEvenementFamille]);


GO
PRINT N'Création de [dbo].[FK_CfgEvenementControlRule_RessourceFamille]...';


GO
ALTER TABLE [dbo].[CfgEvenementControlRule] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementControlRule_RessourceFamille] FOREIGN KEY ([id_cfgEvenementControlRule_constRessourceFamille]) REFERENCES [dbo].[ConstRessourcesFamille] ([id_constRessourceFamille]);


GO
PRINT N'Création de [dbo].[FK_CfgEvenementControlRule_State]...';


GO
ALTER TABLE [dbo].[CfgEvenementControlRule] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementControlRule_State] FOREIGN KEY ([id_cfgEvenementControlRule_constState]) REFERENCES [dbo].[ConstEvenementControlState] ([id_constEvenementControlState]);


GO
PRINT N'Création de [dbo].[FK_CfgEvenementControlRule_ModePage]...';


GO
ALTER TABLE [dbo].[CfgEvenementControlRule] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementControlRule_ModePage] FOREIGN KEY ([id_cfgEvenementControlRule_constModeSaisie]) REFERENCES [dbo].[ConstModeSaisie] ([id_constModeSaisie]);


GO
PRINT N'Création de [dbo].[FK_CfgEvenementControlRule_Terminal]...';


GO
ALTER TABLE [dbo].[CfgEvenementControlRule] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementControlRule_Terminal] FOREIGN KEY ([id_cfgEvenementControlRule_constTerminal]) REFERENCES [dbo].[ConstTerminalSaisie] ([id_constTerminalSaisie]);


GO
PRINT N'Création de [dbo].[FK_CfgEvenementSectionRule_EvenementFamille]...';


GO
ALTER TABLE [dbo].[CfgEvenementSectionRule] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementSectionRule_EvenementFamille] FOREIGN KEY ([id_cfgEvenementSectionRule_constEvenementFamille]) REFERENCES [dbo].[ConstEvenementFamille] ([id_constEvenementFamille]);


GO
PRINT N'Création de [dbo].[FK_CfgEvenementSectionRule_RessourceFamille]...';


GO
ALTER TABLE [dbo].[CfgEvenementSectionRule] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementSectionRule_RessourceFamille] FOREIGN KEY ([id_cfgEvenementSectionRule_constRessourceFamille]) REFERENCES [dbo].[ConstRessourcesFamille] ([id_constRessourceFamille]);


GO
PRINT N'Création de [dbo].[FK_CfgEvenementSectionRule_Section]...';


GO
ALTER TABLE [dbo].[CfgEvenementSectionRule] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementSectionRule_Section] FOREIGN KEY ([id_cfgEvenementSectionRule_constSection]) REFERENCES [dbo].[ConstEvenementSection] ([id_constEvenementSection]);


GO
PRINT N'Création de [dbo].[FK_CfgEvenementSectionRule_State]...';


GO
ALTER TABLE [dbo].[CfgEvenementSectionRule] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementSectionRule_State] FOREIGN KEY ([id_cfgEvenementSectionRule_constState]) REFERENCES [dbo].[ConstEvenementSectionState] ([id_constEvenementSectionControlState]);


GO
PRINT N'Création de [dbo].[FK_CfgEvenementSectionRule_ModePage]...';


GO
ALTER TABLE [dbo].[CfgEvenementSectionRule] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementSectionRule_ModePage] FOREIGN KEY ([id_cfgEvenementSectionRule_constModeSaisie]) REFERENCES [dbo].[ConstModeSaisie] ([id_constModeSaisie]);


GO
PRINT N'Création de [dbo].[FK_CfgEvenementSectionRule_TerminalSaisie]...';


GO
ALTER TABLE [dbo].[CfgEvenementSectionRule] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementSectionRule_TerminalSaisie] FOREIGN KEY ([id_cfgEvenementSectionRule_constTerminal]) REFERENCES [dbo].[ConstTerminalSaisie] ([id_constTerminalSaisie]);


GO
PRINT N'Création de [dbo].[FK_EvenementLieu_MaterielListe]...';


GO
ALTER TABLE [dbo].[EvenementLieu] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementLieu_MaterielListe] FOREIGN KEY ([id_evenementLieu_materielListe]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
PRINT N'Création de [dbo].[FK_MaterielListeAleaListe_MaterielListe]...';


GO
ALTER TABLE [dbo].[LnkMaterielListeAleaListe] WITH NOCHECK
    ADD CONSTRAINT [FK_MaterielListeAleaListe_MaterielListe] FOREIGN KEY ([id_materielListeAleaListe_materielList]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
PRINT N'Création de [dbo].[FK_LnkMaterielListMaterielEtatHistorique_MaterielList]...';


GO
ALTER TABLE [dbo].[LnkMaterielListMaterielEtatHistorique] WITH NOCHECK
    ADD CONSTRAINT [FK_LnkMaterielListMaterielEtatHistorique_MaterielList] FOREIGN KEY ([id_materielListMaterielEtatHistorique_materielListe]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
PRINT N'Création de [dbo].[FK_MaterielListe_ConstRessourceFamille]...';


GO
ALTER TABLE [dbo].[MaterielListe] WITH NOCHECK
    ADD CONSTRAINT [FK_MaterielListe_ConstRessourceFamille] FOREIGN KEY ([id_materielList_constRessourceFamille]) REFERENCES [dbo].[ConstRessourcesFamille] ([id_constRessourceFamille]);


GO
PRINT N'Création de [dbo].[FK_MaterielListe_MaterielDetail]...';


GO
ALTER TABLE [dbo].[MaterielListe] WITH NOCHECK
    ADD CONSTRAINT [FK_MaterielListe_MaterielDetail] FOREIGN KEY ([id_materielList_materielDetail]) REFERENCES [dbo].[MaterielDetail] ([id_materielDetail]);


GO
PRINT N'Création de [dbo].[FK_MaterielListe_MaterielEtat]...';


GO
ALTER TABLE [dbo].[MaterielListe] WITH NOCHECK
    ADD CONSTRAINT [FK_MaterielListe_MaterielEtat] FOREIGN KEY ([id_materielList_materielEtat]) REFERENCES [dbo].[MaterielEtat] ([id_materielEtat]);


GO
PRINT N'Création de [dbo].[FK_MaterielListe_MaterielType]...';


GO
ALTER TABLE [dbo].[MaterielListe] WITH NOCHECK
    ADD CONSTRAINT [FK_MaterielListe_MaterielType] FOREIGN KEY ([id_materielList_materielType]) REFERENCES [dbo].[MaterielType] ([id_materielType]);


GO
PRINT N'Création de [dbo].[FK_EvenementAffectation_MaterielListe_Agent]...';


GO
ALTER TABLE [dbo].[EvenementAffectation] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementAffectation_MaterielListe_Agent] FOREIGN KEY ([id_evenementAffectation_agent]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
PRINT N'Création de [dbo].[FK_EvenementAffectation_MaterielListe_Vehicule]...';


GO
ALTER TABLE [dbo].[EvenementAffectation] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementAffectation_MaterielListe_Vehicule] FOREIGN KEY ([id_evenementAffectation_vehicule]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
PRINT N'Création de [dbo].[FK_EvenementDommage_Ressource]...';


GO
ALTER TABLE [dbo].[EvenementDommage] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementDommage_Ressource] FOREIGN KEY ([id_evenementommage_ressource]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
PRINT N'Création de [dbo].[FK_EvenementExploitationConseq_RessourceHomme]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementExploitationConseq_RessourceHomme] FOREIGN KEY ([id_eevenementExploitationConseq_ressourceHomme]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
PRINT N'Création de [dbo].[FK_EvenementExploitationConseq_RessourceVehicule]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementExploitationConseq_RessourceVehicule] FOREIGN KEY ([id_eevenementExploitationConseq_ressourceVehicule]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
PRINT N'Création de [dbo].[FK_MaterielListe_MaterielDetailCategorie]...';


GO
ALTER TABLE [dbo].[MaterielListe] WITH NOCHECK
    ADD CONSTRAINT [FK_MaterielListe_MaterielDetailCategorie] FOREIGN KEY ([id_materielListe_materielCategorie]) REFERENCES [dbo].[MaterielDetailCategorie] ([id_materielDetailCategorie]);


GO
PRINT N'Création de [dbo].[FK_CfgConfigurationRules_CfgGeneral]...';


GO
ALTER TABLE [dbo].[CfgConfigurationRules] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgConfigurationRules_CfgGeneral] FOREIGN KEY ([id_cfgConfigurationRules_CfgGeneralMainCourante]) REFERENCES [dbo].[CfgGeneralMainCourante] ([id_configurationMainCourante]);


GO
PRINT N'Création de [dbo].[FK_CfgConfigurationRules_ConfigurationRules]...';


GO
ALTER TABLE [dbo].[CfgConfigurationRules] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgConfigurationRules_ConfigurationRules] FOREIGN KEY ([id_cfgConfigurationRules]) REFERENCES [dbo].[ConstConfigurationRules] ([id_configurationRules]);


GO
PRINT N'Création de [dbo].[FK_CfgEvenementFermetureRaisonFamille_EvenementFamille]...';


GO
ALTER TABLE [dbo].[CfgEvenementFermetureRaisonFamille] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementFermetureRaisonFamille_EvenementFamille] FOREIGN KEY ([id_cfgEvenementFermetureRaisonFamille_constEvenementFamille]) REFERENCES [dbo].[ConstEvenementFamille] ([id_constEvenementFamille]);


GO
PRINT N'Création de [dbo].[FK_CfgEvenementFermetureRaisonFamille_EvenementFermetureRaison]...';


GO
ALTER TABLE [dbo].[CfgEvenementFermetureRaisonFamille] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementFermetureRaisonFamille_EvenementFermetureRaison] FOREIGN KEY ([id_cfgEvenementFermetureRaisonFamille_fermetureRaison]) REFERENCES [dbo].[EvenementFermetureRaison] ([id_evenementFermetureRaison]);


GO
PRINT N'Création de [dbo].[FK_CfgRessourceSousClasse_RessourceSousClasse]...';


GO
ALTER TABLE [dbo].[CfgRessourceSousClasse] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgRessourceSousClasse_RessourceSousClasse] FOREIGN KEY ([id_cfgRessourceSousClasse_RessourceSousClasse]) REFERENCES [dbo].[MaterielType] ([id_materielType]);


GO
PRINT N'Modification de [dbo].[VueAleaListe]...';


GO
ALTER VIEW dbo.VueAleaListe
AS
SELECT        dbo.AleaListe.id_aleaListe, dbo.AleaListe.timeStamp, dbo.AleaListe.comentaires, dbo.AleaListe.dateApparition, dbo.AleaListe.heureApparition, dbo.AleaListe.heureFin, dbo.AleaType.id_aleaType, 
                         dbo.AleaType.nom AS [AleaType nom], dbo.AleaEtat.id_aleaEtat, dbo.AleaEtat.nom AS [AleaEtat nom], dbo.AleaListe.indentifiant, dbo.AleaListe.longitude_saisie, dbo.AleaListe.latitude_saisie, dbo.AleaListe.lastModification
FROM            dbo.AleaListe INNER JOIN
                         dbo.AleaType ON dbo.AleaListe.id_aleaListe_aleaType = dbo.AleaType.id_aleaType INNER JOIN
                         dbo.AleaEtat ON dbo.AleaListe.id_aleaListe_aleaEtat = dbo.AleaEtat.id_aleaEtat
GO
PRINT N'Modification de [dbo].[VueAleaListeMateriel]...';


GO
ALTER VIEW dbo.VueAleaListeMateriel
AS
SELECT        dbo.AleaListe.id_aleaListe, dbo.AleaListe.timeStamp, dbo.AleaListe.comentaires, dbo.AleaListe.dateApparition, dbo.AleaListe.heureApparition, dbo.AleaListe.heureFin, dbo.AleaType.id_aleaType, 
                         dbo.AleaType.nom AS [AleaType nom], dbo.AleaEtat.id_aleaEtat, dbo.AleaEtat.nom AS [AleaEtat nom], dbo.MaterielListe.id_materielListe, dbo.MaterielListe.id_reseau, dbo.AleaListe.indentifiant, dbo.AleaListe.ponderation, 
                         dbo.AleaListe.id_aleaListe_ressourceRemplacement, dbo.AleaListe.longitude_saisie, dbo.AleaListe.latitude_saisie, dbo.AleaListe.lastModification
FROM            dbo.AleaListe INNER JOIN
                         dbo.AleaType ON dbo.AleaListe.id_aleaListe_aleaType = dbo.AleaType.id_aleaType INNER JOIN
                         dbo.AleaEtat ON dbo.AleaListe.id_aleaListe_aleaEtat = dbo.AleaEtat.id_aleaEtat INNER JOIN
                         dbo.LnkMaterielListeAleaListe ON dbo.AleaListe.id_aleaListe = dbo.LnkMaterielListeAleaListe.id_materielListeAleaListe_aleaListe INNER JOIN
                         dbo.MaterielListe ON dbo.LnkMaterielListeAleaListe.id_materielListeAleaListe_materielList = dbo.MaterielListe.id_materielListe
GO
PRINT N'Actualisation de [dbo].[VueMaterielListeCountAleaEtat]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[VueMaterielListeCountAleaEtat]';


GO
PRINT N'Actualisation de [dbo].[VueAleaActionHistoriqueMateriel]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[VueAleaActionHistoriqueMateriel]';


GO
PRINT N'Actualisation de [dbo].[VueMaterielEtatHistorique]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[VueMaterielEtatHistorique]';


GO
PRINT N'Modification de [dbo].[VueMaterielListe]...';


GO
ALTER VIEW dbo.VueMaterielListe
AS
SELECT        dbo.MaterielListe.id_materielListe, dbo.MaterielListe.id_reseau, dbo.MaterielDetail.id_materielDetail, dbo.MaterielDetail.code, dbo.MaterielDetail.nomLong, dbo.MaterielDetailCategorie.id_materielDetailCategorie, 
                         dbo.MaterielDetailCategorie.nom AS MaterielDetailCategorieNom, dbo.MaterielType.id_materielType, dbo.MaterielType.nom AS MaterielTypeNom, dbo.MaterielElement.id_materielElement, 
                         dbo.MaterielElement.nom AS MaterielElementNom, dbo.MaterielListe.id_materielroulants2006m401, dbo.ConstRessourcesFamille.id_constRessourceFamille, 
                         dbo.ConstRessourcesFamille.code AS ConstRessourcesFamilleCode, dbo.ConstRessourcesFamille.libelle, dbo.MaterielListe.id_materielList_materielEtat, dbo.MaterielListe.deleted, dbo.MaterielListe.lastModification
FROM            dbo.MaterielListe LEFT OUTER JOIN
                         dbo.ConstRessourcesFamille ON dbo.MaterielListe.id_materielList_constRessourceFamille = dbo.ConstRessourcesFamille.id_constRessourceFamille LEFT OUTER JOIN
                         dbo.MaterielDetail ON dbo.MaterielListe.id_materielList_materielDetail = dbo.MaterielDetail.id_materielDetail LEFT OUTER JOIN
                         dbo.MaterielDetailCategorie ON dbo.MaterielDetailCategorie.id_materielDetailCategorie = dbo.MaterielDetail.id_materielDetail_materielCategorie LEFT OUTER JOIN
                         dbo.MaterielType ON dbo.MaterielListe.id_materielList_materielType = dbo.MaterielType.id_materielType LEFT OUTER JOIN
                         dbo.MaterielElement ON dbo.MaterielType.id_materielType_MaterielElement = dbo.MaterielElement.id_materielElement
GO
PRINT N'Modification de [dbo].[VueAleaListe].[MS_DiagramPane1]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[35] 4[26] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "AleaListe"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 294
            End
            DisplayFlags = 280
            TopColumn = 14
         End
         Begin Table = "AleaType"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 251
               Right = 258
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AleaEtat"
            Begin Extent = 
               Top = 138
               Left = 296
               Bottom = 234
               Right = 466
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueAleaListe';


GO
PRINT N'Modification de [dbo].[VueAleaListeMateriel].[MS_DiagramPane1]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "AleaListe"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 294
            End
            DisplayFlags = 280
            TopColumn = 14
         End
         Begin Table = "AleaType"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 251
               Right = 258
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AleaEtat"
            Begin Extent = 
               Top = 138
               Left = 296
               Bottom = 234
               Right = 466
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LnkMaterielListeAleaListe"
            Begin Extent = 
               Top = 252
               Left = 38
               Bottom = 365
               Right = 322
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterielListe"
            Begin Extent = 
               Top = 366
               Left = 38
               Bottom = 496
               Right = 281
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
        ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueAleaListeMateriel';


GO
PRINT N'Modification de [dbo].[VueMaterielListe].[MS_DiagramPane1]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[61] 4[1] 2[31] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "MaterielListe"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 328
            End
            DisplayFlags = 280
            TopColumn = 8
         End
         Begin Table = "ConstRessourcesFamille"
            Begin Extent = 
               Top = 6
               Left = 366
               Bottom = 119
               Right = 590
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterielDetail"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 314
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterielDetailCategorie"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 366
               Right = 266
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterielType"
            Begin Extent = 
               Top = 366
               Left = 38
               Bottom = 496
               Right = 302
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterielElement"
            Begin Extent = 
               Top = 498
               Left = 38
               Bottom = 628
               Right = 228
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin Colu', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueMaterielListe';


GO
PRINT N'Modification de [dbo].[VueMaterielListe].[MS_DiagramPane2]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane2', @value = N'mnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueMaterielListe';


GO
PRINT N'Vérification de données existantes par rapport aux nouvelles contraintes';


GO
USE [$(DatabaseName)];


GO
ALTER TABLE [dbo].[EvenementMaintenance] WITH CHECK CHECK CONSTRAINT [FK_EvenementMaintenance_EvenementListe];

ALTER TABLE [dbo].[EvenementSecurite] WITH CHECK CHECK CONSTRAINT [FK_EvenementSecurite_EvenementListe];

ALTER TABLE [dbo].[AleaHistorique] WITH CHECK CHECK CONSTRAINT [FK_AleaHistorique_Evenement];

ALTER TABLE [dbo].[AleaListe] WITH CHECK CHECK CONSTRAINT [FK_AleaListe_Affectation];

ALTER TABLE [dbo].[AleaListe] WITH CHECK CHECK CONSTRAINT [FK_AleaListe_AleaEtat];

ALTER TABLE [dbo].[AleaListe] WITH CHECK CHECK CONSTRAINT [FK_AleaListe_AleaType];

ALTER TABLE [dbo].[AleaListe] WITH CHECK CHECK CONSTRAINT [FK_AleaListe_EvenementLieu];

ALTER TABLE [dbo].[AleaListe] WITH CHECK CHECK CONSTRAINT [FK_AleaListe_MeteoDetails];

ALTER TABLE [dbo].[AleaListe] WITH CHECK CHECK CONSTRAINT [FK_AleaListe_Ressource];

ALTER TABLE [dbo].[AleaListe] WITH CHECK CHECK CONSTRAINT [FK_AleaListe_RessourceRemplacement];

ALTER TABLE [dbo].[LnkAleaListAleaHistorique] WITH CHECK CHECK CONSTRAINT [FK_AleaListAleaHistorique_AleaList];

ALTER TABLE [dbo].[LnkAleaListeFicheRealise] WITH CHECK CHECK CONSTRAINT [FK_LnkAleaListeFicheRealise_AleaListe];

ALTER TABLE [dbo].[LnkEvenementAlertHistorique] WITH CHECK CHECK CONSTRAINT [FK_LnkEvenementAlertHistorique_Evenement];

ALTER TABLE [dbo].[LnkMaterielListeAleaListe] WITH CHECK CHECK CONSTRAINT [FK_MaterielListeAleaListe_AleaListe];

ALTER TABLE [dbo].[QueryTriggerAsynchoneHistorique] WITH CHECK CHECK CONSTRAINT [FK_QueryTriggerAsynchoneHistorique_Alea];

ALTER TABLE [dbo].[EvenementDommage] WITH CHECK CHECK CONSTRAINT [FK_EvenementDommage_AleaListe];

ALTER TABLE [dbo].[EvenementExploitationConseq] WITH CHECK CHECK CONSTRAINT [FK_EvenementExploitationConseq_EvenementList];

ALTER TABLE [dbo].[EvenementFichier] WITH CHECK CHECK CONSTRAINT [FK_EvenementFichier_AleaListe];

ALTER TABLE [dbo].[EvenementHistoriqueView] WITH CHECK CHECK CONSTRAINT [FK_EvenementHistoriqueView_Evenement];

ALTER TABLE [dbo].[EvenementIntervention] WITH CHECK CHECK CONSTRAINT [FK_EvenementIntervention_EvenementList];

ALTER TABLE [dbo].[AleaListe] WITH CHECK CHECK CONSTRAINT [FK_AleaListe_FermetureRaison];

ALTER TABLE [dbo].[CfgEvenementControlRule] WITH CHECK CHECK CONSTRAINT [FK_CfgEvenementControlRule_Control];

ALTER TABLE [dbo].[CfgEvenementControlRule] WITH CHECK CHECK CONSTRAINT [FK_CfgEvenementControlRule_EvenementFamille];

ALTER TABLE [dbo].[CfgEvenementControlRule] WITH CHECK CHECK CONSTRAINT [FK_CfgEvenementControlRule_RessourceFamille];

ALTER TABLE [dbo].[CfgEvenementControlRule] WITH CHECK CHECK CONSTRAINT [FK_CfgEvenementControlRule_State];

ALTER TABLE [dbo].[CfgEvenementControlRule] WITH CHECK CHECK CONSTRAINT [FK_CfgEvenementControlRule_ModePage];

ALTER TABLE [dbo].[CfgEvenementControlRule] WITH CHECK CHECK CONSTRAINT [FK_CfgEvenementControlRule_Terminal];

ALTER TABLE [dbo].[CfgEvenementSectionRule] WITH CHECK CHECK CONSTRAINT [FK_CfgEvenementSectionRule_EvenementFamille];

ALTER TABLE [dbo].[CfgEvenementSectionRule] WITH CHECK CHECK CONSTRAINT [FK_CfgEvenementSectionRule_RessourceFamille];

ALTER TABLE [dbo].[CfgEvenementSectionRule] WITH CHECK CHECK CONSTRAINT [FK_CfgEvenementSectionRule_Section];

ALTER TABLE [dbo].[CfgEvenementSectionRule] WITH CHECK CHECK CONSTRAINT [FK_CfgEvenementSectionRule_State];

ALTER TABLE [dbo].[CfgEvenementSectionRule] WITH CHECK CHECK CONSTRAINT [FK_CfgEvenementSectionRule_ModePage];

ALTER TABLE [dbo].[CfgEvenementSectionRule] WITH CHECK CHECK CONSTRAINT [FK_CfgEvenementSectionRule_TerminalSaisie];

ALTER TABLE [dbo].[EvenementLieu] WITH CHECK CHECK CONSTRAINT [FK_EvenementLieu_MaterielListe];

ALTER TABLE [dbo].[LnkMaterielListeAleaListe] WITH CHECK CHECK CONSTRAINT [FK_MaterielListeAleaListe_MaterielListe];

ALTER TABLE [dbo].[LnkMaterielListMaterielEtatHistorique] WITH CHECK CHECK CONSTRAINT [FK_LnkMaterielListMaterielEtatHistorique_MaterielList];

ALTER TABLE [dbo].[MaterielListe] WITH CHECK CHECK CONSTRAINT [FK_MaterielListe_ConstRessourceFamille];

ALTER TABLE [dbo].[MaterielListe] WITH CHECK CHECK CONSTRAINT [FK_MaterielListe_MaterielDetail];

ALTER TABLE [dbo].[MaterielListe] WITH CHECK CHECK CONSTRAINT [FK_MaterielListe_MaterielEtat];

ALTER TABLE [dbo].[MaterielListe] WITH CHECK CHECK CONSTRAINT [FK_MaterielListe_MaterielType];

ALTER TABLE [dbo].[EvenementAffectation] WITH CHECK CHECK CONSTRAINT [FK_EvenementAffectation_MaterielListe_Agent];

ALTER TABLE [dbo].[EvenementAffectation] WITH CHECK CHECK CONSTRAINT [FK_EvenementAffectation_MaterielListe_Vehicule];

ALTER TABLE [dbo].[EvenementDommage] WITH CHECK CHECK CONSTRAINT [FK_EvenementDommage_Ressource];

ALTER TABLE [dbo].[EvenementExploitationConseq] WITH CHECK CHECK CONSTRAINT [FK_EvenementExploitationConseq_RessourceHomme];

ALTER TABLE [dbo].[EvenementExploitationConseq] WITH CHECK CHECK CONSTRAINT [FK_EvenementExploitationConseq_RessourceVehicule];

ALTER TABLE [dbo].[MaterielListe] WITH CHECK CHECK CONSTRAINT [FK_MaterielListe_MaterielDetailCategorie];

ALTER TABLE [dbo].[CfgConfigurationRules] WITH CHECK CHECK CONSTRAINT [FK_CfgConfigurationRules_CfgGeneral];

ALTER TABLE [dbo].[CfgConfigurationRules] WITH CHECK CHECK CONSTRAINT [FK_CfgConfigurationRules_ConfigurationRules];

ALTER TABLE [dbo].[CfgEvenementFermetureRaisonFamille] WITH CHECK CHECK CONSTRAINT [FK_CfgEvenementFermetureRaisonFamille_EvenementFamille];

ALTER TABLE [dbo].[CfgEvenementFermetureRaisonFamille] WITH CHECK CHECK CONSTRAINT [FK_CfgEvenementFermetureRaisonFamille_EvenementFermetureRaison];

ALTER TABLE [dbo].[CfgRessourceSousClasse] WITH CHECK CHECK CONSTRAINT [FK_CfgRessourceSousClasse_RessourceSousClasse];


GO
PRINT N'Mise à jour terminée.';


GO


USE [s2002m401]
GO

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])
     VALUES
           (1
           ,47)
GO


USE [s2002m401]
GO

INSERT INTO [dbo].[ConstQueryCriteria]
           ([dataType]
           ,[libelle]
           ,[id_queryCriteria_queryCriteriaGroup]
           ,[code]
           ,[isEligibleOnTheFlight]
           ,[isEligibleAdvancedMode]
           ,[description])
     VALUES
           ('CHECK_PRESENCE'
           ,'Ressource remplacement'
           ,3
           ,'WITH_RESSOURCE_REMPLACEMENT'
           ,1
           ,1
           ,'Evénement contenant une ressource de remplacement')
GO


USE [s2002m401]
GO

INSERT INTO [dbo].[ConstEvenementAttribut]
           ([id_constEvenementAttribut_group]
           ,[code]
           ,[libelle]
           ,[isEligibleQueryResult]
           ,[isEligibleXaxis]
           ,[isEligibleSeriesAnalyse]
           ,[rang]
           ,[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_AUTO'
           ,'Nb. Course auto'
           ,1
           ,0
           ,0
           ,13
           ,'Exploitation conséquence')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstEvenementAttribut]
           ([id_constEvenementAttribut_group]
           ,[code]
           ,[libelle]
           ,[isEligibleQueryResult]
           ,[isEligibleXaxis]
           ,[isEligibleSeriesAnalyse]
           ,[rang]
           ,[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_KM_AUTO'
           ,'Nb. Km auto'
           ,1
           ,0
           ,0
           ,14
           ,'Exploitation conséquence')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstEvenementAttribut]
           ([id_constEvenementAttribut_group]
           ,[code]
           ,[libelle]
           ,[isEligibleQueryResult]
           ,[isEligibleXaxis]
           ,[isEligibleSeriesAnalyse]
           ,[rang]
           ,[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_SV_AUTO'
           ,'SV auto'
           ,1
           ,0
           ,0
           ,22
           ,'Exploitation conséquence')
GO





USE [s2002m401]

---------------------G�n�ral 2.2.0-----------------------

---------------------FIL-330 Gestion des �tats -----------------------
GO
INSERT INTO [dbo].[ConstEvenementEtat]([code],[libelle]) VALUES ('OUVERT','Ouvert')
INSERT INTO [dbo].[ConstEvenementEtat]([code],[libelle]) VALUES ('EN_COURS','En cours')
INSERT INTO [dbo].[ConstEvenementEtat]([code],[libelle]) VALUES ('FERME','Ferm�')
INSERT INTO [dbo].[ConstEvenementEtat]([code],[libelle]) VALUES ('ANNULE','Annul�')
INSERT INTO [dbo].[ConstEvenementEtat]([code],[libelle]) VALUES ('BROUILLON','Brouillon')
GO

---------------------FIL-335 Crit�re sur pr�cision -----------------------
INSERT INTO [dbo].[ConstQueryCriteriaGroup]([nom])
     VALUES ('Pr�cision')
GO
GO

INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('SELECT_OBJECT','Ev�nement pr�cision',8,'EVENEMENT_PRECISION_SELECT',0,1,'Ev�nement pr�cision',0)
GO

---------------------FIL-172 Section action -----------------------

GO

INSERT INTO [dbo].[ConstEvenementAttributGroup]([nom],[rang])
     VALUES ('Actions',10)

INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (18,'ACTION_ACTION_REALISE','Action',1,1,1,1,'Action r�alis�e')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (18,'ACTION_UTILISATEUR','Utilisateur',1,1,1,2,'Action r�alis�e')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (18,'ACTION_DATE','Date',1,0,0,3,'Action r�alis�e')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (18,'ACTION_TIME','Heure',1,0,0,4,'Action r�alis�e')

INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('ACTION_ACTION_REALISE','Action r�alis�e')
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('ACTION_UTILISATEUR','Action utilisateur')
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('ACTION_DATE','Action date.')
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('ACTION_TIME','Action heure')

INSERT INTO [dbo].[ConstEvenementSection]([code],[libelle])
     VALUES ('ACTION','Actions')
  
GO

---------------------FIL-292 Intervenants 1 /2 -----------------------

GO

INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (11,'INTERVENTION_RESSOURCE_1','Agent 1',1,1,1,6,'Intervention')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (11,'INTERVENTION_RESSOURCE_2','Agent 2',1,1,1,7,'Intervention')

INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('INTERVENTION_RESSOURCE_1','Intervention agent 1')
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('INTERVENTION_RESSOURCE_2','Intervention agent 2')

---------------------FIL-88  Permission par �tat / sous cat�gorie / sous classe-----------------------

GO
INSERT INTO [dbo].[ConstPermission] ([code],[label],[rang])
     VALUES ('LECTURE','Lecture et visualisation',10)
INSERT INTO [dbo].[ConstPermission] ([code],[label],[rang])
     VALUES ('ECRITURE','Ecriture et �dition',50)
GO

---------------------Nouveaux crit�re sur manoeuvre / heures supp -----------------------
INSERT INTO [dbo].[ConstQueryCriteriaGroup]([nom])
     VALUES ('RH')
GO
GO

INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('INT','Heures suppl�mentaires',9,'HEURE_SUPPLEMENTAIRE',0,1,'RH Heures suppl�mentaires',0)

		   GO

INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('SELECT_OBJECT','Manoeuvre exploitation',7,'MANOUEVRE_EXPLOITATION',0,1,'Manoeuvre exploitation',0)
GO
USE [s2002m401]

---------------------G�n�ral 2.3.0-----------------------

---------------------FIL-330 Gestion des �tats -----------------------
GO
IF NOT EXISTS(select * from [ConstFichierTypeFolderMode] where code = 'MODELE')
INSERT INTO [dbo].[ConstFichierTypeFolderMode]([code],[libelle]) VALUES ('MODELE','Fichiers mod�les � remplir')

GO
IF NOT EXISTS(select * from [ConstFichierTypeFolderMode] where code = 'PROCESS')
INSERT INTO [dbo].[ConstFichierTypeFolderMode]([code],[libelle]) VALUES ('PROCESS','Fichiers proc�dures')
GO

---------------------FIL-81 GMAO -----------------------

GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'MAINTENANCE_ORGANE_GMAO')
INSERT INTO [dbo].[ConstEvenementAttribut]
           ([id_constEvenementAttribut_group]
           ,[code]
           ,[libelle]
           ,[isEligibleQueryResult]
           ,[isEligibleXaxis]
           ,[isEligibleSeriesAnalyse]
           ,[rang]
           ,[header])
     VALUES
           (12
           ,'MAINTENANCE_ORGANE_GMAO'
           ,'Organe GMAO'
           ,1
           ,1
           ,1
           ,2
           ,'Maintenance')
GO

GO
IF NOT EXISTS(select * from [COM] where [nomAlerteCOM] = 'GMAO')
INSERT INTO [dbo].[COM]
           ([nomAlerteCOM])
     VALUES
           ('GMAO')
GO




USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries]
           ,[id_constAnalyseType_constAnalyseTypeGroup])
     VALUES
           ('NB_EVENEMENT_PAR_JOUR_PAR_SOUS_CLASSE'
           ,'Ev�. par jour sous classe'
           ,'Nombre �v�nement par jour par sous classe'
           ,1
           ,1)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries]
           ,[id_constAnalyseType_constAnalyseTypeGroup])
     VALUES
           ('NB_EVENEMENT_PAR_JOUR_PAR_CATEGORIE'
           ,'Ev�. par jour cat�g'
           ,'Nombre �v�nement par jour par cat�gorie'
           ,1
           ,1)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries]
           ,[id_constAnalyseType_constAnalyseTypeGroup])
     VALUES
           ('NB_EVENEMENT_PAR_SOUS_CATEGORIE_SOUS_CLASSE'
           ,'Ev�. sous cat�g sous class'
           ,'Nombre �v�nement par sous cat�g. par sous classe'
           ,1
           ,1)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries]
           ,[id_constAnalyseType_constAnalyseTypeGroup])
     VALUES
           ('NB_EVENEMENT_PAR_SOUS_CLASSE_CATEG'
           ,'Ev�. sous classe cat�g'
           ,'Nombre �v�nement par sous classe par cat�g.'
           ,1
           ,1)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries]
           ,[id_constAnalyseType_constAnalyseTypeGroup])
     VALUES
           ('NB_EVENEMENT_PAR_LIGNE_PAR_SOUS_CLASSE'
           ,'Ev�. par ligne sous classe'
           ,'Nombre �v�nement par ligne sous classe'
           ,1
           ,1)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries]
           ,[id_constAnalyseType_constAnalyseTypeGroup])
     VALUES
           ('NB_EVENEMENT_PAR_LIGNE_PAR_CATEGORIE'
           ,'Ev�. par ligne sous cat�g.'
           ,'Nombre �v�nement par ligne cat�g.'
           ,1
           ,1)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries]
           ,[id_constAnalyseType_constAnalyseTypeGroup])
     VALUES
           ('NB_EVENEMENT_MOY_PAR_JOUR_SEMAINE_PAR_SOUS_CLASSE'
           ,'Ev�. par j. sem. ss classe'
           ,'Nombre �v�nement moy jour sem. sous classe'
           ,1
           ,1)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries]
           ,[id_constAnalyseType_constAnalyseTypeGroup])
     VALUES
           ('NB_EVENEMENT_MOY_PAR_JOUR_SEMAINE_PAR_CATEGORIE'
           ,'Ev�. par J. sem. cat�gorie'
           ,'Nombre �v�nement moy jour sem. cat�gorie'
           ,1
           ,1)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries]
           ,[id_constAnalyseType_constAnalyseTypeGroup])
     VALUES
           ('NB_EVENEMENT_MOY_PAR_HEURE_PAR_SOUS_CLASSE'
           ,'Ev�. par heure ss classe'
           ,'Nombre �v�nement moy heure sous classe'
           ,1
           ,1)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries]
           ,[id_constAnalyseType_constAnalyseTypeGroup])
     VALUES
           ('NB_EVENEMENT_MOY_PAR_HEURE_PAR_CATEGORIE'
           ,'Ev�. par heure cat�gorie'
           ,'Nombre �v�nement moy heure cat�gorie'
           ,1
           ,1)
GO

USE [s2002m401]
GO
INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries]
           ,[id_constAnalyseType_constAnalyseTypeGroup])
     VALUES
           ('NB_EVENEMENT_PAR_CATEGORIE_RESS_PAR_ORGANE1'
           ,'Ev�. par ress cat org niv1'
           ,'Nombre �v�nements par ress. cat�g. organe niv1'
           ,1
           ,5)
GO

USE [s2002m401]
GO
INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries]
           ,[id_constAnalyseType_constAnalyseTypeGroup])
     VALUES
           ('NB_EVENEMENT_PAR_CATEGORIE_RESS_PAR_ORGANE'
           ,'Ev�. par ress cat org'
           ,'Nombre �v�nements par ress. cat�g. organe'
           ,1
           ,5)
GO














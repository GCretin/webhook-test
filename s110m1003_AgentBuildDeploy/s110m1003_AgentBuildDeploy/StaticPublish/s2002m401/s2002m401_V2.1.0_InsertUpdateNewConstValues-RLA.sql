USE [s2002m401]

---------------------RLA2.1.0-----------------------

---------------------FIL-120 Masquage de la r�gle-----------------------
GO

INSERT INTO [dbo].[CfgEvenementControlRule]
           ([id_cfgEvenementControlRule_constControl]
           ,[id_cfgEvenementControlRule_constState]
           ,[id_cfgEvenementControlRule_reseaus101m401])
     VALUES
           (65
           ,4
           ,1)

INSERT INTO [dbo].[CfgEvenementControlRule]
           ([id_cfgEvenementControlRule_constControl]
           ,[id_cfgEvenementControlRule_constState]
           ,[id_cfgEvenementControlRule_reseaus101m401])
     VALUES
           (64
           ,4
           ,1)

		   INSERT INTO [dbo].[CfgEvenementControlRule]
           ([id_cfgEvenementControlRule_constControl]
           ,[id_cfgEvenementControlRule_constState]
           ,[id_cfgEvenementControlRule_reseaus101m401])
     VALUES
           (8
           ,3
           ,1)




---------------------FIL-128 Masquage du controle fermeture raison (tout le temps)-----------------------

INSERT INTO [dbo].[EvenementFermetureRaison]([libelle],[isGlobal],[id_reseau101m401]) VALUES('Termin�',1,1)
INSERT INTO [dbo].[EvenementFermetureRaison]([libelle],[isGlobal],[id_reseau101m401]) VALUES('Pas pris en compte',1,1)
INSERT INTO [dbo].[EvenementFermetureRaison]([libelle],[isGlobal],[automatiqueHeure],[id_reseau101m401]) VALUES('Automatique la nuit',1,'03:00:00',1)

INSERT INTO [dbo].[CfgEvenementControlRule] ([id_cfgEvenementControlRule_constControl],[id_cfgEvenementControlRule_constState],[id_cfgEvenementControlRule_reseaus101m401]) VALUES
		(66,4,1)

---------------------FIL-169 Section lieu et affectation lecture seule modification-----------------------

INSERT INTO [dbo].[CfgEvenementSectionRule]([id_cfgEvenementSectionRule_constSection],[id_cfgEvenementSectionRule_constState],[id_cfgEvenementSectionRule_constModeSaisie],[id_cfgEvenementSectionRule_reseaus101m401])
     VALUES (4,4,2,1)

INSERT INTO [dbo].[CfgEvenementSectionRule]([id_cfgEvenementSectionRule_constSection],[id_cfgEvenementSectionRule_constState],[id_cfgEvenementSectionRule_constModeSaisie],[id_cfgEvenementSectionRule_reseaus101m401])
     VALUES (8,4,2,1)

---------------------FIL-130 INtervention d�but et fin-----------------------

INSERT INTO [dbo].[CfgEvenementControlRule] ([id_cfgEvenementControlRule_constControl],[id_cfgEvenementControlRule_constState],[id_cfgEvenementControlRule_reseaus101m401])
	VALUES (67,4,1)

INSERT INTO [dbo].[CfgEvenementControlRule] ([id_cfgEvenementControlRule_constControl],[id_cfgEvenementControlRule_constState],[id_cfgEvenementControlRule_reseaus101m401])
	VALUES (68,4,1)

	---------------------FIL-114 Migration mod�le ressource -----------------------

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10017
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 1

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10018
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 2

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10019
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 3

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10020
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 4

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10021 
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 5

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10022
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 6

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10023
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 7

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10024
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] =  8

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10025
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 9

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10026
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] =10

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10027
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] =11

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10028
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] =12

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10029
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] =13

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10030
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] =14

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10031
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 15

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10032
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 16

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10033
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 17

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10034
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 18

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10035
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 19

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10036
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 20

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10037
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 21

---------------------FIL-158 Coloration dans les r�gles---------------------

USE [s2002m401]
GO

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Marron clair','rgb(200, 173, 127)','rgb(0,0,0)','rgb(139, 108, 66)','rgb(255,255,255)',1,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Marron','rgb(187, 174, 152)','rgb(0,0,0)','rgb(104, 94, 67)','rgb(255,255,255)',1,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Orange fonc�','rgb(255, 203, 96)','rgb(0,0,0)','rgb(237, 127, 16)','rgb(255,255,255)',1,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Orange','rgb(255, 228, 196)','rgb(0,0,0)','rgb(250, 164, 1)','rgb(255,255,255)',1,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Violet','rgb(254, 191, 210)','rgb(0,0,0)','rgb(212, 115, 212)','rgb(255,255,255)',1,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Vert','rgb(176, 242, 182)','rgb(255,255,255)','rgb(130, 196, 108)','rgb(255,255,255)',1,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Vert fonc�','rgb(130, 196, 108)','rgb(0,0,0)','rgb(20, 148, 20)','rgb(255,255,255)',90,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Bleu fonc�','rgb(116, 208, 241)','rgb(0,0,0)','rgb(0, 112, 192)','rgb(255,255,255)',80,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Bleu','rgb(169, 234, 254)','rgb(0,0,0)','rgb(0,127,255)','rgb(255,255,255)',1,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Rouge','rgb(254, 150, 160)','rgb(0,0,0)','rgb(139, 108, 66)','rgb(255,255,255)',1,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Rouge fonc�','rgb(254, 150, 160)','rgb(0,0,0)','rgb(233, 56, 63)','rgb(255,255,255)',100,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Jaune','rgb(200, 173, 127)','rgb(255,255,255)','rgb(255, 228, 54)','rgb(255,255,255)',1,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Jaune fonc�','rgb(200, 173, 127)','rgb(255,255,255)','rgb(255, 215, 0)','rgb(255,255,255)',1,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Gris','rgb(221, 221, 221)','rgb(0,0,0)','rgb(128, 128, 128)','rgb(255,255,255)',0,1)
GO


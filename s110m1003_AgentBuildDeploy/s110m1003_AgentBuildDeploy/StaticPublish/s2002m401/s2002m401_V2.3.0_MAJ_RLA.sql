﻿/*
Script de déploiement pour s2002m401_temp

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "s2002m401"
:setvar DefaultFilePrefix "s2002m401"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL13.RLAPRODUCTION01\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL13.RLAPRODUCTION01\MSSQL\DATA\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
/*
La colonne [dbo].[CfgPageAnalyse].[graphHeight] est en cours de suppression, des données risquent d'être perdues.

La colonne [dbo].[CfgPageAnalyse].[graphPositionLeft] est en cours de suppression, des données risquent d'être perdues.

La colonne [dbo].[CfgPageAnalyse].[graphPositionTop] est en cours de suppression, des données risquent d'être perdues.

La colonne [dbo].[CfgPageAnalyse].[graphWidth] est en cours de suppression, des données risquent d'être perdues.

La colonne [dbo].[CfgPageAnalyse].[legendPositionLeft] est en cours de suppression, des données risquent d'être perdues.

La colonne [dbo].[CfgPageAnalyse].[legendPositionTop] est en cours de suppression, des données risquent d'être perdues.

La colonne [dbo].[CfgPageAnalyse].[orientationLabelX] est en cours de suppression, des données risquent d'être perdues.

La colonne [dbo].[CfgPageAnalyse].[positionInPage] est en cours de suppression, des données risquent d'être perdues.
*/

--IF EXISTS (select top 1 1 from [dbo].[CfgPageAnalyse])
--    RAISERROR (N'Lignes détectées. Arrêt de la mise à jour du schéma en raison d''''un risque de perte de données.', 16, 127) WITH NOWAIT

GO
/*
La colonne [dbo].[DBinfos].[valeurs] est en cours de suppression, des données risquent d'être perdues.
*/

--IF EXISTS (select top 1 1 from [dbo].[DBinfos])
--    RAISERROR (N'Lignes détectées. Arrêt de la mise à jour du schéma en raison d''''un risque de perte de données.', 16, 127) WITH NOWAIT

GO
PRINT N'Suppression de [dbo].[DF_CfgPageAnalyse_fontSizeDataLabel]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [DF_CfgPageAnalyse_fontSizeDataLabel];


GO
PRINT N'Suppression de [dbo].[DF_CfgPageAnalyse_colorChart]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [DF_CfgPageAnalyse_colorChart];


GO
PRINT N'Suppression de [dbo].[DF_CfgPageAnalyse_fontSizeXValue]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [DF_CfgPageAnalyse_fontSizeXValue];


GO
PRINT N'Suppression de [dbo].[DF_CfgPageAnalyse_fontSizeXTitle]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [DF_CfgPageAnalyse_fontSizeXTitle];


GO
PRINT N'Suppression de [dbo].[DF_CfgPageAnalyse_fontSizeLegendSerie]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [DF_CfgPageAnalyse_fontSizeLegendSerie];


GO
PRINT N'Suppression de [dbo].[DF_CfgPageAnalyse_fontSizeyLabel]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [DF_CfgPageAnalyse_fontSizeyLabel];


GO
PRINT N'Suppression de [dbo].[DF_CfgPageAnalyse_fontSizeYValue]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [DF_CfgPageAnalyse_fontSizeYValue];


GO
PRINT N'Suppression de [dbo].[DF_CfgPageAnalyse_grahPositionTop]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [DF_CfgPageAnalyse_grahPositionTop];


GO
PRINT N'Suppression de [dbo].[DF_CfgPageAnalyse_graphHeight]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [DF_CfgPageAnalyse_graphHeight];


GO
PRINT N'Suppression de [dbo].[DF_CfgPageAnalyse_graphPositionLeft]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [DF_CfgPageAnalyse_graphPositionLeft];


GO
PRINT N'Suppression de [dbo].[DF_CfgPageAnalyse_graphWidth]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [DF_CfgPageAnalyse_graphWidth];


GO
PRINT N'Suppression de [dbo].[DF_CfgPageAnalyse_legend]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [DF_CfgPageAnalyse_legend];


GO
PRINT N'Suppression de [dbo].[DF_CfgPageAnalyse_legendHeight]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [DF_CfgPageAnalyse_legendHeight];


GO
PRINT N'Suppression de [dbo].[DF_CfgPageAnalyse_legendPositionLeft]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [DF_CfgPageAnalyse_legendPositionLeft];


GO
PRINT N'Suppression de [dbo].[DF_CfgPageAnalyse_legendPositionTop]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [DF_CfgPageAnalyse_legendPositionTop];


GO
PRINT N'Suppression de [dbo].[DF_CfgPageAnalyse_legendWidth]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [DF_CfgPageAnalyse_legendWidth];


GO
PRINT N'Suppression de [dbo].[DF_CfgPageAnalyse_orientationLabelX]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [DF_CfgPageAnalyse_orientationLabelX];


GO
PRINT N'Suppression de [dbo].[DF_CfgPageAnalyse_positionInPage]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [DF_CfgPageAnalyse_positionInPage];


GO
PRINT N'Suppression de [dbo].[DF_CfgPageAnalyse_withDataLabel]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [DF_CfgPageAnalyse_withDataLabel];


GO
PRINT N'Suppression de [dbo].[DF_CfgPageAnalyse_withLegend]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [DF_CfgPageAnalyse_withLegend];


GO
PRINT N'Suppression de [dbo].[DF_CfgPageAnalyse_withTableDisplay]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [DF_CfgPageAnalyse_withTableDisplay];


GO
PRINT N'Suppression de [dbo].[DF_CfgPageAnalyse_withTitleAnalyse]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [DF_CfgPageAnalyse_withTitleAnalyse];


GO
PRINT N'Suppression de [dbo].[DF_EvenementExploitationConseq_isSVnonAssure]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] DROP CONSTRAINT [DF_EvenementExploitationConseq_isSVnonAssure];


GO
PRINT N'Suppression de [dbo].[DF_EvenementExploitationConseq_sv_proportion]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] DROP CONSTRAINT [DF_EvenementExploitationConseq_sv_proportion];


GO
PRINT N'Suppression de [dbo].[FK_CfgConfigurationRules_CfgGeneral]...';


GO
ALTER TABLE [dbo].[CfgConfigurationRules] DROP CONSTRAINT [FK_CfgConfigurationRules_CfgGeneral];


GO
PRINT N'Suppression de [dbo].[FK_CfgGeneralMainCourante_Categorie1]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] DROP CONSTRAINT [FK_CfgGeneralMainCourante_Categorie1];


GO
PRINT N'Suppression de [dbo].[FK_CfgGeneralMainCourante_Categorie2]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] DROP CONSTRAINT [FK_CfgGeneralMainCourante_Categorie2];


GO
PRINT N'Suppression de [dbo].[FK_CfgGeneralMainCourante_CategorieClient]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] DROP CONSTRAINT [FK_CfgGeneralMainCourante_CategorieClient];


GO
PRINT N'Suppression de [dbo].[FK_CfgGeneralMainCourante_ClientClasse]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] DROP CONSTRAINT [FK_CfgGeneralMainCourante_ClientClasse];


GO
PRINT N'Suppression de [dbo].[FK_CfgGeneralMainCourante_Etat1]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] DROP CONSTRAINT [FK_CfgGeneralMainCourante_Etat1];


GO
PRINT N'Suppression de [dbo].[FK_CfgGeneralMainCourante_Etat2]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] DROP CONSTRAINT [FK_CfgGeneralMainCourante_Etat2];


GO
PRINT N'Suppression de [dbo].[FK_CfgGeneralMainCourante_Etat3]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] DROP CONSTRAINT [FK_CfgGeneralMainCourante_Etat3];


GO
PRINT N'Suppression de [dbo].[FK_CfgGeneralMainCourante_RessourceClasse]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] DROP CONSTRAINT [FK_CfgGeneralMainCourante_RessourceClasse];


GO
PRINT N'Suppression de [dbo].[FK_CfgGeneralMainCourante_SousClasseClient]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] DROP CONSTRAINT [FK_CfgGeneralMainCourante_SousClasseClient];


GO
PRINT N'Suppression de [dbo].[FK_ConfigurationMainCourante_AleaState]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] DROP CONSTRAINT [FK_ConfigurationMainCourante_AleaState];


GO
PRINT N'Suppression de [dbo].[FK_CfgPageAnalyse_Analyse]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [FK_CfgPageAnalyse_Analyse];


GO
PRINT N'Suppression de [dbo].[FK_CfgPageAnalyse_Page]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] DROP CONSTRAINT [FK_CfgPageAnalyse_Page];


GO
PRINT N'Suppression de [dbo].[FK_EvenementActionRealise_EvenementAction]...';


GO
ALTER TABLE [dbo].[EvenementActionRealise] DROP CONSTRAINT [FK_EvenementActionRealise_EvenementAction];


GO
PRINT N'Suppression de [dbo].[FK_EvenementActionRealise_EvenementListe]...';


GO
ALTER TABLE [dbo].[EvenementActionRealise] DROP CONSTRAINT [FK_EvenementActionRealise_EvenementListe];


GO
PRINT N'Suppression de [dbo].[FK_EvenementExploitationConseq_RessourceVehicule]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] DROP CONSTRAINT [FK_EvenementExploitationConseq_RessourceVehicule];


GO
PRINT N'Suppression de [dbo].[FK_EvenementExploitationConseq_EvenementExploitationConseq]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] DROP CONSTRAINT [FK_EvenementExploitationConseq_EvenementExploitationConseq];


GO
PRINT N'Suppression de [dbo].[FK_EvenementExploitationConseq_EvenementList]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] DROP CONSTRAINT [FK_EvenementExploitationConseq_EvenementList];


GO
PRINT N'Suppression de [dbo].[FK_EvenementExploitationConseq_ExploitationConseqManoeuvre]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] DROP CONSTRAINT [FK_EvenementExploitationConseq_ExploitationConseqManoeuvre];


GO
PRINT N'Suppression de [dbo].[FK_EvenementExploitationConseq_RessourceHomme]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] DROP CONSTRAINT [FK_EvenementExploitationConseq_RessourceHomme];


GO
PRINT N'Suppression de [dbo].[FK_EvenementIntervention_EvenementList]...';


GO
ALTER TABLE [dbo].[EvenementIntervention] DROP CONSTRAINT [FK_EvenementIntervention_EvenementList];


GO
PRINT N'Suppression de [dbo].[FK_EvenementIntervention_Ressource1]...';


GO
ALTER TABLE [dbo].[EvenementIntervention] DROP CONSTRAINT [FK_EvenementIntervention_Ressource1];


GO
PRINT N'Suppression de [dbo].[FK_EvenementIntervention_Ressource2]...';


GO
ALTER TABLE [dbo].[EvenementIntervention] DROP CONSTRAINT [FK_EvenementIntervention_Ressource2];


GO
PRINT N'Suppression de [dbo].[FK_InterventionConseq_Intervenants]...';


GO
ALTER TABLE [dbo].[EvenementIntervention] DROP CONSTRAINT [FK_InterventionConseq_Intervenants];


GO
PRINT N'Suppression de [dbo].[FK_InterventionConseq_InterventionEtat]...';


GO
ALTER TABLE [dbo].[EvenementIntervention] DROP CONSTRAINT [FK_InterventionConseq_InterventionEtat];


GO
PRINT N'Suppression de [dbo].[FK_EvenementRH_EvenementListe]...';


GO
ALTER TABLE [dbo].[EvenementRH] DROP CONSTRAINT [FK_EvenementRH_EvenementListe];


GO
PRINT N'Suppression de [dbo].[FK_EvenementRH_EvenementRHPrecision]...';


GO
ALTER TABLE [dbo].[EvenementRH] DROP CONSTRAINT [FK_EvenementRH_EvenementRHPrecision];


GO
PRINT N'Suppression de [dbo].[FK_CfgConfigurationRules_ConfigurationRules]...';


GO
ALTER TABLE [dbo].[CfgConfigurationRules] DROP CONSTRAINT [FK_CfgConfigurationRules_ConfigurationRules];


GO
PRINT N'Début de la régénération de la table [dbo].[CfgGeneralMainCourante]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_CfgGeneralMainCourante] (
    [id_configurationMainCourante_reseaus101m401]         INT      NOT NULL,
    [timeEndOfDay]                                        TIME (7) NOT NULL,
    [evenementChangeStateTime]                            TIME (7) NULL,
    [id_configurationMainCourante_evenementEtat]          INT      NULL,
    [id_configurationMainCourante]                        INT      IDENTITY (1, 1) NOT NULL,
    [id_ressourceClasseRH_Imports2006m1055]               INT      NULL,
    [id_ressourceEtatDefaut_Imports2006m1055]             INT      NULL,
    [id_ressourceEtatExploitation_Imports2006m1055]       INT      NULL,
    [id_ressourceEtatExploitationMatin_Imports2006m1055]  INT      NULL,
    [id_ressourceEtatExploitationSoiree_Imports2006m1055] INT      NULL,
    [id_ressourceEtatReserve_Imports2006m1055]            INT      NULL,
    [id_ressourceCategorieCR_Imports2006m1055]            INT      NULL,
    [id_ressourceCategorieAdmin_Imports2006m1055]         INT      NULL,
    [id_ressourceClasseClient_Imports2006m1055]           INT      NULL,
    [id_ressourceSousClasseClient_Imports2006m1055]       INT      NULL,
    [id_ressourceCategorieClient_Imports2006m1055]        INT      NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_ConfigurationMainCourante1] PRIMARY KEY CLUSTERED ([id_configurationMainCourante] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[CfgGeneralMainCourante])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_CfgGeneralMainCourante] ON;
        INSERT INTO [dbo].[tmp_ms_xx_CfgGeneralMainCourante] ([id_configurationMainCourante], [id_configurationMainCourante_reseaus101m401], [timeEndOfDay], [evenementChangeStateTime], [id_configurationMainCourante_evenementEtat], [id_ressourceClasseRH_Imports2006m1055], [id_ressourceEtatDefaut_Imports2006m1055], [id_ressourceEtatExploitation_Imports2006m1055], [id_ressourceEtatReserve_Imports2006m1055], [id_ressourceCategorieCR_Imports2006m1055], [id_ressourceCategorieAdmin_Imports2006m1055], [id_ressourceClasseClient_Imports2006m1055], [id_ressourceSousClasseClient_Imports2006m1055], [id_ressourceCategorieClient_Imports2006m1055])
        SELECT   [id_configurationMainCourante],
                 [id_configurationMainCourante_reseaus101m401],
                 [timeEndOfDay],
                 [evenementChangeStateTime],
                 [id_configurationMainCourante_evenementEtat],
                 [id_ressourceClasseRH_Imports2006m1055],
                 [id_ressourceEtatDefaut_Imports2006m1055],
                 [id_ressourceEtatExploitation_Imports2006m1055],
                 [id_ressourceEtatReserve_Imports2006m1055],
                 [id_ressourceCategorieCR_Imports2006m1055],
                 [id_ressourceCategorieAdmin_Imports2006m1055],
                 [id_ressourceClasseClient_Imports2006m1055],
                 [id_ressourceSousClasseClient_Imports2006m1055],
                 [id_ressourceCategorieClient_Imports2006m1055]
        FROM     [dbo].[CfgGeneralMainCourante]
        ORDER BY [id_configurationMainCourante] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_CfgGeneralMainCourante] OFF;
    END

DROP TABLE [dbo].[CfgGeneralMainCourante];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_CfgGeneralMainCourante]', N'CfgGeneralMainCourante';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_ConfigurationMainCourante1]', N'PK_ConfigurationMainCourante', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Début de la régénération de la table [dbo].[CfgPageAnalyse]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_CfgPageAnalyse] (
    [id_cfgPageAnalyse]             INT            IDENTITY (1, 1) NOT NULL,
    [id_cfgPageAnalyse_page]        INT            NOT NULL,
    [id_cfgPageAnalyse_analyse]     INT            NOT NULL,
    [withTableDisplay]              INT            CONSTRAINT [DF_CfgPageAnalyse_withTableDisplay] DEFAULT ((0)) NOT NULL,
    [graphPositionTopRow]           INT            CONSTRAINT [DF_CfgPageAnalyse_graphPositionTopRow] DEFAULT ((0)) NOT NULL,
    [graphPositionTopCol]           INT            CONSTRAINT [DF_CfgPageAnalyse_graphPositionTopCol] DEFAULT ((0)) NOT NULL,
    [graphPositionBotomRow]         INT            CONSTRAINT [DF_CfgPageAnalyse_graphPositionBotomRow] DEFAULT ((0)) NOT NULL,
    [graphPositionBotomCol]         INT            CONSTRAINT [DF_CfgPageAnalyse_graphPositionBotomCol] DEFAULT ((0)) NOT NULL,
    [withTitleAnalyse]              INT            CONSTRAINT [DF_CfgPageAnalyse_withTitleAnalyse] DEFAULT ((0)) NOT NULL,
    [TitleAnalyse]                  NVARCHAR (MAX) NOT NULL,
    [fontSizeLegendSerie]           INT            CONSTRAINT [DF_CfgPageAnalyse_fontSizeLegendSerie] DEFAULT ((10)) NOT NULL,
    [withYTitle]                    INT            CONSTRAINT [DF_CfgPageAnalyse_withYTitle] DEFAULT ((1)) NOT NULL,
    [fontSizeYTitle]                INT            CONSTRAINT [DF_CfgPageAnalyse_fontSizeyLabel] DEFAULT ((11)) NOT NULL,
    [withXTitle]                    INT            CONSTRAINT [DF_CfgPageAnalyse_withXTitle] DEFAULT ((1)) NOT NULL,
    [fontSizeXTitle]                INT            CONSTRAINT [DF_CfgPageAnalyse_fontSizeXTitle] DEFAULT ((11)) NOT NULL,
    [fontSizeXValue]                INT            CONSTRAINT [DF_CfgPageAnalyse_fontSizeXValue] DEFAULT ((9)) NOT NULL,
    [fontSizeYValue]                INT            CONSTRAINT [DF_CfgPageAnalyse_fontSizeYValue] DEFAULT ((9)) NOT NULL,
    [orientationXvalue]             INT            CONSTRAINT [DF_CfgPageAnalyse_orientationXvalue] DEFAULT ((0)) NOT NULL,
    [withDataLabel]                 INT            CONSTRAINT [DF_CfgPageAnalyse_withDataLabel] DEFAULT ((1)) NOT NULL,
    [fontSizeDataLabel]             INT            CONSTRAINT [DF_CfgPageAnalyse_fontSizeDataLabel] DEFAULT ((8)) NOT NULL,
    [colorDataLabel]                NVARCHAR (MAX) CONSTRAINT [DF_CfgPageAnalyse_colorDataLabel] DEFAULT ('') NOT NULL,
    [withColorDataLabelSameAsSerie] INT            CONSTRAINT [DF_CfgPageAnalyse_withColorDataLabelSameAsSerie] DEFAULT ((0)) NOT NULL,
    [withLegend]                    INT            CONSTRAINT [DF_CfgPageAnalyse_withLegend] DEFAULT ((0)) NOT NULL,
    [legendPositionTopRow]          INT            CONSTRAINT [DF_CfgPageAnalyse_legendPositionTopRow] DEFAULT ((0)) NOT NULL,
    [legendPositionTopCol]          INT            CONSTRAINT [DF_CfgPageAnalyse_legendPositionTopCol] DEFAULT ((0)) NOT NULL,
    [legendHeight]                  INT            CONSTRAINT [DF_CfgPageAnalyse_legendHeight] DEFAULT ((0)) NOT NULL,
    [legendWidth]                   INT            CONSTRAINT [DF_CfgPageAnalyse_legendWidth] DEFAULT ((0)) NOT NULL,
    [legend]                        NVARCHAR (MAX) CONSTRAINT [DF_CfgPageAnalyse_legend] DEFAULT ((0)) NOT NULL,
    [colorChart]                    NVARCHAR (50)  CONSTRAINT [DF_CfgPageAnalyse_colorChart] DEFAULT ((0)) NOT NULL,
    [pagePrintLandscape]            INT            CONSTRAINT [DF_CfgPageAnalyse_pagePrintLandscape] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_CfgPageAnalyse1] PRIMARY KEY CLUSTERED ([id_cfgPageAnalyse] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[CfgPageAnalyse])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_CfgPageAnalyse] ON;
        INSERT INTO [dbo].[tmp_ms_xx_CfgPageAnalyse] ([id_cfgPageAnalyse], [id_cfgPageAnalyse_page], [id_cfgPageAnalyse_analyse], [withTableDisplay], [withTitleAnalyse], [TitleAnalyse], [fontSizeLegendSerie], [fontSizeYTitle], [fontSizeXTitle], [fontSizeXValue], [fontSizeYValue], [withDataLabel], [fontSizeDataLabel], [withLegend], [legendHeight], [legendWidth], [legend], [colorChart])
        SELECT   [id_cfgPageAnalyse],
                 [id_cfgPageAnalyse_page],
                 [id_cfgPageAnalyse_analyse],
                 [withTableDisplay],
                 [withTitleAnalyse],
                 [TitleAnalyse],
                 [fontSizeLegendSerie],
                 [fontSizeYTitle],
                 [fontSizeXTitle],
                 [fontSizeXValue],
                 [fontSizeYValue],
                 [withDataLabel],
                 [fontSizeDataLabel],
                 [withLegend],
                 [legendHeight],
                 [legendWidth],
                 [legend],
                 [colorChart]
        FROM     [dbo].[CfgPageAnalyse]
        ORDER BY [id_cfgPageAnalyse] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_CfgPageAnalyse] OFF;
    END

DROP TABLE [dbo].[CfgPageAnalyse];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_CfgPageAnalyse]', N'CfgPageAnalyse';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_CfgPageAnalyse1]', N'PK_CfgPageAnalyse', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Modification de [dbo].[ConsignesExploitation]...';


GO
ALTER TABLE [dbo].[ConsignesExploitation]
    ADD [checkQuotidien] INT CONSTRAINT [DF_ConsignesExploitation_checkQuotidien] DEFAULT ((1)) NOT NULL;


GO
PRINT N'Modification de [dbo].[ConstEvenementExploitConseq]...';


GO
ALTER TABLE [dbo].[ConstEvenementExploitConseq]
    ADD [isCalculKmUniqueSV] INT CONSTRAINT [DF_ConstEvenementExploitConseq_isCalculKmUniqueSV] DEFAULT ((1)) NOT NULL,
        [isCalculKmMultiSV]  INT CONSTRAINT [DF_ConstEvenementExploitConseq_isCalculKmMultiSV] DEFAULT ((0)) NOT NULL;


GO
PRINT N'Modification de [dbo].[ConstRessourceTag]...';


GO
ALTER TABLE [dbo].[ConstRessourceTag]
    ADD [niveau] INT CONSTRAINT [DF_ConstRessourceTag_niveau] DEFAULT ((2)) NOT NULL;


GO
PRINT N'Modification de [dbo].[DBinfos]...';


GO
ALTER TABLE [dbo].[DBinfos] DROP COLUMN [valeurs];


GO
ALTER TABLE [dbo].[DBinfos] ALTER COLUMN [nom] NVARCHAR (MAX) NULL;


GO
ALTER TABLE [dbo].[DBinfos]
    ADD [valeur] NVARCHAR (MAX) NULL;


GO
PRINT N'Début de la régénération de la table [dbo].[EvenementActionRealise]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_EvenementActionRealise] (
    [id_evenementActionRealise]                 INT            IDENTITY (1, 1) NOT NULL,
    [id_evenementActionRealise_evenementAction] INT            NOT NULL,
    [id_utilisateur]                            INT            NOT NULL,
    [loginUtilisateur]                          NVARCHAR (MAX) NOT NULL,
    [date]                                      DATE           NOT NULL,
    [heure]                                     TIME (7)       NOT NULL,
    [date_realise]                              DATE           NULL,
    [heure_realise]                             TIME (7)       NULL,
    [observation]                               NVARCHAR (MAX) CONSTRAINT [DF_EvenementActionRealise_observation] DEFAULT ('') NOT NULL,
    [id_evenementActionRealise_ressource]       INT            NULL,
    [id_evenementActionRealise_evenementListe]  INT            NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_EvenementActionRealise1] PRIMARY KEY CLUSTERED ([id_evenementActionRealise] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[EvenementActionRealise])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementActionRealise] ON;
        INSERT INTO [dbo].[tmp_ms_xx_EvenementActionRealise] ([id_evenementActionRealise], [id_evenementActionRealise_evenementAction], [id_utilisateur], [loginUtilisateur], [date], [heure], [id_evenementActionRealise_evenementListe])
        SELECT   [id_evenementActionRealise],
                 [id_evenementActionRealise_evenementAction],
                 [id_utilisateur],
                 [loginUtilisateur],
                 [date],
                 [heure],
                 [id_evenementActionRealise_evenementListe]
        FROM     [dbo].[EvenementActionRealise]
        ORDER BY [id_evenementActionRealise] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementActionRealise] OFF;
    END

DROP TABLE [dbo].[EvenementActionRealise];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_EvenementActionRealise]', N'EvenementActionRealise';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_EvenementActionRealise1]', N'PK_EvenementActionRealise', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Début de la régénération de la table [dbo].[EvenementExploitationConseq]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_EvenementExploitationConseq] (
    [id_evenementExploitationConseq]                             INT            IDENTITY (1, 1) NOT NULL,
    [id_evenementExploitationConseq_exploitationConseqManoeuvre] INT            NOT NULL,
    [id_evenementExploitationConseq_evenementListe]              INT            NOT NULL,
    [id_evenementExploitationConseq_lignes2006m401]              INT            NULL,
    [id_evenementExploitationConseq_parcourss2006m401]           INT            NULL,
    [id_evenementExploitationConseq_directions2006m401]          INT            NULL,
    [id_eevenementExploitationConseq_arrets2006m401_1]           INT            NULL,
    [id_eevenementExploitationConseq_arrets2006m401_2]           INT            NULL,
    [heure_1]                                                    TIME (7)       NULL,
    [heure_2]                                                    TIME (7)       NULL,
    [nb_departs]                                                 INT            NULL,
    [nb_departs_manque]                                          INT            NULL,
    [nb_arrive_manque]                                           INT            NULL,
    [nb_courses]                                                 NVARCHAR (50)  NULL,
    [nb_courses_auto]                                            FLOAT (53)     NULL,
    [nb_courses_entiere_manque]                                  INT            NULL,
    [nb_courses_impact]                                          INT            NULL,
    [nb_km]                                                      FLOAT (53)     NULL,
    [nb_km_auto]                                                 FLOAT (53)     NULL,
    [id_evenementExploitationConseq_svs2006m401_auto]            INT            NULL,
    [sv_proportion]                                              INT            CONSTRAINT [DF_EvenementExploitationConseq_sv_proportion] DEFAULT ((0)) NOT NULL,
    [is_sv_nonAssure]                                            INT            CONSTRAINT [DF_EvenementExploitationConseq_isSVnonAssure] DEFAULT ((0)) NOT NULL,
    [décalage_min]                                               FLOAT (53)     NULL,
    [décalage_max]                                               FLOAT (53)     NULL,
    [id_evenementExploitationConseq_parent]                      INT            NULL,
    [id_eevenementExploitationConseq_arretsDepart2006m401_1]     INT            NULL,
    [id_eevenementExploitationConseq_arretsDepart2006m401_2]     INT            NULL,
    [withRetombe]                                                INT            NULL,
    [id_eevenementExploitationConseq_ressourceHomme]             INT            NULL,
    [id_eevenementExploitationConseq_ressourceVehicule]          INT            NULL,
    [observation]                                                NVARCHAR (MAX) NULL,
    [ligne_code]                                                 NVARCHAR (MAX) NULL,
    [ligne_nom]                                                  NVARCHAR (MAX) NULL,
    [parcours_code]                                              NVARCHAR (MAX) NULL,
    [parcours_nom]                                               NVARCHAR (MAX) NULL,
    [parcours_description]                                       NVARCHAR (MAX) NULL,
    [id_utilisateur]                                             INT            NULL,
    [loginUtilisateur]                                           NVARCHAR (MAX) NULL,
    [kmAutoHasError]                                             INT            CONSTRAINT [DF_EvenementExploitationConseq_kmAutoHasError] DEFAULT ((0)) NOT NULL,
    [kmAutoErrorMessage]                                         NVARCHAR (MAX) CONSTRAINT [DF_EvenementExploitationConseq_kmAutoErrorMessage] DEFAULT ('') NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK__Exploita__0C4C4FF61412A0791] PRIMARY KEY CLUSTERED ([id_evenementExploitationConseq] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[EvenementExploitationConseq])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementExploitationConseq] ON;
        INSERT INTO [dbo].[tmp_ms_xx_EvenementExploitationConseq] ([id_evenementExploitationConseq], [id_evenementExploitationConseq_exploitationConseqManoeuvre], [id_evenementExploitationConseq_evenementListe], [id_evenementExploitationConseq_lignes2006m401], [id_evenementExploitationConseq_parcourss2006m401], [id_evenementExploitationConseq_directions2006m401], [id_eevenementExploitationConseq_arrets2006m401_1], [id_eevenementExploitationConseq_arrets2006m401_2], [heure_1], [heure_2], [nb_departs], [nb_courses], [nb_courses_auto], [nb_km], [nb_km_auto], [id_evenementExploitationConseq_svs2006m401_auto], [sv_proportion], [is_sv_nonAssure], [décalage_min], [décalage_max], [id_evenementExploitationConseq_parent], [id_eevenementExploitationConseq_arretsDepart2006m401_1], [id_eevenementExploitationConseq_arretsDepart2006m401_2], [withRetombe], [id_eevenementExploitationConseq_ressourceHomme], [id_eevenementExploitationConseq_ressourceVehicule], [observation], [ligne_code], [ligne_nom], [parcours_code], [parcours_nom], [parcours_description], [id_utilisateur], [loginUtilisateur])
        SELECT   [id_evenementExploitationConseq],
                 [id_evenementExploitationConseq_exploitationConseqManoeuvre],
                 [id_evenementExploitationConseq_evenementListe],
                 [id_evenementExploitationConseq_lignes2006m401],
                 [id_evenementExploitationConseq_parcourss2006m401],
                 [id_evenementExploitationConseq_directions2006m401],
                 [id_eevenementExploitationConseq_arrets2006m401_1],
                 [id_eevenementExploitationConseq_arrets2006m401_2],
                 [heure_1],
                 [heure_2],
                 [nb_departs],
                 [nb_courses],
                 [nb_courses_auto],
                 [nb_km],
                 [nb_km_auto],
                 [id_evenementExploitationConseq_svs2006m401_auto],
                 [sv_proportion],
                 [is_sv_nonAssure],
                 [décalage_min],
                 [décalage_max],
                 [id_evenementExploitationConseq_parent],
                 [id_eevenementExploitationConseq_arretsDepart2006m401_1],
                 [id_eevenementExploitationConseq_arretsDepart2006m401_2],
                 [withRetombe],
                 [id_eevenementExploitationConseq_ressourceHomme],
                 [id_eevenementExploitationConseq_ressourceVehicule],
                 [observation],
                 [ligne_code],
                 [ligne_nom],
                 [parcours_code],
                 [parcours_nom],
                 [parcours_description],
                 [id_utilisateur],
                 [loginUtilisateur]
        FROM     [dbo].[EvenementExploitationConseq]
        ORDER BY [id_evenementExploitationConseq] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementExploitationConseq] OFF;
    END

DROP TABLE [dbo].[EvenementExploitationConseq];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_EvenementExploitationConseq]', N'EvenementExploitationConseq';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK__Exploita__0C4C4FF61412A0791]', N'PK__Exploita__0C4C4FF61412A079', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Début de la régénération de la table [dbo].[EvenementIntervention]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_EvenementIntervention] (
    [id_interventionConseq]                  INT            IDENTITY (1, 1) NOT NULL,
    [id_interventionConseq_intervenant]      INT            NOT NULL,
    [id_evenementIntervention_ressource1]    INT            NULL,
    [id_evenementIntervention_ressource2]    INT            NULL,
    [id_interventionConseq_interventionEtat] INT            NULL,
    [heure_debut]                            TIME (7)       NULL,
    [heure_fin]                              TIME (7)       NULL,
    [observations]                           NVARCHAR (MAX) CONSTRAINT [DF_EvenementIntervention_observations] DEFAULT ('') NOT NULL,
    [id_evenementIntervention_evenementList] INT            NOT NULL,
    [id_utilisateur]                         INT            NULL,
    [loginUtilisateur]                       NVARCHAR (MAX) NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PrimaryKey_cdc2149d-2f89-403c-ad87-ab8d58b253cf1] PRIMARY KEY CLUSTERED ([id_interventionConseq] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[EvenementIntervention])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementIntervention] ON;
        INSERT INTO [dbo].[tmp_ms_xx_EvenementIntervention] ([id_interventionConseq], [id_interventionConseq_intervenant], [id_evenementIntervention_ressource1], [id_evenementIntervention_ressource2], [id_interventionConseq_interventionEtat], [heure_debut], [heure_fin], [id_evenementIntervention_evenementList], [id_utilisateur], [loginUtilisateur])
        SELECT   [id_interventionConseq],
                 [id_interventionConseq_intervenant],
                 [id_evenementIntervention_ressource1],
                 [id_evenementIntervention_ressource2],
                 [id_interventionConseq_interventionEtat],
                 [heure_debut],
                 [heure_fin],
                 [id_evenementIntervention_evenementList],
                 [id_utilisateur],
                 [loginUtilisateur]
        FROM     [dbo].[EvenementIntervention]
        ORDER BY [id_interventionConseq] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementIntervention] OFF;
    END

DROP TABLE [dbo].[EvenementIntervention];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_EvenementIntervention]', N'EvenementIntervention';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PrimaryKey_cdc2149d-2f89-403c-ad87-ab8d58b253cf1]', N'PrimaryKey_cdc2149d-2f89-403c-ad87-ab8d58b253cf', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Modification de [dbo].[EvenementLieu]...';


GO
ALTER TABLE [dbo].[EvenementLieu]
    ADD [autre_lieu] NVARCHAR (MAX) CONSTRAINT [DF_EvenementLieu_autre_lieu] DEFAULT ('') NOT NULL;


GO
PRINT N'Modification de [dbo].[EvenementMaintenance]...';


GO
ALTER TABLE [dbo].[EvenementMaintenance]
    ADD [id_evenementMaintenance_evenementMaintenanceOrganeGMAO]      INT NULL,
        [id_evenementMaintenance_evenementMaintenanceCodeSignalement] INT NULL;


GO
PRINT N'Début de la régénération de la table [dbo].[EvenementRH]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_EvenementRH] (
    [id_evenementRH]                      INT            IDENTITY (1, 1) NOT NULL,
    [id_evenementRH_aleaListe]            INT            NOT NULL,
    [id_evenementRH_evenementRHPrecision] INT            NOT NULL,
    [id_evenementRH_ressource]            INT            NULL,
    [heure_debut]                         TIME (7)       NULL,
    [heure_fin]                           TIME (7)       NULL,
    [nombreHeureSupp]                     FLOAT (53)     NULL,
    [id_evenementRH_evenementAffectation] INT            NULL,
    [id_utilisateur]                      INT            NOT NULL,
    [loginUtilisateur]                    NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_EvenementRH1] PRIMARY KEY CLUSTERED ([id_evenementRH] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[EvenementRH])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementRH] ON;
        INSERT INTO [dbo].[tmp_ms_xx_EvenementRH] ([id_evenementRH], [id_evenementRH_aleaListe], [id_evenementRH_evenementRHPrecision], [id_evenementRH_ressource], [nombreHeureSupp], [id_evenementRH_evenementAffectation], [id_utilisateur], [loginUtilisateur])
        SELECT   [id_evenementRH],
                 [id_evenementRH_aleaListe],
                 [id_evenementRH_evenementRHPrecision],
                 [id_evenementRH_ressource],
                 [nombreHeureSupp],
                 [id_evenementRH_evenementAffectation],
                 [id_utilisateur],
                 [loginUtilisateur]
        FROM     [dbo].[EvenementRH]
        ORDER BY [id_evenementRH] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementRH] OFF;
    END

DROP TABLE [dbo].[EvenementRH];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_EvenementRH]', N'EvenementRH';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_EvenementRH1]', N'PK_EvenementRH', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Modification de [dbo].[QueryTriggerAsynchrone]...';


GO
ALTER TABLE [dbo].[QueryTriggerAsynchrone]
    ADD [message]           NVARCHAR (MAX) CONSTRAINT [DF_QueryTriggerAsynchrone_message] DEFAULT ('') NOT NULL,
        [withAnonymisation] INT            CONSTRAINT [DF_QueryTriggerAsynchrone_withAnonymisation] DEFAULT ((0)) NOT NULL,
        [withActionSuivi]   INT            CONSTRAINT [DF_QueryTriggerAsynchrone_withActionSuivi] DEFAULT ((0)) NOT NULL;


GO
PRINT N'Modification de [dbo].[QueryTriggerSynchrone]...';


GO
ALTER TABLE [dbo].[QueryTriggerSynchrone]
    ADD [message]           NVARCHAR (MAX) CONSTRAINT [DF_QueryTriggerSynchrone_message] DEFAULT ('') NOT NULL,
        [withAnonymisation] INT            CONSTRAINT [DF_QueryTriggerSynchrone_withAnonymisation] DEFAULT ((0)) NOT NULL;


GO
PRINT N'Création de [dbo].[CfgEvenementMaintenanceCodeSignalementLink]...';


GO
CREATE TABLE [dbo].[CfgEvenementMaintenanceCodeSignalementLink] (
    [id_cfgEvenementMaintenanceCodeSignalementLink]                 INT IDENTITY (1, 1) NOT NULL,
    [id_cfgEvenementMaintenanceCodeSignalementLink_codeSignalement] INT NOT NULL,
    [id_cfgEvenementMaintenanceCodeSignalementLink_organe]          INT NOT NULL,
    [id_cfgEvenementMaintenanceCodeSignalementLink_emplacement]     INT NULL,
    [id_cfgEvenementMaintenanceCodeSignalementLink_symptome]        INT NULL,
    [id_reseau]                                                     INT NOT NULL,
    CONSTRAINT [PK_CfgEvenementMaintenanceCodeSignalementLink] PRIMARY KEY CLUSTERED ([id_cfgEvenementMaintenanceCodeSignalementLink] ASC)
);


GO
PRINT N'Création de [dbo].[CfgEvenementMaintenanceOrganeGMAOLink]...';


GO
CREATE TABLE [dbo].[CfgEvenementMaintenanceOrganeGMAOLink] (
    [id_cfgLinkEvenementMaintenanceOrganeGMAO]            INT IDENTITY (1, 1) NOT NULL,
    [id_cfgLinkEvenementMaintenanceOrganeGMAO_organe]     INT NOT NULL,
    [id_cfgLinkEvenementMaintenanceOrganeGMAO_organeGMAO] INT NOT NULL,
    [id_reseau]                                           INT NOT NULL,
    CONSTRAINT [PK_CfgLinkEvenementMaintenanceOrganeGMAO] PRIMARY KEY CLUSTERED ([id_cfgLinkEvenementMaintenanceOrganeGMAO] ASC)
);


GO
PRINT N'Création de [dbo].[CfgFichierFolderTypeRule]...';


GO
CREATE TABLE [dbo].[CfgFichierFolderTypeRule] (
    [id_cfgFichierFolderTypeRule]                   INT IDENTITY (1, 1) NOT NULL,
    [id_cfgFichierFolderTypeRule_fichierFolderType] INT NOT NULL,
    [id_cfgFichierFolderTypeRule_rule]              INT NOT NULL,
    CONSTRAINT [PK_CfgFichierFolderTypeRule] PRIMARY KEY CLUSTERED ([id_cfgFichierFolderTypeRule] ASC)
);


GO
PRINT N'Création de [dbo].[ConstFichierTypeFolderMode]...';


GO
CREATE TABLE [dbo].[ConstFichierTypeFolderMode] (
    [id_constFichierTypeFolderMode] INT            IDENTITY (1, 1) NOT NULL,
    [code]                          NVARCHAR (MAX) NOT NULL,
    [libelle]                       NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_ConstFichierTypeFolderMode] PRIMARY KEY CLUSTERED ([id_constFichierTypeFolderMode] ASC)
);


GO
PRINT N'Création de [dbo].[EvenementMaintenanceCodeSignalement]...';


GO
CREATE TABLE [dbo].[EvenementMaintenanceCodeSignalement] (
    [id_evenementMaintenanceCodeSignalement] INT            IDENTITY (1, 1) NOT NULL,
    [code]                                   NVARCHAR (MAX) NOT NULL,
    [description]                            NVARCHAR (MAX) NOT NULL,
    [id_reseau]                              INT            NOT NULL,
    CONSTRAINT [PK_EvenementMaintenanceCodeSignalement] PRIMARY KEY CLUSTERED ([id_evenementMaintenanceCodeSignalement] ASC)
);


GO
PRINT N'Création de [dbo].[EvenementMaintenanceOrganeGMAO]...';


GO
CREATE TABLE [dbo].[EvenementMaintenanceOrganeGMAO] (
    [id_evenementMaintenanceOrganeGMAO] INT            IDENTITY (1, 1) NOT NULL,
    [code]                              NVARCHAR (MAX) NOT NULL,
    [nom]                               NVARCHAR (MAX) NOT NULL,
    [niveau]                            INT            NOT NULL,
    [id_reseau]                         INT            NOT NULL,
    CONSTRAINT [PK_EvenementMaintenanceOrganeGMAO] PRIMARY KEY CLUSTERED ([id_evenementMaintenanceOrganeGMAO] ASC)
);


GO
PRINT N'Création de [dbo].[EvenementRessourceTagApplied]...';


GO
CREATE TABLE [dbo].[EvenementRessourceTagApplied] (
    [id_evenementRessourceTagApplied]              INT           IDENTITY (1, 1) NOT NULL,
    [id_evenementRessourceTagApplied_ressourceTag] INT           NOT NULL,
    [id_evenementRessourceTagApplied_evenement]    INT           NOT NULL,
    [value]                                        VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_EvenementRessourceTagApplied] PRIMARY KEY CLUSTERED ([id_evenementRessourceTagApplied] ASC)
);


GO
PRINT N'Création de [dbo].[FichierTypeFolder]...';


GO
CREATE TABLE [dbo].[FichierTypeFolder] (
    [id_fichierTypeFolder]                       INT            IDENTITY (1, 1) NOT NULL,
    [nom]                                        NVARCHAR (MAX) NOT NULL,
    [path]                                       NVARCHAR (MAX) NOT NULL,
    [isGlobal]                                   INT            NOT NULL,
    [id_reseaus101m401]                          INT            NOT NULL,
    [id_fichierTypeFolder_fichierTypeFolderMode] INT            NOT NULL,
    CONSTRAINT [PK_FichierTypeFolder] PRIMARY KEY CLUSTERED ([id_fichierTypeFolder] ASC)
);


GO
PRINT N'Création de [dbo].[LnkEvenementRessourceResponsableHierarchique]...';


GO
CREATE TABLE [dbo].[LnkEvenementRessourceResponsableHierarchique] (
    [id_lnkEvenementRessourceResponsableHierarchique]           INT IDENTITY (1, 1) NOT NULL,
    [id_lnkEvenementRessourceResponsableHierarchique_evenement] INT NOT NULL,
    [id_lnkEvenementRessourceResponsableHierarchique_ressource] INT NOT NULL,
    CONSTRAINT [PK_LnkEvenementRessourceResponsableHierarchique] PRIMARY KEY CLUSTERED ([id_lnkEvenementRessourceResponsableHierarchique] ASC)
);


GO
PRINT N'Création de [dbo].[FK_CfgConfigurationRules_CfgGeneral]...';


GO
ALTER TABLE [dbo].[CfgConfigurationRules] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgConfigurationRules_CfgGeneral] FOREIGN KEY ([id_cfgConfigurationRules_CfgGeneralMainCourante]) REFERENCES [dbo].[CfgGeneralMainCourante] ([id_configurationMainCourante]);


GO
PRINT N'Création de [dbo].[FK_CfgGeneralMainCourante_Categorie1]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgGeneralMainCourante_Categorie1] FOREIGN KEY ([id_ressourceCategorieAdmin_Imports2006m1055]) REFERENCES [dbo].[MaterielDetailCategorie] ([id_materielDetailCategorie]);


GO
PRINT N'Création de [dbo].[FK_CfgGeneralMainCourante_Categorie2]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgGeneralMainCourante_Categorie2] FOREIGN KEY ([id_ressourceCategorieCR_Imports2006m1055]) REFERENCES [dbo].[MaterielDetailCategorie] ([id_materielDetailCategorie]);


GO
PRINT N'Création de [dbo].[FK_CfgGeneralMainCourante_CategorieClient]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgGeneralMainCourante_CategorieClient] FOREIGN KEY ([id_ressourceCategorieClient_Imports2006m1055]) REFERENCES [dbo].[MaterielDetailCategorie] ([id_materielDetailCategorie]);


GO
PRINT N'Création de [dbo].[FK_CfgGeneralMainCourante_ClientClasse]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgGeneralMainCourante_ClientClasse] FOREIGN KEY ([id_ressourceClasseClient_Imports2006m1055]) REFERENCES [dbo].[MaterielElement] ([id_materielElement]);


GO
PRINT N'Création de [dbo].[FK_CfgGeneralMainCourante_Etat1]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgGeneralMainCourante_Etat1] FOREIGN KEY ([id_ressourceEtatDefaut_Imports2006m1055]) REFERENCES [dbo].[MaterielEtat] ([id_materielEtat]);


GO
PRINT N'Création de [dbo].[FK_CfgGeneralMainCourante_Etat2]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgGeneralMainCourante_Etat2] FOREIGN KEY ([id_ressourceEtatExploitation_Imports2006m1055]) REFERENCES [dbo].[MaterielEtat] ([id_materielEtat]);


GO
PRINT N'Création de [dbo].[FK_CfgGeneralMainCourante_Etat3]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgGeneralMainCourante_Etat3] FOREIGN KEY ([id_ressourceEtatReserve_Imports2006m1055]) REFERENCES [dbo].[MaterielEtat] ([id_materielEtat]);


GO
PRINT N'Création de [dbo].[FK_CfgGeneralMainCourante_RessourceClasse]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgGeneralMainCourante_RessourceClasse] FOREIGN KEY ([id_ressourceClasseRH_Imports2006m1055]) REFERENCES [dbo].[MaterielElement] ([id_materielElement]);


GO
PRINT N'Création de [dbo].[FK_CfgGeneralMainCourante_SousClasseClient]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgGeneralMainCourante_SousClasseClient] FOREIGN KEY ([id_ressourceSousClasseClient_Imports2006m1055]) REFERENCES [dbo].[MaterielType] ([id_materielType]);


GO
PRINT N'Création de [dbo].[FK_ConfigurationMainCourante_AleaState]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_ConfigurationMainCourante_AleaState] FOREIGN KEY ([id_configurationMainCourante_evenementEtat]) REFERENCES [dbo].[EvenementEtat] ([id_evenementEtat]);


GO
PRINT N'Création de [dbo].[FK_CfgPageAnalyse_Analyse]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgPageAnalyse_Analyse] FOREIGN KEY ([id_cfgPageAnalyse_analyse]) REFERENCES [dbo].[Analyse] ([id_analyse]);


GO
PRINT N'Création de [dbo].[FK_CfgPageAnalyse_Page]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgPageAnalyse_Page] FOREIGN KEY ([id_cfgPageAnalyse_page]) REFERENCES [dbo].[Page] ([id_page]);


GO
PRINT N'Création de [dbo].[FK_EvenementActionRealise_EvenementAction]...';


GO
ALTER TABLE [dbo].[EvenementActionRealise] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementActionRealise_EvenementAction] FOREIGN KEY ([id_evenementActionRealise_evenementAction]) REFERENCES [dbo].[EvenementAction] ([id_evenementAction]);


GO
PRINT N'Création de [dbo].[FK_EvenementActionRealise_EvenementListe]...';


GO
ALTER TABLE [dbo].[EvenementActionRealise] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementActionRealise_EvenementListe] FOREIGN KEY ([id_evenementActionRealise_evenementListe]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
PRINT N'Création de [dbo].[FK_EvenementActionRealise_Ressource]...';


GO
ALTER TABLE [dbo].[EvenementActionRealise] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementActionRealise_Ressource] FOREIGN KEY ([id_evenementActionRealise_ressource]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
PRINT N'Création de [dbo].[FK_EvenementExploitationConseq_RessourceVehicule]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementExploitationConseq_RessourceVehicule] FOREIGN KEY ([id_eevenementExploitationConseq_ressourceVehicule]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
PRINT N'Création de [dbo].[FK_EvenementExploitationConseq_EvenementExploitationConseq]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementExploitationConseq_EvenementExploitationConseq] FOREIGN KEY ([id_evenementExploitationConseq_parent]) REFERENCES [dbo].[EvenementExploitationConseq] ([id_evenementExploitationConseq]);


GO
PRINT N'Création de [dbo].[FK_EvenementExploitationConseq_EvenementList]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementExploitationConseq_EvenementList] FOREIGN KEY ([id_evenementExploitationConseq_evenementListe]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
PRINT N'Création de [dbo].[FK_EvenementExploitationConseq_ExploitationConseqManoeuvre]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementExploitationConseq_ExploitationConseqManoeuvre] FOREIGN KEY ([id_evenementExploitationConseq_exploitationConseqManoeuvre]) REFERENCES [dbo].[EvenementExploitationConseqManoeuvre] ([id_exploitationConseqManoeuvre]);


GO
PRINT N'Création de [dbo].[FK_EvenementExploitationConseq_RessourceHomme]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementExploitationConseq_RessourceHomme] FOREIGN KEY ([id_eevenementExploitationConseq_ressourceHomme]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
PRINT N'Création de [dbo].[FK_EvenementIntervention_EvenementList]...';


GO
ALTER TABLE [dbo].[EvenementIntervention] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementIntervention_EvenementList] FOREIGN KEY ([id_evenementIntervention_evenementList]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
PRINT N'Création de [dbo].[FK_EvenementIntervention_Ressource1]...';


GO
ALTER TABLE [dbo].[EvenementIntervention] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementIntervention_Ressource1] FOREIGN KEY ([id_evenementIntervention_ressource1]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
PRINT N'Création de [dbo].[FK_EvenementIntervention_Ressource2]...';


GO
ALTER TABLE [dbo].[EvenementIntervention] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementIntervention_Ressource2] FOREIGN KEY ([id_evenementIntervention_ressource2]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
PRINT N'Création de [dbo].[FK_InterventionConseq_Intervenants]...';


GO
ALTER TABLE [dbo].[EvenementIntervention] WITH NOCHECK
    ADD CONSTRAINT [FK_InterventionConseq_Intervenants] FOREIGN KEY ([id_interventionConseq_intervenant]) REFERENCES [dbo].[EvenementIntervenant] ([id_intervenant]);


GO
PRINT N'Création de [dbo].[FK_InterventionConseq_InterventionEtat]...';


GO
ALTER TABLE [dbo].[EvenementIntervention] WITH NOCHECK
    ADD CONSTRAINT [FK_InterventionConseq_InterventionEtat] FOREIGN KEY ([id_interventionConseq_interventionEtat]) REFERENCES [dbo].[EvenementInterventionActivite] ([id_interventionEtat]);


GO
PRINT N'Création de [dbo].[FK_CfgEvenementMaintenanceCodeSignalementLink_codeSignalement]...';


GO
ALTER TABLE [dbo].[CfgEvenementMaintenanceCodeSignalementLink] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementMaintenanceCodeSignalementLink_codeSignalement] FOREIGN KEY ([id_cfgEvenementMaintenanceCodeSignalementLink_codeSignalement]) REFERENCES [dbo].[EvenementMaintenanceCodeSignalement] ([id_evenementMaintenanceCodeSignalement]);


GO
PRINT N'Création de [dbo].[FK_CfgEvenementMaintenanceCodeSignalementLink_emplacement]...';


GO
ALTER TABLE [dbo].[CfgEvenementMaintenanceCodeSignalementLink] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementMaintenanceCodeSignalementLink_emplacement] FOREIGN KEY ([id_cfgEvenementMaintenanceCodeSignalementLink_emplacement]) REFERENCES [dbo].[EvenementMaintenanceEmplacement] ([id_evenementMaintenanceEmplacement]);


GO
PRINT N'Création de [dbo].[FK_CfgEvenementMaintenanceCodeSignalementLink_organe]...';


GO
ALTER TABLE [dbo].[CfgEvenementMaintenanceCodeSignalementLink] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementMaintenanceCodeSignalementLink_organe] FOREIGN KEY ([id_cfgEvenementMaintenanceCodeSignalementLink_organe]) REFERENCES [dbo].[EvenementMaintenanceOrgane] ([id_evenementMaintenanceOrgane]);


GO
PRINT N'Création de [dbo].[FK_CfgEvenementMaintenanceCodeSignalementLink_Symptome]...';


GO
ALTER TABLE [dbo].[CfgEvenementMaintenanceCodeSignalementLink] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementMaintenanceCodeSignalementLink_Symptome] FOREIGN KEY ([id_cfgEvenementMaintenanceCodeSignalementLink_symptome]) REFERENCES [dbo].[EvenementMaintenanceSymptome] ([id_evenementMaintenanceSymptome]);


GO
PRINT N'Création de [dbo].[FK_CfgEvenementMaintenanceOrganeGMAOLink_organe]...';


GO
ALTER TABLE [dbo].[CfgEvenementMaintenanceOrganeGMAOLink] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementMaintenanceOrganeGMAOLink_organe] FOREIGN KEY ([id_cfgLinkEvenementMaintenanceOrganeGMAO_organe]) REFERENCES [dbo].[EvenementMaintenanceOrgane] ([id_evenementMaintenanceOrgane]);


GO
PRINT N'Création de [dbo].[FK_CfgEvenementMaintenanceOrganeGMAOLink_organeGMAO]...';


GO
ALTER TABLE [dbo].[CfgEvenementMaintenanceOrganeGMAOLink] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementMaintenanceOrganeGMAOLink_organeGMAO] FOREIGN KEY ([id_cfgLinkEvenementMaintenanceOrganeGMAO_organeGMAO]) REFERENCES [dbo].[EvenementMaintenanceOrganeGMAO] ([id_evenementMaintenanceOrganeGMAO]);


GO
PRINT N'Création de [dbo].[FK_CfgFichierFolderTypeRule_FichierFolderType]...';


GO
ALTER TABLE [dbo].[CfgFichierFolderTypeRule] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgFichierFolderTypeRule_FichierFolderType] FOREIGN KEY ([id_cfgFichierFolderTypeRule_fichierFolderType]) REFERENCES [dbo].[FichierTypeFolder] ([id_fichierTypeFolder]);


GO
PRINT N'Création de [dbo].[FK_CfgFichierFolderTypeRule_Rule]...';


GO
ALTER TABLE [dbo].[CfgFichierFolderTypeRule] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgFichierFolderTypeRule_Rule] FOREIGN KEY ([id_cfgFichierFolderTypeRule_rule]) REFERENCES [dbo].[Rules] ([id_rules]);


GO
PRINT N'Création de [dbo].[FK_EvenementRessourceTagApplied_Evenement]...';


GO
ALTER TABLE [dbo].[EvenementRessourceTagApplied] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementRessourceTagApplied_Evenement] FOREIGN KEY ([id_evenementRessourceTagApplied_evenement]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
PRINT N'Création de [dbo].[FK_EvenementRessourceTagApplied_RessourceTag]...';


GO
ALTER TABLE [dbo].[EvenementRessourceTagApplied] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementRessourceTagApplied_RessourceTag] FOREIGN KEY ([id_evenementRessourceTagApplied_ressourceTag]) REFERENCES [dbo].[ConstRessourceTag] ([id_constRessourceTag]);


GO
PRINT N'Création de [dbo].[FK_FichierTypeFolder_FichierTypeFolderMode]...';


GO
ALTER TABLE [dbo].[FichierTypeFolder] WITH NOCHECK
    ADD CONSTRAINT [FK_FichierTypeFolder_FichierTypeFolderMode] FOREIGN KEY ([id_fichierTypeFolder_fichierTypeFolderMode]) REFERENCES [dbo].[ConstFichierTypeFolderMode] ([id_constFichierTypeFolderMode]);


GO
PRINT N'Création de [dbo].[FK_LnkEvenementRessourceResponsableHierarchique_Evenement]...';


GO
ALTER TABLE [dbo].[LnkEvenementRessourceResponsableHierarchique] WITH NOCHECK
    ADD CONSTRAINT [FK_LnkEvenementRessourceResponsableHierarchique_Evenement] FOREIGN KEY ([id_lnkEvenementRessourceResponsableHierarchique_evenement]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
PRINT N'Création de [dbo].[FK_LnkEvenementRessourceResponsableHierarchique_Ressource]...';


GO
ALTER TABLE [dbo].[LnkEvenementRessourceResponsableHierarchique] WITH NOCHECK
    ADD CONSTRAINT [FK_LnkEvenementRessourceResponsableHierarchique_Ressource] FOREIGN KEY ([id_lnkEvenementRessourceResponsableHierarchique_ressource]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
PRINT N'Création de [dbo].[FK_CfgConfigurationRules_ConfigurationRules2]...';


GO
ALTER TABLE [dbo].[CfgConfigurationRules] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgConfigurationRules_ConfigurationRules2] FOREIGN KEY ([id_cfgConfigurationRules_ConfigurationRules]) REFERENCES [dbo].[ConstConfigurationRules] ([id_configurationRules]);


GO
PRINT N'Création de [dbo].[FK_EvenementMaintenance_CodeSignalement]...';


GO
ALTER TABLE [dbo].[EvenementMaintenance] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementMaintenance_CodeSignalement] FOREIGN KEY ([id_evenementMaintenance_evenementMaintenanceCodeSignalement]) REFERENCES [dbo].[EvenementMaintenanceCodeSignalement] ([id_evenementMaintenanceCodeSignalement]);


GO
PRINT N'Création de [dbo].[FK_EvenementMaintenance_OrganeGMAO]...';


GO
ALTER TABLE [dbo].[EvenementMaintenance] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementMaintenance_OrganeGMAO] FOREIGN KEY ([id_evenementMaintenance_evenementMaintenanceOrganeGMAO]) REFERENCES [dbo].[EvenementMaintenanceOrganeGMAO] ([id_evenementMaintenanceOrganeGMAO]);


GO
PRINT N'Modification de [dbo].[VueMaterielListeCountAleaEtat]...';


GO
ALTER VIEW dbo.VueMaterielListeCountAleaEtat
AS
SELECT        dbo.MaterielListe.id_materielListe, dbo.MaterielListe.id_reseau, dbo.AleaListe.dateApparition, dbo.AleaListe.indentifiant, dbo.EvenementEtat.id_evenementEtat, dbo.EvenementEtat.nom AS Expr1, 
                         dbo.AleaListe.heureApparition
FROM            dbo.MaterielListe INNER JOIN
                         dbo.LnkMaterielListeAleaListe ON dbo.MaterielListe.id_materielListe = dbo.LnkMaterielListeAleaListe.id_materielListeAleaListe_materielList INNER JOIN
                         dbo.AleaListe ON dbo.LnkMaterielListeAleaListe.id_materielListeAleaListe_aleaListe = dbo.AleaListe.id_aleaListe INNER JOIN
                         dbo.EvenementEtat ON dbo.AleaListe.id_aleaListe_aleaEtat = dbo.EvenementEtat.id_evenementEtat
GO
PRINT N'Modification de [dbo].[VueMaterielListeCountAleaEtat].[MS_DiagramPane1]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[16] 2[26] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -192
         Left = 0
      End
      Begin Tables = 
         Begin Table = "MaterielListe"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 328
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LnkMaterielListeAleaListe"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 251
               Right = 322
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AleaListe"
            Begin Extent = 
               Top = 338
               Left = 143
               Bottom = 468
               Right = 427
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "EvenementEtat"
            Begin Extent = 
               Top = 259
               Left = 811
               Bottom = 372
               Right = 1107
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1500
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueMaterielListeCountAleaEtat';


GO
PRINT N'Vérification de données existantes par rapport aux nouvelles contraintes';


GO
USE [$(DatabaseName)];


GO
ALTER TABLE [dbo].[CfgConfigurationRules] WITH CHECK CHECK CONSTRAINT [FK_CfgConfigurationRules_CfgGeneral];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH CHECK CHECK CONSTRAINT [FK_CfgGeneralMainCourante_Categorie1];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH CHECK CHECK CONSTRAINT [FK_CfgGeneralMainCourante_Categorie2];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH CHECK CHECK CONSTRAINT [FK_CfgGeneralMainCourante_CategorieClient];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH CHECK CHECK CONSTRAINT [FK_CfgGeneralMainCourante_ClientClasse];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH CHECK CHECK CONSTRAINT [FK_CfgGeneralMainCourante_Etat1];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH CHECK CHECK CONSTRAINT [FK_CfgGeneralMainCourante_Etat2];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH CHECK CHECK CONSTRAINT [FK_CfgGeneralMainCourante_Etat3];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH CHECK CHECK CONSTRAINT [FK_CfgGeneralMainCourante_RessourceClasse];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH CHECK CHECK CONSTRAINT [FK_CfgGeneralMainCourante_SousClasseClient];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH CHECK CHECK CONSTRAINT [FK_ConfigurationMainCourante_AleaState];

ALTER TABLE [dbo].[CfgPageAnalyse] WITH CHECK CHECK CONSTRAINT [FK_CfgPageAnalyse_Analyse];

ALTER TABLE [dbo].[CfgPageAnalyse] WITH CHECK CHECK CONSTRAINT [FK_CfgPageAnalyse_Page];

ALTER TABLE [dbo].[EvenementActionRealise] WITH CHECK CHECK CONSTRAINT [FK_EvenementActionRealise_EvenementAction];

ALTER TABLE [dbo].[EvenementActionRealise] WITH CHECK CHECK CONSTRAINT [FK_EvenementActionRealise_EvenementListe];

ALTER TABLE [dbo].[EvenementActionRealise] WITH CHECK CHECK CONSTRAINT [FK_EvenementActionRealise_Ressource];

ALTER TABLE [dbo].[EvenementExploitationConseq] WITH CHECK CHECK CONSTRAINT [FK_EvenementExploitationConseq_RessourceVehicule];

ALTER TABLE [dbo].[EvenementExploitationConseq] WITH CHECK CHECK CONSTRAINT [FK_EvenementExploitationConseq_EvenementExploitationConseq];

ALTER TABLE [dbo].[EvenementExploitationConseq] WITH CHECK CHECK CONSTRAINT [FK_EvenementExploitationConseq_EvenementList];

ALTER TABLE [dbo].[EvenementExploitationConseq] WITH CHECK CHECK CONSTRAINT [FK_EvenementExploitationConseq_ExploitationConseqManoeuvre];

ALTER TABLE [dbo].[EvenementExploitationConseq] WITH CHECK CHECK CONSTRAINT [FK_EvenementExploitationConseq_RessourceHomme];

ALTER TABLE [dbo].[EvenementIntervention] WITH CHECK CHECK CONSTRAINT [FK_EvenementIntervention_EvenementList];

ALTER TABLE [dbo].[EvenementIntervention] WITH CHECK CHECK CONSTRAINT [FK_EvenementIntervention_Ressource1];

ALTER TABLE [dbo].[EvenementIntervention] WITH CHECK CHECK CONSTRAINT [FK_EvenementIntervention_Ressource2];

ALTER TABLE [dbo].[EvenementIntervention] WITH CHECK CHECK CONSTRAINT [FK_InterventionConseq_Intervenants];

ALTER TABLE [dbo].[EvenementIntervention] WITH CHECK CHECK CONSTRAINT [FK_InterventionConseq_InterventionEtat];

ALTER TABLE [dbo].[CfgEvenementMaintenanceCodeSignalementLink] WITH CHECK CHECK CONSTRAINT [FK_CfgEvenementMaintenanceCodeSignalementLink_codeSignalement];

ALTER TABLE [dbo].[CfgEvenementMaintenanceCodeSignalementLink] WITH CHECK CHECK CONSTRAINT [FK_CfgEvenementMaintenanceCodeSignalementLink_emplacement];

ALTER TABLE [dbo].[CfgEvenementMaintenanceCodeSignalementLink] WITH CHECK CHECK CONSTRAINT [FK_CfgEvenementMaintenanceCodeSignalementLink_organe];

ALTER TABLE [dbo].[CfgEvenementMaintenanceCodeSignalementLink] WITH CHECK CHECK CONSTRAINT [FK_CfgEvenementMaintenanceCodeSignalementLink_Symptome];

ALTER TABLE [dbo].[CfgEvenementMaintenanceOrganeGMAOLink] WITH CHECK CHECK CONSTRAINT [FK_CfgEvenementMaintenanceOrganeGMAOLink_organe];

ALTER TABLE [dbo].[CfgEvenementMaintenanceOrganeGMAOLink] WITH CHECK CHECK CONSTRAINT [FK_CfgEvenementMaintenanceOrganeGMAOLink_organeGMAO];

ALTER TABLE [dbo].[CfgFichierFolderTypeRule] WITH CHECK CHECK CONSTRAINT [FK_CfgFichierFolderTypeRule_FichierFolderType];

ALTER TABLE [dbo].[CfgFichierFolderTypeRule] WITH CHECK CHECK CONSTRAINT [FK_CfgFichierFolderTypeRule_Rule];

ALTER TABLE [dbo].[EvenementRessourceTagApplied] WITH CHECK CHECK CONSTRAINT [FK_EvenementRessourceTagApplied_Evenement];

ALTER TABLE [dbo].[EvenementRessourceTagApplied] WITH CHECK CHECK CONSTRAINT [FK_EvenementRessourceTagApplied_RessourceTag];

ALTER TABLE [dbo].[FichierTypeFolder] WITH CHECK CHECK CONSTRAINT [FK_FichierTypeFolder_FichierTypeFolderMode];

ALTER TABLE [dbo].[LnkEvenementRessourceResponsableHierarchique] WITH CHECK CHECK CONSTRAINT [FK_LnkEvenementRessourceResponsableHierarchique_Evenement];

ALTER TABLE [dbo].[LnkEvenementRessourceResponsableHierarchique] WITH CHECK CHECK CONSTRAINT [FK_LnkEvenementRessourceResponsableHierarchique_Ressource];

ALTER TABLE [dbo].[CfgConfigurationRules] WITH CHECK CHECK CONSTRAINT [FK_CfgConfigurationRules_ConfigurationRules2];

ALTER TABLE [dbo].[EvenementMaintenance] WITH CHECK CHECK CONSTRAINT [FK_EvenementMaintenance_CodeSignalement];

ALTER TABLE [dbo].[EvenementMaintenance] WITH CHECK CHECK CONSTRAINT [FK_EvenementMaintenance_OrganeGMAO];


GO
PRINT N'Mise à jour terminée.';


GO

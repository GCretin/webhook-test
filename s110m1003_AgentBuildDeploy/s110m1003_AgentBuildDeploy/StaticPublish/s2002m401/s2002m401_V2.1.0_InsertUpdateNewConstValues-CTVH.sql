USE [s2002m401]

---------------------CTVH 2.1.0-----------------------


---------------------FIL-174 Nouvelles ressource famille / �v�nements famille / conseq exploit-----------------------
GO


INSERT INTO [dbo].[ConstEvenementExploitConseq]
           ([code]
           ,[with_lignes2006m401]
           ,[label_lignes2006m401]
           ,[with_parcourss2006m401]
           ,[label_parcourss2006m401]
           ,[with_directions2006m401]
           ,[label_directions2006m401]
           ,[with_arrets2006m401_1]
           ,[arrets2006m401_stationPrincipale_1]
           ,[label_arrets2006m401_1]
           ,[with_arrets2006m401_2]
           ,[arrets2006m401_stationPrincipale_2]
           ,[lable_arrets2006m401_2]
           ,[with_heure_1]
           ,[lable_heure_1]
           ,[with_heure_2]
           ,[label_heure_2]
           ,[with_nb_departs]
           ,[lable_nb_departs]
           ,[with_nb_courses]
           ,[label_nb_courses]
           ,[with_nb_km]
           ,[label_nb_km]
           ,[signe_nb_km]
           ,[with_d�calage_min]
           ,[label_d�calage_min]
           ,[with_d�calage_max]
           ,[label_d�calage_max]
           ,[with_arretsDepart2006m401_1]
           ,[label_arretsDepart2006m401_1]
           ,[with_arretsDepart2006m401_2]
           ,[label_arretsDepart2006m401_2]
           ,[with_ressourceMateriel]
           ,[label_ressourceMateriel]
           ,[with_ressourceHomme]
           ,[label_ressourceHomme]
           ,[with_withRetombe]
           ,[label_withRetombe]
           ,[with_observations]
           ,[label_observations])
     VALUES
           ('CLIENT_RESTE_A_QUAI'
           ,0
           ,'LABEL_A_COMPLETER'
           ,1
           ,'Parcours'
           ,1
           ,'Ligne / Direction'
           ,1
           ,0
           ,'Arr�t'
           ,0
           ,0
           ,'LABEL_A_COMPLETER'
           ,1
           ,''
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,'-'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER')

INSERT INTO [dbo].[EvenementExploitationConseqManoeuvre] ([id_exploitationConseqManoeuvre_exploitConseqType],[nom]) VALUES (16,'Client reste � quai')




---------------------FIL-128 Masquage du controle fermeture raison (ouverture)-----------------------

INSERT INTO [dbo].[EvenementFermetureRaison]([libelle],[isGlobal],[id_reseau101m401]) VALUES('Termin�',1,1)
INSERT INTO [dbo].[EvenementFermetureRaison]([libelle],[isGlobal],[id_reseau101m401]) VALUES('Pas pris en compte',1,1)

INSERT INTO [dbo].[EvenementFermetureRaison]([libelle],[isGlobal],[id_reseau101m401]) VALUES('Avec justificatif',0,1)
INSERT INTO [dbo].[EvenementFermetureRaison]([libelle],[isGlobal],[id_reseau101m401]) VALUES('Sans justificatif',0,1)


INSERT INTO [dbo].[Rules] ([nom],[id_rules_constEvenermentFamille],[id_rules_reseaus101m401])
     VALUES ('Ev�nements ABSENCES',15,1);
INSERT INTO [dbo].[Rules] ([nom],[id_rules_constEvenermentFamille],[id_rules_reseaus101m401])
     VALUES ('Ev�nements RETARD',16,1);

--ATENTION !!!!!!!!!
-- Faire le CFG � la main

INSERT INTO [dbo].[CfgEvenementControlRule] ([id_cfgEvenementControlRule_constControl],[id_cfgEvenementControlRule_constState],[id_cfgEvenementControlRule_constModeSaisie],[id_cfgEvenementControlRule_reseaus101m401],[isDefaut])
     VALUES (66,4,1,1,0)


---------------------FIL-143 Masquage du controle type lieu-----------------------


INSERT INTO [dbo].[CfgEvenementControlRule] ([id_cfgEvenementControlRule_constControl],[id_cfgEvenementControlRule_constState],[id_cfgEvenementControlRule_constEvenermentFamille],[id_cfgEvenementControlRule_reseaus101m401])
	VALUES (2,4,15,1)

INSERT INTO [dbo].[CfgEvenementControlRule] ([id_cfgEvenementControlRule_constControl],[id_cfgEvenementControlRule_constState],[id_cfgEvenementControlRule_constEvenermentFamille],[id_cfgEvenementControlRule_reseaus101m401])
	VALUES (20,4,15,1)

INSERT INTO [dbo].[CfgEvenementControlRule] ([id_cfgEvenementControlRule_constControl],[id_cfgEvenementControlRule_constState],[id_cfgEvenementControlRule_constEvenermentFamille],[id_cfgEvenementControlRule_reseaus101m401])
	VALUES (5,4,15,1)

	---------------------FIL-114 Migration mod�le ressource -----------------------
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10038
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 1

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10039
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 2

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10040
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 3

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10041
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 4

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10042
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 5

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10043
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 6

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10044
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 7

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10045
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 8

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10046
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 9

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10047
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10048
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 11

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10049
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 12

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10050
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 13

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10051
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 14

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10052
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 15

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10053
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 16

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10054
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 17

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10055
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 18

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10056
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 19

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10057
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 20

UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource] 
	SET [id_cfgEvenementMaintenanceRessource_materielCategorie] = 10058
	WHERE [id_cfgEvenementMaintenanceRessource_materielCategorie] = 21

---------------------FIL-160 Section Precision---------------------


---------------------A faire � la main !!!!

GO





USE [s2002m401]

--*******************************************************
--*******************************************************
---------------------G�n�ral 2.2.1-----------------------
--*******************************************************
--*******************************************************-

---------------------FIL-552 Nouvelle section �tat -----------------------
IF NOT EXISTS(select * from [ConstEvenementSection] where code = 'ETAT')
BEGIN
INSERT INTO [dbo].[ConstEvenementSection]([code],[libelle])
     VALUES ('ETAT','Etat �v�nement')
END

UPDATE [dbo].[ConstEvenementSection]
   SET [libelle] = 'Actions suivi'
 WHERE code = 'ACTION'

---------------------FIL-172 Section action -----------------------

IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_SV_PROPORTION')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (18,'EXPLOITATION_CONSEQUENCE_SV_PROPORTION','SV proportion',1,0,0,22,'Exploitation cons�quence')
END

IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_SV_NON_ASSURE')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (18,'EXPLOITATION_CONSEQUENCE_SV_NON_ASSURE','SV non assur�',1,0,0,23,'Exploitation cons�quence')
END

IF NOT EXISTS(select * from [ConstEvenementControl] where code = 'EXPLOITATION_CONSEQUENCE_SV_PROPORTION')
BEGIN
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('EXPLOITATION_CONSEQUENCE_SV_PROPORTION','SV proportion')
END

IF NOT EXISTS(select * from [ConstEvenementControl] where code = 'EXPLOITATION_CONSEQUENCE_SV_NON_ASSURE')
BEGIN
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('EXPLOITATION_CONSEQUENCE_SV_NON_ASSURE','SV non assur�')
END

---------------------Cons�quence suppl�ment fictif---------------
IF NOT EXISTS(select * from ConstEvenementExploitConseq where code = 'SUPPLEMENT_FICTIF')
BEGIN
INSERT INTO [dbo].[ConstEvenementExploitConseq]
           ([code]
           ,[with_lignes2006m401]
           ,[label_lignes2006m401]
           ,[with_parcourss2006m401]
           ,[label_parcourss2006m401]
           ,[with_directions2006m401]
           ,[label_directions2006m401]
           ,[with_arrets2006m401_1]
           ,[arrets2006m401_stationPrincipale_1]
           ,[label_arrets2006m401_1]
           ,[with_arrets2006m401_2]
           ,[arrets2006m401_stationPrincipale_2]
           ,[lable_arrets2006m401_2]
           ,[with_heure_1]
           ,[lable_heure_1]
           ,[with_heure_2]
           ,[label_heure_2]
           ,[with_nb_departs]
           ,[lable_nb_departs]
           ,[with_nb_courses]
           ,[label_nb_courses]
           ,[with_nb_km]
           ,[label_nb_km]
           ,[signe_nb_km]
           ,[with_d�calage_min]
           ,[label_d�calage_min]
           ,[with_d�calage_max]
           ,[label_d�calage_max]
           ,[with_arretsDepart2006m401_1]
           ,[label_arretsDepart2006m401_1]
           ,[with_arretsDepart2006m401_2]
           ,[label_arretsDepart2006m401_2]
           ,[with_ressourceMateriel]
           ,[label_ressourceMateriel]
           ,[with_ressourceHomme]
           ,[label_ressourceHomme]
           ,[with_withRetombe]
           ,[label_withRetombe]
           ,[with_observations]
           ,[label_observations])
     VALUES
           ('SUPPLEMENT_FICTIF'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,0
           ,'LABEL_A_COMPLETER'
           ,1
           ,'D�but'
           ,1
           ,'Fin'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,1
           ,'km(+)'
           ,'+'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,1
           ,'V�hicule'
           ,1
           ,'Agent'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER')


UPDATE [dbo].[EvenementExploitationConseqManoeuvre]
   SET [id_exploitationConseqManoeuvre_exploitConseqType] = (select id_constEvenementExploitConseq from ConstEvenementExploitConseq where code = 'SUPPLEMENT_FICTIF')

 WHERE nom in ('Suppl�m�nt fictif technique','Suppl�m�nt fictif formation')

END

---------------------FIL-618 Reporting TaM -----------------------
UPDATE [dbo].[ConstQueryCriteriaGroup]
   SET [nom] = 'S�curit�'
 WHERE nom = 'S�curti�'

UPDATE [dbo].[ConstQueryCriteria]
   SET [libelle] = 'S�curit� pr�cisions'
      ,[description] = 'S�curit� pr�cisions'
 WHERE code = 'SECURITE_PRECISION'

IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'SECURITE_TYPE')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('SELECT_OBJECT'
           ,'S�curit� types'
           ,6
           ,'SECURITE_TYPE'
           ,0
           ,1
           ,'S�curit� types'
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'SECURITE_TYPE';
END

IF NOT EXISTS(select * from [ConstQueryCriteriaGroup] where [nom] = 'Dommage')

INSERT INTO [dbo].[ConstQueryCriteriaGroup]
           ([nom])
     VALUES
           ('Dommage')

IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'DOMMAGE_TYPE')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('SELECT_OBJECT'
           ,'Dommage types'
           ,10
           ,'DOMMAGE_TYPE'
           ,0
           ,1
           ,'Dommage types'
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'DOMMAGE_TYPE';
END

IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'DOMMAGE_PRECISION')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('SELECT_OBJECT'
           ,'Dommage pr�cisions'
           ,10
           ,'DOMMAGE_PRECISION'
           ,0
           ,1
           ,'Dommage pr�cisions'
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'DOMMAGE_PRECISION';

END

IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'DOMMAGE_CONTEXTE')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('SELECT_OBJECT'
           ,'Dommage contexte'
           ,10
           ,'DOMMAGE_CONTEXTE'
           ,0
           ,1
           ,'Dommage contexte'
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'DOMMAGE_CONTEXTE';

END

IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'DOMMAGE_TIERS')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('SELECT_OBJECT'
           ,'Dommage tiers'
           ,10
           ,'DOMMAGE_TIERS'
           ,0
           ,1
           ,'Dommage tiers'
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'DOMMAGE_TIERS';

END

IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'HEURE_APPARITION' and dataType = 'TIME')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('TIME'
           ,'Heure apparition'
           ,1
           ,'HEURE_APPARITION'
           ,0
           ,1
           ,''
           ,1)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'HEURE_APPARITION' and dataType = 'TIME';

END

IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'HEURE_APPARITION' and dataType = 'TIME_RELATIVE_HEURE')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('TIME_RELATIVE_HEURE'
           ,'Heure apparition (heures relatifs)'
           ,1
           ,'HEURE_APPARITION'
           ,0
           ,1
           ,''
           ,1)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'HEURE_APPARITION' and dataType = 'TIME_RELATIVE_HEURE';

END

IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'EXPLOITATION_CONSEQUENCE_DURATION')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('INT'
           ,'Dur�e cons�quence exploitation (min)'
           ,7
           ,'EXPLOITATION_CONSEQUENCE_DURATION'
           ,0
           ,1
           ,'Dur�e cons�quence exploitation (min)'
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'EXPLOITATION_CONSEQUENCE_DURATION';

END

IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_DURATION')
INSERT INTO [dbo].[ConstEvenementAttribut]
           ([id_constEvenementAttribut_group]
           ,[code]
           ,[libelle]
           ,[isEligibleQueryResult]
           ,[isEligibleXaxis]
           ,[isEligibleSeriesAnalyse]
           ,[rang]
           ,[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_DURATION'
           ,'Dur�e'
           ,1
           ,0
           ,0
           ,16
           ,'Exploitation cons�quence')


---------------------FIL-156 Nouveaux tags -----------------------
IF NOT EXISTS(select * from ConstRessourceTag where code = 'RESPONSABLE')

INSERT INTO [dbo].[ConstRessourceTag]([code],[label],[rang])
     VALUES ('RESPONSABLE','Responsable',1)


IF NOT EXISTS(select * from ConstRessourceTag where code = 'DEPOT')

INSERT INTO [dbo].[ConstRessourceTag]([code],[label],[rang])
     VALUES ('DEPOT','D�p�t',1)


IF NOT EXISTS(select * from ConstRessourceTag where code = 'BOITE_LETTRE')

INSERT INTO [dbo].[ConstRessourceTag]([code],[label],[rang])
     VALUES ('BOITE_LETTRE','Bo�te aux lettres',1)


IF NOT EXISTS(select * from ConstRessourceTag where code = 'AFFECTATION_THEORIQUE')

INSERT INTO [dbo].[ConstRessourceTag]([code],[label],[rang])
     VALUES ('AFFECTATION_THEORIQUE','Affectation th�orique',1)


---------------------FIL-600 Nouvelles manoeuvres -----------------------
IF NOT EXISTS(select * from ConstEvenementExploitConseq where code = 'MANOEUVRE_SIMPLE')

INSERT INTO [dbo].[ConstEvenementExploitConseq]
           ([code]
           ,[with_lignes2006m401]
           ,[label_lignes2006m401]
           ,[with_parcourss2006m401]
           ,[label_parcourss2006m401]
           ,[with_directions2006m401]
           ,[label_directions2006m401]
           ,[with_arrets2006m401_1]
           ,[arrets2006m401_stationPrincipale_1]
           ,[label_arrets2006m401_1]
           ,[with_arrets2006m401_2]
           ,[arrets2006m401_stationPrincipale_2]
           ,[lable_arrets2006m401_2]
           ,[with_heure_1]
           ,[lable_heure_1]
           ,[with_heure_2]
           ,[label_heure_2]
           ,[with_nb_departs]
           ,[lable_nb_departs]
           ,[with_nb_courses]
           ,[label_nb_courses]
           ,[with_nb_km]
           ,[label_nb_km]
           ,[signe_nb_km]
           ,[with_d�calage_min]
           ,[label_d�calage_min]
           ,[with_d�calage_max]
           ,[label_d�calage_max]
           ,[with_arretsDepart2006m401_1]
           ,[label_arretsDepart2006m401_1]
           ,[with_arretsDepart2006m401_2]
           ,[label_arretsDepart2006m401_2]
           ,[with_ressourceMateriel]
           ,[label_ressourceMateriel]
           ,[with_ressourceHomme]
           ,[label_ressourceHomme]
           ,[with_withRetombe]
           ,[label_withRetombe]
           ,[with_observations]
           ,[label_observations])
     VALUES
           ('MANOEUVRE_SIMPLE'
           ,0
           ,'LABEL_A_COMPLETER'
           ,1
           ,'Parcours'
           ,1
           ,'Ligne / Direction'
           ,1
           ,0
           ,'Arr�t'
           ,0
           ,0
           ,'LABEL_A_COMPLETER'
           ,1
           ,''
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,'-'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER')


---------------------FIL-640 crit�re sur envoi SMS -----------------------

IF NOT EXISTS(select * from [ConstQueryCriteriaGroup] where [nom] = 'D�clencheur asynchrone')

INSERT INTO [dbo].[ConstQueryCriteriaGroup]
           ([nom])
     VALUES
           ('D�clencheur asynchrone')

IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'QUERY_DECLENCHEUR_ASYNC_COM_MODE')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('SELECT_OBJECT'
           ,'Mode envoi asynchrone'
           ,11
           ,'QUERY_DECLENCHEUR_ASYNC_COM_MODE'
           ,0
           ,1
           ,'Mode envoi asynchrone'
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'QUERY_DECLENCHEUR_ASYNC_COM_MODE';
END

---------------------FIL-639 Ajustement des ConstTags -----------------------

UPDATE [dbo].[ConstRessourceTag]
   SET [label] = 'Habilitation bus articul�'
      ,[rang] = 109
 WHERE code = 'HABILITATION_ARTICULE';

UPDATE [dbo].[ConstRessourceTag]
   SET [label] = 'Habilitation tramway'
      ,[rang] = 110
 WHERE code = 'HABILITATION_TRAMWAY';

UPDATE [dbo].[ConstRessourceTag]
   SET [label] = 'Habilitation bus pmr'
      ,[rang] = 108
 WHERE code = 'HABILITATION_PMR';

UPDATE [dbo].[ConstRessourceTag]
   SET [rang] = 210
 WHERE code = 'RESTRICTION_MEDICAL';

UPDATE [dbo].[ConstRessourceTag]
   SET [rang] = 180
 WHERE code = 'ADRESSE';

UPDATE [dbo].[ConstRessourceTag]
   SET [label] = 'Habilitation ligne'
      ,[rang] = 106
 WHERE code = 'HABILITATION_LIGNE';

UPDATE [dbo].[ConstRessourceTag]
   SET [rang] = 200
 WHERE code = 'TELEPHONE';

UPDATE [dbo].[ConstRessourceTag]
   SET [label] = 'Habilitation bus standard'
      ,[rang] = 107
 WHERE code = 'HABILITATION_STANDARD';

UPDATE [dbo].[ConstRessourceTag]
   SET [rang] = 170
 WHERE code = 'RESPONSABLE';

UPDATE [dbo].[ConstRessourceTag]
   SET [rang] = 150
 WHERE code = 'DEPOT';

UPDATE [dbo].[ConstRessourceTag]
   SET [rang] = 190
 WHERE code = 'BOITE_LETTRE';

UPDATE [dbo].[ConstRessourceTag]
   SET [rang] = 160
 WHERE code = 'AFFECTATION_THEORIQUE';

---------------------FIL-646 Anonymisation donn�es -----------------------

IF NOT EXISTS(select * from [ConstConfigurationRules] where nom = 'SMS_ANONYMOUS')
INSERT INTO [dbo].[ConstConfigurationRules]
           ([nom]
           ,[libelle])
     VALUES
           ('SMS_ANONYMOUS'
           ,'SMS automatiquement anonymis�')
---------------------FIL-648 Ecrasement des km perdus calcules -----------------------

IF NOT EXISTS(select * from [ConstConfigurationRules] where nom = 'KM_PERUDU_CALCUL_ECRASEMENT')
INSERT INTO [dbo].[ConstConfigurationRules]
           ([nom]
           ,[libelle])
     VALUES
           ('KM_PERUDU_CALCUL_ECRASEMENT'
           ,'Ecrasement des km perdus � chaque calcul')

--------------------- VIA -----------------------

IF NOT EXISTS(select * from ConstEvenementExploitConseq where code = 'VIA')

INSERT INTO [dbo].[ConstEvenementExploitConseq]
           ([code]
           ,[with_lignes2006m401]
           ,[label_lignes2006m401]
           ,[with_parcourss2006m401]
           ,[label_parcourss2006m401]
           ,[with_directions2006m401]
           ,[label_directions2006m401]
           ,[with_arrets2006m401_1]
           ,[arrets2006m401_stationPrincipale_1]
           ,[label_arrets2006m401_1]
           ,[with_arrets2006m401_2]
           ,[arrets2006m401_stationPrincipale_2]
           ,[lable_arrets2006m401_2]
           ,[with_heure_1]
           ,[lable_heure_1]
           ,[with_heure_2]
           ,[label_heure_2]
           ,[with_nb_departs]
           ,[lable_nb_departs]
           ,[with_nb_courses]
           ,[label_nb_courses]
           ,[with_nb_km]
           ,[label_nb_km]
           ,[signe_nb_km]
           ,[with_d�calage_min]
           ,[label_d�calage_min]
           ,[with_d�calage_max]
           ,[label_d�calage_max]
           ,[with_arretsDepart2006m401_1]
           ,[label_arretsDepart2006m401_1]
           ,[with_arretsDepart2006m401_2]
           ,[label_arretsDepart2006m401_2]
           ,[with_ressourceMateriel]
           ,[label_ressourceMateriel]
           ,[with_ressourceHomme]
           ,[label_ressourceHomme]
           ,[with_withRetombe]
           ,[label_withRetombe]
           ,[with_observations]
           ,[label_observations])
     VALUES
           ('VIA'
           ,0
           ,'LABEL_A_COMPLETER'
           ,1
           ,'Parcours'
           ,1
           ,'Ligne / Direction'
           ,1
           ,0
           ,'Arr�t'
           ,0
           ,0
           ,'LABEL_A_COMPLETER'
           ,1
           ,''
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,'-'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER')

---------------------Crit�re RH Type / RH Pr�cision  -----------------------

IF NOT EXISTS(select * from [ConstQueryCriteriaGroup] where [nom] = 'RH')

INSERT INTO [dbo].[ConstQueryCriteriaGroup]
           ([nom])
     VALUES
           ('RH')

IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'RH_TYPE')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('SELECT_OBJECT'
           ,'RH Types'
           ,9
           ,'RH_TYPE'
           ,0
           ,1
           ,'RH Types'
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'RH_TYPE';
END


IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'RH_PRECISION')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('SELECT_OBJECT'
           ,'RH Pr�cisions'
           ,9
           ,'RH_PRECISION'
           ,0
           ,1
           ,'RH Pr�cisions'
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'RH_PRECISION';
END


---------------------RH heures + intervention observations  -----------------------
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'INTERVENTION_OBSERVATION')
INSERT INTO [dbo].[ConstEvenementAttribut]
           ([id_constEvenementAttribut_group]
           ,[code]
           ,[libelle]
           ,[isEligibleQueryResult]
           ,[isEligibleXaxis]
           ,[isEligibleSeriesAnalyse]
           ,[rang]
           ,[header])
     VALUES
           (11
           ,'INTERVENTION_OBSERVATION'
           ,'Observation'
           ,1
           ,1
           ,1
           ,6
           ,'Intervention')

IF NOT EXISTS(select * from [ConstEvenementControl] where code = 'INTERVENTION_OBSERVATION')

INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('INTERVENTION_OBSERVATION','Observation intervention')
-----------
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'RH_HEUREDEBUT')
INSERT INTO [dbo].[ConstEvenementAttribut]
           ([id_constEvenementAttribut_group]
           ,[code]
           ,[libelle]
           ,[isEligibleQueryResult]
           ,[isEligibleXaxis]
           ,[isEligibleSeriesAnalyse]
           ,[rang]
           ,[header])
     VALUES
           (17
           ,'RH_HEUREDEBUT'
           ,'Heure d�but'
           ,1
           ,1
           ,1
           ,4
           ,'RH')

IF NOT EXISTS(select * from [ConstEvenementControl] where code = 'RH_HEUREDEBUT')

INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('RH_HEUREDEBUT','RH Heure d�but')
-----------
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'RH_HEUREFIN')
INSERT INTO [dbo].[ConstEvenementAttribut]
           ([id_constEvenementAttribut_group]
           ,[code]
           ,[libelle]
           ,[isEligibleQueryResult]
           ,[isEligibleXaxis]
           ,[isEligibleSeriesAnalyse]
           ,[rang]
           ,[header])
     VALUES
           (17
           ,'RH_HEUREFIN'
           ,'Heure fin'
           ,1
           ,1
           ,1
           ,4
           ,'RH')

IF NOT EXISTS(select * from [ConstEvenementControl] where code = 'RH_HEUREFIN')

INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('RH_HEUREFIN','RH Heure fin')

---------------------S�paration du crit�re sur les km perdus  -----------------------
IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'KM_PERDU_PERTE')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('INT'
           ,'Km perdus perte'
           ,7
           ,'KM_PERDU_PERTE'
           ,1
           ,1
           ,'Km perdus perte (valeur absolue)'
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'KM_PERDU_PERTE';
END

IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'KM_PERDU_SUPP')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('INT'
           ,'Km perdus suppl�ment'
           ,7
           ,'KM_PERDU_SUPP'
           ,1
           ,1
           ,'Km perdus suppl�ment (valeur absolue)'
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'KM_PERDU_SUPP';
END

---------------------Section responsable hieracrchique  -----------------------
IF NOT EXISTS(select * from [ConstEvenementSection] where code = 'RESPONSABLE')
INSERT INTO [dbo].[ConstEvenementSection]
           ([code]
           ,[libelle])
     VALUES
           ('RESPONSABLE'
           ,'Responsable hi�rarchique')

---------------------Crit�re sur pr�sence heure de fin  -----------------------
IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'WITH_TIME_FIN')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('CHECK_PRESENCE'
           ,'Heure de fin (pr�sence)'
           ,1
           ,'WITH_TIME_FIN'
           ,0
           ,1
           ,''
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'WITH_TIME_FIN';
END

-----------
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'LIEU_AUTRE')
INSERT INTO [dbo].[ConstEvenementAttribut]
           ([id_constEvenementAttribut_group]
           ,[code]
           ,[libelle]
           ,[isEligibleQueryResult]
           ,[isEligibleXaxis]
           ,[isEligibleSeriesAnalyse]
           ,[rang]
           ,[header])
     VALUES
           (5
           ,'LIEU_AUTRE'
           ,'Lieu autre'
           ,1
           ,0
           ,0
           ,8
           ,'Lieu')

IF NOT EXISTS(select * from [ConstEvenementControl] where code = 'LIEU_AUTRE')

INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('LIEU_AUTRE','Lieu autre')

------MAJ nom de section RH / CDS-----


UPDATE [dbo].[ConstEvenementSection]
   SET [libelle] = 'RH / CDS'
 WHERE code = 'RH'


------Crit�re heure de fin-----

IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'HEURE_FIN')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('TIME'
           ,'Heure de fin'
           ,1
           ,'HEURE_FIN'
           ,0
           ,1
           ,''
           ,1)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'HEURE_FIN';

END


------Crit�re raison fermeture-----

IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'RAISON_FERMETURE')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('SELECT_OBJECT'
           ,'Raisons fermetures'
           ,1
           ,'RAISON_FERMETURE'
           ,0
           ,1
           ,''
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'RAISON_FERMETURE';
END



IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'WITH_RAISON_FERMETURE')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('CHECK_PRESENCE'
           ,'Raisons fermetures (pr�sence)'
           ,1
           ,'WITH_RAISON_FERMETURE'
           ,0
           ,1
           ,''
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'WITH_RAISON_FERMETURE';
END


----------Revue des libell� km perdus---------------


UPDATE [dbo].[ConstQueryCriteria]
   SET [libelle] = 'km total'
      ,[description] = 'km total �v�nement'
 WHERE code='KM_PERDU_TOTAL'


UPDATE [dbo].[ConstQueryCriteria]
   SET [libelle] = 'km total perte'
      ,[description] = 'km perte total �v�nement (valeur absolue)'
 WHERE code='KM_PERDU_PERTE'


UPDATE [dbo].[ConstQueryCriteria]
   SET [libelle] = 'km total suppl�ment'
      ,[description] = 'km suppl�ment total �v�nement (valeur absolue)'
 WHERE code='KM_PERDU_SUPP'


---

UPDATE [dbo].[ConstEvenementAttribut]
   SET [libelle] = 'Course'
      ,[rang] = 50
 WHERE code =' EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE'


UPDATE [dbo].[ConstEvenementAttribut]
   SET [libelle] = 'Course (num�rique)'
      ,[rang] = 51
 WHERE code =' EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_CALCULE'



IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_IMPACT')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_IMPACT'
           ,'Course impact�es'
           ,1
           ,0
           ,0
           ,52
           ,'Exploitation cons�quence')
END


IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_DEPART')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_DEPART'
           ,'Terminus d�part'
           ,1
           ,0
           ,0
           ,53
           ,'Exploitation cons�quence')
END


IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_ARRIVE')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_ARRIVE'
           ,'Terminus arriv�e'
           ,1
           ,0
           ,0
           ,54
           ,'Exploitation cons�quence')
END



UPDATE [dbo].[ConstEvenementAttribut]
   SET [libelle] = 'km'
      ,[rang] = 55
 WHERE code =' EXPLOITATION_CONSEQUENCE_NOMBRE_KM'



UPDATE [dbo].[ConstEvenementAttribut]
   SET [libelle] = 'km total �v�nement'
      ,[rang] = 56
 WHERE code ='EXPLOITATION_CONSEQUENCE_NOMBRE_KM_TOTAL'



IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_KM_TOTAL_PERTE')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_KM_TOTAL_PERTE'
           ,'km total �v�nement perte'
           ,1
           ,0
           ,0
           ,57
           ,'Exploitation cons�quence')
END



IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_KM_TOTAL_SUP')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_KM_TOTAL_SUP'
           ,'km total �v�nement suppl�ment'
           ,1
           ,0
           ,0
           ,58
           ,'Exploitation cons�quence')
END



IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_TOTAL')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_TOTAL'
           ,'Course total �v�nement'
           ,1
           ,0
           ,0
           ,59
           ,'Exploitation cons�quence')
END



IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_TOTAL_PERTE')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_TOTAL_PERTE'
           ,'Course total �v�nement perte'
           ,1
           ,0
           ,0
           ,60
           ,'Exploitation cons�quence')
END



IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_TOTAL_SUP')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_TOTAL_SUP'
           ,'Course total �v�nement suppl�ment'
           ,1
           ,0
           ,0
           ,61
           ,'Exploitation cons�quence')
END



IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_IMPACT_TOTAL')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_IMPACT_TOTAL'
           ,'Course impact total �v�nement'
           ,1
           ,0
           ,0
           ,62
           ,'Exploitation cons�quence')
END



IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_IMPACT_TOTAL_PERTE')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_IMPACT_TOTAL_PERTE'
           ,'Course impact total �v�nement perte'
           ,1
           ,0
           ,0
           ,63
           ,'Exploitation cons�quence')
END


IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_IMPACT_TOTAL_SUP')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_IMPACT_TOTAL_SUP'
           ,'Course impact total �v�nement suppl�ment'
           ,1
           ,0
           ,0
           ,64
           ,'Exploitation cons�quence')
END



IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_DEPART_TOTAL')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_DEPART_TOTAL'
           ,'Term d�part total �v�nement'
           ,1
           ,0
           ,0
           ,65
           ,'Exploitation cons�quence')
END


IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_DEPART_TOTAL_PERTE')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_DEPART_TOTAL_PERTE'
           ,'Term d�part total �v�nement p�rte'
           ,1
           ,0
           ,0
           ,66
           ,'Exploitation cons�quence')
END

IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_DEPART_TOTAL_SUP')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_DEPART_TOTAL_SUP'
           ,'Term d�part total �v�nement suppl�ment'
           ,1
           ,0
           ,0
           ,67
           ,'Exploitation cons�quence')
END

IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_ARRIVE_TOTAL')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_ARRIVE_TOTAL'
           ,'Term arriv�e total �v�nement'
           ,1
           ,0
           ,0
           ,68
           ,'Exploitation cons�quence')
END


IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_ARRIVE_TOTAL_PERTE')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_ARRIVE_TOTAL_PERTE'
           ,'Term arriv�e total �v�nement p�rte'
           ,1
           ,0
           ,0
           ,69
           ,'Exploitation cons�quence')
END


IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_ARRIVE_TOTAL_SUP')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_ARRIVE_TOTAL_SUP'
           ,'Term arriv�e total �v�nement suppl�ment'
           ,1
           ,0
           ,0
           ,70
           ,'Exploitation cons�quence')
END


----------Attribut imputation---------------

IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'IMPUTATION')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (1
           ,'IMPUTATION'
           ,'Imputation'
           ,1
           ,0
           ,0
           ,8
           ,'')
END


--*******************************************************
--*******************************************************
---------------------G�n�ral 2.3.0-----------------------
--*******************************************************
--*******************************************************

---------------------FIL-330 Gestion des �tats -----------------------
IF NOT EXISTS(select * from [ConstFichierTypeFolderMode] where code = 'MODELE')
INSERT INTO [dbo].[ConstFichierTypeFolderMode]([code],[libelle]) VALUES ('MODELE','Fichiers mod�les � remplir')

IF NOT EXISTS(select * from [ConstFichierTypeFolderMode] where code = 'PROCESS')
INSERT INTO [dbo].[ConstFichierTypeFolderMode]([code],[libelle]) VALUES ('PROCESS','Fichiers proc�dures')

---------------------FIL-81 GMAO -----------------------

IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'MAINTENANCE_ORGANE_GMAO')
INSERT INTO [dbo].[ConstEvenementAttribut]
           ([id_constEvenementAttribut_group]
           ,[code]
           ,[libelle]
           ,[isEligibleQueryResult]
           ,[isEligibleXaxis]
           ,[isEligibleSeriesAnalyse]
           ,[rang]
           ,[header])
     VALUES
           (12
           ,'MAINTENANCE_ORGANE_GMAO'
           ,'Organe GMAO'
           ,1
           ,1
           ,1
           ,2
           ,'Maintenance')


IF NOT EXISTS(select * from [COM] where [nomAlerteCOM] = 'GMAO')
INSERT INTO [dbo].[COM]
           ([nomAlerteCOM])
     VALUES
           ('GMAO')


---------------------FIL-624 Action de suivis modification CTVH -----------------------

IF NOT EXISTS(select * from [ConstQueryCriteriaGroup] where nom = 'Actions suivis')
INSERT INTO [dbo].[ConstQueryCriteriaGroup]
           ([nom])
     VALUES
           ('Actions suivis')



IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'ACTION_SUIVI_GROUPE')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
	 select 'SELECT_OBJECT','Action suivi r�alis� groupe',id_constQueryCriteriaGroup,'ACTION_SUIVI_GROUPE',0,1,'Action suivi r�alis� groupe',0 from [ConstQueryCriteriaGroup] where nom = 'Actions suivis'

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])
     select 1,id_queryCriteria from ConstQueryCriteria where code = 'ACTION_SUIVI_GROUPE';
END


IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'ACTION_SUIVI')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
	 select 'SELECT_OBJECT','Action suivi r�alis�',id_constQueryCriteriaGroup,'ACTION_SUIVI',0,1,'Action suivi r�alis�',0 from [ConstQueryCriteriaGroup] where nom = 'Actions suivis'

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])
     select 1,id_queryCriteria from ConstQueryCriteria where code = 'ACTION_SUIVI';
END

/*******Controle et attributs********/
UPDATE [dbo].[ConstEvenementControl]
   SET [libelle] = 'Action date saisie'
 WHERE code = 'ACTION_DATE'

 UPDATE [dbo].[ConstEvenementControl]
   SET [libelle] = 'Action heure saisie'
 WHERE code = 'ACTION_TIME'

UPDATE [dbo].[ConstEvenementAttribut]
   SET [libelle] = 'Date saisie'
 WHERE code = 'ACTION_DATE'

 UPDATE [dbo].[ConstEvenementAttribut]
   SET [libelle] = 'Heure saisie'
 WHERE code = 'ACTION_TIME'

IF NOT EXISTS(select * from [ConstEvenementControl] where code = 'ACTION_DATE_REALISE')
INSERT INTO [dbo].[ConstEvenementControl]
           ([code]
           ,[libelle])
     VALUES
           ('ACTION_DATE_REALISE'
           ,'Action date r�alis�e')

IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'ACTION_DATE_REALISE')
INSERT INTO [dbo].[ConstEvenementAttribut]
			([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])

			select id_constEvenementAttributGroup,'ACTION_DATE_REALISE','Date r�alis�e',1,0,0,5,'Action r�alis�e' from ConstEvenementAttributGroup where nom = 'Actions';

IF NOT EXISTS(select * from [ConstEvenementControl] where code = 'ACTION_TIME_REALISE')
INSERT INTO [dbo].[ConstEvenementControl]
           ([code]
           ,[libelle])
     VALUES
           ('ACTION_TIME_REALISE'
           ,'Action heure r�alis�e')



IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'ACTION_TIME_REALISE')
INSERT INTO [dbo].[ConstEvenementAttribut]
			([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])

			select id_constEvenementAttributGroup,'ACTION_TIME_REALISE','Heure r�alis�e',1,0,0,6,'Action r�alis�e' from ConstEvenementAttributGroup where nom = 'Actions';



IF NOT EXISTS(select * from [ConstEvenementControl] where code = 'ACTION_OBSERVATION')
INSERT INTO [dbo].[ConstEvenementControl]
           ([code]
           ,[libelle])
     VALUES
           ('ACTION_OBSERVATION'
           ,'Action observations')



IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'ACTION_OBSERVATION')
INSERT INTO [dbo].[ConstEvenementAttribut]
			([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])

			select id_constEvenementAttributGroup,'ACTION_OBSERVATION','Observation',1,0,0,7,'Action r�alis�e' from ConstEvenementAttributGroup where nom = 'Actions';


IF NOT EXISTS(select * from [ConstEvenementControl] where code = 'ACTION_RESSOURCE')
INSERT INTO [dbo].[ConstEvenementControl]
           ([code]
           ,[libelle])
     VALUES
           ('ACTION_RESSOURCE'
           ,'Action ressource')


IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'ACTION_RESSOURCE')
INSERT INTO [dbo].[ConstEvenementAttribut]
			([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])

			select id_constEvenementAttributGroup,'ACTION_RESSOURCE','Ressource',1,0,0,8,'Action r�alis�e' from ConstEvenementAttributGroup where nom = 'Actions';



--FIL-81 Interface GMAO ajustemenrt crit�re organes

IF NOT EXISTS(select * from [ConstQueryCriteriaGroup] where nom = 'Maintenance')
INSERT INTO [dbo].[ConstQueryCriteriaGroup]
           ([nom])
     VALUES
           ('Maintenance')


UPDATE [dbo].[ConstQueryCriteria]
   SET
      [id_queryCriteria_queryCriteriaGroup] = gr.id_constQueryCriteriaGroup
	FROM
		(select ConstQueryCriteriaGroup.id_constQueryCriteriaGroup from  ConstQueryCriteriaGroup WHERE nom = 'Maintenance') gr
	WHERE code = 'MAINTENANCE_ORGANE';

UPDATE [dbo].[ConstQueryCriteria]
   SET
      [id_queryCriteria_queryCriteriaGroup] = gr.id_constQueryCriteriaGroup
	FROM
		(select ConstQueryCriteriaGroup.id_constQueryCriteriaGroup from  ConstQueryCriteriaGroup WHERE nom = 'Maintenance') gr
	WHERE code = 'MAINTENANCE_SYMPTOME';

IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'WITH_MAINTENANCE_ORGANE_GMAO')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
	 select 'CHECK_PRESENCE','Organe GMAO (pr�sence)',id_constQueryCriteriaGroup,'WITH_MAINTENANCE_ORGANE_GMAO',0,1,'Pr�sence organe GMAO',0 from [ConstQueryCriteriaGroup] where nom = 'Maintenance'

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])
     select 1,id_queryCriteria from ConstQueryCriteria where code = 'WITH_MAINTENANCE_ORGANE_GMAO';
END
--------------------------------------------------------
--------------------------------------------------------
------------        NUMERO DE VERSION        -----------
--------------------------------------------------------
--------------------------------------------------------
UPDATE [dbo].[DBinfos]
   SET [valeur] = '2.3.0'
 WHERE nom = 'version';

IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'MAINTENANCE_IMPACT')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
	 select 'SELECT_OBJECT','Impact maintenance',id_constQueryCriteriaGroup,'MAINTENANCE_IMPACT',0,1,'Impacts de maintenance',0 from [ConstQueryCriteriaGroup] where nom = 'Maintenance'

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])
     select 1,id_queryCriteria from ConstQueryCriteria where code = 'MAINTENANCE_IMPACT';
END

 --------------------------------------------------------
--------------------------------------------------------
------------        NUMERO DE VERSION        -----------
--------------------------------------------------------
--------------------------------------------------------
UPDATE [dbo].[DBinfos]
   SET [valeur] = '2.3.5'
 WHERE nom = 'version';

  --------------------------------------------------------
--------------------------------------------------------
------------        NUMERO DE VERSION        -----------
--------------------------------------------------------
--------------------------------------------------------
UPDATE [dbo].[DBinfos]
   SET [valeur] = '3.0.0'
 WHERE nom = 'version';

 --FIL-691 Canal de saisie

IF EXISTS(select * from [ConstCanal] where code = 'MOBILE')
BEGIN
DELETE FROM [dbo].[ConstCanal]
      where code = 'MOBILE'
END

IF EXISTS(select * from [ConstCanal] where code = 'WEB')
BEGIN
DELETE FROM [dbo].[ConstCanal]
      where code = 'WEB'
END

IF NOT EXISTS(select * from [ConstCanal] where code = 'APPLICATION_WEB')
BEGIN
INSERT INTO [dbo].[ConstCanal]
           ([code]
           ,[libelle])
     VALUES
           ('APPLICATION_WEB'
           ,'Bureautique')
END

IF NOT EXISTS(select * from [ConstCanal] where code = 'APPLICATION_MOBILE')
BEGIN
INSERT INTO [dbo].[ConstCanal]
           ([code]
           ,[libelle])
     VALUES
           ('APPLICATION_MOBILE'
           ,'Mobile')
END

IF NOT EXISTS(select * from [ConstCanal] where code = 'SAE_CODE_SIGNALEMENT')
BEGIN
INSERT INTO [dbo].[ConstCanal]
           ([code]
           ,[libelle])
     VALUES
           ('SAE_CODE_SIGNALEMENT'
           ,'SAE SAT')
END

IF NOT EXISTS(select * from [ConstCanal] where code = 'JESIGNALE_FORMULAIRE')
BEGIN
INSERT INTO [dbo].[ConstCanal]
           ([code]
           ,[libelle])
     VALUES
           ('JESIGNALE_FORMULAIRE'
           ,'Vigi TaM')
END

IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'CANAL_CREATION')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
	 select 'SELECT_OBJECT','Canal cr�ation',id_constQueryCriteriaGroup,'CANAL_CREATION',0,1,'',0 from [ConstQueryCriteriaGroup] where nom = 'Informations �v�nement'

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])
     select 1,id_queryCriteria from ConstQueryCriteria where code = 'CANAL_CREATION';
END


IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'CANAL_CREATION')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]
	([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])

	select id_constEvenementAttributGroup,'CANAL_CREATION','Canal cr�ation',1,0,0,9,'Action r�alis�e' from ConstEvenementAttributGroup where nom = 'G�n�ral';
END

--FIL-621 Nouvelle ressource JeSignale

IF NOT EXISTS(select * from [ConstRessourcesFamille] where code = 'VELO')
BEGIN
INSERT INTO [dbo].[ConstRessourcesFamille]([code],[libelle],[ponderation])
     VALUES('VELO','V�lo',1)
END

IF NOT EXISTS(select * from [ConstRessourcesFamille] where code = 'VELO_PARC')
BEGIN
INSERT INTO [dbo].[ConstRessourcesFamille]([code],[libelle],[ponderation])
     VALUES('VELO_PARC','V�lo parc',1)
END

IF NOT EXISTS(select * from [ConstRessourcesFamille] where code = 'VELO_STATION')
BEGIN
INSERT INTO [dbo].[ConstRessourcesFamille]([code],[libelle],[ponderation])
     VALUES('VELO_STATION','V�lo station',1)
END

IF NOT EXISTS(select * from [ConstRessourcesFamille] where code = 'HORODATEUR')
BEGIN
INSERT INTO [dbo].[ConstRessourcesFamille]([code],[libelle],[ponderation])
     VALUES('HORODATEUR','Horodateur',1)
END

IF NOT EXISTS(select * from [ConstRessourcesFamille] where code = 'STATION_TRAMWAY')
BEGIN
INSERT INTO [dbo].[ConstRessourcesFamille]([code],[libelle],[ponderation])
     VALUES('STATION_TRAMWAY','Station tramway',1)
END

IF NOT EXISTS(select * from [ConstRessourcesFamille] where code = 'STATION_BUS')
BEGIN
INSERT INTO [dbo].[ConstRessourcesFamille]([code],[libelle],[ponderation])
     VALUES('STATION_BUS','Station bus',1)
END

IF NOT EXISTS(select * from [ConstRessourcesFamille] where code = 'VOITURE')
BEGIN
INSERT INTO [dbo].[ConstRessourcesFamille]([code],[libelle],[ponderation])
     VALUES('VOITURE','Voiture',1)
END

--FIL-755 D�doublonnage

IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'OCCURENCE_COUNT')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (1,'OCCURENCE_COUNT','Occurence',1,0,0,9,'')
END

--FIL-782 Impressions

IF NOT EXISTS(select * from [COM] where [nomAlerteCOM] = 'Imprimante')
INSERT INTO [dbo].[COM]
           ([nomAlerteCOM])
     VALUES
           ('Imprimante')


--- FIL-789 Terminus partiel

IF NOT EXISTS(select * from [ConstEvenementExploitConseq] where [code] = 'TERMINUS_PARTIEL')
INSERT INTO [dbo].[ConstEvenementExploitConseq]
           ([code])
     VALUES
           ('TERMINUS_PARTIEL')

  --------------------------------------------------------
--------------------------------------------------------
------------        NUMERO DE VERSION        -----------
--------------------------------------------------------
--------------------------------------------------------
UPDATE [dbo].[DBinfos]
   SET [valeur] = '3.0.1'
 WHERE nom = 'version';






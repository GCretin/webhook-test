USE [s2002m401]
GO

INSERT INTO [dbo].[ConstQueryCriteriaGroup]
           ([nom])
     VALUES
           ('Action Historique')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstQueryCriteria]
           ([dataType]
           ,[libelle]
           ,[id_queryCriteria_queryCriteriaGroup]
           ,[code]
           ,[isEligibleOnTheFlight]
           ,[description])
     VALUES
           ('SELECT_OBJECT'
           ,'HIstorique cr�ateur'
           ,5
           ,'UTILISATEUR_CREATEUR'
           ,0
           ,'Utilisateur ayant ouvert un �v�nement')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstQueryCriteria]
           ([dataType]
           ,[libelle]
           ,[id_queryCriteria_queryCriteriaGroup]
           ,[code]
           ,[isEligibleOnTheFlight]
           ,[description])
     VALUES
           ('SELECT_OBJECT'
           ,'Historique utilisateur'
           ,5
           ,'UTILISATEUR_ACTION'
           ,0
           ,'Utilisateur ayant effectu� une action sur un �v�nement')
GO



USE [s2002m401]
GO

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])
     VALUES
           (1
           ,40)
GO


USE [s2002m401]
GO

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])
     VALUES
           (1
           ,41)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries])
     VALUES
           ('ACTION_HISTO_PAR_UTILISATEUR'
           ,'Action Histo. par util.'
           ,'Nombre d''actions histo. par utilisateur'
           ,1)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries])
     VALUES
           ('ACTION_HISTO_PAR_DATE'
           ,'Action HIsto. par date'
           ,'Nombre d''actions histo. par date'
           ,1)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstEvenementControl]
           ([code]
           ,[libelle])
     VALUES
           ('ARRET_LIEU'
           ,'Arr�t du lieu')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[CfgEvenementControlRule]
           ([id_cfgEvenementControlRule_constControl]
           ,[id_cfgEvenementControlRule_constState]
           ,[id_cfgEvenementControlRule_constRessourceFamille]
           ,[id_cfgEvenementControlRule_reseaus101m401]
           ,[isDefaut])
     VALUES
           (5
           ,2
           ,6
           ,1
           ,0)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[CfgEvenementControlRule]
           ([id_cfgEvenementControlRule_constControl]
           ,[id_cfgEvenementControlRule_constState]
           ,[id_cfgEvenementControlRule_constRessourceFamille]
           ,[id_cfgEvenementControlRule_reseaus101m401]
           ,[isDefaut])
     VALUES
           (5
           ,2
           ,7
           ,1
           ,0)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[CfgEvenementControlRule]
           ([id_cfgEvenementControlRule_constControl]
           ,[id_cfgEvenementControlRule_constState]
           ,[id_cfgEvenementControlRule_constRessourceFamille]
           ,[id_cfgEvenementControlRule_reseaus101m401]
           ,[isDefaut])
     VALUES
           (5
           ,2
           ,8
           ,1
           ,0)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[CfgEvenementControlRule]
           ([id_cfgEvenementControlRule_constControl]
           ,[id_cfgEvenementControlRule_constState]
           ,[id_cfgEvenementControlRule_constRessourceFamille]
           ,[id_cfgEvenementControlRule_reseaus101m401]
           ,[isDefaut])
     VALUES
           (5
           ,2
           ,13
           ,1
           ,0)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[CfgEvenementControlRule]
           ([id_cfgEvenementControlRule_constControl]
           ,[id_cfgEvenementControlRule_constState]
           ,[id_cfgEvenementControlRule_constRessourceFamille]
           ,[id_cfgEvenementControlRule_reseaus101m401]
           ,[isDefaut])
     VALUES
           (5
           ,2
           ,14
           ,1
           ,0)
GO















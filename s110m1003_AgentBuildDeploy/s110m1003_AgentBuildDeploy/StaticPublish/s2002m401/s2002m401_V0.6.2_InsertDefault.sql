USE [s2002m401]
GO

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[AleaAction] ON 

GO
INSERT [dbo].[AleaAction] ([id_aleaAction], [nom]) VALUES (1, N'Ouverture')
GO
INSERT [dbo].[AleaAction] ([id_aleaAction], [nom]) VALUES (2, N'Fermeture')
GO
INSERT [dbo].[AleaAction] ([id_aleaAction], [nom]) VALUES (3, N'Modification')
GO
SET IDENTITY_INSERT [dbo].[AleaAction] OFF
GO

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[AleaEtat] ON 

GO
INSERT [dbo].[AleaEtat] ([id_aleaEtat], [nom]) VALUES (1, N'Ouvert')
GO
INSERT [dbo].[AleaEtat] ([id_aleaEtat], [nom]) VALUES (2, N'Fermé')
GO
SET IDENTITY_INSERT [dbo].[AleaEtat] OFF
GO

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[AlerteTrigger] ON 

GO
INSERT [dbo].[AlerteTrigger] ([id_aleaTrigger], [nomAleaTrigger]) VALUES (1, N'Ouverture')
GO
INSERT [dbo].[AlerteTrigger] ([id_aleaTrigger], [nomAleaTrigger]) VALUES (2, N'Fermeture')
GO
INSERT [dbo].[AlerteTrigger] ([id_aleaTrigger], [nomAleaTrigger]) VALUES (3, N'Modification')
GO
SET IDENTITY_INSERT [dbo].[AlerteTrigger] OFF
GO

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[COM] ON 

GO
INSERT [dbo].[COM] ([id_aleaCOM], [nomAlerteCOM]) VALUES (1, N'SMS')
GO
INSERT [dbo].[COM] ([id_aleaCOM], [nomAlerteCOM]) VALUES (2, N'Mail')
GO
SET IDENTITY_INSERT [dbo].[COM] OFF
GO

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[DBinfos] ON 

GO
INSERT [dbo].[DBinfos] ([id_dbinfos], [nom], [valeurs]) VALUES (1, N'version', N'0.6.2')
GO
INSERT [dbo].[DBinfos] ([id_dbinfos], [nom], [valeurs]) VALUES (2, N'uid', N's2002m401')
GO
INSERT [dbo].[DBinfos] ([id_dbinfos], [nom], [valeurs]) VALUES (3, N'env', N'----')
GO
SET IDENTITY_INSERT [dbo].[DBinfos] OFF
GO

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[ExploitationConseqType] ON 

GO
INSERT [dbo].[ExploitationConseqType] ([id_exploitationConseqType], [nom]) VALUES (1, N'Sans conséquences')
GO
INSERT [dbo].[ExploitationConseqType] ([id_exploitationConseqType], [nom]) VALUES (2, N'Retard')
GO
INSERT [dbo].[ExploitationConseqType] ([id_exploitationConseqType], [nom]) VALUES (3, N'Interruption parcours')
GO
INSERT [dbo].[ExploitationConseqType] ([id_exploitationConseqType], [nom]) VALUES (4, N'Interruption lignes(s)')
GO
INSERT [dbo].[ExploitationConseqType] ([id_exploitationConseqType], [nom]) VALUES (5, N'Interruption réseau')
GO
SET IDENTITY_INSERT [dbo].[ExploitationConseqType] OFF
GO

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[HistoriqueCause] ON 

GO
INSERT [dbo].[HistoriqueCause] ([id_historiqueCause], [nom]) VALUES (1, N'Automatique')
GO
INSERT [dbo].[HistoriqueCause] ([id_historiqueCause], [nom]) VALUES (2, N'Manuel')
GO
SET IDENTITY_INSERT [dbo].[HistoriqueCause] OFF
GO

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[HumiditeInfos] ON 

GO
INSERT [dbo].[HumiditeInfos] ([id_humiditeInfos], [nom]) VALUES (1, N'Sec')
GO
INSERT [dbo].[HumiditeInfos] ([id_humiditeInfos], [nom]) VALUES (2, N'Humide')
GO
INSERT [dbo].[HumiditeInfos] ([id_humiditeInfos], [nom]) VALUES (3, N'Trempé')
GO
INSERT [dbo].[HumiditeInfos] ([id_humiditeInfos], [nom]) VALUES (4, N'Verglas')
GO
INSERT [dbo].[HumiditeInfos] ([id_humiditeInfos], [nom]) VALUES (5, N'Neige')
GO
SET IDENTITY_INSERT [dbo].[HumiditeInfos] OFF
GO

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[InterventionEtat] ON 

GO
INSERT [dbo].[InterventionEtat] ([id_interventionEtat], [nom]) VALUES (1, N'En cours')
GO
INSERT [dbo].[InterventionEtat] ([id_interventionEtat], [nom]) VALUES (2, N'En attente')
GO
INSERT [dbo].[InterventionEtat] ([id_interventionEtat], [nom]) VALUES (3, N'Terminé')
GO
SET IDENTITY_INSERT [dbo].[InterventionEtat] OFF

GO
SET IDENTITY_INSERT [dbo].[TemperatureInfos] ON 

GO
INSERT [dbo].[TemperatureInfos] ([id_temperatureInfos], [nom]) VALUES (1, N'-20 C à -10 C')
GO
INSERT [dbo].[TemperatureInfos] ([id_temperatureInfos], [nom]) VALUES (2, N'-10 C à 0 C')
GO
INSERT [dbo].[TemperatureInfos] ([id_temperatureInfos], [nom]) VALUES (3, N'0 C à 10 C')
GO
INSERT [dbo].[TemperatureInfos] ([id_temperatureInfos], [nom]) VALUES (4, N'10 C à 20 C')
GO
INSERT [dbo].[TemperatureInfos] ([id_temperatureInfos], [nom]) VALUES (5, N'20 C à 30 C')
GO
INSERT [dbo].[TemperatureInfos] ([id_temperatureInfos], [nom]) VALUES (6, N'30 C à 40 C')
GO
SET IDENTITY_INSERT [dbo].[TemperatureInfos] OFF
GO

GO
SET IDENTITY_INSERT [dbo].[MaterielEtat] ON 

GO
INSERT [dbo].[MaterielEtat] ([id_materielEtat], [nom]) VALUES (1, N'Inconnu')
GO
SET IDENTITY_INSERT [dbo].[MaterielEtat] OFF
GO



﻿/*
Script de déploiement pour s2002m401

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "s2002m401"
:setvar DefaultFilePrefix "s2002m401"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.DEVSQLSP\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.DEVSQLSP\MSSQL\DATA\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Suppression de [dbo].[VueCfgAlerteAleaDiffusionListReseau].[MS_DiagramPane1]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPane1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueCfgAlerteAleaDiffusionListReseau';


GO
PRINT N'Suppression de [dbo].[VueCfgAlerteAleaDiffusionListReseau].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPaneCount', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueCfgAlerteAleaDiffusionListReseau';


GO
PRINT N'Suppression de [dbo].[VueCfgAlerteAleaDiffusionListReseau]...';


GO
DROP VIEW [dbo].[VueCfgAlerteAleaDiffusionListReseau];


GO
PRINT N'Modification de [dbo].[AleaHistorique]...';


GO
ALTER TABLE [dbo].[AleaHistorique]
    ADD [id_aleaHistorique_Evenement] INT NULL;


GO
PRINT N'Modification de [dbo].[AleaListe]...';


GO
ALTER TABLE [dbo].[AleaListe]
    ADD [id_aleaListe_ressource] INT NULL;


GO
PRINT N'Création de [dbo].[ConstEvenementAttribut]...';


GO
CREATE TABLE [dbo].[ConstEvenementAttribut] (
    [id_constEvenementAttribut]       INT            IDENTITY (1, 1) NOT NULL,
    [id_constEvenementAttribut_group] INT            NOT NULL,
    [code]                            NVARCHAR (MAX) NOT NULL,
    [libelle]                         NVARCHAR (MAX) NOT NULL,
    [isEligibleQueryResult]           INT            NOT NULL,
    [isEligibleXaxis]                 INT            NOT NULL,
    [isEligibleSeriesAnalyse]         INT            NOT NULL,
    [rang]                            INT            NOT NULL,
    [header]                          NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_ContEvenementAttribut] PRIMARY KEY CLUSTERED ([id_constEvenementAttribut] ASC)
);


GO
PRINT N'Création de [dbo].[ConstEvenementAttributGroup]...';


GO
CREATE TABLE [dbo].[ConstEvenementAttributGroup] (
    [id_constEvenementAttributGroup] INT            IDENTITY (1, 1) NOT NULL,
    [nom]                            NVARCHAR (MAX) NOT NULL,
    [rang]                           INT            NOT NULL,
    CONSTRAINT [PK_ConstEvenementAttributGroup] PRIMARY KEY CLUSTERED ([id_constEvenementAttributGroup] ASC)
);


GO
PRINT N'Création de [dbo].[EvenementHistoriqueView]...';


GO
CREATE TABLE [dbo].[EvenementHistoriqueView] (
    [id_EvenementHistoriqueView]  INT           IDENTITY (1, 1) NOT NULL,
    [id_utilisateur]              INT           NOT NULL,
    [loginUtilisateur]            NVARCHAR (50) NOT NULL,
    [date]                        DATE          NOT NULL,
    [heure]                       TIME (7)      NOT NULL,
    [id_aleaHistorique_Evenement] INT           NOT NULL,
    CONSTRAINT [PK_EvenementHistoriqueView] PRIMARY KEY CLUSTERED ([id_EvenementHistoriqueView] ASC)
);


GO
PRINT N'Création de [dbo].[LnkQueryEvenementAttribut]...';


GO
CREATE TABLE [dbo].[LnkQueryEvenementAttribut] (
    [id_lnkQueryEvenementAttribut]                   INT IDENTITY (1, 1) NOT NULL,
    [id_lnkQueryEvenementAttribut_Query]             INT NOT NULL,
    [id_lnkQueryEvenementAttribut_EvenementAttribut] INT NOT NULL,
    [rang]                                           INT NOT NULL,
    CONSTRAINT [PK_LnkQueryEvenementAttribut] PRIMARY KEY CLUSTERED ([id_lnkQueryEvenementAttribut] ASC)
);


GO
PRINT N'Création de [dbo].[DF_ConstEvenementAttribut_header]...';


GO
ALTER TABLE [dbo].[ConstEvenementAttribut]
    ADD CONSTRAINT [DF_ConstEvenementAttribut_header] DEFAULT ('') FOR [header];


GO
PRINT N'Création de [dbo].[FK_ConstEvenementAttribut_ConstEvenementAttributGroup]...';


GO
ALTER TABLE [dbo].[ConstEvenementAttribut] WITH NOCHECK
    ADD CONSTRAINT [FK_ConstEvenementAttribut_ConstEvenementAttributGroup] FOREIGN KEY ([id_constEvenementAttribut_group]) REFERENCES [dbo].[ConstEvenementAttributGroup] ([id_constEvenementAttributGroup]);


GO
PRINT N'Création de [dbo].[FK_EvenementHistoriqueView_Evenement]...';


GO
ALTER TABLE [dbo].[EvenementHistoriqueView] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementHistoriqueView_Evenement] FOREIGN KEY ([id_aleaHistorique_Evenement]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
PRINT N'Création de [dbo].[FK_LnkQueryEvenementAttribut_EvenementAttribut]...';


GO
ALTER TABLE [dbo].[LnkQueryEvenementAttribut] WITH NOCHECK
    ADD CONSTRAINT [FK_LnkQueryEvenementAttribut_EvenementAttribut] FOREIGN KEY ([id_lnkQueryEvenementAttribut_EvenementAttribut]) REFERENCES [dbo].[ConstEvenementAttribut] ([id_constEvenementAttribut]);


GO
PRINT N'Création de [dbo].[FK_LnkQueryEvenementAttribut_Query]...';


GO
ALTER TABLE [dbo].[LnkQueryEvenementAttribut] WITH NOCHECK
    ADD CONSTRAINT [FK_LnkQueryEvenementAttribut_Query] FOREIGN KEY ([id_lnkQueryEvenementAttribut_Query]) REFERENCES [dbo].[Query] ([id_query]);


GO
PRINT N'Création de [dbo].[FK_AleaHistorique_Evenement]...';


GO
ALTER TABLE [dbo].[AleaHistorique] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaHistorique_Evenement] FOREIGN KEY ([id_aleaHistorique_Evenement]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
PRINT N'Création de [dbo].[FK_AleaListe_Ressource]...';


GO
ALTER TABLE [dbo].[AleaListe] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaListe_Ressource] FOREIGN KEY ([id_aleaListe_ressource]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
PRINT N'Actualisation de [dbo].[VueAleaActionHistorique]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[VueAleaActionHistorique]';


GO
PRINT N'Actualisation de [dbo].[VueAleaActionHistoriqueMateriel]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[VueAleaActionHistoriqueMateriel]';


GO
PRINT N'Actualisation de [dbo].[VueAleaListe]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[VueAleaListe]';


GO
PRINT N'Actualisation de [dbo].[VueAleaListeMateriel]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[VueAleaListeMateriel]';


GO
PRINT N'Actualisation de [dbo].[VueMaterielListeCountAleaEtat]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[VueMaterielListeCountAleaEtat]';


GO
PRINT N'Vérification de données existantes par rapport aux nouvelles contraintes';


GO
USE [$(DatabaseName)];


GO
ALTER TABLE [dbo].[ConstEvenementAttribut] WITH CHECK CHECK CONSTRAINT [FK_ConstEvenementAttribut_ConstEvenementAttributGroup];

ALTER TABLE [dbo].[EvenementHistoriqueView] WITH CHECK CHECK CONSTRAINT [FK_EvenementHistoriqueView_Evenement];

ALTER TABLE [dbo].[LnkQueryEvenementAttribut] WITH CHECK CHECK CONSTRAINT [FK_LnkQueryEvenementAttribut_EvenementAttribut];

ALTER TABLE [dbo].[LnkQueryEvenementAttribut] WITH CHECK CHECK CONSTRAINT [FK_LnkQueryEvenementAttribut_Query];

ALTER TABLE [dbo].[AleaHistorique] WITH CHECK CHECK CONSTRAINT [FK_AleaHistorique_Evenement];

ALTER TABLE [dbo].[AleaListe] WITH CHECK CHECK CONSTRAINT [FK_AleaListe_Ressource];


GO
PRINT N'Mise à jour terminée.';


GO

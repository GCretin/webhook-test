﻿/*
Script de déploiement pour s2002m401_vs-sqp1_2017-06-27_v1.4.0

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "s2002m401"
:setvar DefaultFilePrefix "s2002m401"
--:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.DEVSQLSP\MSSQL\DATA\"
--:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.DEVSQLSP\MSSQL\DATA\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Modification de [dbo].[EvenementExploitationConseq]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq]
    ADD [ligne_code]           NVARCHAR (MAX) NULL,
        [ligne_nom]            NVARCHAR (MAX) NULL,
        [parcours_code]        NVARCHAR (MAX) NULL,
        [parcours_nom]         NVARCHAR (MAX) NULL,
        [parcours_description] NVARCHAR (MAX) NULL;


GO
PRINT N'Modification de [dbo].[EvenementLieu]...';


GO
ALTER TABLE [dbo].[EvenementLieu]
    ADD [ligne_code]           NVARCHAR (50)  NULL,
        [ligne_nom]            NVARCHAR (50)  NULL,
        [parcours_code]        NVARCHAR (MAX) NULL,
        [parcours_nom]         NVARCHAR (MAX) NULL,
        [parcours_description] NVARCHAR (MAX) NULL;


GO
PRINT N'Création de [dbo].[QueryTriggerSynchroneHistorique]...';


GO
CREATE TABLE [dbo].[QueryTriggerSynchroneHistorique] (
    [id_QueryTriggerSynchroneHistorique]                       INT      IDENTITY (1, 1) NOT NULL,
    [id_QueryTriggerSynchroneHistorique_queryTriggerSynchrone] INT      NOT NULL,
    [date]                                                     DATE     NOT NULL,
    [heure]                                                    TIME (7) NOT NULL,
    CONSTRAINT [PK_QueryTriggerSynchroneHistorique] PRIMARY KEY CLUSTERED ([id_QueryTriggerSynchroneHistorique] ASC)
);


GO
PRINT N'Création de [dbo].[FK_QueryTriggerSynchroneHistorique_QueryTriggerSynchrone]...';


GO
ALTER TABLE [dbo].[QueryTriggerSynchroneHistorique] WITH NOCHECK
    ADD CONSTRAINT [FK_QueryTriggerSynchroneHistorique_QueryTriggerSynchrone] FOREIGN KEY ([id_QueryTriggerSynchroneHistorique_queryTriggerSynchrone]) REFERENCES [dbo].[QueryTriggerSynchrone] ([id_queryTriggerSynchrone]);


GO
PRINT N'Vérification de données existantes par rapport aux nouvelles contraintes';


GO
USE [$(DatabaseName)];


GO
ALTER TABLE [dbo].[QueryTriggerSynchroneHistorique] WITH CHECK CHECK CONSTRAINT [FK_QueryTriggerSynchroneHistorique_QueryTriggerSynchrone];


GO
PRINT N'Mise à jour terminée.';


GO

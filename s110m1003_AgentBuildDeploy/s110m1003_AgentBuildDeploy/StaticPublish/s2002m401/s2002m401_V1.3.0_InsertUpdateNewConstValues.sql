
--s2002m401

use s2002m401;

GO

UPDATE [dbo].[ConstQueryCriteria]
   SET [libelle] = 'Ressource (�v�nement + affect. + cons�quences)'
      ,[description] = 'Ressource base de l''�v�nement + affectation + cons�quences d''exploitation (ex : bus 218, tramway 1002, conducteur,...)'
 WHERE code = 'ALL_RESSOURCE'
GO

GO

INSERT INTO [dbo].[ConstQueryCriteria]
           ([dataType]
           ,[libelle]
           ,[id_queryCriteria_queryCriteriaGroup]
           ,[code]
           ,[isEligibleOnTheFlight]
           ,[description])
     VALUES
           ('SELECT_OBJECT'
           ,'Ressource (�v�nement + affect.)'
           ,3
           ,'RESSOURCE_BASE_AND_AFFECTATION'
           ,1
           ,'Ressource base de l''�v�nement + affectation (ex : bus 218, tramway 1002, conducteur,...)')
GO

GO

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])
     VALUES
           (1
           ,38)
GO



GO

INSERT INTO [dbo].[ConstQueryCriteria]
           ([dataType]
           ,[libelle]
           ,[id_queryCriteria_queryCriteriaGroup]
           ,[code]
           ,[isEligibleOnTheFlight]
           ,[description])
     VALUES
           ('SELECT_OBJECT'
           ,'Organe maintenance'
           ,4
           ,'MAINTENANCE_ORGANE'
           ,0
           ,'Organes de maintenance')
GO

GO

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])
     VALUES
           (1
           ,39)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries])
     VALUES
           ('DATE_LIGNE_SOUS_CATEGORIE_PAR_RESSOURCE_CR'
           ,'Date Ligne Cat�g. CR'
           ,'Date Ligne et sous cat�gorie par CR'
           ,1)
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries])
     VALUES
           ('NB_EVENEMENT_SOUS_CATEGORIE__RESSOURCE_CR'
           ,'Nb. �v�nement CR'
           ,'Nombre d''�venement sous cat�gorie par CR'
           ,1)
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries])
     VALUES
           ('NB_EVENEMENT_PAR_SOUS_CATEGORIE'
           ,'Ev�nement par sous cat�g'
           ,'Nombre d''�v�nement par sous cat�gorie'
           ,0)
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries])
     VALUES
           ('NB_EVENEMENT_PAR_SOUS_CLASSE'
           ,'Ev�nement par sous classe'
           ,'Nombre d''�venement par sous classe'
           ,0)
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries])
     VALUES
           ('NB_EVENEMENT_PAR_SOUS_CLASSE_SOUS_CATEGORIE'
           ,'Ev�. par sous classe categ.'
           ,'Nombre d''�venement par sous classe / cat�gorie'
           ,0)
GO
INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries])
     VALUES
           ('NB_EVENEMENT_PAR_CLASSE_CATEGORIE'
           ,'Ev�. par classe cat�gorie'
           ,'Nombre d''�venement par classe / cat�gorie'
           ,0)
GO
INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries])
     VALUES
           ('NB_EVENEMENT_PAR_LIGNE'
           ,'Ev�nement par ligne'
           ,'Nombre d''�v�nement par ligne'
           ,0)
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries])
     VALUES
           ('NB_EVENEMENT_PAR_CATEGORIE_RESSOURCE'
           ,'Ev�nement par cat�g ress'
           ,'Nombre d''�v�nement par cat�gorie ressource'
           ,0)
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries])
     VALUES
           ('NB_EVENEMENT_PAR_RESSOURCE'
           ,'Ev�nement par ressource'
           ,'Nombre d''�v�nement par ressource'
           ,0)
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries])
     VALUES
           ('NB_EVENEMENT_MOY_PAR_JOUR_SEMAINE'
           ,'Ev�nement par jour semaine'
           ,'Nombre d''�v�nement moyen par jour de la semaine'
           ,0)
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries])
     VALUES
           ('NB_EVENEMENT_MOY_PAR_HEURE'
           ,'Ev�nement par heure'
           ,'Nombre d''�v�nement moyen par heure'
           ,0)
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries])
     VALUES
           ('NB_EVENEMENT_PAR_MANEUVRE'
           ,'Ev�nement par manoeuvre'
           ,'Nombre d''�v�nement par manoeuvre'
           ,0)
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries])
     VALUES
           ('NB_EVENEMENT_PAR_ORGANE'
           ,'Ev�nement par organe'
           ,'Nombre d''�venement par organe'
           ,0)
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries])
     VALUES
           ('NB_EVENEMENT_PAR_ORGANE_NIV_1'
           ,'Ev�nement par organe niv1'
           ,'Nombre d''�v�nement par organe niv. 1'
           ,0)
GO

GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries])
     VALUES
           ('NB_EVENEMENT_PAR_CATEG_RESSOURCE_ORGANE'
           ,'Ev�. par cat�g. ress. organ'
           ,'Nombre d''�v�nement par cat�g. ress. / organe'
           ,0)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstRessourcesFamille]
           ([code]
           ,[libelle])
     VALUES
           ('RESEAU_SOUS_TRAITANT'
           ,'R�seau sous-traitants')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstEvenementFamille]
           ([code]
           ,[libelle])
     VALUES
           ('SIGNALEMENT'
           ,'Signalement')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstEvenementFamille]
           ([code]
           ,[libelle])
     VALUES
           ('DEMANDE_INTERVENTION'
           ,'Demande d''intervention')
GO

















/*
Script de déploiement pour s2002m401

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "s2002m401"
:setvar DefaultFilePrefix "s2002m401"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.RLARECETTE\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.RLARECETTE\MSSQL\DATA\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO

IF (SELECT OBJECT_ID('tempdb..#tmpErrors')) IS NOT NULL DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
GO
BEGIN TRANSACTION
GO
PRINT N'Suppression de [dbo].[FK_EvenementExploitationConseq_EvenementList]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] DROP CONSTRAINT [FK_EvenementExploitationConseq_EvenementList];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_EvenementExploitationConseq_EvenementExploitationConseq]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] DROP CONSTRAINT [FK_EvenementExploitationConseq_EvenementExploitationConseq];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_EvenementExploitationConseq_ExploitationConseqManoeuvre]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] DROP CONSTRAINT [FK_EvenementExploitationConseq_ExploitationConseqManoeuvre];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_EvenementExploitationConseq_RessourceHomme]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] DROP CONSTRAINT [FK_EvenementExploitationConseq_RessourceHomme];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_EvenementExploitationConseq_RessourceVehicule]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] DROP CONSTRAINT [FK_EvenementExploitationConseq_RessourceVehicule];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Début de la régénération de la table [dbo].[EvenementExploitationConseq]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_EvenementExploitationConseq] (
    [id_evenementExploitationConseq]                             INT            IDENTITY (1, 1) NOT NULL,
    [id_evenementExploitationConseq_exploitationConseqManoeuvre] INT            NOT NULL,
    [id_evenementExploitationConseq_evenementListe]              INT            NOT NULL,
    [id_evenementExploitationConseq_lignes2006m401]              INT            NULL,
    [id_evenementExploitationConseq_parcourss2006m401]           INT            NULL,
    [id_evenementExploitationConseq_directions2006m401]          INT            NULL,
    [id_eevenementExploitationConseq_arrets2006m401_1]           INT            NULL,
    [id_eevenementExploitationConseq_arrets2006m401_2]           INT            NULL,
    [heure_1]                                                    TIME (7)       NULL,
    [heure_2]                                                    TIME (7)       NULL,
    [nb_departs]                                                 INT            NULL,
    [nb_courses]                                                 NVARCHAR (50)  NULL,
    [nb_courses_auto]                                            FLOAT (53)     NULL,
    [nb_km]                                                      FLOAT (53)     NULL,
    [nb_km_auto]                                                 FLOAT (53)     NULL,
    [id_evenementExploitationConseq_svs2006m401_auto]            INT            NULL,
    [sv_proportion]                                              INT            CONSTRAINT [DF_EvenementExploitationConseq_sv_proportion] DEFAULT ((0)) NOT NULL,
    [is_sv_nonAssure]                                            INT            CONSTRAINT [DF_EvenementExploitationConseq_isSVnonAssure] DEFAULT ((0)) NOT NULL,
    [décalage_min]                                               FLOAT (53)     NULL,
    [décalage_max]                                               FLOAT (53)     NULL,
    [id_evenementExploitationConseq_parent]                      INT            NULL,
    [id_eevenementExploitationConseq_arretsDepart2006m401_1]     INT            NULL,
    [id_eevenementExploitationConseq_arretsDepart2006m401_2]     INT            NULL,
    [withRetombe]                                                INT            NULL,
    [id_eevenementExploitationConseq_ressourceHomme]             INT            NULL,
    [id_eevenementExploitationConseq_ressourceVehicule]          INT            NULL,
    [observation]                                                NVARCHAR (MAX) NULL,
    [ligne_code]                                                 NVARCHAR (MAX) NULL,
    [ligne_nom]                                                  NVARCHAR (MAX) NULL,
    [parcours_code]                                              NVARCHAR (MAX) NULL,
    [parcours_nom]                                               NVARCHAR (MAX) NULL,
    [parcours_description]                                       NVARCHAR (MAX) NULL,
    [id_utilisateur]                                             INT            NULL,
    [loginUtilisateur]                                           NVARCHAR (MAX) NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK__Exploita__0C4C4FF61412A0791] PRIMARY KEY CLUSTERED ([id_evenementExploitationConseq] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[EvenementExploitationConseq])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementExploitationConseq] ON;
        INSERT INTO [dbo].[tmp_ms_xx_EvenementExploitationConseq] ([id_evenementExploitationConseq], [id_evenementExploitationConseq_exploitationConseqManoeuvre], [id_evenementExploitationConseq_evenementListe], [id_evenementExploitationConseq_lignes2006m401], [id_evenementExploitationConseq_parcourss2006m401], [id_evenementExploitationConseq_directions2006m401], [id_eevenementExploitationConseq_arrets2006m401_1], [id_eevenementExploitationConseq_arrets2006m401_2], [heure_1], [heure_2], [nb_departs], [nb_courses], [nb_courses_auto], [nb_km], [nb_km_auto], [id_evenementExploitationConseq_svs2006m401_auto], [décalage_min], [décalage_max], [id_evenementExploitationConseq_parent], [id_eevenementExploitationConseq_arretsDepart2006m401_1], [id_eevenementExploitationConseq_arretsDepart2006m401_2], [withRetombe], [id_eevenementExploitationConseq_ressourceHomme], [id_eevenementExploitationConseq_ressourceVehicule], [observation], [ligne_code], [ligne_nom], [parcours_code], [parcours_nom], [parcours_description], [id_utilisateur], [loginUtilisateur])
        SELECT   [id_evenementExploitationConseq],
                 [id_evenementExploitationConseq_exploitationConseqManoeuvre],
                 [id_evenementExploitationConseq_evenementListe],
                 [id_evenementExploitationConseq_lignes2006m401],
                 [id_evenementExploitationConseq_parcourss2006m401],
                 [id_evenementExploitationConseq_directions2006m401],
                 [id_eevenementExploitationConseq_arrets2006m401_1],
                 [id_eevenementExploitationConseq_arrets2006m401_2],
                 [heure_1],
                 [heure_2],
                 [nb_departs],
                 [nb_courses],
                 [nb_courses_auto],
                 [nb_km],
                 [nb_km_auto],
                 [id_evenementExploitationConseq_svs2006m401_auto],
                 [décalage_min],
                 [décalage_max],
                 [id_evenementExploitationConseq_parent],
                 [id_eevenementExploitationConseq_arretsDepart2006m401_1],
                 [id_eevenementExploitationConseq_arretsDepart2006m401_2],
                 [withRetombe],
                 [id_eevenementExploitationConseq_ressourceHomme],
                 [id_eevenementExploitationConseq_ressourceVehicule],
                 [observation],
                 [ligne_code],
                 [ligne_nom],
                 [parcours_code],
                 [parcours_nom],
                 [parcours_description],
                 [id_utilisateur],
                 [loginUtilisateur]
        FROM     [dbo].[EvenementExploitationConseq]
        ORDER BY [id_evenementExploitationConseq] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementExploitationConseq] OFF;
    END

DROP TABLE [dbo].[EvenementExploitationConseq];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_EvenementExploitationConseq]', N'EvenementExploitationConseq';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK__Exploita__0C4C4FF61412A0791]', N'PK__Exploita__0C4C4FF61412A079', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementExploitationConseq_EvenementList]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementExploitationConseq_EvenementList] FOREIGN KEY ([id_evenementExploitationConseq_evenementListe]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementExploitationConseq_EvenementExploitationConseq]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementExploitationConseq_EvenementExploitationConseq] FOREIGN KEY ([id_evenementExploitationConseq_parent]) REFERENCES [dbo].[EvenementExploitationConseq] ([id_evenementExploitationConseq]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementExploitationConseq_ExploitationConseqManoeuvre]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementExploitationConseq_ExploitationConseqManoeuvre] FOREIGN KEY ([id_evenementExploitationConseq_exploitationConseqManoeuvre]) REFERENCES [dbo].[EvenementExploitationConseqManoeuvre] ([id_exploitationConseqManoeuvre]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementExploitationConseq_RessourceHomme]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementExploitationConseq_RessourceHomme] FOREIGN KEY ([id_eevenementExploitationConseq_ressourceHomme]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementExploitationConseq_RessourceVehicule]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementExploitationConseq_RessourceVehicule] FOREIGN KEY ([id_eevenementExploitationConseq_ressourceVehicule]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_AleaListe_EvenementEtat]...';


GO
ALTER TABLE [dbo].[AleaListe] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaListe_EvenementEtat] FOREIGN KEY ([id_aleaListe_aleaEtat]) REFERENCES [dbo].[EvenementEtat] ([id_evenementEtat]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO

IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT N'Succès de la mise à jour de la portion de base de données traitée.'
COMMIT TRANSACTION
END
ELSE PRINT N'Échec de la mise à jour de la portion de base de données traitée.'
GO
DROP TABLE #tmpErrors
GO
PRINT N'Vérification de données existantes par rapport aux nouvelles contraintes';


GO
USE [$(DatabaseName)];


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] WITH CHECK CHECK CONSTRAINT [FK_EvenementExploitationConseq_EvenementList];

ALTER TABLE [dbo].[EvenementExploitationConseq] WITH CHECK CHECK CONSTRAINT [FK_EvenementExploitationConseq_EvenementExploitationConseq];

ALTER TABLE [dbo].[EvenementExploitationConseq] WITH CHECK CHECK CONSTRAINT [FK_EvenementExploitationConseq_ExploitationConseqManoeuvre];

ALTER TABLE [dbo].[EvenementExploitationConseq] WITH CHECK CHECK CONSTRAINT [FK_EvenementExploitationConseq_RessourceHomme];

ALTER TABLE [dbo].[EvenementExploitationConseq] WITH CHECK CHECK CONSTRAINT [FK_EvenementExploitationConseq_RessourceVehicule];

ALTER TABLE [dbo].[AleaListe] WITH CHECK CHECK CONSTRAINT [FK_AleaListe_EvenementEtat];


GO
PRINT N'Mise à jour terminée.';


GO

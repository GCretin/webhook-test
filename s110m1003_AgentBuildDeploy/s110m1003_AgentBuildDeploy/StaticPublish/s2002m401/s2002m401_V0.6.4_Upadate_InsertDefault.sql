------------------------------------------------------------------------------------------------------------------------------------
--									0.6.4											 											  --
------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[ConstEvenementLieuType] ON 

GO
INSERT [dbo].[ConstEvenementLieuType] ([id_constEvenementLieuType], [code], [libelle]) VALUES (1, N'COM', N'Commercial')
GO
INSERT [dbo].[ConstEvenementLieuType] ([id_constEvenementLieuType], [code], [libelle]) VALUES (2, N'HLP', N'Haut-le-pied')
GO
INSERT [dbo].[ConstEvenementLieuType] ([id_constEvenementLieuType], [code], [libelle]) VALUES (3, N'NO-COM', N'Non commercial')
GO
SET IDENTITY_INSERT [dbo].[ConstEvenementLieuType] OFF
GO
SET IDENTITY_INSERT [dbo].[ConstRessourcesFamille] ON 

GO
INSERT [dbo].[ConstRessourcesFamille] ([id_constRessourceFamille], [code], [libelle]) VALUES (1, N'NONE', N'Aucune')
GO
INSERT [dbo].[ConstRessourcesFamille] ([id_constRessourceFamille], [code], [libelle]) VALUES (3, N'DEPOT', N'Dépôt')
GO
INSERT [dbo].[ConstRessourcesFamille] ([id_constRessourceFamille], [code], [libelle]) VALUES (4, N'PARKING', N'Parking')
GO
INSERT [dbo].[ConstRessourcesFamille] ([id_constRessourceFamille], [code], [libelle]) VALUES (5, N'LIGNE', N'Ligne')
GO
INSERT [dbo].[ConstRessourcesFamille] ([id_constRessourceFamille], [code], [libelle]) VALUES (6, N'BUS', N'Bus')
GO
INSERT [dbo].[ConstRessourcesFamille] ([id_constRessourceFamille], [code], [libelle]) VALUES (7, N'TRAMWAY', N'Tramway')
GO
INSERT [dbo].[ConstRessourcesFamille] ([id_constRessourceFamille], [code], [libelle]) VALUES (8, N'CONDUCTEUR', N'Conducteur')
GO
INSERT [dbo].[ConstRessourcesFamille] ([id_constRessourceFamille], [code], [libelle]) VALUES (9, N'LIEU_NO_COM', N'Lieu non commercial')
GO
SET IDENTITY_INSERT [dbo].[ConstRessourcesFamille] OFF
GO

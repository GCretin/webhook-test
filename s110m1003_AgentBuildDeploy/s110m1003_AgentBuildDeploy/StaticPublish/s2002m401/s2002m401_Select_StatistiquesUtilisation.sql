/****** Script de la commande SelectTopNRows à partir de SSMS  ******/
SELECT count(id_aleaListe) as 'Nb. événement', AleaEtat.nom as 'Etat'
  FROM [s2002m401].[dbo].[AleaListe], AleaEtat
  where id_aleaEtat= id_aleaListe_aleaEtat
  group by id_aleaListe_aleaEtat,AleaEtat.nom
  order by [Nb. événement] desc;

  SELECT count(id_aleaListe) as 'Nb. événement', AleaType.nom as 'Sous-catégorie'
  FROM [s2002m401].[dbo].[AleaListe], AleaType
  where id_aleaType= id_aleaListe_aleaType
  group by id_aleaListe_aleaType,AleaType.nom
  order by [Nb. événement] desc;

  SELECT count(id_aleaListe) as 'Nb. événement', MaterielType.nom as 'Sous-classe'
  FROM [s2002m401].[dbo].[AleaListe], MaterielListe,MaterielType
  where id_aleaListe_ressource= id_materielListe and id_materielList_materielType= id_materielType
  group by id_materielType,MaterielType.nom
  order by [Nb. événement] desc;


  select count(id_materielListe) as 'Nb. ressource' 
  from MaterielListe where deleted is null;

  select count(id_materielListe) as 'Nb. ressource',MaterielType.nom 
  from MaterielListe,MaterielType
  where deleted is null and id_materielList_materielType= id_materielType
  group by id_materielList_materielType,MaterielType.nom
  order by [Nb. ressource] desc;

  select count(id_aleaHistorique) as 'Nb. actions' 
  from AleaHistorique;

  select COUNT(*) as 'Nb. alerte événéments mail' from QueryTriggerAsynchoneHistorique;

  select COUNT(*) as 'Nb. rapports mail' from QueryTriggerSynchroneHistorique;


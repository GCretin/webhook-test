USE [s2002m401]




 
GO
INSERT INTO [dbo].[MaterielDetailCategorie] ([nom]) VALUES ('S 416 UL')
GO
INSERT INTO [dbo].[MaterielDetailCategorie] ([nom]) VALUES ('O405G')
GO
INSERT INTO [dbo].[MaterielDetailCategorie] ([nom]) VALUES ('JUMPER')
GO
INSERT INTO [dbo].[MaterielDetailCategorie] ([nom]) VALUES ('GX127 C')
GO
INSERT INTO [dbo].[MaterielDetailCategorie] ([nom]) VALUES ('GX127 L')
GO
INSERT INTO [dbo].[MaterielDetailCategorie] ([nom]) VALUES ('GX327 E3')
GO
INSERT INTO [dbo].[MaterielDetailCategorie] ([nom]) VALUES ('GX327 E4')
GO
INSERT INTO [dbo].[MaterielDetailCategorie] ([nom]) VALUES ('GX327 EEV')
GO
INSERT INTO [dbo].[MaterielDetailCategorie] ([nom]) VALUES ('URBANWAY')
GO
INSERT INTO [dbo].[MaterielDetailCategorie] ([nom]) VALUES ('CITARO')
GO
INSERT INTO [dbo].[MaterielDetailCategorie] ([nom]) VALUES ('GX217 E2')
GO
INSERT INTO [dbo].[MaterielDetailCategorie] ([nom]) VALUES ('GX217 E3')
GO
INSERT INTO [dbo].[MaterielDetailCategorie] ([nom]) VALUES ('MASTER')
GO
INSERT INTO [dbo].[MaterielDetailCategorie] ([nom]) VALUES ('R312')
GO
INSERT INTO [dbo].[MaterielDetailCategorie] ([nom]) VALUES ('GX 317')
GO
INSERT INTO [dbo].[MaterielDetailCategorie] ([nom]) VALUES ('AGORA')
GO
INSERT INTO [dbo].[MaterielDetailCategorie] ([nom]) VALUES ('Agora L GNV')
GO
INSERT INTO [dbo].[MaterielDetailCategorie] ([nom]) VALUES ('Citelis E3')
GO
INSERT INTO [dbo].[MaterielDetailCategorie] ([nom]) VALUES ('Citelis E4')
GO
INSERT INTO [dbo].[MaterielDetailCategorie] ([nom]) VALUES ('CITADIS 302')
GO
INSERT INTO [dbo].[MaterielDetailCategorie] ([nom]) VALUES ('GX 137 C')
GO

INSERT INTO [dbo].[CfgMaterielDetailCategorie]
           ([id_cfgMaterielDetailCategorie_MaterielDetailCategorie]
           ,[id_cfgMaterielDetailCategorie_Reseau])
    select id_materielDetailCategorie,1 from MaterielDetailCategorie
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[CfgRessourceSousClasse]
           ([id_cfgRessourceSousClasse_reseaus101m401]
           ,[id_cfgRessourceSousClasse_RessourceSousClasse])
     select 1,id_materielType from MaterielType
GO




use s2002m401;

--[AleaHistorique]
GO
CREATE NONCLUSTERED INDEX [_dta_index_AleaHistorique_6_629577281__K8_1_2_3_4_5_6_7] ON [dbo].[AleaHistorique]
(
	[id_aleaHistorique_Evenement] ASC
)
INCLUDE([id_aleaHistorique],[id_aleaHistorique_aleaAction],[id_utilisateur],[loginUtilisateur],[date],[heure],[details]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

--[[AleaListe]]
GO
CREATE STATISTICS [_dta_stat_661577395_1_2] ON [dbo].[AleaListe]([id_aleaListe], [id_aleaListe_aleaEtat])

GO
CREATE STATISTICS [_dta_stat_661577395_2_3_1] ON [dbo].[AleaListe]([id_aleaListe_aleaEtat], [id_aleaListe_aleaType], [id_aleaListe])

GO
CREATE STATISTICS [_dta_stat_661577395_3_5] ON [dbo].[AleaListe]([id_aleaListe_aleaType], [dateApparition])

GO
CREATE STATISTICS [_dta_stat_661577395_5_1_2_3] ON [dbo].[AleaListe]([dateApparition], [id_aleaListe], [id_aleaListe_aleaEtat], [id_aleaListe_aleaType])

GO
CREATE STATISTICS [_dta_stat_661577395_5_2_3] ON [dbo].[AleaListe]([dateApparition], [id_aleaListe_aleaEtat], [id_aleaListe_aleaType])

--[ConsignesExploitation]
GO
CREATE STATISTICS [_dta_stat_1589580701_2_8] ON [dbo].[ConsignesExploitation]([id_consignesExploitation_reseau_s101m401], [deleted])

GO
CREATE STATISTICS [_dta_stat_1589580701_8_1_2] ON [dbo].[ConsignesExploitation]([deleted], [id_consignesExploitation], [id_consignesExploitation_reseau_s101m401])

GO
CREATE STATISTICS [_dta_stat_1589580701_1_2] ON [dbo].[ConsignesExploitation]([id_consignesExploitation], [id_consignesExploitation_reseau_s101m401])

--[LnkEvenementMaintenanceOrganeEmplacement]
GO
CREATE NONCLUSTERED INDEX [_dta_index_LnkEvenementMaintenanceOrganeEmp_6_1906105831__K2_K3_1] ON [dbo].[LnkEvenementMaintenanceOrganeEmplacement]
(
	[id_lnkEvenementMaintenanceOrganeEmplacement_Organe] ASC,
	[id_lnkEvenementMaintenanceOrganeEmplacement_Emplacement] ASC
)
INCLUDE([id_lnkEvenementMaintenanceOrganeEmplacement]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

GO
CREATE STATISTICS [_dta_stat_1906105831_3_2] ON [dbo].[LnkEvenementMaintenanceOrganeEmplacement]([id_lnkEvenementMaintenanceOrganeEmplacement_Emplacement], [id_lnkEvenementMaintenanceOrganeEmplacement_Organe])

--[LnkEvenementMaintenanceOrganeSymptome]
GO
CREATE NONCLUSTERED INDEX [_dta_index_LnkEvenementMaintenanceOrganeSym_6_1938105945__K2_K3_1] ON [dbo].[LnkEvenementMaintenanceOrganeSymptome]
(
	[id_lnkEvenementMaintenanceOrganeSymptome_Organe] ASC,
	[id_lnkEvenementMaintenanceOrganeSymptome_Symptome] ASC
)
INCLUDE([id_lnkEvenementMaintenanceOrganeSymptome]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

--[LnkMaterielListeAleaListe]
GO
CREATE STATISTICS [_dta_stat_2002106173_3_2] ON [dbo].[LnkMaterielListeAleaListe]([id_materielListeAleaListe_aleaListe], [id_materielListeAleaListe_materielList])

--[[MaterielListe]
GO
CREATE STATISTICS [_dta_stat_142623551_8_5] ON [dbo].[MaterielListe]([id_reseau], [id_materielListe_materielCategorie])

GO
CREATE STATISTICS [_dta_stat_142623551_1_8] ON [dbo].[MaterielListe]([id_materielListe], [id_reseau])

GO
CREATE STATISTICS [_dta_stat_142623551_4_5_6] ON [dbo].[MaterielListe]([id_materielList_materielType], [id_materielListe_materielCategorie], [id_materielList_constRessourceFamille])

GO
CREATE STATISTICS [_dta_stat_142623551_8_4_5_6] ON [dbo].[MaterielListe]([id_reseau], [id_materielList_materielType], [id_materielListe_materielCategorie], [id_materielList_constRessourceFamille])

GO
CREATE STATISTICS [_dta_stat_142623551_8_6_4] ON [dbo].[MaterielListe]([id_reseau], [id_materielList_constRessourceFamille], [id_materielList_materielType])






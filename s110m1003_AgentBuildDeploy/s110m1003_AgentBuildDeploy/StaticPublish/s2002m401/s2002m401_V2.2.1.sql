USE [s2002m401]

---------------------G�n�ral 2.2.1 insertion des nouvelles valeurs avec v�rification-----------------------

---------------------FIL-552 Nouvelle section �tat -----------------------
GO
IF NOT EXISTS(select * from [ConstEvenementSection] where code = 'ETAT')
BEGIN
INSERT INTO [dbo].[ConstEvenementSection]([code],[libelle])
     VALUES ('ETAT','Etat �v�nement')
END
GO

GO
UPDATE [dbo].[ConstEvenementSection]
   SET [libelle] = 'Actions suivi'
 WHERE code = 'ACTION'
GO

---------------------FIL-172 Section action -----------------------

GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_SV_PROPORTION')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (18,'EXPLOITATION_CONSEQUENCE_SV_PROPORTION','SV proportion',1,0,0,22,'Exploitation cons�quence')
END
GO

GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_SV_NON_ASSURE')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (18,'EXPLOITATION_CONSEQUENCE_SV_NON_ASSURE','SV non assur�',1,0,0,23,'Exploitation cons�quence')
END
GO

GO
IF NOT EXISTS(select * from [ConstEvenementControl] where code = 'EXPLOITATION_CONSEQUENCE_SV_PROPORTION')
BEGIN
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('EXPLOITATION_CONSEQUENCE_SV_PROPORTION','SV proportion')
END
GO

GO
IF NOT EXISTS(select * from [ConstEvenementControl] where code = 'EXPLOITATION_CONSEQUENCE_SV_NON_ASSURE')
BEGIN
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('EXPLOITATION_CONSEQUENCE_SV_NON_ASSURE','SV non assur�')
END
GO

---------------------Cons�quence suppl�ment fictif---------------

GO
IF NOT EXISTS(select * from ConstEvenementExploitConseq where code = 'SUPPLEMENT_FICTIF')
BEGIN
INSERT INTO [dbo].[ConstEvenementExploitConseq]
           ([code]
           ,[with_lignes2006m401]
           ,[label_lignes2006m401]
           ,[with_parcourss2006m401]
           ,[label_parcourss2006m401]
           ,[with_directions2006m401]
           ,[label_directions2006m401]
           ,[with_arrets2006m401_1]
           ,[arrets2006m401_stationPrincipale_1]
           ,[label_arrets2006m401_1]
           ,[with_arrets2006m401_2]
           ,[arrets2006m401_stationPrincipale_2]
           ,[lable_arrets2006m401_2]
           ,[with_heure_1]
           ,[lable_heure_1]
           ,[with_heure_2]
           ,[label_heure_2]
           ,[with_nb_departs]
           ,[lable_nb_departs]
           ,[with_nb_courses]
           ,[label_nb_courses]
           ,[with_nb_km]
           ,[label_nb_km]
           ,[signe_nb_km]
           ,[with_d�calage_min]
           ,[label_d�calage_min]
           ,[with_d�calage_max]
           ,[label_d�calage_max]
           ,[with_arretsDepart2006m401_1]
           ,[label_arretsDepart2006m401_1]
           ,[with_arretsDepart2006m401_2]
           ,[label_arretsDepart2006m401_2]
           ,[with_ressourceMateriel]
           ,[label_ressourceMateriel]
           ,[with_ressourceHomme]
           ,[label_ressourceHomme]
           ,[with_withRetombe]
           ,[label_withRetombe]
           ,[with_observations]
           ,[label_observations])
     VALUES
           ('SUPPLEMENT_FICTIF'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,0
           ,'LABEL_A_COMPLETER'
           ,1
           ,'D�but'
           ,1
           ,'Fin'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,1
           ,'km(+)'
           ,'+'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,1
           ,'V�hicule'
           ,1
           ,'Agent'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER')


UPDATE [dbo].[EvenementExploitationConseqManoeuvre]
   SET [id_exploitationConseqManoeuvre_exploitConseqType] = (select id_constEvenementExploitConseq from ConstEvenementExploitConseq where code = 'SUPPLEMENT_FICTIF')

 WHERE nom in ('Suppl�m�nt fictif technique','Suppl�m�nt fictif formation')

END
GO

---------------------FIL-618 Reporting TaM -----------------------
GO
UPDATE [dbo].[ConstQueryCriteriaGroup]
   SET [nom] = 'S�curit�'
 WHERE nom = 'S�curti�'
GO

GO

UPDATE [dbo].[ConstQueryCriteria]
   SET [libelle] = 'S�curit� pr�cisions'
      ,[description] = 'S�curit� pr�cisions'
 WHERE code = 'SECURITE_PRECISION'
GO

GO
IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'SECURITE_TYPE')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('SELECT_OBJECT'
           ,'S�curit� types'
           ,6
           ,'SECURITE_TYPE'
           ,0
           ,1
           ,'S�curit� types'
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'SECURITE_TYPE';
END
GO

GO
IF NOT EXISTS(select * from [ConstQueryCriteriaGroup] where [nom] = 'Dommage')

INSERT INTO [dbo].[ConstQueryCriteriaGroup]
           ([nom])
     VALUES
           ('Dommage')
GO

GO
IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'DOMMAGE_TYPE')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('SELECT_OBJECT'
           ,'Dommage types'
           ,10
           ,'DOMMAGE_TYPE'
           ,0
           ,1
           ,'Dommage types'
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'DOMMAGE_TYPE';
END
GO

GO
IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'DOMMAGE_PRECISION')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('SELECT_OBJECT'
           ,'Dommage pr�cisions'
           ,10
           ,'DOMMAGE_PRECISION'
           ,0
           ,1
           ,'Dommage pr�cisions'
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'DOMMAGE_PRECISION';

END
GO

GO
IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'DOMMAGE_CONTEXTE')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('SELECT_OBJECT'
           ,'Dommage contexte'
           ,10
           ,'DOMMAGE_CONTEXTE'
           ,0
           ,1
           ,'Dommage contexte'
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'DOMMAGE_CONTEXTE';

END
GO

GO
IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'DOMMAGE_TIERS')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('SELECT_OBJECT'
           ,'Dommage tiers'
           ,10
           ,'DOMMAGE_TIERS'
           ,0
           ,1
           ,'Dommage tiers'
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'DOMMAGE_TIERS';

END
GO

GO
IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'HEURE_APPARITION' and dataType = 'TIME')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('TIME'
           ,'Heure apparition'
           ,1
           ,'HEURE_APPARITION'
           ,0
           ,1
           ,''
           ,1)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'HEURE_APPARITION' and dataType = 'TIME';

END
GO

GO
IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'HEURE_APPARITION' and dataType = 'TIME_RELATIVE_HEURE')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('TIME_RELATIVE_HEURE'
           ,'Heure apparition (heures relatifs)'
           ,1
           ,'HEURE_APPARITION'
           ,0
           ,1
           ,''
           ,1)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'HEURE_APPARITION' and dataType = 'TIME_RELATIVE_HEURE';

END
GO

GO
IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'EXPLOITATION_CONSEQUENCE_DURATION')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('INT'
           ,'Dur�e cons�quence exploitation (min)'
           ,7
           ,'EXPLOITATION_CONSEQUENCE_DURATION'
           ,0
           ,1
           ,'Dur�e cons�quence exploitation (min)'
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'EXPLOITATION_CONSEQUENCE_DURATION';

END
GO


GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_DURATION')
INSERT INTO [dbo].[ConstEvenementAttribut]
           ([id_constEvenementAttribut_group]
           ,[code]
           ,[libelle]
           ,[isEligibleQueryResult]
           ,[isEligibleXaxis]
           ,[isEligibleSeriesAnalyse]
           ,[rang]
           ,[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_DURATION'
           ,'Dur�e'
           ,1
           ,0
           ,0
           ,16
           ,'Exploitation cons�quence')
GO

---------------------FIL-156 Nouveaux tags -----------------------
GO
IF NOT EXISTS(select * from ConstRessourceTag where code = 'RESPONSABLE')

INSERT INTO [dbo].[ConstRessourceTag]([code],[label],[rang])
     VALUES ('RESPONSABLE','Responsable',1)

GO

GO
IF NOT EXISTS(select * from ConstRessourceTag where code = 'DEPOT')

INSERT INTO [dbo].[ConstRessourceTag]([code],[label],[rang])
     VALUES ('DEPOT','D�p�t',1)

GO

GO
IF NOT EXISTS(select * from ConstRessourceTag where code = 'BOITE_LETTRE')

INSERT INTO [dbo].[ConstRessourceTag]([code],[label],[rang])
     VALUES ('BOITE_LETTRE','Bo�te aux lettres',1)

GO

GO
IF NOT EXISTS(select * from ConstRessourceTag where code = 'AFFECTATION_THEORIQUE')

INSERT INTO [dbo].[ConstRessourceTag]([code],[label],[rang])
     VALUES ('AFFECTATION_THEORIQUE','Affectation th�orique',1)

GO

---------------------FIL-600 Nouvelles manoeuvres -----------------------
GO
IF NOT EXISTS(select * from ConstEvenementExploitConseq where code = 'MANOEUVRE_SIMPLE')

INSERT INTO [dbo].[ConstEvenementExploitConseq]
           ([code]
           ,[with_lignes2006m401]
           ,[label_lignes2006m401]
           ,[with_parcourss2006m401]
           ,[label_parcourss2006m401]
           ,[with_directions2006m401]
           ,[label_directions2006m401]
           ,[with_arrets2006m401_1]
           ,[arrets2006m401_stationPrincipale_1]
           ,[label_arrets2006m401_1]
           ,[with_arrets2006m401_2]
           ,[arrets2006m401_stationPrincipale_2]
           ,[lable_arrets2006m401_2]
           ,[with_heure_1]
           ,[lable_heure_1]
           ,[with_heure_2]
           ,[label_heure_2]
           ,[with_nb_departs]
           ,[lable_nb_departs]
           ,[with_nb_courses]
           ,[label_nb_courses]
           ,[with_nb_km]
           ,[label_nb_km]
           ,[signe_nb_km]
           ,[with_d�calage_min]
           ,[label_d�calage_min]
           ,[with_d�calage_max]
           ,[label_d�calage_max]
           ,[with_arretsDepart2006m401_1]
           ,[label_arretsDepart2006m401_1]
           ,[with_arretsDepart2006m401_2]
           ,[label_arretsDepart2006m401_2]
           ,[with_ressourceMateriel]
           ,[label_ressourceMateriel]
           ,[with_ressourceHomme]
           ,[label_ressourceHomme]
           ,[with_withRetombe]
           ,[label_withRetombe]
           ,[with_observations]
           ,[label_observations])
     VALUES
           ('MANOEUVRE_SIMPLE'
           ,0
           ,'LABEL_A_COMPLETER'
           ,1
           ,'Parcours'
           ,1
           ,'Ligne / Direction'
           ,1
           ,0
           ,'Arr�t'
           ,0
           ,0
           ,'LABEL_A_COMPLETER'
           ,1
           ,''
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,'-'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER')

GO


---------------------FIL-640 crit�re sur envoi SMS -----------------------

GO
IF NOT EXISTS(select * from [ConstQueryCriteriaGroup] where [nom] = 'D�clencheur asynchrone')

INSERT INTO [dbo].[ConstQueryCriteriaGroup]
           ([nom])
     VALUES
           ('D�clencheur asynchrone')
GO

GO
IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'QUERY_DECLENCHEUR_ASYNC_COM_MODE')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('SELECT_OBJECT'
           ,'Mode envoi asynchrone'
           ,11
           ,'QUERY_DECLENCHEUR_ASYNC_COM_MODE'
           ,0
           ,1
           ,'Mode envoi asynchrone'
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'QUERY_DECLENCHEUR_ASYNC_COM_MODE';
END
GO

---------------------FIL-639 Ajustement des ConstTags -----------------------

GO
UPDATE [dbo].[ConstRessourceTag]
   SET [label] = 'Habilitation bus articul�'
      ,[rang] = 109
 WHERE code = 'HABILITATION_ARTICULE';
GO

GO
UPDATE [dbo].[ConstRessourceTag]
   SET [label] = 'Habilitation tramway'
      ,[rang] = 110
 WHERE code = 'HABILITATION_TRAMWAY';
GO

GO
UPDATE [dbo].[ConstRessourceTag]
   SET [label] = 'Habilitation bus pmr'
      ,[rang] = 108
 WHERE code = 'HABILITATION_PMR';
GO

GO
UPDATE [dbo].[ConstRessourceTag]
   SET [rang] = 210
 WHERE code = 'RESTRICTION_MEDICAL';
GO

GO
UPDATE [dbo].[ConstRessourceTag]
   SET [rang] = 180
 WHERE code = 'ADRESSE';
GO

GO
UPDATE [dbo].[ConstRessourceTag]
   SET [label] = 'Habilitation ligne'
      ,[rang] = 106
 WHERE code = 'HABILITATION_LIGNE';
GO

GO
UPDATE [dbo].[ConstRessourceTag]
   SET [rang] = 200
 WHERE code = 'TELEPHONE';
GO

GO
UPDATE [dbo].[ConstRessourceTag]
   SET [label] = 'Habilitation bus standard'
      ,[rang] = 107
 WHERE code = 'HABILITATION_STANDARD';
GO

GO
UPDATE [dbo].[ConstRessourceTag]
   SET [rang] = 170
 WHERE code = 'RESPONSABLE';
GO

GO
UPDATE [dbo].[ConstRessourceTag]
   SET [rang] = 150
 WHERE code = 'DEPOT';
GO

GO
UPDATE [dbo].[ConstRessourceTag]
   SET [rang] = 190
 WHERE code = 'BOITE_LETTRE';
GO

GO
UPDATE [dbo].[ConstRessourceTag]
   SET [rang] = 160
 WHERE code = 'AFFECTATION_THEORIQUE';
GO


---------------------FIL-646 Anonymisation donn�es -----------------------

GO
IF NOT EXISTS(select * from [ConstConfigurationRules] where nom = 'SMS_ANONYMOUS')
INSERT INTO [dbo].[ConstConfigurationRules]
           ([nom]
           ,[libelle])
     VALUES
           ('SMS_ANONYMOUS'
           ,'SMS automatiquement anonymis�')
GO

---------------------FIL-648 Ecrasement des km perdus calcules -----------------------

GO
IF NOT EXISTS(select * from [ConstConfigurationRules] where nom = 'KM_PERUDU_CALCUL_ECRASEMENT')
INSERT INTO [dbo].[ConstConfigurationRules]
           ([nom]
           ,[libelle])
     VALUES
           ('KM_PERUDU_CALCUL_ECRASEMENT'
           ,'Ecrasement des km perdus � chaque calcul')
GO

--------------------- VIA -----------------------

GO
IF NOT EXISTS(select * from ConstEvenementExploitConseq where code = 'VIA')

INSERT INTO [dbo].[ConstEvenementExploitConseq]
           ([code]
           ,[with_lignes2006m401]
           ,[label_lignes2006m401]
           ,[with_parcourss2006m401]
           ,[label_parcourss2006m401]
           ,[with_directions2006m401]
           ,[label_directions2006m401]
           ,[with_arrets2006m401_1]
           ,[arrets2006m401_stationPrincipale_1]
           ,[label_arrets2006m401_1]
           ,[with_arrets2006m401_2]
           ,[arrets2006m401_stationPrincipale_2]
           ,[lable_arrets2006m401_2]
           ,[with_heure_1]
           ,[lable_heure_1]
           ,[with_heure_2]
           ,[label_heure_2]
           ,[with_nb_departs]
           ,[lable_nb_departs]
           ,[with_nb_courses]
           ,[label_nb_courses]
           ,[with_nb_km]
           ,[label_nb_km]
           ,[signe_nb_km]
           ,[with_d�calage_min]
           ,[label_d�calage_min]
           ,[with_d�calage_max]
           ,[label_d�calage_max]
           ,[with_arretsDepart2006m401_1]
           ,[label_arretsDepart2006m401_1]
           ,[with_arretsDepart2006m401_2]
           ,[label_arretsDepart2006m401_2]
           ,[with_ressourceMateriel]
           ,[label_ressourceMateriel]
           ,[with_ressourceHomme]
           ,[label_ressourceHomme]
           ,[with_withRetombe]
           ,[label_withRetombe]
           ,[with_observations]
           ,[label_observations])
     VALUES
           ('VIA'
           ,0
           ,'LABEL_A_COMPLETER'
           ,1
           ,'Parcours'
           ,1
           ,'Ligne / Direction'
           ,1
           ,0
           ,'Arr�t'
           ,0
           ,0
           ,'LABEL_A_COMPLETER'
           ,1
           ,''
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,'-'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER')

GO

---------------------Crit�re RH Type / RH Pr�cision  -----------------------

GO
IF NOT EXISTS(select * from [ConstQueryCriteriaGroup] where [nom] = 'RH')

INSERT INTO [dbo].[ConstQueryCriteriaGroup]
           ([nom])
     VALUES
           ('RH')
GO

GO
IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'RH_TYPE')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('SELECT_OBJECT'
           ,'RH Types'
           ,9
           ,'RH_TYPE'
           ,0
           ,1
           ,'RH Types'
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'RH_TYPE';
END
GO

GO
IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'RH_PRECISION')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('SELECT_OBJECT'
           ,'RH Pr�cisions'
           ,9
           ,'RH_PRECISION'
           ,0
           ,1
           ,'RH Pr�cisions'
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'RH_PRECISION';
END
GO


---------------------RH heures + intervention observations  -----------------------
GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'INTERVENTION_OBSERVATION')
INSERT INTO [dbo].[ConstEvenementAttribut]
           ([id_constEvenementAttribut_group]
           ,[code]
           ,[libelle]
           ,[isEligibleQueryResult]
           ,[isEligibleXaxis]
           ,[isEligibleSeriesAnalyse]
           ,[rang]
           ,[header])
     VALUES
           (11
           ,'INTERVENTION_OBSERVATION'
           ,'Observation'
           ,1
           ,1
           ,1
           ,6
           ,'Intervention')
GO

GO
IF NOT EXISTS(select * from [ConstEvenementControl] where code = 'INTERVENTION_OBSERVATION')

INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('INTERVENTION_OBSERVATION','Observation intervention')
GO
-----------
GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'RH_HEUREDEBUT')
INSERT INTO [dbo].[ConstEvenementAttribut]
           ([id_constEvenementAttribut_group]
           ,[code]
           ,[libelle]
           ,[isEligibleQueryResult]
           ,[isEligibleXaxis]
           ,[isEligibleSeriesAnalyse]
           ,[rang]
           ,[header])
     VALUES
           (17
           ,'RH_HEUREDEBUT'
           ,'Heure d�but'
           ,1
           ,1
           ,1
           ,4
           ,'RH')
GO

GO
IF NOT EXISTS(select * from [ConstEvenementControl] where code = 'RH_HEUREDEBUT')

INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('RH_HEUREDEBUT','RH Heure d�but')
GO
-----------
GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'RH_HEUREFIN')
INSERT INTO [dbo].[ConstEvenementAttribut]
           ([id_constEvenementAttribut_group]
           ,[code]
           ,[libelle]
           ,[isEligibleQueryResult]
           ,[isEligibleXaxis]
           ,[isEligibleSeriesAnalyse]
           ,[rang]
           ,[header])
     VALUES
           (17
           ,'RH_HEUREFIN'
           ,'Heure fin'
           ,1
           ,1
           ,1
           ,4
           ,'RH')
GO

GO
IF NOT EXISTS(select * from [ConstEvenementControl] where code = 'RH_HEUREFIN')

INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('RH_HEUREFIN','RH Heure fin')
GO

---------------------S�paration du crit�re sur les km perdus  -----------------------
GO
IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'KM_PERDU_PERTE')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('INT'
           ,'Km perdus perte'
           ,7
           ,'KM_PERDU_PERTE'
           ,1
           ,1
           ,'Km perdus perte (valeur absolue)'
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'KM_PERDU_PERTE';
END
GO

GO
IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'KM_PERDU_SUPP')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('INT'
           ,'Km perdus suppl�ment'
           ,7
           ,'KM_PERDU_SUPP'
           ,1
           ,1
           ,'Km perdus suppl�ment (valeur absolue)'
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'KM_PERDU_SUPP';
END
GO

---------------------Section responsable hieracrchique  -----------------------
GO
IF NOT EXISTS(select * from [ConstEvenementSection] where code = 'RESPONSABLE')
INSERT INTO [dbo].[ConstEvenementSection]
           ([code]
           ,[libelle])
     VALUES
           ('RESPONSABLE'
           ,'Responsable hi�rarchique')
GO

---------------------Crit�re sur pr�sence heure de fin  -----------------------
GO
IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'WITH_TIME_FIN')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('CHECK_PRESENCE'
           ,'Heure de fin (pr�sence)'
           ,1
           ,'WITH_TIME_FIN'
           ,0
           ,1
           ,''
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'WITH_TIME_FIN';
END
GO

-----------
GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'LIEU_AUTRE')
INSERT INTO [dbo].[ConstEvenementAttribut]
           ([id_constEvenementAttribut_group]
           ,[code]
           ,[libelle]
           ,[isEligibleQueryResult]
           ,[isEligibleXaxis]
           ,[isEligibleSeriesAnalyse]
           ,[rang]
           ,[header])
     VALUES
           (5
           ,'LIEU_AUTRE'
           ,'Lieu autre'
           ,1
           ,0
           ,0
           ,8
           ,'Lieu')
GO

GO
IF NOT EXISTS(select * from [ConstEvenementControl] where code = 'LIEU_AUTRE')

INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('LIEU_AUTRE','Lieu autre')
GO
------MAJ nom de section RH / CDS-----
GO

UPDATE [dbo].[ConstEvenementSection]
   SET [libelle] = 'RH / CDS'
 WHERE code = 'RH'
GO

------Crit�re heure de fin-----
GO
IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'HEURE_FIN')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('TIME'
           ,'Heure de fin'
           ,1
           ,'HEURE_FIN'
           ,0
           ,1
           ,''
           ,1)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'HEURE_FIN';

END
GO

------Crit�re raison fermeture-----
GO
IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'RAISON_FERMETURE')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('SELECT_OBJECT'
           ,'Raisons fermetures'
           ,1
           ,'RAISON_FERMETURE'
           ,0
           ,1
           ,''
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'RAISON_FERMETURE';
END
GO

GO
IF NOT EXISTS(select * from [ConstQueryCriteria] where code = 'WITH_RAISON_FERMETURE')
BEGIN
INSERT INTO [dbo].[ConstQueryCriteria]([dataType],[libelle],[id_queryCriteria_queryCriteriaGroup],[code],[isEligibleOnTheFlight],[isEligibleAdvancedMode],[description],[isCriteriaTemporel])
     VALUES
           ('CHECK_PRESENCE'
           ,'Raisons fermetures (pr�sence)'
           ,1
           ,'WITH_RAISON_FERMETURE'
           ,0
           ,1
           ,''
           ,0)

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])

     select 1,id_queryCriteria from ConstQueryCriteria where code = 'WITH_RAISON_FERMETURE';
END
GO

----------Revue des libell� km perdus---------------
GO

UPDATE [dbo].[ConstQueryCriteria]
   SET [libelle] = 'km total'
      ,[description] = 'km total �v�nement'
 WHERE code='KM_PERDU_TOTAL'
GO

UPDATE [dbo].[ConstQueryCriteria]
   SET [libelle] = 'km total perte'
      ,[description] = 'km perte total �v�nement (valeur absolue)'
 WHERE code='KM_PERDU_PERTE'
GO

UPDATE [dbo].[ConstQueryCriteria]
   SET [libelle] = 'km total suppl�ment'
      ,[description] = 'km suppl�ment total �v�nement (valeur absolue)'
 WHERE code='KM_PERDU_SUPP'
GO

---
GO
UPDATE [dbo].[ConstEvenementAttribut]
   SET [libelle] = 'Course'
      ,[rang] = 50
 WHERE code =' EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE'
GO

GO
UPDATE [dbo].[ConstEvenementAttribut]
   SET [libelle] = 'Course (num�rique)'
      ,[rang] = 51
 WHERE code =' EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_CALCULE'
GO

GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_IMPACT')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_IMPACT'
           ,'Course impact�es'
           ,1
           ,0
           ,0
           ,52
           ,'Exploitation cons�quence')
END
GO

IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_DEPART')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_DEPART'
           ,'Terminus d�part'
           ,1
           ,0
           ,0
           ,53
           ,'Exploitation cons�quence')
END
GO

IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_ARRIVE')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_ARRIVE'
           ,'Terminus arriv�e'
           ,1
           ,0
           ,0
           ,54
           ,'Exploitation cons�quence')
END
GO


GO
UPDATE [dbo].[ConstEvenementAttribut]
   SET [libelle] = 'km'
      ,[rang] = 55
 WHERE code =' EXPLOITATION_CONSEQUENCE_NOMBRE_KM'
GO


GO
UPDATE [dbo].[ConstEvenementAttribut]
   SET [libelle] = 'km total �v�nement'
      ,[rang] = 56
 WHERE code ='EXPLOITATION_CONSEQUENCE_NOMBRE_KM_TOTAL'
GO

GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_KM_TOTAL_PERTE')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_KM_TOTAL_PERTE'
           ,'km total �v�nement perte'
           ,1
           ,0
           ,0
           ,57
           ,'Exploitation cons�quence')
END
GO

GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_KM_TOTAL_SUP')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_KM_TOTAL_SUP'
           ,'km total �v�nement suppl�ment'
           ,1
           ,0
           ,0
           ,58
           ,'Exploitation cons�quence')
END
GO

GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_TOTAL')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_TOTAL'
           ,'Course total �v�nement'
           ,1
           ,0
           ,0
           ,59
           ,'Exploitation cons�quence')
END
GO

GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_TOTAL_PERTE')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_TOTAL_PERTE'
           ,'Course total �v�nement perte'
           ,1
           ,0
           ,0
           ,60
           ,'Exploitation cons�quence')
END
GO

GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_TOTAL_SUP')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_TOTAL_SUP'
           ,'Course total �v�nement suppl�ment'
           ,1
           ,0
           ,0
           ,61
           ,'Exploitation cons�quence')
END
GO

GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_IMPACT_TOTAL')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_IMPACT_TOTAL'
           ,'Course impact total �v�nement'
           ,1
           ,0
           ,0
           ,62
           ,'Exploitation cons�quence')
END
GO

GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_IMPACT_TOTAL_PERTE')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_IMPACT_TOTAL_PERTE'
           ,'Course impact total �v�nement perte'
           ,1
           ,0
           ,0
           ,63
           ,'Exploitation cons�quence')
END
GO

GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_IMPACT_TOTAL_SUP')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_COURSE_IMPACT_TOTAL_SUP'
           ,'Course impact total �v�nement suppl�ment'
           ,1
           ,0
           ,0
           ,64
           ,'Exploitation cons�quence')
END
GO

GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_DEPART_TOTAL')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_DEPART_TOTAL'
           ,'Term d�part total �v�nement'
           ,1
           ,0
           ,0
           ,65
           ,'Exploitation cons�quence')
END
GO

GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_DEPART_TOTAL_PERTE')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_DEPART_TOTAL_PERTE'
           ,'Term d�part total �v�nement p�rte'
           ,1
           ,0
           ,0
           ,66
           ,'Exploitation cons�quence')
END
GO

GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_DEPART_TOTAL_SUP')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_DEPART_TOTAL_SUP'
           ,'Term d�part total �v�nement suppl�ment'
           ,1
           ,0
           ,0
           ,67
           ,'Exploitation cons�quence')
END
GO

GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_ARRIVE_TOTAL')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_ARRIVE_TOTAL'
           ,'Term arriv�e total �v�nement'
           ,1
           ,0
           ,0
           ,68
           ,'Exploitation cons�quence')
END
GO

GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_ARRIVE_TOTAL_PERTE')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_ARRIVE_TOTAL_PERTE'
           ,'Term arriv�e total �v�nement p�rte'
           ,1
           ,0
           ,0
           ,69
           ,'Exploitation cons�quence')
END
GO

GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_ARRIVE_TOTAL_SUP')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (14
           ,'EXPLOITATION_CONSEQUENCE_NOMBRE_TERMINUS_ARRIVE_TOTAL_SUP'
           ,'Term arriv�e total �v�nement suppl�ment'
           ,1
           ,0
           ,0
           ,70
           ,'Exploitation cons�quence')
END
GO

----------Attribut imputation---------------

GO
IF NOT EXISTS(select * from [ConstEvenementAttribut] where code = 'IMPUTATION')
BEGIN
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES
           (1
           ,'IMPUTATION'
           ,'Imputation'
           ,1
           ,0
           ,0
           ,8
           ,'')
END
GO


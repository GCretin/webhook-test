use s2002m401;


delete from LnkAleaListAleaHistorique;
delete from AleaHistorique;



delete from EvenementIdentifiantMax;

delete from EvenementDommage;
delete from EvenementExploitationConseq;
delete from EvenementIntervention;
delete from EvenementMaintenance;
delete from EvenementPrecision;
delete from EvenementInfoVoyageur;
delete from EvenementRH;
delete from EvenementSecurite;
delete from EvenementFichier;
delete from EvenementHistoriqueView;
delete from EvenementActionRealise;

delete from QueryTriggerAsynchoneAccuse;
delete from QueryTriggerAsynchoneHistorique;
delete from QueryTriggerSynchroneHistorique;

delete from LnkMaterielListeAleaListe;
delete from AleaListe;

delete from EvenementAffectation;
delete from EvenementLieu;
delete from EvenementMeteo;


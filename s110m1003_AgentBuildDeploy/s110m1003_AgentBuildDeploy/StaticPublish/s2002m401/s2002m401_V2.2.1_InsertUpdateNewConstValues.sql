USE [s2002m401]

---------------------G�n�ral 2.2.1-----------------------

---------------------FIL-172 Section action -----------------------

GO

INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (18,'EXPLOITATION_CONSEQUENCE_SV_PROPORTION','SV proportion',1,0,0,22,'Exploitation cons�quence')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (18,'EXPLOITATION_CONSEQUENCE_SV_NON_ASSURE','SV non assur�',1,0,0,23,'Exploitation cons�quence')

INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('EXPLOITATION_CONSEQUENCE_SV_PROPORTION','SV proportion')
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('EXPLOITATION_CONSEQUENCE_SV_NON_ASSURE','SV non assur�')

GO

USE [s2002m401]
GO

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[AleaAction] ON 

GO
INSERT [dbo].[AleaAction] ([id_aleaAction], [nom]) VALUES (1, N'Ouverture')
GO
INSERT [dbo].[AleaAction] ([id_aleaAction], [nom]) VALUES (2, N'Fermeture')
GO
INSERT [dbo].[AleaAction] ([id_aleaAction], [nom]) VALUES (3, N'Modification')
GO
SET IDENTITY_INSERT [dbo].[AleaAction] OFF
GO

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[AleaEtat] ON 

GO
INSERT [dbo].[AleaEtat] ([id_aleaEtat], [nom]) VALUES (1, N'Ouvert')
GO
INSERT [dbo].[AleaEtat] ([id_aleaEtat], [nom]) VALUES (2, N'Fermé')
GO
SET IDENTITY_INSERT [dbo].[AleaEtat] OFF
GO

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[AlerteTrigger] ON 

GO
INSERT [dbo].[AlerteTrigger] ([id_aleaTrigger], [nomAleaTrigger]) VALUES (1, N'Ouverture')
GO
INSERT [dbo].[AlerteTrigger] ([id_aleaTrigger], [nomAleaTrigger]) VALUES (2, N'Fermeture')
GO
INSERT [dbo].[AlerteTrigger] ([id_aleaTrigger], [nomAleaTrigger]) VALUES (3, N'Modification')
GO
INSERT [dbo].[AlerteTrigger] ([id_aleaTrigger], [nomAleaTrigger], [valThresholdMin], [valThresholdMax]) VALUES (4, N'Perte Km', 0, NULL)
GO
SET IDENTITY_INSERT [dbo].[AlerteTrigger] OFF
GO

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[COM] ON 

GO
INSERT [dbo].[COM] ([id_aleaCOM], [nomAlerteCOM]) VALUES (1, N'SMS')
GO
INSERT [dbo].[COM] ([id_aleaCOM], [nomAlerteCOM]) VALUES (2, N'Mail')
GO
SET IDENTITY_INSERT [dbo].[COM] OFF
GO

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[DBinfos] ON 

GO
INSERT [dbo].[DBinfos] ([id_dbinfos], [nom], [valeurs]) VALUES (1, N'version', N'0.6.2')
GO
INSERT [dbo].[DBinfos] ([id_dbinfos], [nom], [valeurs]) VALUES (2, N'uid', N's2002m401')
GO
INSERT [dbo].[DBinfos] ([id_dbinfos], [nom], [valeurs]) VALUES (3, N'env', N'----')
GO
SET IDENTITY_INSERT [dbo].[DBinfos] OFF
GO

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[ExploitationConseqType] ON 

GO
INSERT [dbo].[ExploitationConseqType] ([id_exploitationConseqType], [nom]) VALUES (1, N'Sans conséquences')
GO
INSERT [dbo].[ExploitationConseqType] ([id_exploitationConseqType], [nom]) VALUES (2, N'Retard')
GO
INSERT [dbo].[ExploitationConseqType] ([id_exploitationConseqType], [nom]) VALUES (3, N'Interruption parcours')
GO
INSERT [dbo].[ExploitationConseqType] ([id_exploitationConseqType], [nom]) VALUES (4, N'Interruption lignes(s)')
GO
INSERT [dbo].[ExploitationConseqType] ([id_exploitationConseqType], [nom]) VALUES (5, N'Interruption réseau')
GO
SET IDENTITY_INSERT [dbo].[ExploitationConseqType] OFF
GO

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[HistoriqueCause] ON 

GO
INSERT [dbo].[HistoriqueCause] ([id_historiqueCause], [nom]) VALUES (1, N'Automatique')
GO
INSERT [dbo].[HistoriqueCause] ([id_historiqueCause], [nom]) VALUES (2, N'Manuel')
GO
SET IDENTITY_INSERT [dbo].[HistoriqueCause] OFF
GO

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[HumiditeInfos] ON 

GO
INSERT [dbo].[HumiditeInfos] ([id_humiditeInfos], [nom]) VALUES (1, N'Sec')
GO
INSERT [dbo].[HumiditeInfos] ([id_humiditeInfos], [nom]) VALUES (2, N'Humide')
GO
INSERT [dbo].[HumiditeInfos] ([id_humiditeInfos], [nom]) VALUES (3, N'Trempé')
GO
INSERT [dbo].[HumiditeInfos] ([id_humiditeInfos], [nom]) VALUES (4, N'Verglas')
GO
INSERT [dbo].[HumiditeInfos] ([id_humiditeInfos], [nom]) VALUES (5, N'Neige')
GO
SET IDENTITY_INSERT [dbo].[HumiditeInfos] OFF
GO

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[InterventionEtat] ON 

GO
INSERT [dbo].[InterventionEtat] ([id_interventionEtat], [nom]) VALUES (1, N'En cours')
GO
INSERT [dbo].[InterventionEtat] ([id_interventionEtat], [nom]) VALUES (2, N'En attente')
GO
INSERT [dbo].[InterventionEtat] ([id_interventionEtat], [nom]) VALUES (3, N'Terminé')
GO
SET IDENTITY_INSERT [dbo].[InterventionEtat] OFF

GO

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[TemperatureInfos] ON 

GO
INSERT [dbo].[TemperatureInfos] ([id_temperatureInfos], [nom]) VALUES (1, N'-20 C à -10 C')
GO
INSERT [dbo].[TemperatureInfos] ([id_temperatureInfos], [nom]) VALUES (2, N'-10 C à 0 C')
GO
INSERT [dbo].[TemperatureInfos] ([id_temperatureInfos], [nom]) VALUES (3, N'0 C à 10 C')
GO
INSERT [dbo].[TemperatureInfos] ([id_temperatureInfos], [nom]) VALUES (4, N'10 C à 20 C')
GO
INSERT [dbo].[TemperatureInfos] ([id_temperatureInfos], [nom]) VALUES (5, N'20 C à 30 C')
GO
INSERT [dbo].[TemperatureInfos] ([id_temperatureInfos], [nom]) VALUES (6, N'30 C à 40 C')
GO
SET IDENTITY_INSERT [dbo].[TemperatureInfos] OFF
GO

GO

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[MaterielEtat] ON 

GO
INSERT [dbo].[MaterielEtat] ([id_materielEtat], [nom]) VALUES (1, N'Inconnu')
GO
SET IDENTITY_INSERT [dbo].[MaterielEtat] OFF
GO

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[PersonneType] ON 

GO
INSERT [dbo].[PersonneType] ([id_personneType], [nom]) VALUES (1, N'Témoin tiers')
GO
INSERT [dbo].[PersonneType] ([id_personneType], [nom]) VALUES (2, N'Victime tiers')
GO
INSERT [dbo].[PersonneType] ([id_personneType], [nom]) VALUES (3, N'Conducteur')
GO
INSERT [dbo].[PersonneType] ([id_personneType], [nom]) VALUES (4, N'GSCT')
GO
INSERT [dbo].[PersonneType] ([id_personneType], [nom]) VALUES (5, N'Gardien')
GO
INSERT [dbo].[PersonneType] ([id_personneType], [nom]) VALUES (6, N'Service technique')
GO
INSERT [dbo].[PersonneType] ([id_personneType], [nom]) VALUES (7, N'Agent de maitrise')
GO
INSERT [dbo].[PersonneType] ([id_personneType], [nom]) VALUES (8, N'Administratif')
GO
SET IDENTITY_INSERT [dbo].[PersonneType] OFF
GO

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[DommageType] ON 

GO
INSERT [dbo].[DommageType] ([id_dommageType], [nom]) VALUES (1, N'Matériel - Obstacle fixe')
GO
INSERT [dbo].[DommageType] ([id_dommageType], [nom]) VALUES (2, N'Matériel - Véhicule tiers')
GO
INSERT [dbo].[DommageType] ([id_dommageType], [nom]) VALUES (3, N'Matériel - Véhicule RLA')
GO
INSERT [dbo].[DommageType] ([id_dommageType], [nom]) VALUES (4, N'Corporel - Chute à bord')
GO
INSERT [dbo].[DommageType] ([id_dommageType], [nom]) VALUES (5, N'Corporel - Chute à quai')
GO
INSERT [dbo].[DommageType] ([id_dommageType], [nom]) VALUES (6, N'Corporel - Percussion tiers')
GO
SET IDENTITY_INSERT [dbo].[DommageType] OFF
GO

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[DommageGravite] ON 

GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (1, 1, N'Aucun')
GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (3, 1, N'Dégat léger')
GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (4, 1, N'Dégat lourd')
GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (5, 1, N'Destruction')
GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (6, 2, N'Aucun')
GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (7, 2, N'Dégat léger')
GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (8, 2, N'Dégat lourd')
GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (9, 2, N'Destruction')
GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (10, 3, N'Aucun')
GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (11, 3, N'Dégat léger')
GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (12, 3, N'Dégat lourd')
GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (13, 3, N'Destruction')
GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (14, 4, N'Aucun')
GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (15, 4, N'Blessure légère')
GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (16, 4, N'Blessure grave')
GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (17, 4, N'Décès')
GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (18, 5, N'Aucun')
GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (19, 5, N'Blessure légère')
GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (20, 5, N'Blessure grave')
GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (21, 5, N'Décès')
GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (22, 6, N'Aucun')
GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (23, 6, N'Blessure légère')
GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (24, 6, N'Blessure grave')
GO
INSERT [dbo].[DommageGravite] ([id_dommageGravite], [id_dommageGravite_dommageType], [nom]) VALUES (25, 6, N'Décès')
GO
SET IDENTITY_INSERT [dbo].[DommageGravite] OFF
GO



SET IDENTITY_INSERT [dbo].[EvenementDataSectionType] ON 

GO
INSERT [dbo].[EvenementDataSectionType] ([id_evenementDataSectionType], [nom]) VALUES (1, N'SECTIONTYPE_RESSOURCE')
GO
INSERT [dbo].[EvenementDataSectionType] ([id_evenementDataSectionType], [nom]) VALUES (2, N'SECTIONTYPE_EVENT')
GO
INSERT [dbo].[EvenementDataSectionType] ([id_evenementDataSectionType], [nom]) VALUES (3, N'SECTIONTYPE_APPARITION')
GO
INSERT [dbo].[EvenementDataSectionType] ([id_evenementDataSectionType], [nom]) VALUES (4, N'SECTIONTYPE_LIEU_COM')
GO
INSERT [dbo].[EvenementDataSectionType] ([id_evenementDataSectionType], [nom]) VALUES (5, N'SECTIONTYPE_LIEU_HORS_COM')
GO
INSERT [dbo].[EvenementDataSectionType] ([id_evenementDataSectionType], [nom]) VALUES (6, N'SECTIONTYPE_INFOS_CONSULTATION')
GO
INSERT [dbo].[EvenementDataSectionType] ([id_evenementDataSectionType], [nom]) VALUES (7, N'SECTIONTYPE_ACTIONHISTORIQUE')
GO
INSERT [dbo].[EvenementDataSectionType] ([id_evenementDataSectionType], [nom]) VALUES (8, N'SECTIONTYPE_ALERTEHISTORIQUE')
GO
INSERT [dbo].[EvenementDataSectionType] ([id_evenementDataSectionType], [nom]) VALUES (9, N'SECTIONTYPE_EXPLOITATIONDETAIL')
GO
INSERT [dbo].[EvenementDataSectionType] ([id_evenementDataSectionType], [nom]) VALUES (10, N'SECTIONTYPE_COMMENTAIRE')
GO
INSERT [dbo].[EvenementDataSectionType] ([id_evenementDataSectionType], [nom]) VALUES (11, N'SECTIONTYPE_FICHIER')
GO
INSERT [dbo].[EvenementDataSectionType] ([id_evenementDataSectionType], [nom]) VALUES (12, N'SECTIONTYPE_VEHICULE')
GO
INSERT [dbo].[EvenementDataSectionType] ([id_evenementDataSectionType], [nom]) VALUES (13, N'SECTIONTYPE_METEO')
GO
INSERT [dbo].[EvenementDataSectionType] ([id_evenementDataSectionType], [nom]) VALUES (14, N'SECTIONTYPE_MAINTENANCE')
GO
INSERT [dbo].[EvenementDataSectionType] ([id_evenementDataSectionType], [nom]) VALUES (15, N'SECTIONTYPE_DOMMAGE')
GO
INSERT [dbo].[EvenementDataSectionType] ([id_evenementDataSectionType], [nom]) VALUES (16, N'SECTIONTYPE_SERVICE')
GO
INSERT [dbo].[EvenementDataSectionType] ([id_evenementDataSectionType], [nom]) VALUES (17, N'SECTIONTYPE_EXPLOITATIONCONSEQ')
GO
SET IDENTITY_INSERT [dbo].[EvenementDataSectionType] OFF
GO

------------------------------------------------------------------------------------------------------------------------------------
--									0.6.4											 											  --
------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[ConstEvenementLieuType] ON 

GO
INSERT [dbo].[ConstEvenementLieuType] ([id_constEvenementLieuType], [code], [libelle]) VALUES (1, N'COM', N'Commercial')
GO
INSERT [dbo].[ConstEvenementLieuType] ([id_constEvenementLieuType], [code], [libelle]) VALUES (2, N'HLP', N'Haut-le-pied')
GO
INSERT [dbo].[ConstEvenementLieuType] ([id_constEvenementLieuType], [code], [libelle]) VALUES (3, N'NO-COM', N'Non commercial')
GO
SET IDENTITY_INSERT [dbo].[ConstEvenementLieuType] OFF
GO
SET IDENTITY_INSERT [dbo].[ConstRessourcesFamille] ON 

GO
INSERT [dbo].[ConstRessourcesFamille] ([id_constRessourceFamille], [code], [libelle]) VALUES (1, N'NONE', N'Aucune')
GO
INSERT [dbo].[ConstRessourcesFamille] ([id_constRessourceFamille], [code], [libelle]) VALUES (3, N'DEPOT', N'Dépôt')
GO
INSERT [dbo].[ConstRessourcesFamille] ([id_constRessourceFamille], [code], [libelle]) VALUES (4, N'PARKING', N'Parking')
GO
INSERT [dbo].[ConstRessourcesFamille] ([id_constRessourceFamille], [code], [libelle]) VALUES (5, N'LIGNE', N'Ligne')
GO
INSERT [dbo].[ConstRessourcesFamille] ([id_constRessourceFamille], [code], [libelle]) VALUES (6, N'BUS', N'Bus')
GO
INSERT [dbo].[ConstRessourcesFamille] ([id_constRessourceFamille], [code], [libelle]) VALUES (7, N'TRAMWAY', N'Tramway')
GO
INSERT [dbo].[ConstRessourcesFamille] ([id_constRessourceFamille], [code], [libelle]) VALUES (8, N'CONDUCTEUR', N'Conducteur')
GO
INSERT [dbo].[ConstRessourcesFamille] ([id_constRessourceFamille], [code], [libelle]) VALUES (9, N'LIEU_NO_COM', N'Lieu non commercial')
GO
SET IDENTITY_INSERT [dbo].[ConstRessourcesFamille] OFF
GO


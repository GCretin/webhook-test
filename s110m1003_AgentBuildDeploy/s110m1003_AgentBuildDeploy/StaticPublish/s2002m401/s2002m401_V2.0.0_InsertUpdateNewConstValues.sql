USE [s2002m401]
GO

INSERT INTO [dbo].[ConstEvenementAttribut]
           ([id_constEvenementAttribut_group]
           ,[code]
           ,[libelle]
           ,[isEligibleQueryResult]
           ,[isEligibleXaxis]
           ,[isEligibleSeriesAnalyse]
           ,[rang]
           ,[header])
     VALUES
           (7
           ,'HISTO_CREATEUR'
           ,'Cr�ateur'
           ,1
           ,1
           ,1
           ,6
           ,'Historique')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConsignesExploitation]
           ([id_consignesExploitation_reseau_s101m401]
           ,[message]
           ,[dateDebut]
           ,[heureDebut]
           ,[dateFin]
           ,[heureFin]
           ,[isDefaut])
     VALUES
           (1
           ,'Pas de consignes d''exploitation applicables'
           ,'2001-01-01'
           ,'00:00:01.1853731'
           ,'2001-01-01'
           ,'23:59:01.1853731'
           ,1)
GO

GO

UPDATE [dbo].[ConsignesExploitation]
   SET [isDefaut] = 0
 WHERE [isDefaut] is null
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstRessourcesFamille]
           ([code]
           ,[libelle])
     VALUES
           ('CLIENT'
           ,'Client')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstEvenementFamille]
           ([code]
           ,[libelle])
     VALUES
           ('RECLAMATION_CLIENT'
           ,'R�clamation client')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstTerminalSaisie]
           ([code]
           ,[libelle])
     VALUES
           ('MOBILE'
           ,'Application mobile')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstTerminalSaisie]
           ([code]
           ,[libelle])
     VALUES
           ('WEB'
           ,'Application Web')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstModeSaisie]
           ([code]
           ,[libelle])
     VALUES
           ('CREATION'
           ,'Mode cr�ation')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstModeSaisie]
           ([code]
           ,[libelle])
     VALUES
           ('MODIFICATION'
           ,'Mode modification')
GO

USE [s2002m401]
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('RESSOURCE_CLASSE','Ressource classe')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('EVENEMENT_CATEGORIE','Ev�n�ment cat�gorie')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('DATE_APPARITION','Apparition date')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('HEURE_APPARITION','Apparition heure')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('HEURE_FIN','Fin heure')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('CREATEUR','Cr�ateur')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('HEURE_OUVERTURE','Ouverture date')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('HEURE_MODIFICATION','Modification heure')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('PONDERATION','Pond�ration')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('IDENTIFIANT','Identifiant')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('ETAT','Etat')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('COMMENTAIRE','Commentaire')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('METEO','M�t�o')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('LIEU','Lieu')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('LIEU_LIGNE','Lieu ligne')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('AFFECT_VEHICULE','Affectation v�hicule')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('AFFECT_SV','Affectation SV')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('AFFECT_SA','Affectation SA')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('AFFECT_AGENT','Affectation agent')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('MAINTENANCE_IMPACT','Maintenance impact')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('MAINTENANCE_ORGANE','Maintenance organe')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('MAINTENANCE_SYMPTOME','Maintenance symptome')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('MAINTENANCE_EMPLACEMENT','Maintenance emplacement')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('DOMMAGE_TYPE','Dommage type')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('DOMMAGE_IMMAT','Dommage impact')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('DOMMAGE_ASSURANCE_NOM','Dommage assurance nom')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('DOMMAGE_ASSURANCE_NUM','Dommage assurance num�ro')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('DOMMAGE_TIERS_NOM','Dommage nom tiers')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('DOMMAGE_TIERS_PRENOM','Dommage pr�nom tiers')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('DOMMAGE_OBSERVATIONS','Dommage observations')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('DOMMAGE_RESSOURCE','Dommage ressources')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('EXPLOIT_CONS_MANOEUVRE','Exploitation cons�quence manoeuvre')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('EXPLOIT_CONS_LIGNE','Exploitation cons�quence')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('EXPLOIT_CONS_DIRECTION','Exploitation cons�quence')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('EXPLOIT_CONS_ARRET1','Exploitation cons�quence')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('EXPLOIT_CONS_ARRET2','Exploitation cons�quence')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('EXPLOIT_CONS_DEPART1','Exploitation cons�quence')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('EXPLOIT_CONS_DEPART2','Exploitation cons�quence')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('EXPLOIT_CONS_HEURE1','Exploitation cons�quence')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('EXPLOIT_CONS_HEURE2','Exploitation cons�quence')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('EXPLOIT_CONS_NB_COURSE','Exploitation cons�quence')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('EXPLOIT_CONS_NB_KM','Exploitation cons�quence')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('EXPLOIT_CONS_DESC_KM_AUTO','Exploitation cons�quence')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('EXPLOIT_CONS_DECAL_MIN','Exploitation cons�quence')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('EXPLOIT_CONS_DECAL_MAX','Exploitation cons�quence')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('EXPLOIT_CONS_ISRETOMBE','Exploitation cons�quence')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('EXPLOIT_CONS_OBSERVATION','Exploitation cons�quence')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('EXPLOIT_CONS_VEHICULE','Exploitation cons�quence')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('EXPLOIT_CONS_AGENT','Exploitation cons�quence')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('INTERVENTION_SERVICE','Intervention service')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('INTERVENTION_ETAT','Intervention �tat')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('SECURITE_TYPE','S�curit� type')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('MEDIA_PICKER','M�dia s�lection')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('CARROUSSEL','Interne mobile')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('AFFECT_PRISE_THEO','Affectation prise service th�orique')
GO
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('AFFECT_PRISE_REEL','Affectation prise service r��l')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstConfigurationRules]
           ([nom]
           ,[libelle])
     VALUES
           ('FILTRE_LIEU_LIGNE_ON_SV'
           ,'Filtrage des lignes (lieu) � partir du SV')
GO


USE [s2002m401]
GO

INSERT INTO [dbo].[ConstConfigurationRules]
           ([nom]
           ,[libelle])
     VALUES
           ('FILTRE_LIEU_PARCOURS_ON_SV'
           ,'Filtrage des parcours (lieu) � partir du SV')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstConfigurationRules]
           ([nom]
           ,[libelle])
     VALUES
           ('EVENEMENT_FERMETURE_AUTO'
           ,'Fermeture auto. des �v�nements � une heure d�finie')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[CfgConfigurationRules]
           ([id_cfgConfigurationRules_CfgGeneralMainCourante]
           ,[id_cfgConfigurationRules_ConfigurationRules])
     VALUES
           (1
           ,1)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[CfgConfigurationRules]
           ([id_cfgConfigurationRules_CfgGeneralMainCourante]
           ,[id_cfgConfigurationRules_ConfigurationRules])
     VALUES
           (1
           ,2)
GO

USE [s2002m401]
GO

UPDATE [dbo].[ConstEvenementAttribut]
   SET [isEligibleQueryResult] = 1
      WHERE code = ' INTERVENTION_DESCRIPTION'
GO















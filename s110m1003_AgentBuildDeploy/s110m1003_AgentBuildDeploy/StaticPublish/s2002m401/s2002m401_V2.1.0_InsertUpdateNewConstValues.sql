USE [s2002m401]

---------------------G�n�ral 2.1.0-----------------------

---------------------FIL-174 Nouvelles ressource famille / �v�nements famille / conseq exploit-----------------------
GO

INSERT INTO [dbo].[ConstRessourcesFamille] ([code],[libelle]) VALUES ('BUS_SOUS_TRAITANT','Bus sous-traitant')

INSERT INTO [dbo].[ConstEvenementFamille] ([code],[libelle]) VALUES ('ABSENCE','Absences agents')
INSERT INTO [dbo].[ConstEvenementFamille] ([code],[libelle]) VALUES ('RETARD','Retards agents')
INSERT INTO [dbo].[ConstEvenementFamille] ([code],[libelle]) VALUES ('ERREUR_HUMAINE','Erreur humaine')
INSERT INTO [dbo].[ConstEvenementFamille] ([code],[libelle]) VALUES ('OBJET_TROUVE','Objets trouv�s')
INSERT INTO [dbo].[ConstEvenementFamille] ([code],[libelle]) VALUES ('TECHNIQUE','Technique maintenance')
INSERT INTO [dbo].[ConstEvenementFamille] ([code],[libelle]) VALUES ('MODIFICATION_OFFRE','Modification offre de transport')
INSERT INTO [dbo].[ConstEvenementFamille] ([code],[libelle]) VALUES ('HYGIENE','Hygi�ne')
INSERT INTO [dbo].[ConstEvenementFamille] ([code],[libelle]) VALUES ('PERTURBATION','Perturbation exploitation')
INSERT INTO [dbo].[ConstEvenementFamille] ([code],[libelle]) VALUES ('AVANCE_RETARD','Avance / Retard exploitation')
INSERT INTO [dbo].[ConstEvenementFamille] ([code],[libelle]) VALUES ('SECURITE_FERROVIAIRE','S�curiti� ferroviaire')
INSERT INTO [dbo].[ConstEvenementFamille] ([code],[libelle]) VALUES ('ASSISTANCE_CLIENT','Assistance client')
INSERT INTO [dbo].[ConstEvenementFamille] ([code],[libelle]) VALUES ('DISPONIBILITE_MATERIEL','Disponibilit� mat�riel')


---------------------FIL-123 Ajout fin de service th�o / r��l-----------------------
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('AFFECT_FIN_THEO','Affectation fin service th�orique')
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('AFFECT_FIN_REEL','Affectation fin service r��l')

INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES(6,' AFFECTATION_PRISE_THEO','Prise service th�orique',1,0,0,6,'Affectation')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES(6,' AFFECTATION_PRISE_REEL','Prise service r��l',1,0,0,6,'Affectation')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES(6,' AFFECTATION_FIN_THEO','Fin service th�orique',1,0,0,6,'Affectation')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES(6,' AFFECTATION_FIN_REEL','Fin service r��l',1,0,0,6,'Affectation')

---------------------FIL-137 et FIL-120 Heure appel et fin-----------------------

INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('HEURE_APPEL','Heure appel')
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('DATE_FIN','Date de fin')

INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES(2,' HEURE_APPEL','Heure',1,1,1,18,'Appel')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES(2,' DATE_FIN','Date',1,1,1,18,'Fin')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
	VALUES(7,' HISTO_FERMEUR','Date',1,1,1,7,'Ferm� par')

---------------------FIL-128 Fermeture des �v�nements-----------------------

INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('FERMETURE_RAISON','Raison de fermeture')

INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES(1,' FERMETURE_RAISON','Raison fermeture',1,1,1,4,'')

---------------------FIL-130 INtervention d�but et fin-----------------------

INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('INTERVENTION_HEUREDEBUT','D�but intervention')
INSERT INTO [dbo].[ConstEvenementControl] ([code],[libelle]) VALUES ('INTERVENTION_HEUREFIN','Fin intervention')

---------------------FIL-202 R�gle de configuration (gestion des �tats ressource)-----------------------

INSERT INTO [dbo].[ConstConfigurationRules]([nom],[libelle]) VALUES ('RESSOURCE_AVEC_ETAT','Ressource avec gestion des �tats')


INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES('R�gle ressource DEPOT',3,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES('R�gle ressource PARKING',4,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES('R�gle ressource LIGNE',5,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES('R�gle ressource BUS',6,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES('R�gle ressource TRAMWAY',7,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES('R�gle ressource CONDUCTEUR',8,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES('R�gle ressource LIEU_NO_COM',9,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES('R�gle ressource ADMINISTRATIF',13,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES('R�gle ressource AGENT_AUTRE',14,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES('R�gle ressource RESEAU',15,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES('R�gle ressource INSTALLATION_FIXE',16,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES('R�gle ressource DEPOT_BUS',17,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES('R�gle ressource DEPOT_TRAM',18,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES('R�gle ressource RESEAU_SOUS_TRAITANT',19,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES('R�gle ressource CLIENT',20,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES('R�gle ressource BUS_SOUS_TRAITANT',21,1)

---------------------FIL-201 Nouvelles ressource famille IF-----------------------

INSERT INTO [dbo].[ConstRessourcesFamille] ([code],[libelle]) VALUES ('VOIE','Voie')
INSERT INTO [dbo].[ConstRessourcesFamille] ([code],[libelle]) VALUES ('LAC','LAC')
INSERT INTO [dbo].[ConstRessourcesFamille] ([code],[libelle]) VALUES ('SIG','Signalisation')
INSERT INTO [dbo].[ConstRessourcesFamille] ([code],[libelle]) VALUES ('SIG_FERRO','Signalisation  ferroviaire')
INSERT INTO [dbo].[ConstRessourcesFamille] ([code],[libelle]) VALUES ('POINT_HAUT','Point haut')
INSERT INTO [dbo].[ConstRessourcesFamille] ([code],[libelle]) VALUES ('DAT','DAT')
INSERT INTO [dbo].[ConstRessourcesFamille] ([code],[libelle]) VALUES ('ABRI_VOYAGEUR','Abri voyageur')
INSERT INTO [dbo].[ConstRessourcesFamille] ([code],[libelle]) VALUES ('CAMERA','Cam�ra')
INSERT INTO [dbo].[ConstRessourcesFamille] ([code],[libelle]) VALUES ('SOUS_STATION','Sous station')


---------------------FIL-158 Coloration dans les r�gles---------------------

USE [s2002m401]
GO

INSERT INTO [dbo].[Rules]([nom],[id_rules_reseaus101m401])
     VALUES ('DEFAUT',1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constEvenermentFamille],[id_rules_reseaus101m401])
     VALUES ('Ev�nements OBJETS_TROUVES',18,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constEvenermentFamille],[id_rules_reseaus101m401])
     VALUES ('Ev�nements MODIFICATION_OFFRE',20,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constEvenermentFamille],[id_rules_reseaus101m401])
     VALUES ('Ev�nements ERREUR_HUMAINE',17,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constEvenermentFamille],[id_rules_reseaus101m401])
     VALUES ('Ev�nements PERSONNEL',8,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constEvenermentFamille],[id_rules_reseaus101m401])
     VALUES ('Ev�nements ACCIDENT',3,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constEvenermentFamille],[id_rules_reseaus101m401])
     VALUES ('Ev�nements ASSISTANCE_CLIENT',25,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constEvenermentFamille],[id_rules_reseaus101m401])
     VALUES ('Ev�nements SECURITE_FERROVIAIRE',24,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constEvenermentFamille],[id_rules_reseaus101m401])
     VALUES ('Ev�nements PERTURBATION',22,1)

INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES ('Ressource VOIE',22,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES ('Ressource LAC',23,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES ('Ressource SIG',24,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES ('Ressource SIG_FERRO',25,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES ('Ressource POINT_HAUT',26,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES ('Ressource DAT',27,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES ('Ressource ABRI_VOYAGEUR',28,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES ('Ressource CAMERA',29,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[id_rules_reseaus101m401])
     VALUES ('Ressource SOUS_STATION',30,1)

INSERT INTO [dbo].[Rules]([nom],[ponderation_seuil],[id_rules_reseaus101m401])
     VALUES ('Pond�ration >= 0',0,1)
INSERT INTO [dbo].[Rules]([nom],[ponderation_seuil],[id_rules_reseaus101m401])
     VALUES ('Pond�ration >= 2',2,1)
INSERT INTO [dbo].[Rules]([nom],[ponderation_seuil],[id_rules_reseaus101m401])
     VALUES ('Pond�ration >= 4',4,1)
INSERT INTO [dbo].[Rules]([nom],[ponderation_seuil],[id_rules_reseaus101m401])
     VALUES ('Pond�ration >= 6',6,1)
INSERT INTO [dbo].[Rules]([nom],[ponderation_seuil],[id_rules_reseaus101m401])
     VALUES ('Pond�ration >= 8',8,1)

INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[avecMaintenance],[id_rules_reseaus101m401])
     VALUES ('Ressource BUS avec maintenance',6,1,1)
INSERT INTO [dbo].[Rules]([nom],[id_rules_constRessourceFamille],[avecMaintenance],[id_rules_reseaus101m401])
     VALUES ('Ressource TRAMWAY avec maintenance',7,1,1)
INSERT INTO [dbo].[Rules]([nom],[avecSecurite],[id_rules_reseaus101m401])
     VALUES ('Pr�sence s�curit�',1,1)
INSERT INTO [dbo].[Rules]([nom],[avecDommage],[id_rules_reseaus101m401])
     VALUES ('Pr�sence dommage',1,1)


INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Marron clair','rgb(200, 173, 127)','rgb(0,0,0)','rgb(139, 108, 66)','rgb(255,255,255)',1,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Marron','rgb(187, 174, 152)','rgb(0,0,0)','rgb(104, 94, 67)','rgb(255,255,255)',2,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Orange fonc�','rgb(255, 203, 96)','rgb(0,0,0)','rgb(237, 127, 16)','rgb(255,255,255)',100,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Orange','rgb(255, 228, 196)','rgb(0,0,0)','rgb(250, 164, 1)','rgb(255,255,255)',101,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Violet','rgb(254, 191, 210)','rgb(0,0,0)','rgb(212, 115, 212)','rgb(255,255,255)',30,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Vert','rgb(176, 242, 182)','rgb(255,255,255)','rgb(130, 196, 108)','rgb(255,255,255)',10,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Vert fonc�','rgb(130, 196, 108)','rgb(0,0,0)','rgb(20, 148, 20)','rgb(255,255,255)',11,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Bleu fonc�','rgb(116, 208, 241)','rgb(0,0,0)','rgb(0, 32, 96)','rgb(255,255,255)',21,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Bleu','rgb(169, 234, 254)','rgb(0,0,0)','rgb(0,127,255)','rgb(255,255,255)',20,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Rouge','rgb(254, 150, 160)','rgb(0,0,0)','rgb(255, 0, 0)','rgb(255,255,255)',200,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Rouge fonc�','rgb(254, 150, 160)','rgb(0,0,0)','rgb(199, 44, 72)','rgb(255,255,255)',201,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Jaune','rgb(255, 255, 153)','rgb(0,0,0)','rgb(255, 228, 54)','rgb(0,0,0)',40,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Jaune fonc�','rgb(255, 255, 153)','rgb(255,255,255)','rgb(255, 215, 0)','rgb(255,255,255)',41,1)

INSERT INTO [dbo].[Coloration]([label],[backgroudColorBody],[fontColorBody],[backgroudColorHead],[fontColorHead],[priorite],[id_coloration_reseaus101m401])
VALUES('Gris','rgb(221, 221, 221)','rgb(0,0,0)','rgb(128, 128, 128)','rgb(255,255,255)',0,1)


---------!!!!!!!!!!!!!!!!!!!!!! FAIRE LES CFG A LA MAIN 


---------------------FIL-240 Cat�gorie �v�nement PMR---------------------

INSERT INTO [dbo].[ConstEvenementFamille] ([code],[libelle]) VALUES ('PMR','PMR')


---------------------FIL-160 Section Precision---------------------

INSERT INTO [dbo].[ConstEvenementAttributGroup]([nom],[rang])
     VALUES ('Pr�cisions',3)

INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (15,'PRECISION_SELECT_VALUE_1','Pr�cision select valeur 1',1,1,1,1,'Pr�cisions')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (15,'PRECISION_SELECT_VALUE_2','Pr�cision select valeur 2',1,1,1,2,'Pr�cisions')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (15,'PRECISION_SELECT_VALUE_3','Pr�cision select valeur 3',1,1,1,3,'Pr�cisions')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (15,'PRECISION_TIME_1','Pr�cision heure 1',1,0,0,4,'Pr�cisions')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (15,'PRECISION_TIME_2','Pr�cision heure 2',1,0,0,5,'Pr�cisions')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (15,'PRECISION_TIME_3','Pr�cision heure 3',1,0,0,6,'Pr�cisions')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (15,'PRECISION_TEXT_1','Pr�cision texte 1',1,0,0,7,'Pr�cisions')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (15,'PRECISION_TEXT_2','Pr�cision texte 2',1,0,0,8,'Pr�cisions')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (15,'PRECISION_TEXT_3','Pr�cision texte 3',1,0,0,9,'Pr�cisions')

INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('PRECISION_SELECT_VALUE_1','Pr�cision select valeur 1')
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('PRECISION_SELECT_VALUE_2','Pr�cision select valeur 2')
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('PRECISION_SELECT_VALUE_3','Pr�cision select valeur 3')
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('PRECISION_TIME_1','Pr�cision heure 1')
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('PRECISION_TIME_2','Pr�cision heure 2')
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('PRECISION_TIME_3','Pr�cision heure 3')
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('PRECISION_TEXT_1','Pr�cision texte 1')
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('PRECISION_TEXT_2','Pr�cision texte 2')
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('PRECISION_TEXT_3','Pr�cision texte 3')

INSERT INTO [dbo].[ConstEvenementSection]([code],[libelle])
     VALUES ('PRECISION','Pr�cisions')

---------------------FIL-160 Section Info voyageur---------------------

INSERT INTO [dbo].[EvenementInfoVoyageurMedia]([nom],[id_evenementInfoVoyageurMedia_reseaus101m401])
     VALUES('Mail',1)
INSERT INTO [dbo].[EvenementInfoVoyageurMedia]([nom],[id_evenementInfoVoyageurMedia_reseaus101m401])
     VALUES('SMS',1)
INSERT INTO [dbo].[EvenementInfoVoyageurMedia]([nom],[id_evenementInfoVoyageurMedia_reseaus101m401])
     VALUES('Site internet',1)

INSERT INTO [dbo].[ConstEvenementAttributGroup]([nom],[rang])
     VALUES ('Info. voyageurs',10)

INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (16,'INFO_VOYAGEUR_MEDIA','Info voyageur m�dia',1,1,1,1,'Info. voyageur')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (16,'INFO_VOYAGEUR_ADHERENT','Info voyageur adh�rents',1,0,0,2,'Info. voyageur')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (16,'INFO_VOYAGEUR_MESSAGE','Info voyageur message',1,0,0,2,'Info. voyageur')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (16,'INFO_VOYAGEUR_DATE_DEBUT','Info voyageur date d�but',1,1,1,3,'Info. voyageur')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (16,'INFO_VOYAGEUR_HEURE_DEBUT','Info voyageur heure d�but',1,1,1,4,'Info. voyageur')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (16,'INFO_VOYAGEUR_DATE_FIN','Info voyageur date fin',1,1,1,5,'Info. voyageur')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (16,'INFO_VOYAGEUR_HEURE_FIN','Info voyageur heure fin',1,1,1,6,'Info. voyageur')

INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('INFO_VOYAGEUR_MEDIA','Info voyageurs m�dia')
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('INFO_VOYAGEUR_ADHERENT','Info voyageurs adh�rents')
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('INFO_VOYAGEUR_MESSAGE','Info voyageurs message')	
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('INFO_VOYAGEUR_DATE_DEBUT','Info voyageurs date d�but')
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('INFO_VOYAGEUR_HEURE_DEBUT','Info voyageurs heure d�but')
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('INFO_VOYAGEUR_DATE_FIN','Info voyageurs date fin')
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('INFO_VOYAGEUR_HEURE_FIN','Info voyageurs heure fin')

INSERT INTO [dbo].[ConstEvenementSection]([code],[libelle])
     VALUES ('INFO_VOYAGEUR','Info voyageurs')

---------------------FIL-150 Ajustement dommage---------------------

INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (9,'DOMMAGE_CONTEXTE','Dommage contexte',1,1,1,11,'Dommage')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (9,'DOMMAGE_TIERS_AGE','Dommage tiers �ge',1,1,1,12,'Dommage')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (9,'DOMMAGE_TIERS_TYPE','Dommage tiers type',1,1,1,13,'Dommage')

INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('DOMMAGE_CONTEXTE','Dommage contexte')
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('DOMMAGE_TIERS_AGE','Dommage tiers �ge')
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('DOMMAGE_TIERS_TYPE','Dommage tiers type')

---------------------FIL-161 Section agent---------------------

INSERT INTO [dbo].[EvenementRHType]([nom])
     VALUES('Ressources humaines')


INSERT INTO [dbo].[EvenementRHPrecision]([id_evenementRHPrecision_RHType],[nom],[withHeureSuppl�mentaire],[withAffectation],[withAgentConduite],[withAgentAdmin])
     VALUES(1,'Heures supp. admninistratif',1,0,0,1)
INSERT INTO [dbo].[EvenementRHPrecision]([id_evenementRHPrecision_RHType],[nom],[withHeureSuppl�mentaire],[withAffectation],[withAgentConduite],[withAgentAdmin])
     VALUES(1,'Heures supp. conduite',1,0,1,0)
INSERT INTO [dbo].[EvenementRHPrecision]([id_evenementRHPrecision_RHType],[nom],[withHeureSuppl�mentaire],[withAffectation],[withAgentConduite],[withAgentAdmin])
     VALUES(1,'Droit de retrait',0,0,1,1)
INSERT INTO [dbo].[EvenementRHPrecision]([id_evenementRHPrecision_RHType],[nom],[withHeureSuppl�mentaire],[withAffectation],[withAgentConduite],[withAgentAdmin])
     VALUES(1,'Suivi agent',0,0,1,1)

INSERT INTO [dbo].[ConstEvenementAttributGroup]([nom],[rang])
     VALUES ('RH',7)

INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (17,'RH_TYPE','RH Type',1,1,1,1,'RH')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (17,'RH_AGENT','RH Agent',1,1,1,2,'RH')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (17,'RH_HEUREE_SUPP','RH Heures supp.',1,1,1,3,'RH')
INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (17,'RH_DESCRIPTION','RH Description',1,1,1,4,'RH')

INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('RH_TYPE','RH Type')
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('RH_AGENT','RH Agent')
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('RH_HEUREE_SUPP','RH Heures supp.')
INSERT INTO [dbo].[ConstEvenementControl]([code],[libelle])
	VALUES ('RH_DESCRIPTION','RH Description')

INSERT INTO [dbo].[ConstEvenementSection]([code],[libelle])
     VALUES ('RH','Ressources humaines')

---------------------FIL-256 Nouveaux crit�res---------------------

INSERT INTO [dbo].[ConstQueryCriteriaGroup]
           ([nom])
     VALUES
           ('S�curti�')
INSERT INTO [dbo].[ConstQueryCriteriaGroup]
           ([nom])
     VALUES
           ('Cons�quences exploitation')

INSERT INTO [dbo].[ConstQueryCriteria]
           ([dataType]
           ,[libelle]
           ,[id_queryCriteria_queryCriteriaGroup]
           ,[code]
           ,[isEligibleOnTheFlight]
           ,[isEligibleAdvancedMode]
           ,[description])
     VALUES
           ('CHECK_PRESENCE'
           ,'Info. voyageur'
           ,4
           ,'WITH_INFO_VOYAGEUR'
           ,1
           ,1
           ,'Ev�nement contenant des informations de infos. voyageurs')

INSERT INTO [dbo].[ConstQueryCriteria]
           ([dataType]
           ,[libelle]
           ,[id_queryCriteria_queryCriteriaGroup]
           ,[code]
           ,[isEligibleOnTheFlight]
           ,[isEligibleAdvancedMode]
           ,[description])
     VALUES
           ('CHECK_PRESENCE'
           ,'Fichiers'
           ,4
           ,'WITH_FICHIER'
           ,1
           ,1
           ,'Ev�nement contenant des informations de fichiers')

INSERT INTO [dbo].[ConstQueryCriteria]
           ([dataType]
           ,[libelle]
           ,[id_queryCriteria_queryCriteriaGroup]
           ,[code]
           ,[isEligibleOnTheFlight]
           ,[isEligibleAdvancedMode]
           ,[description])
     VALUES
           ('CHECK_PRESENCE'
           ,'RH'
           ,4
           ,'WITH_RH'
           ,1
           ,1
           ,'Ev�nement contenant des informations de RH')

INSERT INTO [dbo].[ConstQueryCriteria]
           ([dataType]
           ,[libelle]
           ,[id_queryCriteria_queryCriteriaGroup]
           ,[code]
           ,[isEligibleOnTheFlight]
           ,[isEligibleAdvancedMode]
           ,[description])
     VALUES
           ('SELECT_OBJECT'
           ,'S�curti� pr�cisions'
           ,6
           ,'SECURITE_PRECISION'
           ,0
           ,1
           ,'S�curti� pr�cisions')

INSERT INTO [dbo].[ConstQueryCriteria]
           ([dataType]
           ,[libelle]
           ,[id_queryCriteria_queryCriteriaGroup]
           ,[code]
           ,[isEligibleOnTheFlight]
           ,[isEligibleAdvancedMode]
           ,[description])
     VALUES
           ('STRING'
           ,'Km perdus'
           ,7
           ,'KM_PERDU_TOTAL'
           ,0
           ,1
           ,'Km perdus total �v�nement')

---------------------FIL-149 Habilitation ressources---------------------
INSERT INTO [dbo].[ConstRessourceTag]([code],[label])
     VALUES('HABILITATION_ARTICULE','ART')
INSERT INTO [dbo].[ConstRessourceTag]([code],[label])
     VALUES('HABILITATION_TRAMWAY','TRAM')
INSERT INTO [dbo].[ConstRessourceTag]([code],[label])
     VALUES('HABILITATION_PMR','PMR')
INSERT INTO [dbo].[ConstRessourceTag]([code],[label])
     VALUES('RESTRICTION_MEDICAL','Restriction m�dical')


---------------------FIL-229 ---------------------
INSERT INTO [dbo].[ConstRessourcesFamille] ([code],[libelle]) VALUES ('DEPOT_BUS_TRAM','D�p�t Bus et Tramway')
INSERT INTO [dbo].[ConstRessourcesFamille] ([code],[libelle]) VALUES ('ATELIER_BUS_TRAM','Atelier Bus et Tramway')
INSERT INTO [dbo].[ConstRessourcesFamille] ([code],[libelle]) VALUES ('ATELIER_BUS','Atelier Bus')
INSERT INTO [dbo].[ConstRessourcesFamille] ([code],[libelle]) VALUES ('ATELIER_TRAM','Atelier Tramway')
INSERT INTO [dbo].[ConstRessourcesFamille] ([code],[libelle]) VALUES ('SANISETTE','Sanisettes')
INSERT INTO [dbo].[ConstRessourcesFamille] ([code],[libelle]) VALUES ('AGENCE_COMMERCIALE','Agence commerciales')
INSERT INTO [dbo].[ConstRessourcesFamille] ([code],[libelle]) VALUES ('SALLE_CONDUCTEUR','Salle conducteur')
INSERT INTO [dbo].[ConstRessourcesFamille] ([code],[libelle]) VALUES ('BATIMENT','Batiment')
INSERT INTO [dbo].[ConstRessourcesFamille] ([code],[libelle]) VALUES ('PARKING_RELAIS','P+R')
GO

---------------------FIL-149 Habilitation ressources---------------------
INSERT INTO [dbo].[ConstRessourceTag]([code],[label])
     VALUES('ADRESSE','Adresse')
INSERT INTO [dbo].[ConstRessourceTag]([code],[label])
     VALUES('HABILITATION_LIGNE','Ligne habilit�es')
INSERT INTO [dbo].[ConstRessourceTag]([code],[label])
     VALUES('TELEPHONE','T�l�phone')
INSERT INTO [dbo].[ConstRessourceTag]([code],[label])
     VALUES('HABILITATION_STANDARD','STD')

---------------------Nouvelles cons�quences avance/retard---------------------
	 INSERT INTO [dbo].[ConstEvenementExploitConseq]
           ([code]
           ,[with_lignes2006m401]
           ,[label_lignes2006m401]
           ,[with_parcourss2006m401]
           ,[label_parcourss2006m401]
           ,[with_directions2006m401]
           ,[label_directions2006m401]
           ,[with_arrets2006m401_1]
           ,[arrets2006m401_stationPrincipale_1]
           ,[label_arrets2006m401_1]
           ,[with_arrets2006m401_2]
           ,[arrets2006m401_stationPrincipale_2]
           ,[lable_arrets2006m401_2]
           ,[with_heure_1]
           ,[lable_heure_1]
           ,[with_heure_2]
           ,[label_heure_2]
           ,[with_nb_departs]
           ,[lable_nb_departs]
           ,[with_nb_courses]
           ,[label_nb_courses]
           ,[with_nb_km]
           ,[label_nb_km]
           ,[signe_nb_km]
           ,[with_d�calage_min]
           ,[label_d�calage_min]
           ,[with_d�calage_max]
           ,[label_d�calage_max]
           ,[with_arretsDepart2006m401_1]
           ,[label_arretsDepart2006m401_1]
           ,[with_arretsDepart2006m401_2]
           ,[label_arretsDepart2006m401_2]
           ,[with_ressourceMateriel]
           ,[label_ressourceMateriel]
           ,[with_ressourceHomme]
           ,[label_ressourceHomme]
           ,[with_withRetombe]
           ,[label_withRetombe]
           ,[with_observations]
           ,[label_observations])
     VALUES
           ('RETARD'
           ,0
           ,'LABEL_A_COMPLETER'
           ,1
           ,'Parcours'
           ,1
           ,'Ligne / Direction'
           ,1
           ,0
           ,'Arr�t'
           ,0
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,'-'
           ,1
           ,'minutes (+)'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER')


		   INSERT INTO [dbo].[ConstEvenementExploitConseq]
           ([code]
           ,[with_lignes2006m401]
           ,[label_lignes2006m401]
           ,[with_parcourss2006m401]
           ,[label_parcourss2006m401]
           ,[with_directions2006m401]
           ,[label_directions2006m401]
           ,[with_arrets2006m401_1]
           ,[arrets2006m401_stationPrincipale_1]
           ,[label_arrets2006m401_1]
           ,[with_arrets2006m401_2]
           ,[arrets2006m401_stationPrincipale_2]
           ,[lable_arrets2006m401_2]
           ,[with_heure_1]
           ,[lable_heure_1]
           ,[with_heure_2]
           ,[label_heure_2]
           ,[with_nb_departs]
           ,[lable_nb_departs]
           ,[with_nb_courses]
           ,[label_nb_courses]
           ,[with_nb_km]
           ,[label_nb_km]
           ,[signe_nb_km]
           ,[with_d�calage_min]
           ,[label_d�calage_min]
           ,[with_d�calage_max]
           ,[label_d�calage_max]
           ,[with_arretsDepart2006m401_1]
           ,[label_arretsDepart2006m401_1]
           ,[with_arretsDepart2006m401_2]
           ,[label_arretsDepart2006m401_2]
           ,[with_ressourceMateriel]
           ,[label_ressourceMateriel]
           ,[with_ressourceHomme]
           ,[label_ressourceHomme]
           ,[with_withRetombe]
           ,[label_withRetombe]
           ,[with_observations]
           ,[label_observations])
     VALUES
           ('RETARD_GENERAL'
           ,1
           ,'Ligne'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,'-'
           ,1
           ,'minutes (+)'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER')

		    INSERT INTO [dbo].[ConstEvenementExploitConseq]
           ([code]
           ,[with_lignes2006m401]
           ,[label_lignes2006m401]
           ,[with_parcourss2006m401]
           ,[label_parcourss2006m401]
           ,[with_directions2006m401]
           ,[label_directions2006m401]
           ,[with_arrets2006m401_1]
           ,[arrets2006m401_stationPrincipale_1]
           ,[label_arrets2006m401_1]
           ,[with_arrets2006m401_2]
           ,[arrets2006m401_stationPrincipale_2]
           ,[lable_arrets2006m401_2]
           ,[with_heure_1]
           ,[lable_heure_1]
           ,[with_heure_2]
           ,[label_heure_2]
           ,[with_nb_departs]
           ,[lable_nb_departs]
           ,[with_nb_courses]
           ,[label_nb_courses]
           ,[with_nb_km]
           ,[label_nb_km]
           ,[signe_nb_km]
           ,[with_d�calage_min]
           ,[label_d�calage_min]
           ,[with_d�calage_max]
           ,[label_d�calage_max]
           ,[with_arretsDepart2006m401_1]
           ,[label_arretsDepart2006m401_1]
           ,[with_arretsDepart2006m401_2]
           ,[label_arretsDepart2006m401_2]
           ,[with_ressourceMateriel]
           ,[label_ressourceMateriel]
           ,[with_ressourceHomme]
           ,[label_ressourceHomme]
           ,[with_withRetombe]
           ,[label_withRetombe]
           ,[with_observations]
           ,[label_observations])
     VALUES
           ('AVANCE'
           ,0
           ,'LABEL_A_COMPLETER'
           ,1
           ,'Parcours'
           ,1
           ,'Ligne / Direction'
           ,1
           ,0
           ,'Arr�t'
           ,0
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,'-'
           ,1
           ,'minutes (-)'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER')

		   INSERT INTO [dbo].[ConstEvenementExploitConseq]
           ([code]
           ,[with_lignes2006m401]
           ,[label_lignes2006m401]
           ,[with_parcourss2006m401]
           ,[label_parcourss2006m401]
           ,[with_directions2006m401]
           ,[label_directions2006m401]
           ,[with_arrets2006m401_1]
           ,[arrets2006m401_stationPrincipale_1]
           ,[label_arrets2006m401_1]
           ,[with_arrets2006m401_2]
           ,[arrets2006m401_stationPrincipale_2]
           ,[lable_arrets2006m401_2]
           ,[with_heure_1]
           ,[lable_heure_1]
           ,[with_heure_2]
           ,[label_heure_2]
           ,[with_nb_departs]
           ,[lable_nb_departs]
           ,[with_nb_courses]
           ,[label_nb_courses]
           ,[with_nb_km]
           ,[label_nb_km]
           ,[signe_nb_km]
           ,[with_d�calage_min]
           ,[label_d�calage_min]
           ,[with_d�calage_max]
           ,[label_d�calage_max]
           ,[with_arretsDepart2006m401_1]
           ,[label_arretsDepart2006m401_1]
           ,[with_arretsDepart2006m401_2]
           ,[label_arretsDepart2006m401_2]
           ,[with_ressourceMateriel]
           ,[label_ressourceMateriel]
           ,[with_ressourceHomme]
           ,[label_ressourceHomme]
           ,[with_withRetombe]
           ,[label_withRetombe]
           ,[with_observations]
           ,[label_observations])
     VALUES
           ('AVANCE_GENERAL'
           ,1
           ,'Ligne'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,'-'
           ,1
           ,'minutes (-)'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER'
           ,0
           ,'LABEL_A_COMPLETER')



--Version 2.1.1============================================================================================================================================================


--TAM

---------------------FIL-315 Imputabilit�---------------------
INSERT INTO [dbo].[EvenementImputabilite] ([nom],[isDefaut])
     VALUES ('A d�terminer',1)

INSERT INTO [dbo].[EvenementImputabilite] ([nom],[isDefaut])
     VALUES ('Non imputable - exploitation',0)

INSERT INTO [dbo].[EvenementImputabilite] ([nom],[isDefaut])
     VALUES ('Imputable - exploitation',0)

INSERT INTO [dbo].[EvenementImputabilite] ([nom],[isDefaut])
     VALUES ('Imputable - maintenance bus',0)

INSERT INTO [dbo].[EvenementImputabilite] ([nom],[isDefaut])
     VALUES ('Imputable - maintenance tramway',0)

INSERT INTO [dbo].[EvenementImputabilite] ([nom],[isDefaut])
     VALUES ('Imputable - maintenance IF',0)

INSERT INTO [dbo].[EvenementImputabilite] ([nom],[isDefaut])
     VALUES ('Imputable - maintenance syst�me',0)

INSERT INTO [dbo].[ConstEvenementAttribut]([id_constEvenementAttribut_group],[code],[libelle],[isEligibleQueryResult],[isEligibleXaxis],[isEligibleSeriesAnalyse],[rang],[header])
     VALUES (1,'IMPUTATION','Imputation',1,1,1,4,'')

---------------------FIL-315 Crit�re globaux ressources classe / sous classe---------------------
INSERT INTO [dbo].[ConstQueryCriteria]
           ([dataType]
           ,[libelle]
           ,[id_queryCriteria_queryCriteriaGroup]
           ,[code]
           ,[isEligibleOnTheFlight]
           ,[isEligibleAdvancedMode]
           ,[description])
     VALUES
           ('SELECT_OBJECT'
           ,'Ressource classe (�v�nement + affect. + cons�q.)'
           ,3
           ,'ALL_RESSOURCE_CLASSE'
           ,0
           ,1
           ,'Ressource classe �v�nement + affect. + cons�quences d''exploitation (ex : mat�riel roulant, RH,...)')

INSERT INTO [dbo].[ConstQueryCriteria]
           ([dataType]
           ,[libelle]
           ,[id_queryCriteria_queryCriteriaGroup]
           ,[code]
           ,[isEligibleOnTheFlight]
           ,[isEligibleAdvancedMode]
           ,[description])
     VALUES
           ('SELECT_OBJECT'
           ,'Ressource sousclasse (�v�nement+affect.+cons�q.)'
           ,3
           ,'ALL_RESSOURCE_SUBCLASSE'
           ,0
           ,1
           ,'Ressource sous classe �v�nement + affect. + cons�quences d''exploitation (ex : bus, tram,conducteur,...)')

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])
		select 1,id_queryCriteria from [ConstQueryCriteria] where code = 'ALL_RESSOURCE_CLASSE'

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])
		select 1,id_queryCriteria from [ConstQueryCriteria] where code = 'ALL_RESSOURCE_SUBCLASSE'

---------------------FIL-295 FAMILLE S�curti� s�paration BIEN /  ADMIN / CONDUITE---------------------

INSERT INTO [dbo].[ConstEvenementFamille] ([code],[libelle]) VALUES ('SECURTIE_BIEN_FIXE','S�curit� biens fixes')
INSERT INTO [dbo].[ConstEvenementFamille] ([code],[libelle]) VALUES ('SECURTIE_BIEN_MR','S�curit� biens roulants')
INSERT INTO [dbo].[ConstEvenementFamille] ([code],[libelle]) VALUES ('SECURTIE_AGENT','S�curit� agents')
INSERT INTO [dbo].[ConstEvenementFamille] ([code],[libelle]) VALUES ('SECURTIE_CONDUITE','S�curit� agents conduite')

---------------------FIL-294 FAMILLE Outils informatique---------------------

INSERT INTO [dbo].[ConstRessourcesFamille] ([code],[libelle]) VALUES ('OUTILS_INFORMATIQUE','Outil informatique')

--CTVH
﻿/*
Script de déploiement pour s2002m401

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "s2002m401"
:setvar DefaultFilePrefix "s2002m401"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.DEVSQLSP\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.DEVSQLSP\MSSQL\DATA\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Suppression de [dbo].[FK_EvenementExploitationConseq_EvenementExploitationConseq]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] DROP CONSTRAINT [FK_EvenementExploitationConseq_EvenementExploitationConseq];


GO
PRINT N'Suppression de [dbo].[FK_EvenementExploitationConseq_EvenementList]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] DROP CONSTRAINT [FK_EvenementExploitationConseq_EvenementList];


GO
PRINT N'Suppression de [dbo].[FK_EvenementExploitationConseq_ExploitationConseqManoeuvre]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] DROP CONSTRAINT [FK_EvenementExploitationConseq_ExploitationConseqManoeuvre];


GO
PRINT N'Suppression de [dbo].[FK_EvenementExploitationConseq_RessourceHomme]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] DROP CONSTRAINT [FK_EvenementExploitationConseq_RessourceHomme];


GO
PRINT N'Suppression de [dbo].[FK_EvenementExploitationConseq_RessourceVehicule]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] DROP CONSTRAINT [FK_EvenementExploitationConseq_RessourceVehicule];


GO
PRINT N'Modification de [dbo].[AleaListe]...';


GO
ALTER TABLE [dbo].[AleaListe]
    ADD [longitude_saisie] FLOAT (53) CONSTRAINT [DF_AleaListe_longitude_saisie] DEFAULT ((0)) NOT NULL,
        [latitude_saisie]  FLOAT (53) CONSTRAINT [DF_AleaListe_latitude_saisie] DEFAULT ((0)) NOT NULL;


GO
PRINT N'Modification de [dbo].[EvenementDommage]...';


GO
ALTER TABLE [dbo].[EvenementDommage]
    ADD [id_utilisateur]   INT            NULL,
        [loginUtilisateur] NVARCHAR (MAX) NULL;


GO
PRINT N'Début de la régénération de la table [dbo].[EvenementExploitationConseq]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_EvenementExploitationConseq] (
    [id_evenementExploitationConseq]                             INT            IDENTITY (1, 1) NOT NULL,
    [id_evenementExploitationConseq_exploitationConseqManoeuvre] INT            NOT NULL,
    [id_evenementExploitationConseq_evenementListe]              INT            NOT NULL,
    [id_evenementExploitationConseq_lignes2006m401]              INT            NULL,
    [id_evenementExploitationConseq_parcourss2006m401]           INT            NULL,
    [id_evenementExploitationConseq_directions2006m401]          INT            NULL,
    [id_eevenementExploitationConseq_arrets2006m401_1]           INT            NULL,
    [id_eevenementExploitationConseq_arrets2006m401_2]           INT            NULL,
    [heure_1]                                                    TIME (7)       NULL,
    [heure_2]                                                    TIME (7)       NULL,
    [nb_departs]                                                 INT            NULL,
    [nb_courses]                                                 NVARCHAR (50)  NULL,
    [nb_courses_auto]                                            FLOAT (53)     NULL,
    [nb_km]                                                      FLOAT (53)     NULL,
    [nb_km_auto]                                                 FLOAT (53)     NULL,
    [id_evenementExploitationConseq_svs2006m401_auto]            INT            NULL,
    [décalage_min]                                               FLOAT (53)     NULL,
    [décalage_max]                                               FLOAT (53)     NULL,
    [id_evenementExploitationConseq_parent]                      INT            NULL,
    [id_eevenementExploitationConseq_arretsDepart2006m401_1]     INT            NULL,
    [id_eevenementExploitationConseq_arretsDepart2006m401_2]     INT            NULL,
    [withRetombe]                                                INT            NULL,
    [id_eevenementExploitationConseq_ressourceHomme]             INT            NULL,
    [id_eevenementExploitationConseq_ressourceVehicule]          INT            NULL,
    [observation]                                                NVARCHAR (MAX) NULL,
    [ligne_code]                                                 NVARCHAR (MAX) NULL,
    [ligne_nom]                                                  NVARCHAR (MAX) NULL,
    [parcours_code]                                              NVARCHAR (MAX) NULL,
    [parcours_nom]                                               NVARCHAR (MAX) NULL,
    [parcours_description]                                       NVARCHAR (MAX) NULL,
    [id_utilisateur]                                             INT            NULL,
    [loginUtilisateur]                                           NVARCHAR (MAX) NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK__Exploita__0C4C4FF61412A0791] PRIMARY KEY CLUSTERED ([id_evenementExploitationConseq] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[EvenementExploitationConseq])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementExploitationConseq] ON;
        INSERT INTO [dbo].[tmp_ms_xx_EvenementExploitationConseq] ([id_evenementExploitationConseq], [id_evenementExploitationConseq_exploitationConseqManoeuvre], [id_evenementExploitationConseq_evenementListe], [id_evenementExploitationConseq_lignes2006m401], [id_evenementExploitationConseq_parcourss2006m401], [id_evenementExploitationConseq_directions2006m401], [id_eevenementExploitationConseq_arrets2006m401_1], [id_eevenementExploitationConseq_arrets2006m401_2], [heure_1], [heure_2], [nb_departs], [nb_courses], [nb_km], [décalage_min], [décalage_max], [id_evenementExploitationConseq_parent], [id_eevenementExploitationConseq_arretsDepart2006m401_1], [id_eevenementExploitationConseq_arretsDepart2006m401_2], [withRetombe], [id_eevenementExploitationConseq_ressourceHomme], [id_eevenementExploitationConseq_ressourceVehicule], [observation], [ligne_code], [ligne_nom], [parcours_code], [parcours_nom], [parcours_description])
        SELECT   [id_evenementExploitationConseq],
                 [id_evenementExploitationConseq_exploitationConseqManoeuvre],
                 [id_evenementExploitationConseq_evenementListe],
                 [id_evenementExploitationConseq_lignes2006m401],
                 [id_evenementExploitationConseq_parcourss2006m401],
                 [id_evenementExploitationConseq_directions2006m401],
                 [id_eevenementExploitationConseq_arrets2006m401_1],
                 [id_eevenementExploitationConseq_arrets2006m401_2],
                 [heure_1],
                 [heure_2],
                 [nb_departs],
                 [nb_courses],
                 [nb_km],
                 [décalage_min],
                 [décalage_max],
                 [id_evenementExploitationConseq_parent],
                 [id_eevenementExploitationConseq_arretsDepart2006m401_1],
                 [id_eevenementExploitationConseq_arretsDepart2006m401_2],
                 [withRetombe],
                 [id_eevenementExploitationConseq_ressourceHomme],
                 [id_eevenementExploitationConseq_ressourceVehicule],
                 [observation],
                 [ligne_code],
                 [ligne_nom],
                 [parcours_code],
                 [parcours_nom],
                 [parcours_description]
        FROM     [dbo].[EvenementExploitationConseq]
        ORDER BY [id_evenementExploitationConseq] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementExploitationConseq] OFF;
    END

DROP TABLE [dbo].[EvenementExploitationConseq];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_EvenementExploitationConseq]', N'EvenementExploitationConseq';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK__Exploita__0C4C4FF61412A0791]', N'PK__Exploita__0C4C4FF61412A079', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Modification de [dbo].[EvenementIntervention]...';


GO
ALTER TABLE [dbo].[EvenementIntervention]
    ADD [id_utilisateur]   INT            NULL,
        [loginUtilisateur] NVARCHAR (MAX) NULL;


GO
PRINT N'Modification de [dbo].[EvenementMaintenance]...';


GO
ALTER TABLE [dbo].[EvenementMaintenance]
    ADD [id_utilisateur]   INT            NULL,
        [loginUtilisateur] NVARCHAR (MAX) NULL;


GO
PRINT N'Modification de [dbo].[EvenementMaintenanceOrgane]...';


GO
ALTER TABLE [dbo].[EvenementMaintenanceOrgane]
    ADD [isExpendable] INT CONSTRAINT [DF_EvenementMaintenanceOrgane_isExpendable] DEFAULT ((0)) NOT NULL;


GO
PRINT N'Modification de [dbo].[EvenementSecurite]...';


GO
ALTER TABLE [dbo].[EvenementSecurite]
    ADD [id_utilisateur]   INT            NULL,
        [loginUtilisateur] NVARCHAR (MAX) NULL;


GO
PRINT N'Création de [dbo].[FK_EvenementExploitationConseq_EvenementExploitationConseq]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementExploitationConseq_EvenementExploitationConseq] FOREIGN KEY ([id_evenementExploitationConseq_parent]) REFERENCES [dbo].[EvenementExploitationConseq] ([id_evenementExploitationConseq]);


GO
PRINT N'Création de [dbo].[FK_EvenementExploitationConseq_EvenementList]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementExploitationConseq_EvenementList] FOREIGN KEY ([id_evenementExploitationConseq_evenementListe]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
PRINT N'Création de [dbo].[FK_EvenementExploitationConseq_ExploitationConseqManoeuvre]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementExploitationConseq_ExploitationConseqManoeuvre] FOREIGN KEY ([id_evenementExploitationConseq_exploitationConseqManoeuvre]) REFERENCES [dbo].[EvenementExploitationConseqManoeuvre] ([id_exploitationConseqManoeuvre]);


GO
PRINT N'Création de [dbo].[FK_EvenementExploitationConseq_RessourceHomme]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementExploitationConseq_RessourceHomme] FOREIGN KEY ([id_eevenementExploitationConseq_ressourceHomme]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
PRINT N'Création de [dbo].[FK_EvenementExploitationConseq_RessourceVehicule]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementExploitationConseq_RessourceVehicule] FOREIGN KEY ([id_eevenementExploitationConseq_ressourceVehicule]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
PRINT N'Modification de [dbo].[VueAleaListe]...';


GO
ALTER VIEW dbo.VueAleaListe
AS
SELECT        dbo.AleaListe.id_aleaListe, dbo.AleaListe.timeStamp, dbo.AleaListe.comentaires, dbo.AleaListe.dateApparition, dbo.AleaListe.heureApparition, dbo.AleaListe.heureFin, dbo.AleaType.id_aleaType, 
                         dbo.AleaType.nom AS [AleaType nom], dbo.AleaEtat.id_aleaEtat, dbo.AleaEtat.nom AS [AleaEtat nom], dbo.AleaListe.indentifiant, dbo.AleaListe.longitude_saisie, dbo.AleaListe.latitude_saisie
FROM            dbo.AleaListe INNER JOIN
                         dbo.AleaType ON dbo.AleaListe.id_aleaListe_aleaType = dbo.AleaType.id_aleaType INNER JOIN
                         dbo.AleaEtat ON dbo.AleaListe.id_aleaListe_aleaEtat = dbo.AleaEtat.id_aleaEtat
GO
PRINT N'Modification de [dbo].[VueAleaListeMateriel]...';


GO
ALTER VIEW dbo.VueAleaListeMateriel
AS
SELECT        dbo.AleaListe.id_aleaListe, dbo.AleaListe.timeStamp, dbo.AleaListe.comentaires, dbo.AleaListe.dateApparition, dbo.AleaListe.heureApparition, dbo.AleaListe.heureFin, dbo.AleaType.id_aleaType, 
                         dbo.AleaType.nom AS [AleaType nom], dbo.AleaEtat.id_aleaEtat, dbo.AleaEtat.nom AS [AleaEtat nom], dbo.MaterielListe.id_materielListe, dbo.MaterielListe.id_reseau, dbo.AleaListe.indentifiant, 
                         dbo.AleaListe.ponderation, dbo.AleaListe.id_aleaListe_ressourceRemplacement, dbo.AleaListe.longitude_saisie, dbo.AleaListe.latitude_saisie
FROM            dbo.AleaListe INNER JOIN
                         dbo.AleaType ON dbo.AleaListe.id_aleaListe_aleaType = dbo.AleaType.id_aleaType INNER JOIN
                         dbo.AleaEtat ON dbo.AleaListe.id_aleaListe_aleaEtat = dbo.AleaEtat.id_aleaEtat INNER JOIN
                         dbo.LnkMaterielListeAleaListe ON dbo.AleaListe.id_aleaListe = dbo.LnkMaterielListeAleaListe.id_materielListeAleaListe_aleaListe INNER JOIN
                         dbo.MaterielListe ON dbo.LnkMaterielListeAleaListe.id_materielListeAleaListe_materielList = dbo.MaterielListe.id_materielListe
GO
PRINT N'Actualisation de [dbo].[VueMaterielListeCountAleaEtat]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[VueMaterielListeCountAleaEtat]';


GO
PRINT N'Modification de [dbo].[VueAleaListe].[MS_DiagramPane1]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[35] 4[26] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "AleaListe"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 294
            End
            DisplayFlags = 280
            TopColumn = 13
         End
         Begin Table = "AleaType"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 251
               Right = 258
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AleaEtat"
            Begin Extent = 
               Top = 138
               Left = 296
               Bottom = 234
               Right = 466
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueAleaListe';


GO
PRINT N'Modification de [dbo].[VueAleaListeMateriel].[MS_DiagramPane1]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "AleaListe"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 294
            End
            DisplayFlags = 280
            TopColumn = 13
         End
         Begin Table = "AleaType"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 251
               Right = 258
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AleaEtat"
            Begin Extent = 
               Top = 138
               Left = 296
               Bottom = 234
               Right = 466
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LnkMaterielListeAleaListe"
            Begin Extent = 
               Top = 252
               Left = 38
               Bottom = 365
               Right = 322
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterielListe"
            Begin Extent = 
               Top = 366
               Left = 38
               Bottom = 496
               Right = 281
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
        ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueAleaListeMateriel';


GO
PRINT N'Vérification de données existantes par rapport aux nouvelles contraintes';


GO
USE [$(DatabaseName)];


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] WITH CHECK CHECK CONSTRAINT [FK_EvenementExploitationConseq_EvenementExploitationConseq];

ALTER TABLE [dbo].[EvenementExploitationConseq] WITH CHECK CHECK CONSTRAINT [FK_EvenementExploitationConseq_EvenementList];

ALTER TABLE [dbo].[EvenementExploitationConseq] WITH CHECK CHECK CONSTRAINT [FK_EvenementExploitationConseq_ExploitationConseqManoeuvre];

ALTER TABLE [dbo].[EvenementExploitationConseq] WITH CHECK CHECK CONSTRAINT [FK_EvenementExploitationConseq_RessourceHomme];

ALTER TABLE [dbo].[EvenementExploitationConseq] WITH CHECK CHECK CONSTRAINT [FK_EvenementExploitationConseq_RessourceVehicule];


GO
PRINT N'Mise à jour terminée.';


GO

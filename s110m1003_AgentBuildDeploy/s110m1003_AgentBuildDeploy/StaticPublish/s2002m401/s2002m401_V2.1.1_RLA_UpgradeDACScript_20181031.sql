/*
Script de déploiement pour s2002m401

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "s2002m401"
:setvar DefaultFilePrefix "s2002m401"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.RLARECETTE\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.RLARECETTE\MSSQL\DATA\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO

IF (SELECT OBJECT_ID('tempdb..#tmpErrors')) IS NOT NULL DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
GO
BEGIN TRANSACTION
GO
PRINT N'Suppression de [dbo].[DF_AleaListe_dateModificationModel]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [DF_AleaListe_dateModificationModel];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[DF_AleaListe_indentifiant]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [DF_AleaListe_indentifiant];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[DF_AleaListe_latitude_saisie]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [DF_AleaListe_latitude_saisie];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[DF_AleaListe_longitude_saisie]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [DF_AleaListe_longitude_saisie];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[DF_AleaListe_ponderation]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [DF_AleaListe_ponderation];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_EvenementFichier_AleaListe]...';


GO
ALTER TABLE [dbo].[EvenementFichier] DROP CONSTRAINT [FK_EvenementFichier_AleaListe];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_EvenementHistoriqueView_Evenement]...';


GO
ALTER TABLE [dbo].[EvenementHistoriqueView] DROP CONSTRAINT [FK_EvenementHistoriqueView_Evenement];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_EvenementIntervention_EvenementList]...';


GO
ALTER TABLE [dbo].[EvenementIntervention] DROP CONSTRAINT [FK_EvenementIntervention_EvenementList];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_EvenementMaintenance_EvenementListe]...';


GO
ALTER TABLE [dbo].[EvenementMaintenance] DROP CONSTRAINT [FK_EvenementMaintenance_EvenementListe];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_AleaHistorique_Evenement]...';


GO
ALTER TABLE [dbo].[AleaHistorique] DROP CONSTRAINT [FK_AleaHistorique_Evenement];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_AleaListe_Affectation]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [FK_AleaListe_Affectation];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_AleaListe_AleaEtat]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [FK_AleaListe_AleaEtat];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_AleaListe_AleaType]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [FK_AleaListe_AleaType];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_AleaListe_EvenementLieu]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [FK_AleaListe_EvenementLieu];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_AleaListe_FermetureRaison]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [FK_AleaListe_FermetureRaison];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_AleaListe_MeteoDetails]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [FK_AleaListe_MeteoDetails];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_AleaListe_Ressource]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [FK_AleaListe_Ressource];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_AleaListe_RessourceRemplacement]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [FK_AleaListe_RessourceRemplacement];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_EvenementSecurite_EvenementListe]...';


GO
ALTER TABLE [dbo].[EvenementSecurite] DROP CONSTRAINT [FK_EvenementSecurite_EvenementListe];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_AleaListAleaHistorique_AleaList]...';


GO
ALTER TABLE [dbo].[LnkAleaListAleaHistorique] DROP CONSTRAINT [FK_AleaListAleaHistorique_AleaList];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_LnkAleaListeFicheRealise_AleaListe]...';


GO
ALTER TABLE [dbo].[LnkAleaListeFicheRealise] DROP CONSTRAINT [FK_LnkAleaListeFicheRealise_AleaListe];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_LnkEvenementAlertHistorique_Evenement]...';


GO
ALTER TABLE [dbo].[LnkEvenementAlertHistorique] DROP CONSTRAINT [FK_LnkEvenementAlertHistorique_Evenement];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_MaterielListeAleaListe_AleaListe]...';


GO
ALTER TABLE [dbo].[LnkMaterielListeAleaListe] DROP CONSTRAINT [FK_MaterielListeAleaListe_AleaListe];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_QueryTriggerAsynchoneHistorique_Alea]...';


GO
ALTER TABLE [dbo].[QueryTriggerAsynchoneHistorique] DROP CONSTRAINT [FK_QueryTriggerAsynchoneHistorique_Alea];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_EvenementDommage_AleaListe]...';


GO
ALTER TABLE [dbo].[EvenementDommage] DROP CONSTRAINT [FK_EvenementDommage_AleaListe];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_EvenementExploitationConseq_EvenementList]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] DROP CONSTRAINT [FK_EvenementExploitationConseq_EvenementList];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_MaterielListe_MaterielDetail]...';


GO
ALTER TABLE [dbo].[MaterielListe] DROP CONSTRAINT [FK_MaterielListe_MaterielDetail];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_CfgEvenementFermetureRaisonFamille_EvenementFamille]...';


GO
ALTER TABLE [dbo].[CfgEvenementFermetureRaisonFamille] DROP CONSTRAINT [FK_CfgEvenementFermetureRaisonFamille_EvenementFamille];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_CfgEvenementFermetureRaisonFamille_EvenementFermetureRaison]...';


GO
ALTER TABLE [dbo].[CfgEvenementFermetureRaisonFamille] DROP CONSTRAINT [FK_CfgEvenementFermetureRaisonFamille_EvenementFermetureRaison];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[CfgEvenementFermetureRaisonFamille]...';


GO
DROP TABLE [dbo].[CfgEvenementFermetureRaisonFamille];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[CfgEvenementPonderationColorRule]...';


GO
DROP TABLE [dbo].[CfgEvenementPonderationColorRule];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[MaterielDetail]...';


GO
DROP TABLE [dbo].[MaterielDetail];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Début de la régénération de la table [dbo].[AleaListe]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_AleaListe] (
    [id_aleaListe]                       INT            IDENTITY (1, 1) NOT NULL,
    [id_aleaListe_aleaEtat]              INT            NOT NULL,
    [id_aleaListe_aleaType]              INT            NOT NULL,
    [timeStamp]                          FLOAT (53)     NOT NULL,
    [dateApparition]                     DATE           NULL,
    [heureApparition]                    TIME (7)       NULL,
    [heureAppel]                         TIME (7)       NULL,
    [comentaires]                        NVARCHAR (MAX) NULL,
    [id_aleaListe_meteoDetails]          INT            NULL,
    [indentifiant]                       NVARCHAR (50)  CONSTRAINT [DF_AleaListe_indentifiant] DEFAULT (N'XXXXXX') NOT NULL,
    [id_aleaListe_evenementLieu]         INT            NULL,
    [dateFin]                            DATE           NULL,
    [heureFin]                           TIME (7)       NULL,
    [id_aleaListe_affectation]           INT            NULL,
    [ponderation]                        INT            CONSTRAINT [DF_AleaListe_ponderation] DEFAULT ((0)) NOT NULL,
    [id_aleaListe_ressourceRemplacement] INT            NULL,
    [id_aleaListe_ressource]             INT            NULL,
    [longitude_saisie]                   FLOAT (53)     CONSTRAINT [DF_AleaListe_longitude_saisie] DEFAULT ((0)) NOT NULL,
    [latitude_saisie]                    FLOAT (53)     CONSTRAINT [DF_AleaListe_latitude_saisie] DEFAULT ((0)) NOT NULL,
    [lastModification]                   DATETIME       CONSTRAINT [DF_AleaListe_dateModificationModel] DEFAULT ('2010-01-01 00:00:00') NOT NULL,
    [id_aleaListe_FermetureRaison]       INT            NULL,
    [id_aleaListe_Imputabilite]          INT            NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK__AleaList__DD8219B1B6F91A411] PRIMARY KEY CLUSTERED ([id_aleaListe] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[AleaListe])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_AleaListe] ON;
        INSERT INTO [dbo].[tmp_ms_xx_AleaListe] ([id_aleaListe], [id_aleaListe_aleaEtat], [id_aleaListe_aleaType], [timeStamp], [dateApparition], [heureApparition], [comentaires], [id_aleaListe_meteoDetails], [indentifiant], [id_aleaListe_evenementLieu], [dateFin], [heureFin], [id_aleaListe_affectation], [ponderation], [id_aleaListe_ressourceRemplacement], [id_aleaListe_ressource], [longitude_saisie], [latitude_saisie], [lastModification], [id_aleaListe_FermetureRaison])
        SELECT   [id_aleaListe],
                 [id_aleaListe_aleaEtat],
                 [id_aleaListe_aleaType],
                 [timeStamp],
                 [dateApparition],
                 [heureApparition],
                 [comentaires],
                 [id_aleaListe_meteoDetails],
                 [indentifiant],
                 [id_aleaListe_evenementLieu],
                 [dateFin],
                 [heureFin],
                 [id_aleaListe_affectation],
                 [ponderation],
                 [id_aleaListe_ressourceRemplacement],
                 [id_aleaListe_ressource],
                 [longitude_saisie],
                 [latitude_saisie],
                 [lastModification],
                 [id_aleaListe_FermetureRaison]
        FROM     [dbo].[AleaListe]
        ORDER BY [id_aleaListe] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_AleaListe] OFF;
    END

DROP TABLE [dbo].[AleaListe];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_AleaListe]', N'AleaListe';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK__AleaList__DD8219B1B6F91A411]', N'PK__AleaList__DD8219B1B6F91A41', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[CfgEvenementMaintenanceOrganeRessource]...';


GO
ALTER TABLE [dbo].[CfgEvenementMaintenanceOrganeRessource] DROP COLUMN [id_cfgEvenementMaintenanceRessource_materielRoulantCategories2006m401];


GO
ALTER TABLE [dbo].[CfgEvenementMaintenanceOrganeRessource]
    ADD [id_cfgEvenementMaintenanceRessource_materielCategorie] INT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[CfgGeneralMainCourante]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante]
    ADD [id_ressourceClasseRH_Imports2006m1055]         INT NULL,
        [id_ressourceEtatDefaut_Imports2006m1055]       INT NULL,
        [id_ressourceEtatExploitation_Imports2006m1055] INT NULL,
        [id_ressourceEtatReserve_Imports2006m1055]      INT NULL,
        [id_ressourceCategorieCR_Imports2006m1055]      INT NULL,
        [id_ressourceCategorieAdmin_Imports2006m1055]   INT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[EvenementAffectation]...';


GO
ALTER TABLE [dbo].[EvenementAffectation] ALTER COLUMN [heurePriseServiceReelle] NVARCHAR (50) NULL;

ALTER TABLE [dbo].[EvenementAffectation] ALTER COLUMN [heurePriseServiceThéorique] NVARCHAR (50) NULL;


GO
ALTER TABLE [dbo].[EvenementAffectation]
    ADD [heureFinServiceReelle]    NVARCHAR (50) NULL,
        [heureFinServiceThéorique] NVARCHAR (50) NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[EvenementDommage]...';


GO
ALTER TABLE [dbo].[EvenementDommage]
    ADD [id_dommageConseq_tiersType] INT NULL,
        [id_dommageConseq_tiersAge]  INT NULL,
        [id_dommageConseq_contexte]  INT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[EvenementDommagePrecision]...';


GO
SET QUOTED_IDENTIFIER ON;

SET ANSI_NULLS OFF;


GO
ALTER TABLE [dbo].[EvenementDommagePrecision]
    ADD [withTiersType] INT CONSTRAINT [DF_EvenementDommagePrecision_withTiersType] DEFAULT ((0)) NOT NULL,
        [withTiersAge]  INT CONSTRAINT [DF_EvenementDommagePrecision_withTiersAge] DEFAULT ((0)) NOT NULL,
        [withContexte]  INT CONSTRAINT [DF_EvenementDommagePrecision_withContexte] DEFAULT ((0)) NOT NULL;


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[EvenementExploitationConseqManoeuvre]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseqManoeuvre]
    ADD [id_exploitationConseqManoeuvre_groupe] INT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[EvenementFermetureRaison]...';


GO
ALTER TABLE [dbo].[EvenementFermetureRaison]
    ADD [id_reseau101m401] INT NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[EvenementLieu]...';


GO
ALTER TABLE [dbo].[EvenementLieu]
    ADD [longitude_lieu] FLOAT (53) CONSTRAINT [DF_EvenementLieu_longitude_lieu] DEFAULT ((0)) NOT NULL,
        [latitude_lieu]  FLOAT (53) CONSTRAINT [DF_EvenementLieu_latitude_lieu] DEFAULT ((0)) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[MaterielListe]...';


GO
ALTER TABLE [dbo].[MaterielListe] DROP COLUMN [id_materielList_materielDetail], COLUMN [id_materielroulants2006m401];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[Query]...';


GO
ALTER TABLE [dbo].[Query]
    ADD [id_query_Imputabilite] INT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[CfgConfigurationRulesRule]...';


GO
CREATE TABLE [dbo].[CfgConfigurationRulesRule] (
    [id_cfgConfigurationRulesRule]                   INT IDENTITY (1, 1) NOT NULL,
    [id_cfgConfigurationRulesRule_rule]              INT NOT NULL,
    [id_cfgConfigurationRulesRule_configurationRule] INT NOT NULL,
    CONSTRAINT [PK_CfgConfigurationRulesRule] PRIMARY KEY CLUSTERED ([id_cfgConfigurationRulesRule] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[CfgEvenementFermetureRaisonRule]...';


GO
CREATE TABLE [dbo].[CfgEvenementFermetureRaisonRule] (
    [id_cfgEvenementFermetureRaisonRule]                 INT IDENTITY (1, 1) NOT NULL,
    [id_cfgEvenementFermetureRaisonRule_rule]            INT NOT NULL,
    [id_cfgEvenementFermetureRaisonRule_fermetureRaison] INT NOT NULL,
    CONSTRAINT [PK_CfgEvenementFermetureRaisonFamille] PRIMARY KEY CLUSTERED ([id_cfgEvenementFermetureRaisonRule] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[Coloration]...';


GO
CREATE TABLE [dbo].[Coloration] (
    [id_coloration]                INT           IDENTITY (1, 1) NOT NULL,
    [label]                        NVARCHAR (50) NOT NULL,
    [backgroudColorBody]           NVARCHAR (50) NOT NULL,
    [fontColorBody]                NVARCHAR (50) NOT NULL,
    [backgroudColorHead]           NVARCHAR (50) NOT NULL,
    [fontColorHead]                NVARCHAR (50) NOT NULL,
    [priorite]                     INT           NOT NULL,
    [id_coloration_reseaus101m401] INT           NOT NULL,
    CONSTRAINT [PK_Coloration] PRIMARY KEY CLUSTERED ([id_coloration] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[ConstRessourceTag]...';


GO
CREATE TABLE [dbo].[ConstRessourceTag] (
    [id_constRessourceTag] INT            IDENTITY (1, 1) NOT NULL,
    [code]                 NVARCHAR (MAX) NOT NULL,
    [label]                NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_ConstRessourceTag] PRIMARY KEY CLUSTERED ([id_constRessourceTag] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[EvenementDommageContexte]...';


GO
CREATE TABLE [dbo].[EvenementDommageContexte] (
    [id_evenementDommageContexte] INT            IDENTITY (1, 1) NOT NULL,
    [nom]                         NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_EvenementDommageContexte] PRIMARY KEY CLUSTERED ([id_evenementDommageContexte] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[EvenementDommageTiersAge]...';


GO
CREATE TABLE [dbo].[EvenementDommageTiersAge] (
    [id_evenementDommageTiersAge] INT            IDENTITY (1, 1) NOT NULL,
    [nom]                         NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_EvenementDommageTiersAge] PRIMARY KEY CLUSTERED ([id_evenementDommageTiersAge] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[EvenementDommageTiersType]...';


GO
CREATE TABLE [dbo].[EvenementDommageTiersType] (
    [id_evenementDommageTiersType] INT            IDENTITY (1, 1) NOT NULL,
    [nom]                          NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_EvenementDommageTiersType] PRIMARY KEY CLUSTERED ([id_evenementDommageTiersType] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[EvenementExploitationConseqManoeuvreGroupe]...';


GO
CREATE TABLE [dbo].[EvenementExploitationConseqManoeuvreGroupe] (
    [id_evenementExploitationConseqManoeuvreGroupe] INT            IDENTITY (1, 1) NOT NULL,
    [nom]                                           NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_EvenementExploitationConseqManoeuvreGroupe] PRIMARY KEY CLUSTERED ([id_evenementExploitationConseqManoeuvreGroupe] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[EvenementImputabilite]...';


GO
CREATE TABLE [dbo].[EvenementImputabilite] (
    [id_evenementImputabilite] INT            IDENTITY (1, 1) NOT NULL,
    [nom]                      NVARCHAR (MAX) NOT NULL,
    [isDefaut]                 INT            NOT NULL,
    CONSTRAINT [PK_EvenementImputabilite] PRIMARY KEY CLUSTERED ([id_evenementImputabilite] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[EvenementInfoVoyageur]...';


GO
CREATE TABLE [dbo].[EvenementInfoVoyageur] (
    [id_evenementInfoVoyageur]                            INT            IDENTITY (1, 1) NOT NULL,
    [id_evenementInfoVoyageur_evenementListe]             INT            NOT NULL,
    [id_evenementInfoVoyageur_evenementInfoVoyageurMedia] INT            NULL,
    [dateDebut]                                           DATE           NULL,
    [heureDebut]                                          TIME (7)       NULL,
    [dateFin]                                             DATE           NULL,
    [heureFin]                                            TIME (7)       NULL,
    [adherents]                                           NVARCHAR (MAX) NULL,
    [message]                                             NVARCHAR (MAX) NULL,
    [id_utilisateur]                                      INT            NOT NULL,
    [loginUtilisateur]                                    NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_EvenementInfoVoyageur] PRIMARY KEY CLUSTERED ([id_evenementInfoVoyageur] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[EvenementInfoVoyageurMedia]...';


GO
CREATE TABLE [dbo].[EvenementInfoVoyageurMedia] (
    [id_evenementInfoVoyageurMedia]                INT            IDENTITY (1, 1) NOT NULL,
    [nom]                                          NVARCHAR (MAX) NOT NULL,
    [id_evenementInfoVoyageurMedia_reseaus101m401] INT            NOT NULL,
    CONSTRAINT [PK_EvenementInfoVoyageurMedia] PRIMARY KEY CLUSTERED ([id_evenementInfoVoyageurMedia] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[EvenementPrecision]...';


GO
CREATE TABLE [dbo].[EvenementPrecision] (
    [id_evenementPrecision]               INT            IDENTITY (1, 1) NOT NULL,
    [id_evenementPrecision_evenement]     INT            NOT NULL,
    [id_evenementPrecision_precisionType] INT            NOT NULL,
    [id_evenementPrecision_selectOption1] INT            NULL,
    [id_evenementPrecision_selectOption2] INT            NULL,
    [id_evenementPrecision_selectOption3] INT            NULL,
    [evenementPrecision_time1]            TIME (7)       NULL,
    [evenementPrecision_time2]            TIME (7)       NULL,
    [evenementPrecision_time3]            TIME (7)       NULL,
    [evenementPrecision_text1]            NVARCHAR (MAX) NULL,
    [evenementPrecision_text2]            NVARCHAR (MAX) NULL,
    [evenementPrecision_text3]            NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_EvenementPrecision] PRIMARY KEY CLUSTERED ([id_evenementPrecision] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[EvenementPrecisonType]...';


GO
CREATE TABLE [dbo].[EvenementPrecisonType] (
    [id_evenementPrecisonType] INT           IDENTITY (1, 1) NOT NULL,
    [id_reseau_s101m401]       INT           NOT NULL,
    [label]                    NVARCHAR (50) NOT NULL,
    [with_select1]             INT           NOT NULL,
    [label_select1]            NVARCHAR (50) NOT NULL,
    [id_select1]               INT           NOT NULL,
    [with_select2]             INT           NOT NULL,
    [label_select2]            NVARCHAR (50) NOT NULL,
    [id_select2]               INT           NOT NULL,
    [with_select3]             INT           NOT NULL,
    [label_select3]            NVARCHAR (50) NOT NULL,
    [id_select3]               INT           NOT NULL,
    [with_time1]               INT           NOT NULL,
    [label_time1]              NVARCHAR (50) NOT NULL,
    [with_time2]               INT           NOT NULL,
    [label_time2]              NVARCHAR (50) NOT NULL,
    [with_time3]               INT           NOT NULL,
    [label_time3]              NVARCHAR (50) NOT NULL,
    [with_text1]               INT           NOT NULL,
    [label_text1]              NVARCHAR (50) NOT NULL,
    [with_text2]               INT           NOT NULL,
    [label_text2]              NVARCHAR (50) NOT NULL,
    [with_text3]               INT           NOT NULL,
    [label_text3]              NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_EvenementPrecisonType] PRIMARY KEY CLUSTERED ([id_evenementPrecisonType] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[EvenementPrecisonTypeSelect]...';


GO
CREATE TABLE [dbo].[EvenementPrecisonTypeSelect] (
    [id_evenementPrecisonTypeSelect] INT           IDENTITY (1, 1) NOT NULL,
    [label]                          NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_EvenementPrecisonTypeSelect] PRIMARY KEY CLUSTERED ([id_evenementPrecisonTypeSelect] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[EvenementPrecisonTypeSelectOption]...';


GO
CREATE TABLE [dbo].[EvenementPrecisonTypeSelectOption] (
    [id_evenementPrecisonTypeSelectOption]                 INT           IDENTITY (1, 1) NOT NULL,
    [id_evenementPrecisonTypeSelectOption_precisionSelect] INT           NOT NULL,
    [label]                                                NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_EvenementPrecisonTypeSelectOption] PRIMARY KEY CLUSTERED ([id_evenementPrecisonTypeSelectOption] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[EvenementRH]...';


GO
CREATE TABLE [dbo].[EvenementRH] (
    [id_evenementRH]                      INT            IDENTITY (1, 1) NOT NULL,
    [id_evenementRH_aleaListe]            INT            NOT NULL,
    [id_evenementRH_evenementRHPrecision] INT            NOT NULL,
    [id_evenementRH_ressource]            INT            NULL,
    [nombreHeureSupp]                     FLOAT (53)     NULL,
    [id_evenementRH_evenementAffectation] INT            NULL,
    [id_utilisateur]                      INT            NOT NULL,
    [loginUtilisateur]                    NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_EvenementRH] PRIMARY KEY CLUSTERED ([id_evenementRH] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[EvenementRHPrecision]...';


GO
CREATE TABLE [dbo].[EvenementRHPrecision] (
    [id_evenementRHPrecision]        INT            IDENTITY (1, 1) NOT NULL,
    [id_evenementRHPrecision_RHType] INT            NOT NULL,
    [nom]                            NVARCHAR (MAX) NOT NULL,
    [withHeureSupplémentaire]        INT            NOT NULL,
    [withAffectation]                INT            NOT NULL,
    [withAgentConduite]              INT            NOT NULL,
    [withAgentAdmin]                 INT            NOT NULL,
    CONSTRAINT [PK_EvenementRHPrecision] PRIMARY KEY CLUSTERED ([id_evenementRHPrecision] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[EvenementRHType]...';


GO
CREATE TABLE [dbo].[EvenementRHType] (
    [id_evenementRHType] INT            IDENTITY (1, 1) NOT NULL,
    [nom]                NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_EvenementRHType] PRIMARY KEY CLUSTERED ([id_evenementRHType] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[RessourceTagRessourceApplied]...';


GO
CREATE TABLE [dbo].[RessourceTagRessourceApplied] (
    [id_ressourceTagRessourceApplied]              INT            IDENTITY (1, 1) NOT NULL,
    [id_ressourceTagRessourceApplied_RessourceTag] INT            NOT NULL,
    [id_ressourceTagRessourceApplied_Ressource]    INT            NOT NULL,
    [value]                                        NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_LnkRessourceTagRessource] PRIMARY KEY CLUSTERED ([id_ressourceTagRessourceApplied] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[Rules]...';


GO
CREATE TABLE [dbo].[Rules] (
    [id_rules]                        INT           IDENTITY (1, 1) NOT NULL,
    [nom]                             NVARCHAR (50) NOT NULL,
    [id_rules_constRessourceFamille]  INT           NULL,
    [id_rules_constEvenermentFamille] INT           NULL,
    [id_rules_constTerminal]          INT           NULL,
    [id_rules_constModeSaisie]        INT           NULL,
    [avecMaintenance]                 INT           NOT NULL,
    [avecSecurite]                    INT           NOT NULL,
    [avecDommage]                     INT           NOT NULL,
    [ponderation_seuil]               INT           NULL,
    [id_rules_reseaus101m401]         INT           NOT NULL,
    [id_rules_coloration]             INT           NULL,
    [id_rules_evenementPrecisionType] INT           NULL,
    CONSTRAINT [PK_Rules] PRIMARY KEY CLUSTERED ([id_rules] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_Coloration_id_coloration_reseaus101m401]...';


GO
ALTER TABLE [dbo].[Coloration]
    ADD CONSTRAINT [DF_Coloration_id_coloration_reseaus101m401] DEFAULT ((1)) FOR [id_coloration_reseaus101m401];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_Coloration_priorite]...';


GO
ALTER TABLE [dbo].[Coloration]
    ADD CONSTRAINT [DF_Coloration_priorite] DEFAULT ((0)) FOR [priorite];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementImputabilite_isDefaut]...';


GO
ALTER TABLE [dbo].[EvenementImputabilite]
    ADD CONSTRAINT [DF_EvenementImputabilite_isDefaut] DEFAULT ((0)) FOR [isDefaut];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementPrecisonType_id_select1]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType]
    ADD CONSTRAINT [DF_EvenementPrecisonType_id_select1] DEFAULT ((0)) FOR [id_select1];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementPrecisonType_id_select2]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType]
    ADD CONSTRAINT [DF_EvenementPrecisonType_id_select2] DEFAULT ((0)) FOR [id_select2];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementPrecisonType_id_select3]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType]
    ADD CONSTRAINT [DF_EvenementPrecisonType_id_select3] DEFAULT ((0)) FOR [id_select3];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementPrecisonType_label_select1]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType]
    ADD CONSTRAINT [DF_EvenementPrecisonType_label_select1] DEFAULT (N'LABEL_A_COMPLETER') FOR [label_select1];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementPrecisonType_label_select2]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType]
    ADD CONSTRAINT [DF_EvenementPrecisonType_label_select2] DEFAULT (N'LABEL_A_COMPLETER') FOR [label_select2];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementPrecisonType_label_select3]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType]
    ADD CONSTRAINT [DF_EvenementPrecisonType_label_select3] DEFAULT (N'LABEL_A_COMPLETER') FOR [label_select3];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementPrecisonType_label_text1]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType]
    ADD CONSTRAINT [DF_EvenementPrecisonType_label_text1] DEFAULT (N'LABEL_A_COMPLETER') FOR [label_text1];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementPrecisonType_label_text2]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType]
    ADD CONSTRAINT [DF_EvenementPrecisonType_label_text2] DEFAULT (N'LABEL_A_COMPLETER') FOR [label_text2];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementPrecisonType_label_text3]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType]
    ADD CONSTRAINT [DF_EvenementPrecisonType_label_text3] DEFAULT (N'LABEL_A_COMPLETER') FOR [label_text3];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementPrecisonType_label_time1]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType]
    ADD CONSTRAINT [DF_EvenementPrecisonType_label_time1] DEFAULT (N'LABEL_A_COMPLETER') FOR [label_time1];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementPrecisonType_label_time2]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType]
    ADD CONSTRAINT [DF_EvenementPrecisonType_label_time2] DEFAULT (N'LABEL_A_COMPLETER') FOR [label_time2];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementPrecisonType_label_time3]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType]
    ADD CONSTRAINT [DF_EvenementPrecisonType_label_time3] DEFAULT (N'LABEL_A_COMPLETER') FOR [label_time3];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementPrecisonType_with_select1]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType]
    ADD CONSTRAINT [DF_EvenementPrecisonType_with_select1] DEFAULT ((0)) FOR [with_select1];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementPrecisonType_with_select2]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType]
    ADD CONSTRAINT [DF_EvenementPrecisonType_with_select2] DEFAULT ((0)) FOR [with_select2];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementPrecisonType_with_select3]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType]
    ADD CONSTRAINT [DF_EvenementPrecisonType_with_select3] DEFAULT ((0)) FOR [with_select3];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementPrecisonType_with_text1]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType]
    ADD CONSTRAINT [DF_EvenementPrecisonType_with_text1] DEFAULT ((0)) FOR [with_text1];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementPrecisonType_with_text2]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType]
    ADD CONSTRAINT [DF_EvenementPrecisonType_with_text2] DEFAULT ((0)) FOR [with_text2];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementPrecisonType_with_text3]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType]
    ADD CONSTRAINT [DF_EvenementPrecisonType_with_text3] DEFAULT ((0)) FOR [with_text3];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementPrecisonType_with_time1]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType]
    ADD CONSTRAINT [DF_EvenementPrecisonType_with_time1] DEFAULT ((0)) FOR [with_time1];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementPrecisonType_with_time2]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType]
    ADD CONSTRAINT [DF_EvenementPrecisonType_with_time2] DEFAULT ((0)) FOR [with_time2];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementPrecisonType_with_time3]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType]
    ADD CONSTRAINT [DF_EvenementPrecisonType_with_time3] DEFAULT ((0)) FOR [with_time3];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementRHPrecision_withAgentAdmin]...';


GO
ALTER TABLE [dbo].[EvenementRHPrecision]
    ADD CONSTRAINT [DF_EvenementRHPrecision_withAgentAdmin] DEFAULT ((0)) FOR [withAgentAdmin];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementRHPrecision_withAgentConduite]...';


GO
ALTER TABLE [dbo].[EvenementRHPrecision]
    ADD CONSTRAINT [DF_EvenementRHPrecision_withAgentConduite] DEFAULT ((0)) FOR [withAgentConduite];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_Rules_avecDommage]...';


GO
ALTER TABLE [dbo].[Rules]
    ADD CONSTRAINT [DF_Rules_avecDommage] DEFAULT ((0)) FOR [avecDommage];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_Rules_avecMaintenance]...';


GO
ALTER TABLE [dbo].[Rules]
    ADD CONSTRAINT [DF_Rules_avecMaintenance] DEFAULT ((0)) FOR [avecMaintenance];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_Rules_avecSecurite]...';


GO
ALTER TABLE [dbo].[Rules]
    ADD CONSTRAINT [DF_Rules_avecSecurite] DEFAULT ((0)) FOR [avecSecurite];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementFichier_AleaListe]...';


GO
ALTER TABLE [dbo].[EvenementFichier] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementFichier_AleaListe] FOREIGN KEY ([id_evenementFichier_evenement]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementHistoriqueView_Evenement]...';


GO
ALTER TABLE [dbo].[EvenementHistoriqueView] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementHistoriqueView_Evenement] FOREIGN KEY ([id_aleaHistorique_Evenement]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementIntervention_EvenementList]...';


GO
ALTER TABLE [dbo].[EvenementIntervention] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementIntervention_EvenementList] FOREIGN KEY ([id_evenementIntervention_evenementList]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementMaintenance_EvenementListe]...';


GO
ALTER TABLE [dbo].[EvenementMaintenance] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementMaintenance_EvenementListe] FOREIGN KEY ([id_evenementMaintenance_evenementListe]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_AleaHistorique_Evenement]...';


GO
ALTER TABLE [dbo].[AleaHistorique] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaHistorique_Evenement] FOREIGN KEY ([id_aleaHistorique_Evenement]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_AleaListe_Affectation]...';


GO
ALTER TABLE [dbo].[AleaListe] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaListe_Affectation] FOREIGN KEY ([id_aleaListe_affectation]) REFERENCES [dbo].[EvenementAffectation] ([id_evenementAffectation]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_AleaListe_AleaEtat]...';


GO
ALTER TABLE [dbo].[AleaListe] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaListe_AleaEtat] FOREIGN KEY ([id_aleaListe_aleaEtat]) REFERENCES [dbo].[AleaEtat] ([id_aleaEtat]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_AleaListe_AleaType]...';


GO
ALTER TABLE [dbo].[AleaListe] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaListe_AleaType] FOREIGN KEY ([id_aleaListe_aleaType]) REFERENCES [dbo].[AleaType] ([id_aleaType]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_AleaListe_EvenementLieu]...';


GO
ALTER TABLE [dbo].[AleaListe] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaListe_EvenementLieu] FOREIGN KEY ([id_aleaListe_evenementLieu]) REFERENCES [dbo].[EvenementLieu] ([id_evenementLieu]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_AleaListe_FermetureRaison]...';


GO
ALTER TABLE [dbo].[AleaListe] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaListe_FermetureRaison] FOREIGN KEY ([id_aleaListe_FermetureRaison]) REFERENCES [dbo].[EvenementFermetureRaison] ([id_evenementFermetureRaison]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_AleaListe_MeteoDetails]...';


GO
ALTER TABLE [dbo].[AleaListe] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaListe_MeteoDetails] FOREIGN KEY ([id_aleaListe_meteoDetails]) REFERENCES [dbo].[EvenementMeteo] ([id_evenementMeteo]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_AleaListe_Ressource]...';


GO
ALTER TABLE [dbo].[AleaListe] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaListe_Ressource] FOREIGN KEY ([id_aleaListe_ressource]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_AleaListe_RessourceRemplacement]...';


GO
ALTER TABLE [dbo].[AleaListe] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaListe_RessourceRemplacement] FOREIGN KEY ([id_aleaListe_ressourceRemplacement]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementSecurite_EvenementListe]...';


GO
ALTER TABLE [dbo].[EvenementSecurite] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementSecurite_EvenementListe] FOREIGN KEY ([id_evenementSecurite_evenmentList]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_AleaListAleaHistorique_AleaList]...';


GO
ALTER TABLE [dbo].[LnkAleaListAleaHistorique] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaListAleaHistorique_AleaList] FOREIGN KEY ([id_aleaListeAleaHistorique_aleaList]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_LnkAleaListeFicheRealise_AleaListe]...';


GO
ALTER TABLE [dbo].[LnkAleaListeFicheRealise] WITH NOCHECK
    ADD CONSTRAINT [FK_LnkAleaListeFicheRealise_AleaListe] FOREIGN KEY ([id_lnkAleaListeFicheRealise_aleaListe]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_LnkEvenementAlertHistorique_Evenement]...';


GO
ALTER TABLE [dbo].[LnkEvenementAlertHistorique] WITH NOCHECK
    ADD CONSTRAINT [FK_LnkEvenementAlertHistorique_Evenement] FOREIGN KEY ([id_lnkEvenementAlertHistorique_evenement]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_MaterielListeAleaListe_AleaListe]...';


GO
ALTER TABLE [dbo].[LnkMaterielListeAleaListe] WITH NOCHECK
    ADD CONSTRAINT [FK_MaterielListeAleaListe_AleaListe] FOREIGN KEY ([id_materielListeAleaListe_aleaListe]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_QueryTriggerAsynchoneHistorique_Alea]...';


GO
ALTER TABLE [dbo].[QueryTriggerAsynchoneHistorique] WITH NOCHECK
    ADD CONSTRAINT [FK_QueryTriggerAsynchoneHistorique_Alea] FOREIGN KEY ([id_queryDeclencheurAsynchoneHistorique_alea]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementDommage_AleaListe]...';


GO
ALTER TABLE [dbo].[EvenementDommage] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementDommage_AleaListe] FOREIGN KEY ([id_evenementDommage_aleaListe]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementExploitationConseq_EvenementList]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementExploitationConseq_EvenementList] FOREIGN KEY ([id_evenementExploitationConseq_evenementListe]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_AleaListe_Imputabilite]...';


GO
ALTER TABLE [dbo].[AleaListe] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaListe_Imputabilite] FOREIGN KEY ([id_aleaListe_Imputabilite]) REFERENCES [dbo].[EvenementImputabilite] ([id_evenementImputabilite]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgConfigurationRulesRule_ConfigurationRules]...';


GO
ALTER TABLE [dbo].[CfgConfigurationRulesRule] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgConfigurationRulesRule_ConfigurationRules] FOREIGN KEY ([id_cfgConfigurationRulesRule_configurationRule]) REFERENCES [dbo].[ConstConfigurationRules] ([id_configurationRules]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgConfigurationRulesRule_Rule]...';


GO
ALTER TABLE [dbo].[CfgConfigurationRulesRule] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgConfigurationRulesRule_Rule] FOREIGN KEY ([id_cfgConfigurationRulesRule_rule]) REFERENCES [dbo].[Rules] ([id_rules]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgEvenementFermetureRaisonFamille_EvenementFermetureRaison]...';


GO
ALTER TABLE [dbo].[CfgEvenementFermetureRaisonRule] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementFermetureRaisonFamille_EvenementFermetureRaison] FOREIGN KEY ([id_cfgEvenementFermetureRaisonRule_fermetureRaison]) REFERENCES [dbo].[EvenementFermetureRaison] ([id_evenementFermetureRaison]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgEvenementFermetureRaisonFamille_Rule]...';


GO
ALTER TABLE [dbo].[CfgEvenementFermetureRaisonRule] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementFermetureRaisonFamille_Rule] FOREIGN KEY ([id_cfgEvenementFermetureRaisonRule_rule]) REFERENCES [dbo].[Rules] ([id_rules]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementInfoVoyageur_EvenementListe]...';


GO
ALTER TABLE [dbo].[EvenementInfoVoyageur] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementInfoVoyageur_EvenementListe] FOREIGN KEY ([id_evenementInfoVoyageur_evenementListe]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementInfoVoyageur_Media]...';


GO
ALTER TABLE [dbo].[EvenementInfoVoyageur] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementInfoVoyageur_Media] FOREIGN KEY ([id_evenementInfoVoyageur_evenementInfoVoyageurMedia]) REFERENCES [dbo].[EvenementInfoVoyageurMedia] ([id_evenementInfoVoyageurMedia]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementPrecision_Evenement]...';


GO
ALTER TABLE [dbo].[EvenementPrecision] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementPrecision_Evenement] FOREIGN KEY ([id_evenementPrecision_evenement]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementPrecision_EvenementPrecisionType]...';


GO
ALTER TABLE [dbo].[EvenementPrecision] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementPrecision_EvenementPrecisionType] FOREIGN KEY ([id_evenementPrecision_precisionType]) REFERENCES [dbo].[EvenementPrecisonType] ([id_evenementPrecisonType]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementPrecision_SelectOption1]...';


GO
ALTER TABLE [dbo].[EvenementPrecision] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementPrecision_SelectOption1] FOREIGN KEY ([id_evenementPrecision_selectOption1]) REFERENCES [dbo].[EvenementPrecisonTypeSelectOption] ([id_evenementPrecisonTypeSelectOption]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementPrecision_SelectOption2]...';


GO
ALTER TABLE [dbo].[EvenementPrecision] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementPrecision_SelectOption2] FOREIGN KEY ([id_evenementPrecision_selectOption2]) REFERENCES [dbo].[EvenementPrecisonTypeSelectOption] ([id_evenementPrecisonTypeSelectOption]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementPrecision_SelectOption3]...';


GO
ALTER TABLE [dbo].[EvenementPrecision] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementPrecision_SelectOption3] FOREIGN KEY ([id_evenementPrecision_selectOption3]) REFERENCES [dbo].[EvenementPrecisonTypeSelectOption] ([id_evenementPrecisonTypeSelectOption]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementPrecisonType_Select1]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementPrecisonType_Select1] FOREIGN KEY ([id_select1]) REFERENCES [dbo].[EvenementPrecisonTypeSelect] ([id_evenementPrecisonTypeSelect]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementPrecisonType_Select2]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementPrecisonType_Select2] FOREIGN KEY ([id_select2]) REFERENCES [dbo].[EvenementPrecisonTypeSelect] ([id_evenementPrecisonTypeSelect]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementPrecisonType_Select3]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementPrecisonType_Select3] FOREIGN KEY ([id_select3]) REFERENCES [dbo].[EvenementPrecisonTypeSelect] ([id_evenementPrecisonTypeSelect]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementPrecisonTypeSelectOption_EvenementPrecisonTypeSelect]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonTypeSelectOption] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementPrecisonTypeSelectOption_EvenementPrecisonTypeSelect] FOREIGN KEY ([id_evenementPrecisonTypeSelectOption_precisionSelect]) REFERENCES [dbo].[EvenementPrecisonTypeSelect] ([id_evenementPrecisonTypeSelect]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementRH_EvenementListe]...';


GO
ALTER TABLE [dbo].[EvenementRH] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementRH_EvenementListe] FOREIGN KEY ([id_evenementRH_aleaListe]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementRH_EvenementRHPrecision]...';


GO
ALTER TABLE [dbo].[EvenementRH] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementRH_EvenementRHPrecision] FOREIGN KEY ([id_evenementRH_evenementRHPrecision]) REFERENCES [dbo].[EvenementRHPrecision] ([id_evenementRHPrecision]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementRHPrecision_EvenementRHType]...';


GO
ALTER TABLE [dbo].[EvenementRHPrecision] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementRHPrecision_EvenementRHType] FOREIGN KEY ([id_evenementRHPrecision_RHType]) REFERENCES [dbo].[EvenementRHType] ([id_evenementRHType]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_LnkRessourceTagRessource_Ressource]...';


GO
ALTER TABLE [dbo].[RessourceTagRessourceApplied] WITH NOCHECK
    ADD CONSTRAINT [FK_LnkRessourceTagRessource_Ressource] FOREIGN KEY ([id_ressourceTagRessourceApplied_Ressource]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_LnkRessourceTagRessource_RessourceTag]...';


GO
ALTER TABLE [dbo].[RessourceTagRessourceApplied] WITH NOCHECK
    ADD CONSTRAINT [FK_LnkRessourceTagRessource_RessourceTag] FOREIGN KEY ([id_ressourceTagRessourceApplied_RessourceTag]) REFERENCES [dbo].[ConstRessourceTag] ([id_constRessourceTag]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_Rules_Coloration]...';


GO
ALTER TABLE [dbo].[Rules] WITH NOCHECK
    ADD CONSTRAINT [FK_Rules_Coloration] FOREIGN KEY ([id_rules_coloration]) REFERENCES [dbo].[Coloration] ([id_coloration]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_Rules_EvenementFamille]...';


GO
ALTER TABLE [dbo].[Rules] WITH NOCHECK
    ADD CONSTRAINT [FK_Rules_EvenementFamille] FOREIGN KEY ([id_rules_constEvenermentFamille]) REFERENCES [dbo].[ConstEvenementFamille] ([id_constEvenementFamille]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_Rules_EvenementPrecisionType]...';


GO
ALTER TABLE [dbo].[Rules] WITH NOCHECK
    ADD CONSTRAINT [FK_Rules_EvenementPrecisionType] FOREIGN KEY ([id_rules_evenementPrecisionType]) REFERENCES [dbo].[EvenementPrecisonType] ([id_evenementPrecisonType]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_Rules_ModeSaisie]...';


GO
ALTER TABLE [dbo].[Rules] WITH NOCHECK
    ADD CONSTRAINT [FK_Rules_ModeSaisie] FOREIGN KEY ([id_rules_constModeSaisie]) REFERENCES [dbo].[ConstModeSaisie] ([id_constModeSaisie]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_Rules_RessourceFamille]...';


GO
ALTER TABLE [dbo].[Rules] WITH NOCHECK
    ADD CONSTRAINT [FK_Rules_RessourceFamille] FOREIGN KEY ([id_rules_constRessourceFamille]) REFERENCES [dbo].[ConstRessourcesFamille] ([id_constRessourceFamille]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_Rules_Terminal]...';


GO
ALTER TABLE [dbo].[Rules] WITH NOCHECK
    ADD CONSTRAINT [FK_Rules_Terminal] FOREIGN KEY ([id_rules_constTerminal]) REFERENCES [dbo].[ConstTerminalSaisie] ([id_constTerminalSaisie]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgGeneralMainCourante_Categorie1]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgGeneralMainCourante_Categorie1] FOREIGN KEY ([id_ressourceCategorieAdmin_Imports2006m1055]) REFERENCES [dbo].[MaterielDetailCategorie] ([id_materielDetailCategorie]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgGeneralMainCourante_Categorie2]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgGeneralMainCourante_Categorie2] FOREIGN KEY ([id_ressourceCategorieCR_Imports2006m1055]) REFERENCES [dbo].[MaterielDetailCategorie] ([id_materielDetailCategorie]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgGeneralMainCourante_Etat1]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgGeneralMainCourante_Etat1] FOREIGN KEY ([id_ressourceEtatDefaut_Imports2006m1055]) REFERENCES [dbo].[MaterielEtat] ([id_materielEtat]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgGeneralMainCourante_Etat2]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgGeneralMainCourante_Etat2] FOREIGN KEY ([id_ressourceEtatExploitation_Imports2006m1055]) REFERENCES [dbo].[MaterielEtat] ([id_materielEtat]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgGeneralMainCourante_Etat3]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgGeneralMainCourante_Etat3] FOREIGN KEY ([id_ressourceEtatReserve_Imports2006m1055]) REFERENCES [dbo].[MaterielEtat] ([id_materielEtat]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgGeneralMainCourante_RessourceClasse]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgGeneralMainCourante_RessourceClasse] FOREIGN KEY ([id_ressourceClasseRH_Imports2006m1055]) REFERENCES [dbo].[MaterielElement] ([id_materielElement]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementDommage_Contexte]...';


GO
ALTER TABLE [dbo].[EvenementDommage] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementDommage_Contexte] FOREIGN KEY ([id_dommageConseq_contexte]) REFERENCES [dbo].[EvenementDommageContexte] ([id_evenementDommageContexte]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementDommage_TiersAge]...';


GO
ALTER TABLE [dbo].[EvenementDommage] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementDommage_TiersAge] FOREIGN KEY ([id_dommageConseq_tiersAge]) REFERENCES [dbo].[EvenementDommageTiersAge] ([id_evenementDommageTiersAge]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementDommage_TiersType]...';


GO
ALTER TABLE [dbo].[EvenementDommage] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementDommage_TiersType] FOREIGN KEY ([id_dommageConseq_tiersType]) REFERENCES [dbo].[EvenementDommageTiersType] ([id_evenementDommageTiersType]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementExploitationConseqManoeuvre_Group]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseqManoeuvre] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementExploitationConseqManoeuvre_Group] FOREIGN KEY ([id_exploitationConseqManoeuvre_groupe]) REFERENCES [dbo].[EvenementExploitationConseqManoeuvreGroupe] ([id_evenementExploitationConseqManoeuvreGroupe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_Query_Imputabilite]...';


GO
ALTER TABLE [dbo].[Query] WITH NOCHECK
    ADD CONSTRAINT [FK_Query_Imputabilite] FOREIGN KEY ([id_query_Imputabilite]) REFERENCES [dbo].[EvenementImputabilite] ([id_evenementImputabilite]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[VueAleaListe]...';


GO
ALTER VIEW dbo.VueAleaListe
AS
SELECT        dbo.AleaListe.id_aleaListe, dbo.AleaListe.timeStamp, dbo.AleaListe.comentaires, dbo.AleaListe.dateApparition, dbo.AleaListe.heureApparition, dbo.AleaListe.heureFin, dbo.AleaType.id_aleaType, 
                         dbo.AleaType.nom AS [AleaType nom], dbo.AleaEtat.id_aleaEtat, dbo.AleaEtat.nom AS [AleaEtat nom], dbo.AleaListe.indentifiant, dbo.AleaListe.longitude_saisie, dbo.AleaListe.latitude_saisie, dbo.AleaListe.lastModification, 
                         dbo.AleaListe.heureAppel, dbo.AleaListe.dateFin, dbo.AleaListe.id_aleaListe_FermetureRaison, dbo.AleaListe.id_aleaListe_Imputabilite
FROM            dbo.AleaListe INNER JOIN
                         dbo.AleaType ON dbo.AleaListe.id_aleaListe_aleaType = dbo.AleaType.id_aleaType INNER JOIN
                         dbo.AleaEtat ON dbo.AleaListe.id_aleaListe_aleaEtat = dbo.AleaEtat.id_aleaEtat
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[VueAleaListeMateriel]...';


GO
ALTER VIEW dbo.VueAleaListeMateriel
AS
SELECT        dbo.AleaListe.id_aleaListe, dbo.AleaListe.timeStamp, dbo.AleaListe.comentaires, dbo.AleaListe.dateApparition, dbo.AleaListe.heureApparition, dbo.AleaListe.heureFin, dbo.AleaType.id_aleaType, 
                         dbo.AleaType.nom AS [AleaType nom], dbo.AleaEtat.id_aleaEtat, dbo.AleaEtat.nom AS [AleaEtat nom], dbo.MaterielListe.id_materielListe, dbo.MaterielListe.id_reseau, dbo.AleaListe.indentifiant, dbo.AleaListe.ponderation, 
                         dbo.AleaListe.id_aleaListe_ressourceRemplacement, dbo.AleaListe.longitude_saisie, dbo.AleaListe.latitude_saisie, dbo.AleaListe.lastModification, dbo.AleaListe.dateFin, dbo.AleaListe.heureAppel, 
                         dbo.AleaListe.id_aleaListe_FermetureRaison, dbo.AleaListe.id_aleaListe_Imputabilite
FROM            dbo.AleaListe INNER JOIN
                         dbo.AleaType ON dbo.AleaListe.id_aleaListe_aleaType = dbo.AleaType.id_aleaType INNER JOIN
                         dbo.AleaEtat ON dbo.AleaListe.id_aleaListe_aleaEtat = dbo.AleaEtat.id_aleaEtat INNER JOIN
                         dbo.LnkMaterielListeAleaListe ON dbo.AleaListe.id_aleaListe = dbo.LnkMaterielListeAleaListe.id_materielListeAleaListe_aleaListe INNER JOIN
                         dbo.MaterielListe ON dbo.LnkMaterielListeAleaListe.id_materielListeAleaListe_materielList = dbo.MaterielListe.id_materielListe
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[VueMaterielListeCountAleaEtat]...';


GO
ALTER VIEW dbo.VueMaterielListeCountAleaEtat
AS
SELECT        dbo.MaterielListe.id_materielListe, dbo.MaterielListe.id_reseau, dbo.AleaEtat.id_aleaEtat, dbo.AleaEtat.nom, dbo.AleaListe.dateApparition, dbo.AleaListe.indentifiant
FROM            dbo.MaterielListe INNER JOIN
                         dbo.LnkMaterielListeAleaListe ON dbo.MaterielListe.id_materielListe = dbo.LnkMaterielListeAleaListe.id_materielListeAleaListe_materielList INNER JOIN
                         dbo.AleaListe ON dbo.LnkMaterielListeAleaListe.id_materielListeAleaListe_aleaListe = dbo.AleaListe.id_aleaListe INNER JOIN
                         dbo.AleaEtat ON dbo.AleaListe.id_aleaListe_aleaEtat = dbo.AleaEtat.id_aleaEtat
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualisation de [dbo].[VueAleaActionHistoriqueMateriel]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[VueAleaActionHistoriqueMateriel]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualisation de [dbo].[VueMaterielEtatHistorique]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[VueMaterielEtatHistorique]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[VueMaterielListe]...';


GO
ALTER VIEW dbo.VueMaterielListe
AS
SELECT        dbo.MaterielListe.id_materielListe, dbo.MaterielListe.id_reseau, dbo.MaterielDetailCategorie.nom AS MaterielDetailCategorieNom, dbo.MaterielType.id_materielType, dbo.MaterielType.nom AS MaterielTypeNom, 
                         dbo.MaterielElement.id_materielElement, dbo.MaterielElement.nom AS MaterielElementNom, dbo.ConstRessourcesFamille.id_constRessourceFamille, dbo.ConstRessourcesFamille.code AS ConstRessourcesFamilleCode, 
                         dbo.ConstRessourcesFamille.libelle, dbo.MaterielListe.id_materielList_materielEtat, dbo.MaterielListe.deleted, dbo.MaterielListe.lastModification, dbo.MaterielListe.code, dbo.MaterielListe.nomLong, 
                         dbo.MaterielListe.id_materielList_materielType, dbo.MaterielListe.id_materielListe_materielCategorie, dbo.MaterielListe.id_materielList_constRessourceFamille
FROM            dbo.MaterielListe LEFT OUTER JOIN
                         dbo.ConstRessourcesFamille ON dbo.MaterielListe.id_materielList_constRessourceFamille = dbo.ConstRessourcesFamille.id_constRessourceFamille LEFT OUTER JOIN
                         dbo.MaterielDetailCategorie ON dbo.MaterielDetailCategorie.id_materielDetailCategorie = dbo.MaterielListe.id_materielListe_materielCategorie LEFT OUTER JOIN
                         dbo.MaterielType ON dbo.MaterielListe.id_materielList_materielType = dbo.MaterielType.id_materielType LEFT OUTER JOIN
                         dbo.MaterielElement ON dbo.MaterielType.id_materielType_MaterielElement = dbo.MaterielElement.id_materielElement
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[VueAleaListe].[MS_DiagramPane1]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[35] 4[26] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "AleaListe"
            Begin Extent = 
               Top = 9
               Left = 673
               Bottom = 292
               Right = 929
            End
            DisplayFlags = 280
            TopColumn = 9
         End
         Begin Table = "AleaType"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 251
               Right = 258
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AleaEtat"
            Begin Extent = 
               Top = 138
               Left = 296
               Bottom = 234
               Right = 466
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueAleaListe';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[VueAleaListeMateriel].[MS_DiagramPane1]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "AleaListe"
            Begin Extent = 
               Top = 9
               Left = 682
               Bottom = 344
               Right = 938
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "AleaType"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 251
               Right = 258
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AleaEtat"
            Begin Extent = 
               Top = 138
               Left = 296
               Bottom = 234
               Right = 466
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LnkMaterielListeAleaListe"
            Begin Extent = 
               Top = 252
               Left = 38
               Bottom = 365
               Right = 322
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterielListe"
            Begin Extent = 
               Top = 366
               Left = 38
               Bottom = 496
               Right = 281
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
        ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueAleaListeMateriel';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[VueMaterielListe].[MS_DiagramPane1]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[61] 4[1] 2[31] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "MaterielListe"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 252
               Right = 328
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "ConstRessourcesFamille"
            Begin Extent = 
               Top = 6
               Left = 366
               Bottom = 119
               Right = 590
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterielDetailCategorie"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 366
               Right = 266
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterielType"
            Begin Extent = 
               Top = 366
               Left = 38
               Bottom = 496
               Right = 302
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterielElement"
            Begin Extent = 
               Top = 498
               Left = 38
               Bottom = 628
               Right = 228
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
       ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueMaterielListe';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[VueMaterielListe].[MS_DiagramPane2]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane2', @value = N'  Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueMaterielListe';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[VueMaterielListeCountAleaEtat].[MS_DiagramPane1]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[21] 4[34] 2[11] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -96
         Left = 0
      End
      Begin Tables = 
         Begin Table = "MaterielListe"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 328
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LnkMaterielListeAleaListe"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 251
               Right = 322
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AleaListe"
            Begin Extent = 
               Top = 252
               Left = 38
               Bottom = 382
               Right = 322
            End
            DisplayFlags = 280
            TopColumn = 8
         End
         Begin Table = "AleaEtat"
            Begin Extent = 
               Top = 384
               Left = 38
               Bottom = 480
               Right = 247
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1500
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
    ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueMaterielListeCountAleaEtat';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[VueMaterielListeCountAleaEtat].[MS_DiagramPane2]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'     Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueMaterielListeCountAleaEtat';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[VueMaterielListeCountAleaEtat].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueMaterielListeCountAleaEtat';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO

IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT N'Succès de la mise à jour de la portion de base de données traitée.'
COMMIT TRANSACTION
END
ELSE PRINT N'Échec de la mise à jour de la portion de base de données traitée.'
GO
DROP TABLE #tmpErrors
GO
PRINT N'Vérification de données existantes par rapport aux nouvelles contraintes';


GO
USE [$(DatabaseName)];


GO
ALTER TABLE [dbo].[EvenementFichier] WITH CHECK CHECK CONSTRAINT [FK_EvenementFichier_AleaListe];

ALTER TABLE [dbo].[EvenementHistoriqueView] WITH CHECK CHECK CONSTRAINT [FK_EvenementHistoriqueView_Evenement];

ALTER TABLE [dbo].[EvenementIntervention] WITH CHECK CHECK CONSTRAINT [FK_EvenementIntervention_EvenementList];

ALTER TABLE [dbo].[EvenementMaintenance] WITH CHECK CHECK CONSTRAINT [FK_EvenementMaintenance_EvenementListe];

ALTER TABLE [dbo].[AleaHistorique] WITH CHECK CHECK CONSTRAINT [FK_AleaHistorique_Evenement];

ALTER TABLE [dbo].[AleaListe] WITH CHECK CHECK CONSTRAINT [FK_AleaListe_Affectation];

ALTER TABLE [dbo].[AleaListe] WITH CHECK CHECK CONSTRAINT [FK_AleaListe_AleaEtat];

ALTER TABLE [dbo].[AleaListe] WITH CHECK CHECK CONSTRAINT [FK_AleaListe_AleaType];

ALTER TABLE [dbo].[AleaListe] WITH CHECK CHECK CONSTRAINT [FK_AleaListe_EvenementLieu];

ALTER TABLE [dbo].[AleaListe] WITH CHECK CHECK CONSTRAINT [FK_AleaListe_FermetureRaison];

ALTER TABLE [dbo].[AleaListe] WITH CHECK CHECK CONSTRAINT [FK_AleaListe_MeteoDetails];

ALTER TABLE [dbo].[AleaListe] WITH CHECK CHECK CONSTRAINT [FK_AleaListe_Ressource];

ALTER TABLE [dbo].[AleaListe] WITH CHECK CHECK CONSTRAINT [FK_AleaListe_RessourceRemplacement];

ALTER TABLE [dbo].[EvenementSecurite] WITH CHECK CHECK CONSTRAINT [FK_EvenementSecurite_EvenementListe];

ALTER TABLE [dbo].[LnkAleaListAleaHistorique] WITH CHECK CHECK CONSTRAINT [FK_AleaListAleaHistorique_AleaList];

ALTER TABLE [dbo].[LnkAleaListeFicheRealise] WITH CHECK CHECK CONSTRAINT [FK_LnkAleaListeFicheRealise_AleaListe];

ALTER TABLE [dbo].[LnkEvenementAlertHistorique] WITH CHECK CHECK CONSTRAINT [FK_LnkEvenementAlertHistorique_Evenement];

ALTER TABLE [dbo].[LnkMaterielListeAleaListe] WITH CHECK CHECK CONSTRAINT [FK_MaterielListeAleaListe_AleaListe];

ALTER TABLE [dbo].[QueryTriggerAsynchoneHistorique] WITH CHECK CHECK CONSTRAINT [FK_QueryTriggerAsynchoneHistorique_Alea];

ALTER TABLE [dbo].[EvenementDommage] WITH CHECK CHECK CONSTRAINT [FK_EvenementDommage_AleaListe];

ALTER TABLE [dbo].[EvenementExploitationConseq] WITH CHECK CHECK CONSTRAINT [FK_EvenementExploitationConseq_EvenementList];

ALTER TABLE [dbo].[AleaListe] WITH CHECK CHECK CONSTRAINT [FK_AleaListe_Imputabilite];

ALTER TABLE [dbo].[CfgConfigurationRulesRule] WITH CHECK CHECK CONSTRAINT [FK_CfgConfigurationRulesRule_ConfigurationRules];

ALTER TABLE [dbo].[CfgConfigurationRulesRule] WITH CHECK CHECK CONSTRAINT [FK_CfgConfigurationRulesRule_Rule];

ALTER TABLE [dbo].[CfgEvenementFermetureRaisonRule] WITH CHECK CHECK CONSTRAINT [FK_CfgEvenementFermetureRaisonFamille_EvenementFermetureRaison];

ALTER TABLE [dbo].[CfgEvenementFermetureRaisonRule] WITH CHECK CHECK CONSTRAINT [FK_CfgEvenementFermetureRaisonFamille_Rule];

ALTER TABLE [dbo].[EvenementInfoVoyageur] WITH CHECK CHECK CONSTRAINT [FK_EvenementInfoVoyageur_EvenementListe];

ALTER TABLE [dbo].[EvenementInfoVoyageur] WITH CHECK CHECK CONSTRAINT [FK_EvenementInfoVoyageur_Media];

ALTER TABLE [dbo].[EvenementPrecision] WITH CHECK CHECK CONSTRAINT [FK_EvenementPrecision_Evenement];

ALTER TABLE [dbo].[EvenementPrecision] WITH CHECK CHECK CONSTRAINT [FK_EvenementPrecision_EvenementPrecisionType];

ALTER TABLE [dbo].[EvenementPrecision] WITH CHECK CHECK CONSTRAINT [FK_EvenementPrecision_SelectOption1];

ALTER TABLE [dbo].[EvenementPrecision] WITH CHECK CHECK CONSTRAINT [FK_EvenementPrecision_SelectOption2];

ALTER TABLE [dbo].[EvenementPrecision] WITH CHECK CHECK CONSTRAINT [FK_EvenementPrecision_SelectOption3];

ALTER TABLE [dbo].[EvenementPrecisonType] WITH CHECK CHECK CONSTRAINT [FK_EvenementPrecisonType_Select1];

ALTER TABLE [dbo].[EvenementPrecisonType] WITH CHECK CHECK CONSTRAINT [FK_EvenementPrecisonType_Select2];

ALTER TABLE [dbo].[EvenementPrecisonType] WITH CHECK CHECK CONSTRAINT [FK_EvenementPrecisonType_Select3];

ALTER TABLE [dbo].[EvenementPrecisonTypeSelectOption] WITH CHECK CHECK CONSTRAINT [FK_EvenementPrecisonTypeSelectOption_EvenementPrecisonTypeSelect];

ALTER TABLE [dbo].[EvenementRH] WITH CHECK CHECK CONSTRAINT [FK_EvenementRH_EvenementListe];

ALTER TABLE [dbo].[EvenementRH] WITH CHECK CHECK CONSTRAINT [FK_EvenementRH_EvenementRHPrecision];

ALTER TABLE [dbo].[EvenementRHPrecision] WITH CHECK CHECK CONSTRAINT [FK_EvenementRHPrecision_EvenementRHType];

ALTER TABLE [dbo].[RessourceTagRessourceApplied] WITH CHECK CHECK CONSTRAINT [FK_LnkRessourceTagRessource_Ressource];

ALTER TABLE [dbo].[RessourceTagRessourceApplied] WITH CHECK CHECK CONSTRAINT [FK_LnkRessourceTagRessource_RessourceTag];

ALTER TABLE [dbo].[Rules] WITH CHECK CHECK CONSTRAINT [FK_Rules_Coloration];

ALTER TABLE [dbo].[Rules] WITH CHECK CHECK CONSTRAINT [FK_Rules_EvenementFamille];

ALTER TABLE [dbo].[Rules] WITH CHECK CHECK CONSTRAINT [FK_Rules_EvenementPrecisionType];

ALTER TABLE [dbo].[Rules] WITH CHECK CHECK CONSTRAINT [FK_Rules_ModeSaisie];

ALTER TABLE [dbo].[Rules] WITH CHECK CHECK CONSTRAINT [FK_Rules_RessourceFamille];

ALTER TABLE [dbo].[Rules] WITH CHECK CHECK CONSTRAINT [FK_Rules_Terminal];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH CHECK CHECK CONSTRAINT [FK_CfgGeneralMainCourante_Categorie1];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH CHECK CHECK CONSTRAINT [FK_CfgGeneralMainCourante_Categorie2];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH CHECK CHECK CONSTRAINT [FK_CfgGeneralMainCourante_Etat1];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH CHECK CHECK CONSTRAINT [FK_CfgGeneralMainCourante_Etat2];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH CHECK CHECK CONSTRAINT [FK_CfgGeneralMainCourante_Etat3];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH CHECK CHECK CONSTRAINT [FK_CfgGeneralMainCourante_RessourceClasse];

ALTER TABLE [dbo].[EvenementDommage] WITH CHECK CHECK CONSTRAINT [FK_EvenementDommage_Contexte];

ALTER TABLE [dbo].[EvenementDommage] WITH CHECK CHECK CONSTRAINT [FK_EvenementDommage_TiersAge];

ALTER TABLE [dbo].[EvenementDommage] WITH CHECK CHECK CONSTRAINT [FK_EvenementDommage_TiersType];

ALTER TABLE [dbo].[EvenementExploitationConseqManoeuvre] WITH CHECK CHECK CONSTRAINT [FK_EvenementExploitationConseqManoeuvre_Group];

ALTER TABLE [dbo].[Query] WITH CHECK CHECK CONSTRAINT [FK_Query_Imputabilite];


GO
PRINT N'Mise à jour terminée.';


GO

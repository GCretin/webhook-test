USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseTypeGroup]
           ([nom])
     VALUES
           ('G�n�ral')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseTypeGroup]
           ([nom])
     VALUES
           ('km perdus')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseTypeGroup]
           ([nom])
     VALUES
           ('Personnel')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseTypeGroup]
           ([nom])
     VALUES
           ('Ligne')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseTypeGroup]
           ([nom])
     VALUES
           ('Maintenance')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseTypeGroup]
           ([nom])
     VALUES
           ('Historique')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseTypeGroup]
           ([nom])
     VALUES
           ('M�t�o')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries]
           ,[id_constAnalyseType_constAnalyseTypeGroup])
     VALUES
           ('KM_PERDUS_SUPPLEMENT_DETAIL'
           ,'km perdus d�tails'
           ,'D�tails des km perdus'
           ,1
           ,2)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries]
           ,[id_constAnalyseType_constAnalyseTypeGroup])
     VALUES
           ('NB_EVENEMENT_PAR_METEO_PAR_SOUS_CATEGORIE'
           ,'Ev�. par m�t�o sous categ.'
           ,'Nombre d''�v�nement par m�t�o sous cat�gorie'
           ,1
           ,7)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries]
           ,[id_constAnalyseType_constAnalyseTypeGroup])
     VALUES
           ('NB_EVENEMENT_PAR_METEO_PAR_SOUS_CLASSE'
           ,'Ev�. par m�t�o sous classe'
           ,'Nombre d''�v�nement par m�t�o sous classe'
           ,1
           ,7)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries]
           ,[id_constAnalyseType_constAnalyseTypeGroup])
     VALUES
           ('KM_PERDUS_SUPPLEMENT_PAR_LIGNE_HISTO'
           ,'km perdus par ligne'
           ,'Pertes et suppl�ments km par ligne (bar)'
           ,1
           ,2)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries]
           ,[id_constAnalyseType_constAnalyseTypeGroup])
     VALUES
           ('KM_PERDUS_PAR_LIGNE_ASPECT'
           ,'km perdus par ligne aspects'
           ,'Perdus et suppl�ments km par ligne aspects'
           ,1
           ,2)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstAnalyseType]
           ([code]
           ,[libelleCourt]
           ,[libelle]
           ,[hasSeries]
           ,[id_constAnalyseType_constAnalyseTypeGroup])
     VALUES
           ('KM_PERDUS_PAR_LIGNE_ASPECT_DETAIL'
           ,'km ligne aspects detail'
           ,'Perdus et suppl�ments km par ligne aspects d�tails'
           ,1
           ,2)
GO





USE [s2002m401]
GO

INSERT INTO [dbo].[ConstQueryCriteria]
           ([dataType]
           ,[libelle]
           ,[id_queryCriteria_queryCriteriaGroup]
           ,[code]
           ,[isEligibleOnTheFlight]
           ,[isEligibleAdvancedMode]
           ,[description])
     VALUES
           ('SELECT_OBJECT'
           ,'Bus / Tramway'
           ,3
           ,'BUS_TRAMWAY'
           ,1
           ,0
           ,'Ressources bus / tramway')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstQueryCriteria]
           ([dataType]
           ,[libelle]
           ,[id_queryCriteria_queryCriteriaGroup]
           ,[code]
           ,[isEligibleOnTheFlight]
           ,[isEligibleAdvancedMode]
           ,[description])
     VALUES
           ('SELECT_OBJECT'
           ,'Agent'
           ,3
           ,'AGENT'
           ,1
           ,0
           ,'Ressources agent')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[ConstQueryCriteria]
           ([dataType]
           ,[libelle]
           ,[id_queryCriteria_queryCriteriaGroup]
           ,[code]
           ,[isEligibleOnTheFlight]
           ,[isEligibleAdvancedMode]
           ,[description])
     VALUES
           ('SELECT_OBJECT'
           ,'Symptome maintenance'
           ,4
           ,'MAINTENANCE_SYMPTOME'
           ,0
           ,1
           ,'Symptomes de maintenance')
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])
     VALUES
           (1
           ,42)
GO


USE [s2002m401]
GO

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])
     VALUES
           (1
           ,43)
GO

USE [s2002m401]
GO

INSERT INTO [dbo].[CfgQueryDataSourceQueryCriteria]
           ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]
           ,[id_cfgQueryDataSourceQueryCriteria_queryCriteria])
     VALUES
           (1
           ,44)
GO
















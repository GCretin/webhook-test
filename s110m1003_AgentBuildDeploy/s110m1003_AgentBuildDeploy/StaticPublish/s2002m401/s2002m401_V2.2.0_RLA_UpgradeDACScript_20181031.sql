/*
Script de déploiement pour s2002m401

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "s2002m401"
:setvar DefaultFilePrefix "s2002m401"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.RLARECETTE\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.RLARECETTE\MSSQL\DATA\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO

IF (SELECT OBJECT_ID('tempdb..#tmpErrors')) IS NOT NULL DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
GO
BEGIN TRANSACTION
GO
PRINT N'Suppression de [dbo].[DF_EvenementDommagePrecision_withCommentaires]...';


GO
ALTER TABLE [dbo].[EvenementDommagePrecision] DROP CONSTRAINT [DF_EvenementDommagePrecision_withCommentaires];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[DF_EvenementDommagePrecision_withImmatriculation]...';


GO
ALTER TABLE [dbo].[EvenementDommagePrecision] DROP CONSTRAINT [DF_EvenementDommagePrecision_withImmatriculation];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[DF_EvenementDommagePrecision_withNom]...';


GO
ALTER TABLE [dbo].[EvenementDommagePrecision] DROP CONSTRAINT [DF_EvenementDommagePrecision_withNom];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[DF_EvenementDommagePrecision_withNomAssurance]...';


GO
ALTER TABLE [dbo].[EvenementDommagePrecision] DROP CONSTRAINT [DF_EvenementDommagePrecision_withNomAssurance];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[DF_EvenementDommagePrecision_withNumeroContratAssurance]...';


GO
ALTER TABLE [dbo].[EvenementDommagePrecision] DROP CONSTRAINT [DF_EvenementDommagePrecision_withNumeroContratAssurance];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[DF_EvenementDommagePrecision_withPrenom]...';


GO
ALTER TABLE [dbo].[EvenementDommagePrecision] DROP CONSTRAINT [DF_EvenementDommagePrecision_withPrenom];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[DF_EvenementDommagePrecision_withRessourceHomme]...';


GO
ALTER TABLE [dbo].[EvenementDommagePrecision] DROP CONSTRAINT [DF_EvenementDommagePrecision_withRessourceHomme];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[DF_EvenementDommagePrecision_withRessourceMateriel]...';


GO
ALTER TABLE [dbo].[EvenementDommagePrecision] DROP CONSTRAINT [DF_EvenementDommagePrecision_withRessourceMateriel];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[DF_EvenementDommagePrecision_withTiersType]...';


GO
ALTER TABLE [dbo].[EvenementDommagePrecision] DROP CONSTRAINT [DF_EvenementDommagePrecision_withTiersType];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[DF_EvenementDommagePrecision_withTiersAge]...';


GO
ALTER TABLE [dbo].[EvenementDommagePrecision] DROP CONSTRAINT [DF_EvenementDommagePrecision_withTiersAge];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[DF_EvenementDommagePrecision_withContexte]...';


GO
ALTER TABLE [dbo].[EvenementDommagePrecision] DROP CONSTRAINT [DF_EvenementDommagePrecision_withContexte];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[DF_MaterielListe_code]...';


GO
ALTER TABLE [dbo].[MaterielListe] DROP CONSTRAINT [DF_MaterielListe_code];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[DF_MaterielListe_nomLong]...';


GO
ALTER TABLE [dbo].[MaterielListe] DROP CONSTRAINT [DF_MaterielListe_nomLong];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[DF_AleaType_id_aleaType_evenementFamille]...';


GO
ALTER TABLE [dbo].[AleaType] DROP CONSTRAINT [DF_AleaType_id_aleaType_evenementFamille];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[DF_MaterielElement_rank]...';


GO
ALTER TABLE [dbo].[MaterielElement] DROP CONSTRAINT [DF_MaterielElement_rank];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[DF_MaterielType_IsImportant]...';


GO
ALTER TABLE [dbo].[MaterielType] DROP CONSTRAINT [DF_MaterielType_IsImportant];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[DF_MaterielType_rank]...';


GO
ALTER TABLE [dbo].[MaterielType] DROP CONSTRAINT [DF_MaterielType_rank];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de contrainte sans nom sur [dbo].[Query]...';


GO
ALTER TABLE [dbo].[Query] DROP CONSTRAINT [DF__Query__id_query___3BFFE745];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[DF_Query_results_ponderation]...';


GO
ALTER TABLE [dbo].[Query] DROP CONSTRAINT [DF_Query_results_ponderation];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_DommageConseq_dommagePrecision]...';


GO
ALTER TABLE [dbo].[EvenementDommage] DROP CONSTRAINT [FK_DommageConseq_dommagePrecision];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_EvenementDommagePrecision_DommageType]...';


GO
ALTER TABLE [dbo].[EvenementDommagePrecision] DROP CONSTRAINT [FK_EvenementDommagePrecision_DommageType];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_Intervenant_IntervenantType]...';


GO
ALTER TABLE [dbo].[EvenementIntervenant] DROP CONSTRAINT [FK_Intervenant_IntervenantType];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_InterventionConseq_Intervenants]...';


GO
ALTER TABLE [dbo].[EvenementIntervention] DROP CONSTRAINT [FK_InterventionConseq_Intervenants];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_CfgIntervenant_Intervenant]...';


GO
ALTER TABLE [dbo].[CfgIntervenant] DROP CONSTRAINT [FK_CfgIntervenant_Intervenant];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_InterventionConseq_InterventionEtat]...';


GO
ALTER TABLE [dbo].[EvenementIntervention] DROP CONSTRAINT [FK_InterventionConseq_InterventionEtat];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_EvenementIntervention_EvenementList]...';


GO
ALTER TABLE [dbo].[EvenementIntervention] DROP CONSTRAINT [FK_EvenementIntervention_EvenementList];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_QueryTriggerAsynchoneHistorique_Alea]...';


GO
ALTER TABLE [dbo].[QueryTriggerAsynchoneHistorique] DROP CONSTRAINT [FK_QueryTriggerAsynchoneHistorique_Alea];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_QueryTriggerAsynchoneHistorique_QueryTriggerAsynchone]...';


GO
ALTER TABLE [dbo].[QueryTriggerAsynchoneHistorique] DROP CONSTRAINT [FK_QueryTriggerAsynchoneHistorique_QueryTriggerAsynchone];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_ConfigurationMainCourante_AleaState]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] DROP CONSTRAINT [FK_ConfigurationMainCourante_AleaState];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_AleaListe_AleaEtat]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [FK_AleaListe_AleaEtat];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_CfgAleaTypeFicheReseau_Fiche]...';


GO
ALTER TABLE [dbo].[CfgAleaTypeFicheReseau] DROP CONSTRAINT [FK_CfgAleaTypeFicheReseau_Fiche];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_FicheRealise_Fiche]...';


GO
ALTER TABLE [dbo].[FicheRealise] DROP CONSTRAINT [FK_FicheRealise_Fiche];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_LnkFicheFicheItem_Fiche]...';


GO
ALTER TABLE [dbo].[LnkFicheFicheItem] DROP CONSTRAINT [FK_LnkFicheFicheItem_Fiche];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_FicheItemRealise_FicheItem]...';


GO
ALTER TABLE [dbo].[FicheItemRealise] DROP CONSTRAINT [FK_FicheItemRealise_FicheItem];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_LnkFicheFicheItem_FicheItem]...';


GO
ALTER TABLE [dbo].[LnkFicheFicheItem] DROP CONSTRAINT [FK_LnkFicheFicheItem_FicheItem];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_LnkFicheRealiseFicheItemRealise_FicheItemRealise]...';


GO
ALTER TABLE [dbo].[LnkFicheRealiseFicheItemRealise] DROP CONSTRAINT [FK_LnkFicheRealiseFicheItemRealise_FicheItemRealise];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_LnkAleaListeFicheRealise_FicheRealise]...';


GO
ALTER TABLE [dbo].[LnkAleaListeFicheRealise] DROP CONSTRAINT [FK_LnkAleaListeFicheRealise_FicheRealise];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_LnkFicheRealiseFicheItemRealise_ficheRealise]...';


GO
ALTER TABLE [dbo].[LnkFicheRealiseFicheItemRealise] DROP CONSTRAINT [FK_LnkFicheRealiseFicheItemRealise_ficheRealise];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_Fiche_FicheType]...';


GO
ALTER TABLE [dbo].[Fiche] DROP CONSTRAINT [FK_Fiche_FicheType];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_AleaType_AleaElement]...';


GO
ALTER TABLE [dbo].[AleaType] DROP CONSTRAINT [FK_AleaType_AleaElement];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_CfgMaterielElementAleaElement_AleaElement]...';


GO
ALTER TABLE [dbo].[CfgMaterielElementAleaElement] DROP CONSTRAINT [FK_CfgMaterielElementAleaElement_AleaElement];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_CfgAleaTypeFicheReseau_AleaType]...';


GO
ALTER TABLE [dbo].[CfgAleaTypeFicheReseau] DROP CONSTRAINT [FK_CfgAleaTypeFicheReseau_AleaType];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_AlerteList_AleaType]...';


GO
ALTER TABLE [dbo].[CfgAlerteAleaDiffusionListReseau] DROP CONSTRAINT [FK_AlerteList_AleaType];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_AleaListe_AleaType]...';


GO
ALTER TABLE [dbo].[AleaListe] DROP CONSTRAINT [FK_AleaListe_AleaType];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_AleaType_EvenementFamille]...';


GO
ALTER TABLE [dbo].[AleaType] DROP CONSTRAINT [FK_AleaType_EvenementFamille];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_CfgQueryDataSourceQueryCriteria_QueryDataSource]...';


GO
ALTER TABLE [dbo].[CfgQueryDataSourceQueryCriteria] DROP CONSTRAINT [FK_CfgQueryDataSourceQueryCriteria_QueryDataSource];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_Query_QueryDataSource]...';


GO
ALTER TABLE [dbo].[Query] DROP CONSTRAINT [FK_Query_QueryDataSource];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_CfgGeneralMainCourante_RessourceClasse]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] DROP CONSTRAINT [FK_CfgGeneralMainCourante_RessourceClasse];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_CfgMaterielElementAleaElement_MaterielElement]...';


GO
ALTER TABLE [dbo].[CfgMaterielElementAleaElement] DROP CONSTRAINT [FK_CfgMaterielElementAleaElement_MaterielElement];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_CfgMaterielElementMaterielEtat_MaterielElement]...';


GO
ALTER TABLE [dbo].[CfgMaterielElementMaterielEtat] DROP CONSTRAINT [FK_CfgMaterielElementMaterielEtat_MaterielElement];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_MaterielType_MaterielElement]...';


GO
ALTER TABLE [dbo].[MaterielType] DROP CONSTRAINT [FK_MaterielType_MaterielElement];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_CfgGeneralMainCourante_Etat1]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] DROP CONSTRAINT [FK_CfgGeneralMainCourante_Etat1];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_CfgGeneralMainCourante_Etat2]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] DROP CONSTRAINT [FK_CfgGeneralMainCourante_Etat2];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_CfgGeneralMainCourante_Etat3]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] DROP CONSTRAINT [FK_CfgGeneralMainCourante_Etat3];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_CfgMaterielElementMaterielEtat_MaterielEtat]...';


GO
ALTER TABLE [dbo].[CfgMaterielElementMaterielEtat] DROP CONSTRAINT [FK_CfgMaterielElementMaterielEtat_MaterielEtat];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_CfgMaterielEtatColor_MaterielEtat]...';


GO
ALTER TABLE [dbo].[CfgMaterielEtatColor] DROP CONSTRAINT [FK_CfgMaterielEtatColor_MaterielEtat];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_MaterielEtatHistorique_MaterielEtat]...';


GO
ALTER TABLE [dbo].[MaterielEtatHistorique] DROP CONSTRAINT [FK_MaterielEtatHistorique_MaterielEtat];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_MaterielListe_MaterielEtat]...';


GO
ALTER TABLE [dbo].[MaterielListe] DROP CONSTRAINT [FK_MaterielListe_MaterielEtat];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_CfgRessourceSousClasse_RessourceSousClasse]...';


GO
ALTER TABLE [dbo].[CfgRessourceSousClasse] DROP CONSTRAINT [FK_CfgRessourceSousClasse_RessourceSousClasse];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_MaterielListe_MaterielType]...';


GO
ALTER TABLE [dbo].[MaterielListe] DROP CONSTRAINT [FK_MaterielListe_MaterielType];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_Analyse_Query]...';


GO
ALTER TABLE [dbo].[Analyse] DROP CONSTRAINT [FK_Analyse_Query];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_LnkQueryEvenementAttribut_Query]...';


GO
ALTER TABLE [dbo].[LnkQueryEvenementAttribut] DROP CONSTRAINT [FK_LnkQueryEvenementAttribut_Query];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_LnkQueryEvenementSection_Query]...';


GO
ALTER TABLE [dbo].[LnkQueryEvenementSection] DROP CONSTRAINT [FK_LnkQueryEvenementSection_Query];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_LnkQueryQueryCriteria_Query]...';


GO
ALTER TABLE [dbo].[QueryCriteriaApplied] DROP CONSTRAINT [FK_LnkQueryQueryCriteria_Query];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_QueryTriggerAsynchrone_Query]...';


GO
ALTER TABLE [dbo].[QueryTriggerAsynchrone] DROP CONSTRAINT [FK_QueryTriggerAsynchrone_Query];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_QueryTriggerSynchrone_Query]...';


GO
ALTER TABLE [dbo].[QueryTriggerSynchrone] DROP CONSTRAINT [FK_QueryTriggerSynchrone_Query];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_Query_QueryGroup]...';


GO
ALTER TABLE [dbo].[Query] DROP CONSTRAINT [FK_Query_QueryGroup];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_Query_Rapport]...';


GO
ALTER TABLE [dbo].[Query] DROP CONSTRAINT [FK_Query_Rapport];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_Query_Imputabilite]...';


GO
ALTER TABLE [dbo].[Query] DROP CONSTRAINT [FK_Query_Imputabilite];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FK_LnkAleaListeFicheRealise_AleaListe]...';


GO
ALTER TABLE [dbo].[LnkAleaListeFicheRealise] DROP CONSTRAINT [FK_LnkAleaListeFicheRealise_AleaListe];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[AleaEtat]...';


GO
DROP TABLE [dbo].[AleaEtat];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[CfgAleaTypeFicheReseau]...';


GO
DROP TABLE [dbo].[CfgAleaTypeFicheReseau];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[CfgIntervenant]...';


GO
DROP TABLE [dbo].[CfgIntervenant];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[CfgRessourceSousClasse]...';


GO
DROP TABLE [dbo].[CfgRessourceSousClasse];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[Fiche]...';


GO
DROP TABLE [dbo].[Fiche];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FicheItem]...';


GO
DROP TABLE [dbo].[FicheItem];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FicheItemRealise]...';


GO
DROP TABLE [dbo].[FicheItemRealise];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FicheRealise]...';


GO
DROP TABLE [dbo].[FicheRealise];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[FicheType]...';


GO
DROP TABLE [dbo].[FicheType];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[LnkAleaListeFicheRealise]...';


GO
DROP TABLE [dbo].[LnkAleaListeFicheRealise];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[LnkFicheFicheItem]...';


GO
DROP TABLE [dbo].[LnkFicheFicheItem];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[LnkFicheRealiseFicheItemRealise]...';


GO
DROP TABLE [dbo].[LnkFicheRealiseFicheItemRealise];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[VueCfgMaterielDetailCategorie]...';


GO
DROP VIEW [dbo].[VueCfgMaterielDetailCategorie];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[VueCfgMaterielElementAleaElement]...';


GO
DROP VIEW [dbo].[VueCfgMaterielElementAleaElement];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[VueCfgMaterielElementAleaElementAleaType]...';


GO
DROP VIEW [dbo].[VueCfgMaterielElementAleaElementAleaType];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[CfgMaterielDetailCategorie]...';


GO
DROP TABLE [dbo].[CfgMaterielDetailCategorie];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Suppression de [dbo].[CfgMaterielElementAleaElement]...';


GO
DROP TABLE [dbo].[CfgMaterielElementAleaElement];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Début de la régénération de la table [dbo].[AleaElement]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_AleaElement] (
    [id_aleaElement]    INT            IDENTITY (1, 1) NOT NULL,
    [nom]               NVARCHAR (MAX) NOT NULL,
    [id_reseaus101m401] INT            CONSTRAINT [DF_AleaElement_id_reseaus101m401] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK__AleaElem__109AEB6037F750511] PRIMARY KEY CLUSTERED ([id_aleaElement] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[AleaElement])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_AleaElement] ON;
        INSERT INTO [dbo].[tmp_ms_xx_AleaElement] ([id_aleaElement], [nom])
        SELECT   [id_aleaElement],
                 [nom]
        FROM     [dbo].[AleaElement]
        ORDER BY [id_aleaElement] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_AleaElement] OFF;
    END

DROP TABLE [dbo].[AleaElement];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_AleaElement]', N'AleaElement';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK__AleaElem__109AEB6037F750511]', N'PK__AleaElem__109AEB6037F75051', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Début de la régénération de la table [dbo].[AleaType]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_AleaType] (
    [id_aleaType]                  INT            IDENTITY (1, 1) NOT NULL,
    [nom]                          NVARCHAR (MAX) NOT NULL,
    [id_aleaType_aleaElement]      INT            NOT NULL,
    [id_aleaType_evenementFamille] INT            CONSTRAINT [DF_AleaType_id_aleaType_evenementFamille] DEFAULT ((1)) NOT NULL,
    [isPublic]                     INT            CONSTRAINT [DF_AleaType_isPublic] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK__AleaType__E93408F2DDC654C41] PRIMARY KEY CLUSTERED ([id_aleaType] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[AleaType])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_AleaType] ON;
        INSERT INTO [dbo].[tmp_ms_xx_AleaType] ([id_aleaType], [nom], [id_aleaType_aleaElement], [id_aleaType_evenementFamille])
        SELECT   [id_aleaType],
                 [nom],
                 [id_aleaType_aleaElement],
                 [id_aleaType_evenementFamille]
        FROM     [dbo].[AleaType]
        ORDER BY [id_aleaType] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_AleaType] OFF;
    END

DROP TABLE [dbo].[AleaType];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_AleaType]', N'AleaType';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK__AleaType__E93408F2DDC654C41]', N'PK__AleaType__E93408F2DDC654C4', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[CfgEvenementMaintenanceEmplacementRessource]...';


GO
ALTER TABLE [dbo].[CfgEvenementMaintenanceEmplacementRessource]
    ADD [id_cfgEvenementMaintenanceEmplacementRessource_ressourceCategorieGroupe] INT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[CfgEvenementMaintenanceOrganeRessource]...';


GO
ALTER TABLE [dbo].[CfgEvenementMaintenanceOrganeRessource]
    ADD [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] INT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[CfgGeneralMainCourante]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante]
    ADD [id_ressourceClasseClient_Imports2006m1055]     INT NULL,
        [id_ressourceSousClasseClient_Imports2006m1055] INT NULL,
        [id_ressourceCategorieClient_Imports2006m1055]  INT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[ConstAnalyseType]...';


GO
ALTER TABLE [dbo].[ConstAnalyseType] ALTER COLUMN [code] NVARCHAR (MAX) NOT NULL;

ALTER TABLE [dbo].[ConstAnalyseType] ALTER COLUMN [libelle] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[ConstConfigurationRules]...';


GO
ALTER TABLE [dbo].[ConstConfigurationRules] ALTER COLUMN [libelle] NVARCHAR (MAX) NOT NULL;

ALTER TABLE [dbo].[ConstConfigurationRules] ALTER COLUMN [nom] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[ConstEvenementControl]...';


GO
ALTER TABLE [dbo].[ConstEvenementControl] ALTER COLUMN [code] NVARCHAR (MAX) NOT NULL;

ALTER TABLE [dbo].[ConstEvenementControl] ALTER COLUMN [libelle] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[ConstEvenementControlState]...';


GO
ALTER TABLE [dbo].[ConstEvenementControlState] ALTER COLUMN [code] NVARCHAR (MAX) NOT NULL;

ALTER TABLE [dbo].[ConstEvenementControlState] ALTER COLUMN [libelle] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[ConstEvenementExploitConseq]...';


GO
ALTER TABLE [dbo].[ConstEvenementExploitConseq] ALTER COLUMN [code] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[ConstEvenementFamille]...';


GO
ALTER TABLE [dbo].[ConstEvenementFamille] ALTER COLUMN [code] NVARCHAR (MAX) NOT NULL;

ALTER TABLE [dbo].[ConstEvenementFamille] ALTER COLUMN [libelle] NVARCHAR (MAX) NOT NULL;


GO
ALTER TABLE [dbo].[ConstEvenementFamille]
    ADD [ponderation] INT CONSTRAINT [DF_ConstEvenementFamille_ponderation] DEFAULT ((1)) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[ConstEvenementLieuType]...';


GO
ALTER TABLE [dbo].[ConstEvenementLieuType] ALTER COLUMN [code] NVARCHAR (MAX) NOT NULL;

ALTER TABLE [dbo].[ConstEvenementLieuType] ALTER COLUMN [libelle] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[ConstEvenementSection]...';


GO
ALTER TABLE [dbo].[ConstEvenementSection] ALTER COLUMN [code] NVARCHAR (MAX) NOT NULL;

ALTER TABLE [dbo].[ConstEvenementSection] ALTER COLUMN [libelle] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[ConstEvenementSectionState]...';


GO
ALTER TABLE [dbo].[ConstEvenementSectionState] ALTER COLUMN [libelle] NVARCHAR (MAX) NOT NULL;

ALTER TABLE [dbo].[ConstEvenementSectionState] ALTER COLUMN [nom] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[ConstModeSaisie]...';


GO
ALTER TABLE [dbo].[ConstModeSaisie] ALTER COLUMN [code] NVARCHAR (MAX) NOT NULL;

ALTER TABLE [dbo].[ConstModeSaisie] ALTER COLUMN [libelle] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[ConstQueryCriteria]...';


GO
ALTER TABLE [dbo].[ConstQueryCriteria] ALTER COLUMN [code] NVARCHAR (MAX) NOT NULL;

ALTER TABLE [dbo].[ConstQueryCriteria] ALTER COLUMN [libelle] NVARCHAR (MAX) NOT NULL;


GO
ALTER TABLE [dbo].[ConstQueryCriteria]
    ADD [isCriteriaTemporel] INT CONSTRAINT [DF_ConstQueryCriteria_isCriteriaTemporel] DEFAULT ((0)) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[ConstQueryCriteriaGroup]...';


GO
ALTER TABLE [dbo].[ConstQueryCriteriaGroup] ALTER COLUMN [nom] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Début de la régénération de la table [dbo].[ConstQueryDataSource]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_ConstQueryDataSource] (
    [id_queryDataSource] INT            IDENTITY (1, 1) NOT NULL,
    [nom]                NVARCHAR (MAX) NOT NULL,
    [libelle]            NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK__ConstQue__6728C77C8EABD50F1] PRIMARY KEY CLUSTERED ([id_queryDataSource] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[ConstQueryDataSource])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_ConstQueryDataSource] ON;
        INSERT INTO [dbo].[tmp_ms_xx_ConstQueryDataSource] ([id_queryDataSource], [nom], [libelle])
        SELECT   [id_queryDataSource],
                 [nom],
                 [libelle]
        FROM     [dbo].[ConstQueryDataSource]
        ORDER BY [id_queryDataSource] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_ConstQueryDataSource] OFF;
    END

DROP TABLE [dbo].[ConstQueryDataSource];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_ConstQueryDataSource]', N'ConstQueryDataSource';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK__ConstQue__6728C77C8EABD50F1]', N'PK__ConstQue__6728C77C8EABD50F', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[ConstRessourcesFamille]...';


GO
ALTER TABLE [dbo].[ConstRessourcesFamille] ALTER COLUMN [code] NVARCHAR (MAX) NOT NULL;

ALTER TABLE [dbo].[ConstRessourcesFamille] ALTER COLUMN [libelle] NVARCHAR (MAX) NOT NULL;


GO
ALTER TABLE [dbo].[ConstRessourcesFamille]
    ADD [ponderation] INT CONSTRAINT [DF_ConstRessourcesFamille_ponderation] DEFAULT ((1)) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[ConstRessourceTag]...';


GO
ALTER TABLE [dbo].[ConstRessourceTag]
    ADD [rang] INT CONSTRAINT [DF_ConstRessourceTag_rang] DEFAULT ((1)) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[ConstTerminalSaisie]...';


GO
ALTER TABLE [dbo].[ConstTerminalSaisie] ALTER COLUMN [code] NVARCHAR (MAX) NOT NULL;

ALTER TABLE [dbo].[ConstTerminalSaisie] ALTER COLUMN [libelle] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Début de la régénération de la table [dbo].[DBinfos]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_DBinfos] (
    [id_dbinfos] INT            IDENTITY (1, 1) NOT NULL,
    [nom]        NVARCHAR (MAX) NOT NULL,
    [valeurs]    NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK__DBinfos__E305C71E82D8FB5E1] PRIMARY KEY CLUSTERED ([id_dbinfos] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[DBinfos])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_DBinfos] ON;
        INSERT INTO [dbo].[tmp_ms_xx_DBinfos] ([id_dbinfos], [nom], [valeurs])
        SELECT   [id_dbinfos],
                 [nom],
                 [valeurs]
        FROM     [dbo].[DBinfos]
        ORDER BY [id_dbinfos] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_DBinfos] OFF;
    END

DROP TABLE [dbo].[DBinfos];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_DBinfos]', N'DBinfos';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK__DBinfos__E305C71E82D8FB5E1]', N'PK__DBinfos__E305C71E82D8FB5E', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[EvenementDommage]...';


GO
ALTER TABLE [dbo].[EvenementDommage] ALTER COLUMN [immatriculation] NVARCHAR (MAX) NULL;

ALTER TABLE [dbo].[EvenementDommage] ALTER COLUMN [nom] NVARCHAR (MAX) NULL;

ALTER TABLE [dbo].[EvenementDommage] ALTER COLUMN [nomAssurance] NVARCHAR (MAX) NULL;

ALTER TABLE [dbo].[EvenementDommage] ALTER COLUMN [numeroContratAssurance] NVARCHAR (MAX) NULL;

ALTER TABLE [dbo].[EvenementDommage] ALTER COLUMN [prenom] NVARCHAR (MAX) NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Début de la régénération de la table [dbo].[EvenementDommagePrecision]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_EvenementDommagePrecision] (
    [id_evenementDommagePrecision]             INT            IDENTITY (1, 1) NOT NULL,
    [id_evenementDommagePrecision_dommageType] INT            NOT NULL,
    [nom]                                      NVARCHAR (MAX) NOT NULL,
    [withNom]                                  INT            CONSTRAINT [DF_EvenementDommagePrecision_withNom] DEFAULT ((0)) NOT NULL,
    [withPrenom]                               INT            CONSTRAINT [DF_EvenementDommagePrecision_withPrenom] DEFAULT ((0)) NOT NULL,
    [withNomAssurance]                         INT            CONSTRAINT [DF_EvenementDommagePrecision_withNomAssurance] DEFAULT ((0)) NOT NULL,
    [withNumeroContratAssurance]               INT            CONSTRAINT [DF_EvenementDommagePrecision_withNumeroContratAssurance] DEFAULT ((0)) NOT NULL,
    [withImmatriculation]                      INT            CONSTRAINT [DF_EvenementDommagePrecision_withImmatriculation] DEFAULT ((0)) NOT NULL,
    [withRessourceMateriel]                    INT            CONSTRAINT [DF_EvenementDommagePrecision_withRessourceMateriel] DEFAULT ((0)) NOT NULL,
    [withRessourceHomme]                       INT            CONSTRAINT [DF_EvenementDommagePrecision_withRessourceHomme] DEFAULT ((0)) NOT NULL,
    [withCommentaires]                         INT            CONSTRAINT [DF_EvenementDommagePrecision_withCommentaires] DEFAULT ((0)) NOT NULL,
    [withTiersType]                            INT            CONSTRAINT [DF_EvenementDommagePrecision_withTiersType] DEFAULT ((0)) NOT NULL,
    [withTiersAge]                             INT            CONSTRAINT [DF_EvenementDommagePrecision_withTiersAge] DEFAULT ((0)) NOT NULL,
    [withContexte]                             INT            CONSTRAINT [DF_EvenementDommagePrecision_withContexte] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PrimaryKey_01f22329-dbec-4eb9-96c9-ee4868a807b11] PRIMARY KEY CLUSTERED ([id_evenementDommagePrecision] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[EvenementDommagePrecision])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementDommagePrecision] ON;
        INSERT INTO [dbo].[tmp_ms_xx_EvenementDommagePrecision] ([id_evenementDommagePrecision], [id_evenementDommagePrecision_dommageType], [nom], [withNom], [withPrenom], [withNomAssurance], [withNumeroContratAssurance], [withImmatriculation], [withRessourceMateriel], [withRessourceHomme], [withCommentaires], [withTiersType], [withTiersAge], [withContexte])
        SELECT   [id_evenementDommagePrecision],
                 [id_evenementDommagePrecision_dommageType],
                 [nom],
                 [withNom],
                 [withPrenom],
                 [withNomAssurance],
                 [withNumeroContratAssurance],
                 [withImmatriculation],
                 [withRessourceMateriel],
                 [withRessourceHomme],
                 [withCommentaires],
                 [withTiersType],
                 [withTiersAge],
                 [withContexte]
        FROM     [dbo].[EvenementDommagePrecision]
        ORDER BY [id_evenementDommagePrecision] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementDommagePrecision] OFF;
    END

DROP TABLE [dbo].[EvenementDommagePrecision];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_EvenementDommagePrecision]', N'EvenementDommagePrecision';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PrimaryKey_01f22329-dbec-4eb9-96c9-ee4868a807b11]', N'PrimaryKey_01f22329-dbec-4eb9-96c9-ee4868a807b1', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Début de la régénération de la table [dbo].[EvenementDommageType]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_EvenementDommageType] (
    [id_dommageType] INT            IDENTITY (1, 1) NOT NULL,
    [nom]            NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PrimaryKey_d957a7b2-ea1c-4e9b-83e5-ded0b03f360a1] PRIMARY KEY CLUSTERED ([id_dommageType] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[EvenementDommageType])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementDommageType] ON;
        INSERT INTO [dbo].[tmp_ms_xx_EvenementDommageType] ([id_dommageType], [nom])
        SELECT   [id_dommageType],
                 [nom]
        FROM     [dbo].[EvenementDommageType]
        ORDER BY [id_dommageType] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementDommageType] OFF;
    END

DROP TABLE [dbo].[EvenementDommageType];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_EvenementDommageType]', N'EvenementDommageType';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PrimaryKey_d957a7b2-ea1c-4e9b-83e5-ded0b03f360a1]', N'PrimaryKey_d957a7b2-ea1c-4e9b-83e5-ded0b03f360a', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[EvenementExploitationConseqManoeuvre]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseqManoeuvre] ALTER COLUMN [nom] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[EvenementFermetureRaison]...';


GO
ALTER TABLE [dbo].[EvenementFermetureRaison] ALTER COLUMN [libelle] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Début de la régénération de la table [dbo].[EvenementIntervenant]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_EvenementIntervenant] (
    [id_intervenant]                 INT            IDENTITY (1, 1) NOT NULL,
    [nom]                            NVARCHAR (MAX) NOT NULL,
    [id_intervenant_intervenantType] INT            NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PrimaryKey_508e593f-72e2-4ee0-9ec4-2912721459a91] PRIMARY KEY CLUSTERED ([id_intervenant] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[EvenementIntervenant])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementIntervenant] ON;
        INSERT INTO [dbo].[tmp_ms_xx_EvenementIntervenant] ([id_intervenant], [nom], [id_intervenant_intervenantType])
        SELECT   [id_intervenant],
                 [nom],
                 [id_intervenant_intervenantType]
        FROM     [dbo].[EvenementIntervenant]
        ORDER BY [id_intervenant] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementIntervenant] OFF;
    END

DROP TABLE [dbo].[EvenementIntervenant];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_EvenementIntervenant]', N'EvenementIntervenant';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PrimaryKey_508e593f-72e2-4ee0-9ec4-2912721459a91]', N'PrimaryKey_508e593f-72e2-4ee0-9ec4-2912721459a9', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Début de la régénération de la table [dbo].[EvenementIntervenantType]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_EvenementIntervenantType] (
    [id_intervenantType] INT            IDENTITY (1, 1) NOT NULL,
    [nom]                NVARCHAR (MAX) NOT NULL,
    [id_reseaus101m401]  INT            CONSTRAINT [DF_EvenementIntervenantType_id_reseaus101m401] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PrimaryKey_16479eb0-57aa-49ee-a3f6-11693f64d6fc1] PRIMARY KEY CLUSTERED ([id_intervenantType] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[EvenementIntervenantType])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementIntervenantType] ON;
        INSERT INTO [dbo].[tmp_ms_xx_EvenementIntervenantType] ([id_intervenantType], [nom])
        SELECT   [id_intervenantType],
                 [nom]
        FROM     [dbo].[EvenementIntervenantType]
        ORDER BY [id_intervenantType] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementIntervenantType] OFF;
    END

DROP TABLE [dbo].[EvenementIntervenantType];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_EvenementIntervenantType]', N'EvenementIntervenantType';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PrimaryKey_16479eb0-57aa-49ee-a3f6-11693f64d6fc1]', N'PrimaryKey_16479eb0-57aa-49ee-a3f6-11693f64d6fc', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Début de la régénération de la table [dbo].[EvenementIntervention]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_EvenementIntervention] (
    [id_interventionConseq]                  INT            IDENTITY (1, 1) NOT NULL,
    [id_interventionConseq_intervenant]      INT            NOT NULL,
    [id_evenementIntervention_ressource1]    INT            NULL,
    [id_evenementIntervention_ressource2]    INT            NULL,
    [id_interventionConseq_interventionEtat] INT            NULL,
    [heure_debut]                            TIME (7)       NULL,
    [heure_fin]                              TIME (7)       NULL,
    [id_evenementIntervention_evenementList] INT            NOT NULL,
    [id_utilisateur]                         INT            NULL,
    [loginUtilisateur]                       NVARCHAR (MAX) NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PrimaryKey_cdc2149d-2f89-403c-ad87-ab8d58b253cf1] PRIMARY KEY CLUSTERED ([id_interventionConseq] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[EvenementIntervention])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementIntervention] ON;
        INSERT INTO [dbo].[tmp_ms_xx_EvenementIntervention] ([id_interventionConseq], [id_interventionConseq_intervenant], [id_interventionConseq_interventionEtat], [heure_debut], [heure_fin], [id_evenementIntervention_evenementList], [id_utilisateur], [loginUtilisateur])
        SELECT   [id_interventionConseq],
                 [id_interventionConseq_intervenant],
                 [id_interventionConseq_interventionEtat],
                 [heure_debut],
                 [heure_fin],
                 [id_evenementIntervention_evenementList],
                 [id_utilisateur],
                 [loginUtilisateur]
        FROM     [dbo].[EvenementIntervention]
        ORDER BY [id_interventionConseq] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementIntervention] OFF;
    END

DROP TABLE [dbo].[EvenementIntervention];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_EvenementIntervention]', N'EvenementIntervention';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PrimaryKey_cdc2149d-2f89-403c-ad87-ab8d58b253cf1]', N'PrimaryKey_cdc2149d-2f89-403c-ad87-ab8d58b253cf', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Début de la régénération de la table [dbo].[EvenementInterventionActivite]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_EvenementInterventionActivite] (
    [id_interventionEtat] INT            IDENTITY (1, 1) NOT NULL,
    [nom]                 NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PrimaryKey_9868f7a1-e1a4-482e-bc35-f5de48facfd51] PRIMARY KEY CLUSTERED ([id_interventionEtat] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[EvenementInterventionActivite])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementInterventionActivite] ON;
        INSERT INTO [dbo].[tmp_ms_xx_EvenementInterventionActivite] ([id_interventionEtat], [nom])
        SELECT   [id_interventionEtat],
                 [nom]
        FROM     [dbo].[EvenementInterventionActivite]
        ORDER BY [id_interventionEtat] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_EvenementInterventionActivite] OFF;
    END

DROP TABLE [dbo].[EvenementInterventionActivite];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_EvenementInterventionActivite]', N'EvenementInterventionActivite';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PrimaryKey_9868f7a1-e1a4-482e-bc35-f5de48facfd51]', N'PrimaryKey_9868f7a1-e1a4-482e-bc35-f5de48facfd5', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[EvenementMaintenanceEmplacement]...';


GO
ALTER TABLE [dbo].[EvenementMaintenanceEmplacement] ALTER COLUMN [nom] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[EvenementMaintenanceImpact]...';


GO
ALTER TABLE [dbo].[EvenementMaintenanceImpact] ALTER COLUMN [nom] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[EvenementMaintenanceOrgane]...';


GO
ALTER TABLE [dbo].[EvenementMaintenanceOrgane] ALTER COLUMN [nom] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[EvenementMaintenanceSymptome]...';


GO
ALTER TABLE [dbo].[EvenementMaintenanceSymptome] ALTER COLUMN [nom] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[EvenementPrecisonType]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonType] ALTER COLUMN [label] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[EvenementPrecisonTypeSelect]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonTypeSelect] ALTER COLUMN [label] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[EvenementPrecisonTypeSelectOption]...';


GO
ALTER TABLE [dbo].[EvenementPrecisonTypeSelectOption] ALTER COLUMN [label] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[EvenementSecuritePrecision]...';


GO
ALTER TABLE [dbo].[EvenementSecuritePrecision] ALTER COLUMN [nom] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[EvenementSecuriteType]...';


GO
ALTER TABLE [dbo].[EvenementSecuriteType] ALTER COLUMN [nom] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[MaterielDetailCategorie]...';


GO
ALTER TABLE [dbo].[MaterielDetailCategorie] ALTER COLUMN [nom] NVARCHAR (MAX) NOT NULL;


GO
ALTER TABLE [dbo].[MaterielDetailCategorie]
    ADD [id_materielDetailCategorie_ressourceCategorieGroupe] INT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Début de la régénération de la table [dbo].[MaterielElement]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_MaterielElement] (
    [id_materielElement] INT            IDENTITY (1, 1) NOT NULL,
    [nom]                NVARCHAR (MAX) NOT NULL,
    [id_reseau]          INT            NOT NULL,
    [rank]               INT            CONSTRAINT [DF_MaterielElement_rank] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK__Materiel__01271C017914A9961] PRIMARY KEY CLUSTERED ([id_materielElement] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[MaterielElement])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_MaterielElement] ON;
        INSERT INTO [dbo].[tmp_ms_xx_MaterielElement] ([id_materielElement], [nom], [id_reseau], [rank])
        SELECT   [id_materielElement],
                 [nom],
                 [id_reseau],
                 [rank]
        FROM     [dbo].[MaterielElement]
        ORDER BY [id_materielElement] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_MaterielElement] OFF;
    END

DROP TABLE [dbo].[MaterielElement];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_MaterielElement]', N'MaterielElement';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK__Materiel__01271C017914A9961]', N'PK__Materiel__01271C017914A996', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Début de la régénération de la table [dbo].[MaterielEtat]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_MaterielEtat] (
    [id_materielEtat] INT            IDENTITY (1, 1) NOT NULL,
    [nom]             NVARCHAR (MAX) NOT NULL,
    [rang]            INT            CONSTRAINT [DF_MaterielEtat_rang] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK__Materiel__6B05FE83492939CD1] PRIMARY KEY CLUSTERED ([id_materielEtat] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[MaterielEtat])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_MaterielEtat] ON;
        INSERT INTO [dbo].[tmp_ms_xx_MaterielEtat] ([id_materielEtat], [nom])
        SELECT   [id_materielEtat],
                 [nom]
        FROM     [dbo].[MaterielEtat]
        ORDER BY [id_materielEtat] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_MaterielEtat] OFF;
    END

DROP TABLE [dbo].[MaterielEtat];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_MaterielEtat]', N'MaterielEtat';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK__Materiel__6B05FE83492939CD1]', N'PK__Materiel__6B05FE83492939CD', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[MaterielListe]...';


GO
ALTER TABLE [dbo].[MaterielListe] ALTER COLUMN [code] NVARCHAR (MAX) NOT NULL;

ALTER TABLE [dbo].[MaterielListe] ALTER COLUMN [nomLong] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Début de la régénération de la table [dbo].[MaterielType]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_MaterielType] (
    [id_materielType]                 INT            IDENTITY (1, 1) NOT NULL,
    [nom]                             NVARCHAR (MAX) NOT NULL,
    [id_materielType_MaterielElement] INT            NULL,
    [rank]                            INT            CONSTRAINT [DF_MaterielType_rank] DEFAULT ((1)) NOT NULL,
    [IsImportant]                     INT            CONSTRAINT [DF_MaterielType_IsImportant] DEFAULT ((0)) NOT NULL,
    [isPublic]                        INT            CONSTRAINT [DF_MaterielType_isPublic] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK__Materiel__0BD866C0141FFE111] PRIMARY KEY CLUSTERED ([id_materielType] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[MaterielType])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_MaterielType] ON;
        INSERT INTO [dbo].[tmp_ms_xx_MaterielType] ([id_materielType], [nom], [id_materielType_MaterielElement], [rank], [IsImportant])
        SELECT   [id_materielType],
                 [nom],
                 [id_materielType_MaterielElement],
                 [rank],
                 [IsImportant]
        FROM     [dbo].[MaterielType]
        ORDER BY [id_materielType] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_MaterielType] OFF;
    END

DROP TABLE [dbo].[MaterielType];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_MaterielType]', N'MaterielType';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK__Materiel__0BD866C0141FFE111]', N'PK__Materiel__0BD866C0141FFE11', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Début de la régénération de la table [dbo].[Query]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Query] (
    [id_query]                 INT            IDENTITY (1, 1) NOT NULL,
    [nom]                      NVARCHAR (MAX) NOT NULL,
    [id_query_queryGroup]      INT            NOT NULL,
    [id_query_queryDataSource] INT            CONSTRAINT [DF__Query__id_query___30592A6F] DEFAULT ((1)) NOT NULL,
    [results_ponderation]      INT            CONSTRAINT [DF_Query_results_ponderation] DEFAULT ((0)) NOT NULL,
    [id_query_rapport]         INT            NULL,
    [id_query_Imputabilite]    INT            NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK__Query__B9DB2786D170F93B1] PRIMARY KEY CLUSTERED ([id_query] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Query])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Query] ON;
        INSERT INTO [dbo].[tmp_ms_xx_Query] ([id_query], [nom], [id_query_queryGroup], [id_query_queryDataSource], [results_ponderation], [id_query_rapport], [id_query_Imputabilite])
        SELECT   [id_query],
                 [nom],
                 [id_query_queryGroup],
                 [id_query_queryDataSource],
                 [results_ponderation],
                 [id_query_rapport],
                 [id_query_Imputabilite]
        FROM     [dbo].[Query]
        ORDER BY [id_query] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Query] OFF;
    END

DROP TABLE [dbo].[Query];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Query]', N'Query';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK__Query__B9DB2786D170F93B1]', N'PK__Query__B9DB2786D170F93B', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[QueryCriteriaApplied]...';


GO
ALTER TABLE [dbo].[QueryCriteriaApplied]
    ADD [niveau] INT CONSTRAINT [DF_QueryCriteriaApplied_niveau] DEFAULT ((1)) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Début de la régénération de la table [dbo].[QueryGroup]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_QueryGroup] (
    [id_queryGroup] INT            IDENTITY (1, 1) NOT NULL,
    [id_reseau]     INT            NOT NULL,
    [nom]           NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK__QueryGro__C4436C33CD15E7501] PRIMARY KEY CLUSTERED ([id_queryGroup] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[QueryGroup])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_QueryGroup] ON;
        INSERT INTO [dbo].[tmp_ms_xx_QueryGroup] ([id_queryGroup], [id_reseau], [nom])
        SELECT   [id_queryGroup],
                 [id_reseau],
                 [nom]
        FROM     [dbo].[QueryGroup]
        ORDER BY [id_queryGroup] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_QueryGroup] OFF;
    END

DROP TABLE [dbo].[QueryGroup];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_QueryGroup]', N'QueryGroup';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK__QueryGro__C4436C33CD15E7501]', N'PK__QueryGro__C4436C33CD15E750', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Début de la régénération de la table [dbo].[QueryTriggerAsynchoneHistorique]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_QueryTriggerAsynchoneHistorique] (
    [id_queryDeclencheurAsynchoneHistorique]                           INT            IDENTITY (1, 1) NOT NULL,
    [uid]                                                              NVARCHAR (MAX) CONSTRAINT [DF_QueryTriggerAsynchoneHistorique_uid] DEFAULT ((0)) NOT NULL,
    [id_queryDeclencheurAsynchoneHistorique_queryDeclencheurAsynchone] INT            NULL,
    [date]                                                             DATE           NOT NULL,
    [heure]                                                            TIME (7)       NOT NULL,
    [id_queryDeclencheurAsynchoneHistorique_alea]                      INT            NOT NULL,
    [isManuel]                                                         INT            CONSTRAINT [DF_QueryTriggerAsynchoneHistorique_isManuel] DEFAULT ((0)) NOT NULL,
    [id_QueryTriggerAsynchrone_diffusionLists101m401]                  INT            NULL,
    [id_QueryTriggerAsynchrone_com]                                    INT            NULL,
    [message]                                                          NVARCHAR (MAX) CONSTRAINT [DF_QueryTriggerAsynchoneHistorique_message] DEFAULT ('') NOT NULL,
    [id_utilisateur]                                                   INT            NULL,
    [loginUtilisateur]                                                 NVARCHAR (MAX) NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_QueryDeclencheurAsynchoneHistorique1] PRIMARY KEY CLUSTERED ([id_queryDeclencheurAsynchoneHistorique] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[QueryTriggerAsynchoneHistorique])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_QueryTriggerAsynchoneHistorique] ON;
        INSERT INTO [dbo].[tmp_ms_xx_QueryTriggerAsynchoneHistorique] ([id_queryDeclencheurAsynchoneHistorique], [id_queryDeclencheurAsynchoneHistorique_queryDeclencheurAsynchone], [date], [heure], [id_queryDeclencheurAsynchoneHistorique_alea])
        SELECT   [id_queryDeclencheurAsynchoneHistorique],
                 [id_queryDeclencheurAsynchoneHistorique_queryDeclencheurAsynchone],
                 [date],
                 [heure],
                 [id_queryDeclencheurAsynchoneHistorique_alea]
        FROM     [dbo].[QueryTriggerAsynchoneHistorique]
        ORDER BY [id_queryDeclencheurAsynchoneHistorique] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_QueryTriggerAsynchoneHistorique] OFF;
    END

DROP TABLE [dbo].[QueryTriggerAsynchoneHistorique];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_QueryTriggerAsynchoneHistorique]', N'QueryTriggerAsynchoneHistorique';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_QueryDeclencheurAsynchoneHistorique1]', N'PK_QueryDeclencheurAsynchoneHistorique', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[Rules]...';


GO
ALTER TABLE [dbo].[Rules] ALTER COLUMN [nom] NVARCHAR (MAX) NOT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[CfgEvenementActionGroupRule]...';


GO
CREATE TABLE [dbo].[CfgEvenementActionGroupRule] (
    [id_cfgEvenementActionGroupRule]             INT IDENTITY (1, 1) NOT NULL,
    [id_cfgEvenementActionGroupRule_actionGroup] INT NOT NULL,
    [id_cfgEvenementActionGroupRule_rule]        INT NOT NULL,
    CONSTRAINT [PK_CfgEvenementActionGroupRule] PRIMARY KEY CLUSTERED ([id_cfgEvenementActionGroupRule] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[CfgEvenementEtatTransitionGroupPermission]...';


GO
CREATE TABLE [dbo].[CfgEvenementEtatTransitionGroupPermission] (
    [id_cfgEvenementEtatTransitionGroupPermission]                INT IDENTITY (1, 1) NOT NULL,
    [id_cfgEvenementEtatTransitionGroupPermission_groups101m401]  INT NOT NULL,
    [id_cfgEvenementEtatTransitionGroupPermission_permission]     INT NOT NULL,
    [id_cfgEvenementEtatTransitionGroupPermission_etatTransition] INT NOT NULL,
    CONSTRAINT [PK_CfgEvenementEtatTransitionGroupPermission] PRIMARY KEY CLUSTERED ([id_cfgEvenementEtatTransitionGroupPermission] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[CfgEvenementSousCategorieGroupPermission]...';


GO
CREATE TABLE [dbo].[CfgEvenementSousCategorieGroupPermission] (
    [id_cfgEvenementSousCategorieGroupPermission]                        INT IDENTITY (1, 1) NOT NULL,
    [id_cfgEvenementSousCategorieGroupPermission_groups101m401]          INT NOT NULL,
    [id_cfgEvenementSousCategorieGroupPermission_permission]             INT NOT NULL,
    [id_cfgEvenementSousCategorieGroupPermission_evenementSousCategorie] INT NOT NULL,
    CONSTRAINT [PK_CfgEvenementSousCategorieGroupPermission] PRIMARY KEY CLUSTERED ([id_cfgEvenementSousCategorieGroupPermission] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[CfgRessourceSousClasseGroupPermission]...';


GO
CREATE TABLE [dbo].[CfgRessourceSousClasseGroupPermission] (
    [id_cfgRessourceSousClasseGroupPermission]                     INT IDENTITY (1, 1) NOT NULL,
    [id_cfgRessourceSousClasseGroupPermission_groups101m401]       INT NOT NULL,
    [id_cfgRessourceSousClasseGroupPermission_permission]          INT NOT NULL,
    [id_cfgRessourceSousClasseGroupPermission_ressourceSousClasse] INT NOT NULL,
    CONSTRAINT [PK_CfgRessourceSousClasseGroupPermission] PRIMARY KEY CLUSTERED ([id_cfgRessourceSousClasseGroupPermission] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[ConstEvenementEtat]...';


GO
CREATE TABLE [dbo].[ConstEvenementEtat] (
    [id_constEvenementEtat] INT            IDENTITY (1, 1) NOT NULL,
    [code]                  NVARCHAR (MAX) NOT NULL,
    [libelle]               NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_ConstEvenementEtat] PRIMARY KEY CLUSTERED ([id_constEvenementEtat] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[ConstPermission]...';


GO
CREATE TABLE [dbo].[ConstPermission] (
    [id_constPermission] INT            IDENTITY (1, 1) NOT NULL,
    [code]               NVARCHAR (MAX) NOT NULL,
    [label]              NVARCHAR (MAX) NOT NULL,
    [rang]               INT            NOT NULL,
    CONSTRAINT [PK_ConstPermission] PRIMARY KEY CLUSTERED ([id_constPermission] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[EvenementAction]...';


GO
CREATE TABLE [dbo].[EvenementAction] (
    [id_evenementAction]                               INT            IDENTITY (1, 1) NOT NULL,
    [nom]                                              NVARCHAR (MAX) NOT NULL,
    [id_evenementAction_evenementActionGroup]          INT            NOT NULL,
    [id_evenementAction_evenementEtatAfter]            INT            NULL,
    [id_evenementAction_evenementFermetureRaisonAfter] INT            NULL,
    CONSTRAINT [PK_EvenementAction] PRIMARY KEY CLUSTERED ([id_evenementAction] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[EvenementActionGroup]...';


GO
CREATE TABLE [dbo].[EvenementActionGroup] (
    [id_evenementActionGroup] INT            IDENTITY (1, 1) NOT NULL,
    [nom]                     NVARCHAR (MAX) NOT NULL,
    [isGlobal]                INT            NOT NULL,
    CONSTRAINT [PK_EvenementActionGroup] PRIMARY KEY CLUSTERED ([id_evenementActionGroup] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[EvenementActionRealise]...';


GO
CREATE TABLE [dbo].[EvenementActionRealise] (
    [id_evenementActionRealise]                 INT            IDENTITY (1, 1) NOT NULL,
    [id_evenementActionRealise_evenementAction] INT            NOT NULL,
    [id_utilisateur]                            INT            NOT NULL,
    [loginUtilisateur]                          NVARCHAR (MAX) NOT NULL,
    [date]                                      DATE           NOT NULL,
    [heure]                                     TIME (7)       NOT NULL,
    [id_evenementActionRealise_evenementListe]  INT            NOT NULL,
    CONSTRAINT [PK_EvenementActionRealise] PRIMARY KEY CLUSTERED ([id_evenementActionRealise] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[EvenementEtat]...';


GO
CREATE TABLE [dbo].[EvenementEtat] (
    [id_evenementEtat]                                    INT            IDENTITY (1, 1) NOT NULL,
    [nom]                                                 NVARCHAR (MAX) NOT NULL,
    [rank]                                                INT            NOT NULL,
    [id_evenementEtat_constEvenementEtat]                 INT            NOT NULL,
    [id_evenementEtat_evenementActionHistorique_defaut]   INT            NOT NULL,
    [id_evenementEtat_evenementActionHistorique_onChange] INT            NOT NULL,
    CONSTRAINT [PK__AleaEtat__44F9971D08A9FF67] PRIMARY KEY CLUSTERED ([id_evenementEtat] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[EvenementEtatTransition]...';


GO
CREATE TABLE [dbo].[EvenementEtatTransition] (
    [id_evenementEtatTransition]                 INT IDENTITY (1, 1) NOT NULL,
    [id_evenementEtatTransition_etatOriginal]    INT NOT NULL,
    [id_evenementEtatTransition_etatDestination] INT NOT NULL,
    [id_evenementEtatTransition_reseaus101m401]  INT NOT NULL,
    [withRaison]                                 INT NOT NULL,
    [isPublic]                                   INT NOT NULL,
    CONSTRAINT [PK_EvenementEtatTransition] PRIMARY KEY CLUSTERED ([id_evenementEtatTransition] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[QueryTriggerAsynchoneAccuse]...';


GO
CREATE TABLE [dbo].[QueryTriggerAsynchoneAccuse] (
    [id_queryTriggerAsynchoneAccuse]                                 INT            IDENTITY (1, 1) NOT NULL,
    [uid]                                                            NVARCHAR (MAX) NOT NULL,
    [id_queryTriggerAsynchoneAccuse_QueryTriggerAsynchoneHistorique] INT            NOT NULL,
    [id_queryTriggerAsynchoneAccuse_diffusionReciptient101m401]      INT            NULL,
    [email]                                                          NVARCHAR (MAX) NULL,
    [nom]                                                            NVARCHAR (MAX) NULL,
    [prenom]                                                         NVARCHAR (MAX) NULL,
    [date]                                                           DATE           NOT NULL,
    [heure]                                                          TIME (7)       NOT NULL,
    [count]                                                          INT            NOT NULL,
    CONSTRAINT [PK_QueryTriggerAsynchoneAccuse] PRIMARY KEY CLUSTERED ([id_queryTriggerAsynchoneAccuse] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[RessourceCategorieGroupe]...';


GO
CREATE TABLE [dbo].[RessourceCategorieGroupe] (
    [id_ressourceCategorieGroupe] INT            IDENTITY (1, 1) NOT NULL,
    [nom]                         NVARCHAR (MAX) NOT NULL,
    [id_reseau]                   INT            NOT NULL,
    CONSTRAINT [PK_RessourceCategorieGroupe] PRIMARY KEY CLUSTERED ([id_ressourceCategorieGroupe] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_MaterielListe_code]...';


GO
ALTER TABLE [dbo].[MaterielListe]
    ADD CONSTRAINT [DF_MaterielListe_code] DEFAULT (N'A MAJ') FOR [code];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_MaterielListe_nomLong]...';


GO
ALTER TABLE [dbo].[MaterielListe]
    ADD CONSTRAINT [DF_MaterielListe_nomLong] DEFAULT (N'A MAJ') FOR [nomLong];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementActionGroup_isGlobal]...';


GO
ALTER TABLE [dbo].[EvenementActionGroup]
    ADD CONSTRAINT [DF_EvenementActionGroup_isGlobal] DEFAULT ((1)) FOR [isGlobal];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementEtat_rank]...';


GO
ALTER TABLE [dbo].[EvenementEtat]
    ADD CONSTRAINT [DF_EvenementEtat_rank] DEFAULT ((1)) FOR [rank];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementEtatTransition_isPublic]...';


GO
ALTER TABLE [dbo].[EvenementEtatTransition]
    ADD CONSTRAINT [DF_EvenementEtatTransition_isPublic] DEFAULT ((1)) FOR [isPublic];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_EvenementEtatTransition_withRaison]...';


GO
ALTER TABLE [dbo].[EvenementEtatTransition]
    ADD CONSTRAINT [DF_EvenementEtatTransition_withRaison] DEFAULT ((0)) FOR [withRaison];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[DF_RessourceCategorieGroupe_id_reseau]...';


GO
ALTER TABLE [dbo].[RessourceCategorieGroupe]
    ADD CONSTRAINT [DF_RessourceCategorieGroupe_id_reseau] DEFAULT ((1)) FOR [id_reseau];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_DommageConseq_dommagePrecision]...';


GO
ALTER TABLE [dbo].[EvenementDommage] WITH NOCHECK
    ADD CONSTRAINT [FK_DommageConseq_dommagePrecision] FOREIGN KEY ([id_dommageConseq_dommagePrecision]) REFERENCES [dbo].[EvenementDommagePrecision] ([id_evenementDommagePrecision]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementDommagePrecision_DommageType]...';


GO
ALTER TABLE [dbo].[EvenementDommagePrecision] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementDommagePrecision_DommageType] FOREIGN KEY ([id_evenementDommagePrecision_dommageType]) REFERENCES [dbo].[EvenementDommageType] ([id_dommageType]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_Intervenant_IntervenantType]...';


GO
ALTER TABLE [dbo].[EvenementIntervenant] WITH NOCHECK
    ADD CONSTRAINT [FK_Intervenant_IntervenantType] FOREIGN KEY ([id_intervenant_intervenantType]) REFERENCES [dbo].[EvenementIntervenantType] ([id_intervenantType]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_InterventionConseq_Intervenants]...';


GO
ALTER TABLE [dbo].[EvenementIntervention] WITH NOCHECK
    ADD CONSTRAINT [FK_InterventionConseq_Intervenants] FOREIGN KEY ([id_interventionConseq_intervenant]) REFERENCES [dbo].[EvenementIntervenant] ([id_intervenant]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_InterventionConseq_InterventionEtat]...';


GO
ALTER TABLE [dbo].[EvenementIntervention] WITH NOCHECK
    ADD CONSTRAINT [FK_InterventionConseq_InterventionEtat] FOREIGN KEY ([id_interventionConseq_interventionEtat]) REFERENCES [dbo].[EvenementInterventionActivite] ([id_interventionEtat]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementIntervention_EvenementList]...';


GO
ALTER TABLE [dbo].[EvenementIntervention] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementIntervention_EvenementList] FOREIGN KEY ([id_evenementIntervention_evenementList]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementIntervention_Ressource1]...';


GO
ALTER TABLE [dbo].[EvenementIntervention] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementIntervention_Ressource1] FOREIGN KEY ([id_evenementIntervention_ressource1]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementIntervention_Ressource2]...';


GO
ALTER TABLE [dbo].[EvenementIntervention] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementIntervention_Ressource2] FOREIGN KEY ([id_evenementIntervention_ressource2]) REFERENCES [dbo].[MaterielListe] ([id_materielListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_QueryTriggerAsynchoneHistorique_Alea]...';


GO
ALTER TABLE [dbo].[QueryTriggerAsynchoneHistorique] WITH NOCHECK
    ADD CONSTRAINT [FK_QueryTriggerAsynchoneHistorique_Alea] FOREIGN KEY ([id_queryDeclencheurAsynchoneHistorique_alea]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_QueryTriggerAsynchoneHistorique_QueryTriggerAsynchone]...';


GO
ALTER TABLE [dbo].[QueryTriggerAsynchoneHistorique] WITH NOCHECK
    ADD CONSTRAINT [FK_QueryTriggerAsynchoneHistorique_QueryTriggerAsynchone] FOREIGN KEY ([id_queryDeclencheurAsynchoneHistorique_queryDeclencheurAsynchone]) REFERENCES [dbo].[QueryTriggerAsynchrone] ([id_QueryTriggerAsynchrone]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_QueryTriggerAsynchoneHistorique_COM]...';


GO
ALTER TABLE [dbo].[QueryTriggerAsynchoneHistorique] WITH NOCHECK
    ADD CONSTRAINT [FK_QueryTriggerAsynchoneHistorique_COM] FOREIGN KEY ([id_QueryTriggerAsynchrone_com]) REFERENCES [dbo].[COM] ([id_aleaCOM]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_ConfigurationMainCourante_AleaState]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_ConfigurationMainCourante_AleaState] FOREIGN KEY ([id_configurationMainCourante_evenementEtat]) REFERENCES [dbo].[EvenementEtat] ([id_evenementEtat]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_AleaType_AleaElement]...';


GO
ALTER TABLE [dbo].[AleaType] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaType_AleaElement] FOREIGN KEY ([id_aleaType_aleaElement]) REFERENCES [dbo].[AleaElement] ([id_aleaElement]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_AlerteList_AleaType]...';


GO
ALTER TABLE [dbo].[CfgAlerteAleaDiffusionListReseau] WITH NOCHECK
    ADD CONSTRAINT [FK_AlerteList_AleaType] FOREIGN KEY ([id_cfgAlerteAleaGroupReseau_aleaType]) REFERENCES [dbo].[AleaType] ([id_aleaType]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_AleaListe_AleaType]...';


GO
ALTER TABLE [dbo].[AleaListe] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaListe_AleaType] FOREIGN KEY ([id_aleaListe_aleaType]) REFERENCES [dbo].[AleaType] ([id_aleaType]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_AleaType_EvenementFamille]...';


GO
ALTER TABLE [dbo].[AleaType] WITH NOCHECK
    ADD CONSTRAINT [FK_AleaType_EvenementFamille] FOREIGN KEY ([id_aleaType_evenementFamille]) REFERENCES [dbo].[ConstEvenementFamille] ([id_constEvenementFamille]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgQueryDataSourceQueryCriteria_QueryDataSource]...';


GO
ALTER TABLE [dbo].[CfgQueryDataSourceQueryCriteria] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgQueryDataSourceQueryCriteria_QueryDataSource] FOREIGN KEY ([id_cfgQueryDataSourceQueryCriteria_queryDataSource]) REFERENCES [dbo].[ConstQueryDataSource] ([id_queryDataSource]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_Query_QueryDataSource]...';


GO
ALTER TABLE [dbo].[Query] WITH NOCHECK
    ADD CONSTRAINT [FK_Query_QueryDataSource] FOREIGN KEY ([id_query_queryDataSource]) REFERENCES [dbo].[ConstQueryDataSource] ([id_queryDataSource]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgGeneralMainCourante_RessourceClasse]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgGeneralMainCourante_RessourceClasse] FOREIGN KEY ([id_ressourceClasseRH_Imports2006m1055]) REFERENCES [dbo].[MaterielElement] ([id_materielElement]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgMaterielElementMaterielEtat_MaterielElement]...';


GO
ALTER TABLE [dbo].[CfgMaterielElementMaterielEtat] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgMaterielElementMaterielEtat_MaterielElement] FOREIGN KEY ([id_CfgMaterielElementMaterielEtat_MaterielElement]) REFERENCES [dbo].[MaterielElement] ([id_materielElement]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_MaterielType_MaterielElement]...';


GO
ALTER TABLE [dbo].[MaterielType] WITH NOCHECK
    ADD CONSTRAINT [FK_MaterielType_MaterielElement] FOREIGN KEY ([id_materielType_MaterielElement]) REFERENCES [dbo].[MaterielElement] ([id_materielElement]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgGeneralMainCourante_Etat1]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgGeneralMainCourante_Etat1] FOREIGN KEY ([id_ressourceEtatDefaut_Imports2006m1055]) REFERENCES [dbo].[MaterielEtat] ([id_materielEtat]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgGeneralMainCourante_Etat2]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgGeneralMainCourante_Etat2] FOREIGN KEY ([id_ressourceEtatExploitation_Imports2006m1055]) REFERENCES [dbo].[MaterielEtat] ([id_materielEtat]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgGeneralMainCourante_Etat3]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgGeneralMainCourante_Etat3] FOREIGN KEY ([id_ressourceEtatReserve_Imports2006m1055]) REFERENCES [dbo].[MaterielEtat] ([id_materielEtat]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgMaterielElementMaterielEtat_MaterielEtat]...';


GO
ALTER TABLE [dbo].[CfgMaterielElementMaterielEtat] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgMaterielElementMaterielEtat_MaterielEtat] FOREIGN KEY ([id_CfgMaterielElementMaterielEtat_MaterielEtat]) REFERENCES [dbo].[MaterielEtat] ([id_materielEtat]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgMaterielEtatColor_MaterielEtat]...';


GO
ALTER TABLE [dbo].[CfgMaterielEtatColor] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgMaterielEtatColor_MaterielEtat] FOREIGN KEY ([id_CfgMaterielEtatColor_MaterielEtat]) REFERENCES [dbo].[MaterielEtat] ([id_materielEtat]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_MaterielEtatHistorique_MaterielEtat]...';


GO
ALTER TABLE [dbo].[MaterielEtatHistorique] WITH NOCHECK
    ADD CONSTRAINT [FK_MaterielEtatHistorique_MaterielEtat] FOREIGN KEY ([id_materielEtatHistorique_materielEtat]) REFERENCES [dbo].[MaterielEtat] ([id_materielEtat]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_MaterielListe_MaterielEtat]...';


GO
ALTER TABLE [dbo].[MaterielListe] WITH NOCHECK
    ADD CONSTRAINT [FK_MaterielListe_MaterielEtat] FOREIGN KEY ([id_materielList_materielEtat]) REFERENCES [dbo].[MaterielEtat] ([id_materielEtat]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_MaterielListe_MaterielType]...';


GO
ALTER TABLE [dbo].[MaterielListe] WITH NOCHECK
    ADD CONSTRAINT [FK_MaterielListe_MaterielType] FOREIGN KEY ([id_materielList_materielType]) REFERENCES [dbo].[MaterielType] ([id_materielType]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_Analyse_Query]...';


GO
ALTER TABLE [dbo].[Analyse] WITH NOCHECK
    ADD CONSTRAINT [FK_Analyse_Query] FOREIGN KEY ([id_analyse_query]) REFERENCES [dbo].[Query] ([id_query]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_LnkQueryEvenementAttribut_Query]...';


GO
ALTER TABLE [dbo].[LnkQueryEvenementAttribut] WITH NOCHECK
    ADD CONSTRAINT [FK_LnkQueryEvenementAttribut_Query] FOREIGN KEY ([id_lnkQueryEvenementAttribut_Query]) REFERENCES [dbo].[Query] ([id_query]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_LnkQueryEvenementSection_Query]...';


GO
ALTER TABLE [dbo].[LnkQueryEvenementSection] WITH NOCHECK
    ADD CONSTRAINT [FK_LnkQueryEvenementSection_Query] FOREIGN KEY ([id_lnkQueryEvenementSection_Query]) REFERENCES [dbo].[Query] ([id_query]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_LnkQueryQueryCriteria_Query]...';


GO
ALTER TABLE [dbo].[QueryCriteriaApplied] WITH NOCHECK
    ADD CONSTRAINT [FK_LnkQueryQueryCriteria_Query] FOREIGN KEY ([id_queryCriteriaApplied_query]) REFERENCES [dbo].[Query] ([id_query]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_QueryTriggerAsynchrone_Query]...';


GO
ALTER TABLE [dbo].[QueryTriggerAsynchrone] WITH NOCHECK
    ADD CONSTRAINT [FK_QueryTriggerAsynchrone_Query] FOREIGN KEY ([id_QueryTriggerAsynchrone_query]) REFERENCES [dbo].[Query] ([id_query]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_QueryTriggerSynchrone_Query]...';


GO
ALTER TABLE [dbo].[QueryTriggerSynchrone] WITH NOCHECK
    ADD CONSTRAINT [FK_QueryTriggerSynchrone_Query] FOREIGN KEY ([id_queryTriggerSynchrone_query]) REFERENCES [dbo].[Query] ([id_query]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_Query_QueryGroup]...';


GO
ALTER TABLE [dbo].[Query] WITH NOCHECK
    ADD CONSTRAINT [FK_Query_QueryGroup] FOREIGN KEY ([id_query_queryGroup]) REFERENCES [dbo].[QueryGroup] ([id_queryGroup]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_Query_Rapport]...';


GO
ALTER TABLE [dbo].[Query] WITH NOCHECK
    ADD CONSTRAINT [FK_Query_Rapport] FOREIGN KEY ([id_query_rapport]) REFERENCES [dbo].[Rapport] ([id_rapport]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_Query_Imputabilite]...';


GO
ALTER TABLE [dbo].[Query] WITH NOCHECK
    ADD CONSTRAINT [FK_Query_Imputabilite] FOREIGN KEY ([id_query_Imputabilite]) REFERENCES [dbo].[EvenementImputabilite] ([id_evenementImputabilite]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgEvenementActionGroupRule_EvenementActionGroup]...';


GO
ALTER TABLE [dbo].[CfgEvenementActionGroupRule] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementActionGroupRule_EvenementActionGroup] FOREIGN KEY ([id_cfgEvenementActionGroupRule_actionGroup]) REFERENCES [dbo].[EvenementActionGroup] ([id_evenementActionGroup]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgEvenementActionGroupRule_Rule]...';


GO
ALTER TABLE [dbo].[CfgEvenementActionGroupRule] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementActionGroupRule_Rule] FOREIGN KEY ([id_cfgEvenementActionGroupRule_rule]) REFERENCES [dbo].[Rules] ([id_rules]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgEvenementEtatTransitionGroupPermission_EtatTransition]...';


GO
ALTER TABLE [dbo].[CfgEvenementEtatTransitionGroupPermission] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementEtatTransitionGroupPermission_EtatTransition] FOREIGN KEY ([id_cfgEvenementEtatTransitionGroupPermission_etatTransition]) REFERENCES [dbo].[EvenementEtatTransition] ([id_evenementEtatTransition]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgEvenementEtatTransitionGroupPermission_Permission]...';


GO
ALTER TABLE [dbo].[CfgEvenementEtatTransitionGroupPermission] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementEtatTransitionGroupPermission_Permission] FOREIGN KEY ([id_cfgEvenementEtatTransitionGroupPermission_permission]) REFERENCES [dbo].[ConstPermission] ([id_constPermission]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgEvenementSousCategorieGroupPermission_EvenementSousCategorie]...';


GO
ALTER TABLE [dbo].[CfgEvenementSousCategorieGroupPermission] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementSousCategorieGroupPermission_EvenementSousCategorie] FOREIGN KEY ([id_cfgEvenementSousCategorieGroupPermission_evenementSousCategorie]) REFERENCES [dbo].[AleaType] ([id_aleaType]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgEvenementSousCategorieGroupPermission_Permission]...';


GO
ALTER TABLE [dbo].[CfgEvenementSousCategorieGroupPermission] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementSousCategorieGroupPermission_Permission] FOREIGN KEY ([id_cfgEvenementSousCategorieGroupPermission_permission]) REFERENCES [dbo].[ConstPermission] ([id_constPermission]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgRessourceSousClasseGroupPermission_Permission]...';


GO
ALTER TABLE [dbo].[CfgRessourceSousClasseGroupPermission] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgRessourceSousClasseGroupPermission_Permission] FOREIGN KEY ([id_cfgRessourceSousClasseGroupPermission_permission]) REFERENCES [dbo].[ConstPermission] ([id_constPermission]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgRessourceSousClasseGroupPermission_RessourceSousClasse]...';


GO
ALTER TABLE [dbo].[CfgRessourceSousClasseGroupPermission] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgRessourceSousClasseGroupPermission_RessourceSousClasse] FOREIGN KEY ([id_cfgRessourceSousClasseGroupPermission_ressourceSousClasse]) REFERENCES [dbo].[MaterielType] ([id_materielType]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementAction_Etat]...';


GO
ALTER TABLE [dbo].[EvenementAction] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementAction_Etat] FOREIGN KEY ([id_evenementAction_evenementEtatAfter]) REFERENCES [dbo].[EvenementEtat] ([id_evenementEtat]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementAction_EvenementActionGroup]...';


GO
ALTER TABLE [dbo].[EvenementAction] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementAction_EvenementActionGroup] FOREIGN KEY ([id_evenementAction_evenementActionGroup]) REFERENCES [dbo].[EvenementActionGroup] ([id_evenementActionGroup]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementAction_FermetureRaison]...';


GO
ALTER TABLE [dbo].[EvenementAction] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementAction_FermetureRaison] FOREIGN KEY ([id_evenementAction_evenementFermetureRaisonAfter]) REFERENCES [dbo].[EvenementFermetureRaison] ([id_evenementFermetureRaison]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementActionRealise_EvenementAction]...';


GO
ALTER TABLE [dbo].[EvenementActionRealise] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementActionRealise_EvenementAction] FOREIGN KEY ([id_evenementActionRealise_evenementAction]) REFERENCES [dbo].[EvenementAction] ([id_evenementAction]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementActionRealise_EvenementListe]...';


GO
ALTER TABLE [dbo].[EvenementActionRealise] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementActionRealise_EvenementListe] FOREIGN KEY ([id_evenementActionRealise_evenementListe]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementEtat_ActionHistoDefaut]...';


GO
ALTER TABLE [dbo].[EvenementEtat] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementEtat_ActionHistoDefaut] FOREIGN KEY ([id_evenementEtat_evenementActionHistorique_defaut]) REFERENCES [dbo].[AleaAction] ([id_aleaAction]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementEtat_ActionHistoOnChange]...';


GO
ALTER TABLE [dbo].[EvenementEtat] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementEtat_ActionHistoOnChange] FOREIGN KEY ([id_evenementEtat_evenementActionHistorique_onChange]) REFERENCES [dbo].[AleaAction] ([id_aleaAction]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementEtat_constEvenementEtat]...';


GO
ALTER TABLE [dbo].[EvenementEtat] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementEtat_constEvenementEtat] FOREIGN KEY ([id_evenementEtat_constEvenementEtat]) REFERENCES [dbo].[ConstEvenementEtat] ([id_constEvenementEtat]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementEtatTransition_EvenementEtatDest]...';


GO
ALTER TABLE [dbo].[EvenementEtatTransition] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementEtatTransition_EvenementEtatDest] FOREIGN KEY ([id_evenementEtatTransition_etatDestination]) REFERENCES [dbo].[EvenementEtat] ([id_evenementEtat]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_EvenementEtatTransition_EvenementEtatOrg]...';


GO
ALTER TABLE [dbo].[EvenementEtatTransition] WITH NOCHECK
    ADD CONSTRAINT [FK_EvenementEtatTransition_EvenementEtatOrg] FOREIGN KEY ([id_evenementEtatTransition_etatOriginal]) REFERENCES [dbo].[EvenementEtat] ([id_evenementEtat]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_QueryTriggerAsynchoneAccuse_QueryTriggerAsynchoneHistorique]...';


GO
ALTER TABLE [dbo].[QueryTriggerAsynchoneAccuse] WITH NOCHECK
    ADD CONSTRAINT [FK_QueryTriggerAsynchoneAccuse_QueryTriggerAsynchoneHistorique] FOREIGN KEY ([id_queryTriggerAsynchoneAccuse_QueryTriggerAsynchoneHistorique]) REFERENCES [dbo].[QueryTriggerAsynchoneHistorique] ([id_queryDeclencheurAsynchoneHistorique]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgEvenementMaintenanceEmplacementRessource_RessourceCategorieGroupe]...';


GO
ALTER TABLE [dbo].[CfgEvenementMaintenanceEmplacementRessource] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementMaintenanceEmplacementRessource_RessourceCategorieGroupe] FOREIGN KEY ([id_cfgEvenementMaintenanceEmplacementRessource_ressourceCategorieGroupe]) REFERENCES [dbo].[RessourceCategorieGroupe] ([id_ressourceCategorieGroupe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgEvenementMaintenanceOrganeRessource_RessourceCategorieGroupe]...';


GO
ALTER TABLE [dbo].[CfgEvenementMaintenanceOrganeRessource] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgEvenementMaintenanceOrganeRessource_RessourceCategorieGroupe] FOREIGN KEY ([id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe]) REFERENCES [dbo].[RessourceCategorieGroupe] ([id_ressourceCategorieGroupe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgGeneralMainCourante_CategorieClient]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgGeneralMainCourante_CategorieClient] FOREIGN KEY ([id_ressourceCategorieClient_Imports2006m1055]) REFERENCES [dbo].[MaterielDetailCategorie] ([id_materielDetailCategorie]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgGeneralMainCourante_ClientClasse]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgGeneralMainCourante_ClientClasse] FOREIGN KEY ([id_ressourceClasseClient_Imports2006m1055]) REFERENCES [dbo].[MaterielElement] ([id_materielElement]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_CfgGeneralMainCourante_SousClasseClient]...';


GO
ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgGeneralMainCourante_SousClasseClient] FOREIGN KEY ([id_ressourceSousClasseClient_Imports2006m1055]) REFERENCES [dbo].[MaterielType] ([id_materielType]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[FK_MaterielDetailCategorie_RessourceCategorieGroupe]...';


GO
ALTER TABLE [dbo].[MaterielDetailCategorie] WITH NOCHECK
    ADD CONSTRAINT [FK_MaterielDetailCategorie_RessourceCategorieGroupe] FOREIGN KEY ([id_materielDetailCategorie_ressourceCategorieGroupe]) REFERENCES [dbo].[RessourceCategorieGroupe] ([id_ressourceCategorieGroupe]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[VueAleaElementAleaType]...';


GO
ALTER VIEW dbo.VueAleaElementAleaType
AS
SELECT        dbo.AleaElement.id_aleaElement, dbo.AleaElement.nom AS AleaElementNom, dbo.AleaType.id_aleaType, dbo.AleaType.nom AS AleaTypeNom, dbo.AleaType.isPublic
FROM            dbo.AleaElement INNER JOIN
                         dbo.AleaType ON dbo.AleaElement.id_aleaElement = dbo.AleaType.id_aleaType_aleaElement
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[VueAleaListe]...';


GO
ALTER VIEW dbo.VueAleaListe
AS
SELECT        dbo.AleaListe.id_aleaListe, dbo.AleaListe.timeStamp, dbo.AleaListe.comentaires, dbo.AleaListe.dateApparition, dbo.AleaListe.heureApparition, dbo.AleaListe.heureFin, dbo.AleaType.id_aleaType, 
                         dbo.AleaType.nom AS [AleaType nom], dbo.AleaListe.indentifiant, dbo.AleaListe.longitude_saisie, dbo.AleaListe.latitude_saisie, dbo.AleaListe.lastModification, dbo.AleaListe.heureAppel, dbo.AleaListe.dateFin, 
                         dbo.AleaListe.id_aleaListe_FermetureRaison, dbo.AleaListe.id_aleaListe_Imputabilite, dbo.EvenementEtat.id_evenementEtat, dbo.EvenementEtat.nom, dbo.AleaType.isPublic
FROM            dbo.AleaListe INNER JOIN
                         dbo.AleaType ON dbo.AleaListe.id_aleaListe_aleaType = dbo.AleaType.id_aleaType INNER JOIN
                         dbo.EvenementEtat ON dbo.AleaListe.id_aleaListe_aleaEtat = dbo.EvenementEtat.id_evenementEtat
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[VueAleaListeMateriel]...';


GO
ALTER VIEW dbo.VueAleaListeMateriel
AS
SELECT        dbo.AleaListe.id_aleaListe, dbo.AleaListe.timeStamp, dbo.AleaListe.comentaires, dbo.AleaListe.dateApparition, dbo.AleaListe.heureApparition, dbo.AleaListe.heureFin, dbo.AleaType.id_aleaType, 
                         dbo.AleaType.nom AS [AleaType nom], dbo.MaterielListe.id_materielListe, dbo.MaterielListe.id_reseau, dbo.AleaListe.indentifiant, dbo.AleaListe.ponderation, dbo.AleaListe.id_aleaListe_ressourceRemplacement, 
                         dbo.AleaListe.longitude_saisie, dbo.AleaListe.latitude_saisie, dbo.AleaListe.lastModification, dbo.AleaListe.dateFin, dbo.AleaListe.heureAppel, dbo.AleaListe.id_aleaListe_FermetureRaison, 
                         dbo.AleaListe.id_aleaListe_Imputabilite, dbo.EvenementEtat.id_evenementEtat, dbo.EvenementEtat.nom, dbo.AleaType.isPublic
FROM            dbo.AleaListe INNER JOIN
                         dbo.AleaType ON dbo.AleaListe.id_aleaListe_aleaType = dbo.AleaType.id_aleaType INNER JOIN
                         dbo.LnkMaterielListeAleaListe ON dbo.AleaListe.id_aleaListe = dbo.LnkMaterielListeAleaListe.id_materielListeAleaListe_aleaListe INNER JOIN
                         dbo.MaterielListe ON dbo.LnkMaterielListeAleaListe.id_materielListeAleaListe_materielList = dbo.MaterielListe.id_materielListe INNER JOIN
                         dbo.EvenementEtat ON dbo.AleaListe.id_aleaListe_aleaEtat = dbo.EvenementEtat.id_evenementEtat
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[VueMaterielListe]...';


GO
ALTER VIEW dbo.VueMaterielListe
AS
SELECT        dbo.MaterielListe.id_materielListe, dbo.MaterielListe.id_reseau, dbo.MaterielDetailCategorie.nom AS MaterielDetailCategorieNom, dbo.MaterielType.id_materielType, dbo.MaterielType.nom AS MaterielTypeNom, 
                         dbo.MaterielElement.id_materielElement, dbo.MaterielElement.nom AS MaterielElementNom, dbo.ConstRessourcesFamille.id_constRessourceFamille, dbo.ConstRessourcesFamille.code AS ConstRessourcesFamilleCode, 
                         dbo.ConstRessourcesFamille.libelle, dbo.MaterielListe.id_materielList_materielEtat, dbo.MaterielListe.deleted, dbo.MaterielListe.lastModification, dbo.MaterielListe.code, dbo.MaterielListe.nomLong, 
                         dbo.MaterielListe.id_materielList_materielType, dbo.MaterielListe.id_materielListe_materielCategorie, dbo.MaterielListe.id_materielList_constRessourceFamille, dbo.MaterielType.isPublic
FROM            dbo.MaterielListe LEFT OUTER JOIN
                         dbo.ConstRessourcesFamille ON dbo.MaterielListe.id_materielList_constRessourceFamille = dbo.ConstRessourcesFamille.id_constRessourceFamille LEFT OUTER JOIN
                         dbo.MaterielDetailCategorie ON dbo.MaterielDetailCategorie.id_materielDetailCategorie = dbo.MaterielListe.id_materielListe_materielCategorie LEFT OUTER JOIN
                         dbo.MaterielType ON dbo.MaterielListe.id_materielList_materielType = dbo.MaterielType.id_materielType LEFT OUTER JOIN
                         dbo.MaterielElement ON dbo.MaterielType.id_materielType_MaterielElement = dbo.MaterielElement.id_materielElement
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[VueCfgMaterielElementMaterielEtat]...';


GO
ALTER VIEW dbo.VueCfgMaterielElementMaterielEtat
AS
SELECT        dbo.MaterielElement.id_materielElement, dbo.MaterielElement.nom AS MaterielElementNom, dbo.MaterielEtat.id_materielEtat, dbo.MaterielEtat.nom AS MaterielEtatNom, 
                         dbo.CfgMaterielElementMaterielEtat.id_CfgMaterielElementMaterielEtat_Reseau, dbo.CfgMaterielEtatColor.color, dbo.CfgMaterielElementMaterielEtat.isDefaut, dbo.MaterielEtat.rang
FROM            dbo.MaterielElement INNER JOIN
                         dbo.CfgMaterielElementMaterielEtat ON dbo.MaterielElement.id_materielElement = dbo.CfgMaterielElementMaterielEtat.id_CfgMaterielElementMaterielEtat_MaterielElement INNER JOIN
                         dbo.MaterielEtat ON dbo.CfgMaterielElementMaterielEtat.id_CfgMaterielElementMaterielEtat_MaterielEtat = dbo.MaterielEtat.id_materielEtat INNER JOIN
                         dbo.CfgMaterielEtatColor ON dbo.MaterielEtat.id_materielEtat = dbo.CfgMaterielEtatColor.id_CfgMaterielEtatColor_MaterielEtat
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualisation de [dbo].[VueMaterielElementMaterielType]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[VueMaterielElementMaterielType]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[VueMaterielEtatHistorique]...';


GO
ALTER VIEW dbo.VueMaterielEtatHistorique
AS
SELECT        dbo.MaterielEtatHistorique.id_materielEtatHistorique, dbo.MaterielEtatHistorique.dateSaisie, dbo.MaterielEtatHistorique.heureSaisie, dbo.MaterielEtatHistorique.id_utilisateur, dbo.MaterielEtatHistorique.loginUtilisateur, 
                         dbo.MaterielEtat.id_materielEtat, dbo.MaterielEtat.nom AS EtatNom, dbo.MaterielListe.id_materielListe, dbo.MaterielListe.id_reseau, dbo.MaterielEtat.rang
FROM            dbo.MaterielEtatHistorique INNER JOIN
                         dbo.MaterielEtat ON dbo.MaterielEtatHistorique.id_materielEtatHistorique_materielEtat = dbo.MaterielEtat.id_materielEtat INNER JOIN
                         dbo.LnkMaterielListMaterielEtatHistorique ON dbo.MaterielEtatHistorique.id_materielEtatHistorique = dbo.LnkMaterielListMaterielEtatHistorique.id_materielListMaterielEtatHistorique_materielEtatHistorique INNER JOIN
                         dbo.MaterielListe ON dbo.LnkMaterielListMaterielEtatHistorique.id_materielListMaterielEtatHistorique_materielListe = dbo.MaterielListe.id_materielListe
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualisation de [dbo].[VueAleaActionHistoriqueMateriel]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[VueAleaActionHistoriqueMateriel]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[VueMaterielListeCountAleaEtat]...';


GO
ALTER VIEW dbo.VueMaterielListeCountAleaEtat
AS
SELECT        dbo.MaterielListe.id_materielListe, dbo.MaterielListe.id_reseau, dbo.AleaListe.dateApparition, dbo.AleaListe.indentifiant, dbo.EvenementEtat.id_evenementEtat, dbo.EvenementEtat.nom AS Expr1
FROM            dbo.MaterielListe INNER JOIN
                         dbo.LnkMaterielListeAleaListe ON dbo.MaterielListe.id_materielListe = dbo.LnkMaterielListeAleaListe.id_materielListeAleaListe_materielList INNER JOIN
                         dbo.AleaListe ON dbo.LnkMaterielListeAleaListe.id_materielListeAleaListe_aleaListe = dbo.AleaListe.id_aleaListe INNER JOIN
                         dbo.EvenementEtat ON dbo.AleaListe.id_aleaListe_aleaEtat = dbo.EvenementEtat.id_evenementEtat
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[VueAleaListe].[MS_DiagramPane1]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[35] 4[26] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "AleaListe"
            Begin Extent = 
               Top = 9
               Left = 673
               Bottom = 292
               Right = 929
            End
            DisplayFlags = 280
            TopColumn = 9
         End
         Begin Table = "AleaType"
            Begin Extent = 
               Top = 104
               Left = 33
               Bottom = 217
               Right = 253
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "EvenementEtat"
            Begin Extent = 
               Top = 172
               Left = 329
               Bottom = 285
               Right = 625
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueAleaListe';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[VueAleaListeMateriel].[MS_DiagramPane1]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "AleaListe"
            Begin Extent = 
               Top = 9
               Left = 682
               Bottom = 344
               Right = 938
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "AleaType"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 251
               Right = 258
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "LnkMaterielListeAleaListe"
            Begin Extent = 
               Top = 252
               Left = 38
               Bottom = 365
               Right = 322
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterielListe"
            Begin Extent = 
               Top = 366
               Left = 38
               Bottom = 496
               Right = 281
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "EvenementEtat"
            Begin Extent = 
               Top = 180
               Left = 1124
               Bottom = 293
               Right = 1420
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
 ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueAleaListeMateriel';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[VueAleaListeMateriel].[MS_DiagramPane2]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane2', @value = N'        Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueAleaListeMateriel';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[VueCfgMaterielElementMaterielEtat].[MS_DiagramPane1]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "MaterielElement"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 228
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CfgMaterielElementMaterielEtat"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 401
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "MaterielEtat"
            Begin Extent = 
               Top = 6
               Left = 266
               Bottom = 121
               Right = 436
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CfgMaterielEtatColor"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 321
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueCfgMaterielElementMaterielEtat';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[VueMaterielListe].[MS_DiagramPane1]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[61] 4[1] 2[31] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "MaterielListe"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 252
               Right = 328
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "ConstRessourcesFamille"
            Begin Extent = 
               Top = 6
               Left = 366
               Bottom = 119
               Right = 590
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterielDetailCategorie"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 366
               Right = 266
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterielType"
            Begin Extent = 
               Top = 366
               Left = 38
               Bottom = 496
               Right = 302
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "MaterielElement"
            Begin Extent = 
               Top = 498
               Left = 38
               Bottom = 628
               Right = 228
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
       ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueMaterielListe';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[VueMaterielListeCountAleaEtat].[MS_DiagramPane1]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[33] 4[22] 2[26] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -192
         Left = 0
      End
      Begin Tables = 
         Begin Table = "MaterielListe"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 328
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LnkMaterielListeAleaListe"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 251
               Right = 322
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AleaListe"
            Begin Extent = 
               Top = 338
               Left = 143
               Bottom = 468
               Right = 427
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "EvenementEtat"
            Begin Extent = 
               Top = 259
               Left = 811
               Bottom = 372
               Right = 1107
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1500
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueMaterielListeCountAleaEtat';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modification de [dbo].[VueMaterielListeCountAleaEtat].[MS_DiagramPane2]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane2', @value = N'350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueMaterielListeCountAleaEtat';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[VueAleaElementAleaType].[MS_DiagramPane1]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "AleaElement"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 102
               Right = 247
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AleaType"
            Begin Extent = 
               Top = 102
               Left = 38
               Bottom = 232
               Right = 291
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueAleaElementAleaType';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[VueAleaElementAleaType].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueAleaElementAleaType';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[VueMaterielEtatHistorique].[MS_DiagramPane1]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "MaterielEtatHistorique"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 350
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterielEtat"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 251
               Right = 247
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LnkMaterielListMaterielEtatHistorique"
            Begin Extent = 
               Top = 252
               Left = 38
               Bottom = 365
               Right = 444
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterielListe"
            Begin Extent = 
               Top = 366
               Left = 38
               Bottom = 496
               Right = 328
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueMaterielEtatHistorique';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Création de [dbo].[VueMaterielEtatHistorique].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VueMaterielEtatHistorique';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO

IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT N'Succès de la mise à jour de la portion de base de données traitée.'
COMMIT TRANSACTION
END
ELSE PRINT N'Échec de la mise à jour de la portion de base de données traitée.'
GO
DROP TABLE #tmpErrors
GO
PRINT N'Vérification de données existantes par rapport aux nouvelles contraintes';


GO
USE [$(DatabaseName)];


GO
ALTER TABLE [dbo].[EvenementDommage] WITH CHECK CHECK CONSTRAINT [FK_DommageConseq_dommagePrecision];

ALTER TABLE [dbo].[EvenementDommagePrecision] WITH CHECK CHECK CONSTRAINT [FK_EvenementDommagePrecision_DommageType];

ALTER TABLE [dbo].[EvenementIntervenant] WITH CHECK CHECK CONSTRAINT [FK_Intervenant_IntervenantType];

ALTER TABLE [dbo].[EvenementIntervention] WITH CHECK CHECK CONSTRAINT [FK_InterventionConseq_Intervenants];

ALTER TABLE [dbo].[EvenementIntervention] WITH CHECK CHECK CONSTRAINT [FK_InterventionConseq_InterventionEtat];

ALTER TABLE [dbo].[EvenementIntervention] WITH CHECK CHECK CONSTRAINT [FK_EvenementIntervention_EvenementList];

ALTER TABLE [dbo].[EvenementIntervention] WITH CHECK CHECK CONSTRAINT [FK_EvenementIntervention_Ressource1];

ALTER TABLE [dbo].[EvenementIntervention] WITH CHECK CHECK CONSTRAINT [FK_EvenementIntervention_Ressource2];

ALTER TABLE [dbo].[QueryTriggerAsynchoneHistorique] WITH CHECK CHECK CONSTRAINT [FK_QueryTriggerAsynchoneHistorique_Alea];

ALTER TABLE [dbo].[QueryTriggerAsynchoneHistorique] WITH CHECK CHECK CONSTRAINT [FK_QueryTriggerAsynchoneHistorique_QueryTriggerAsynchone];

ALTER TABLE [dbo].[QueryTriggerAsynchoneHistorique] WITH CHECK CHECK CONSTRAINT [FK_QueryTriggerAsynchoneHistorique_COM];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK CHECK CONSTRAINT [FK_ConfigurationMainCourante_AleaState];

ALTER TABLE [dbo].[AleaType] WITH NOCHECK CHECK CONSTRAINT [FK_AleaType_AleaElement];

ALTER TABLE [dbo].[CfgAlerteAleaDiffusionListReseau] WITH NOCHECK CHECK CONSTRAINT [FK_AlerteList_AleaType];

ALTER TABLE [dbo].[AleaListe] WITH NOCHECK CHECK CONSTRAINT [FK_AleaListe_AleaType];

ALTER TABLE [dbo].[AleaType] WITH NOCHECK CHECK CONSTRAINT [FK_AleaType_EvenementFamille];

ALTER TABLE [dbo].[CfgQueryDataSourceQueryCriteria] WITH NOCHECK CHECK CONSTRAINT [FK_CfgQueryDataSourceQueryCriteria_QueryDataSource];

ALTER TABLE [dbo].[Query] WITH NOCHECK CHECK CONSTRAINT [FK_Query_QueryDataSource];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK CHECK CONSTRAINT [FK_CfgGeneralMainCourante_RessourceClasse];

ALTER TABLE [dbo].[CfgMaterielElementMaterielEtat] WITH NOCHECK CHECK CONSTRAINT [FK_CfgMaterielElementMaterielEtat_MaterielElement];

ALTER TABLE [dbo].[MaterielType] WITH NOCHECK CHECK CONSTRAINT [FK_MaterielType_MaterielElement];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK CHECK CONSTRAINT [FK_CfgGeneralMainCourante_Etat1];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK CHECK CONSTRAINT [FK_CfgGeneralMainCourante_Etat2];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK CHECK CONSTRAINT [FK_CfgGeneralMainCourante_Etat3];

ALTER TABLE [dbo].[CfgMaterielElementMaterielEtat] WITH NOCHECK CHECK CONSTRAINT [FK_CfgMaterielElementMaterielEtat_MaterielEtat];

ALTER TABLE [dbo].[CfgMaterielEtatColor] WITH NOCHECK CHECK CONSTRAINT [FK_CfgMaterielEtatColor_MaterielEtat];

ALTER TABLE [dbo].[MaterielEtatHistorique] WITH NOCHECK CHECK CONSTRAINT [FK_MaterielEtatHistorique_MaterielEtat];

ALTER TABLE [dbo].[MaterielListe] WITH NOCHECK CHECK CONSTRAINT [FK_MaterielListe_MaterielEtat];

ALTER TABLE [dbo].[MaterielListe] WITH NOCHECK CHECK CONSTRAINT [FK_MaterielListe_MaterielType];

ALTER TABLE [dbo].[Analyse] WITH NOCHECK CHECK CONSTRAINT [FK_Analyse_Query];

ALTER TABLE [dbo].[LnkQueryEvenementAttribut] WITH NOCHECK CHECK CONSTRAINT [FK_LnkQueryEvenementAttribut_Query];

ALTER TABLE [dbo].[LnkQueryEvenementSection] WITH NOCHECK CHECK CONSTRAINT [FK_LnkQueryEvenementSection_Query];

ALTER TABLE [dbo].[QueryCriteriaApplied] WITH NOCHECK CHECK CONSTRAINT [FK_LnkQueryQueryCriteria_Query];

ALTER TABLE [dbo].[QueryTriggerAsynchrone] WITH NOCHECK CHECK CONSTRAINT [FK_QueryTriggerAsynchrone_Query];

ALTER TABLE [dbo].[QueryTriggerSynchrone] WITH NOCHECK CHECK CONSTRAINT [FK_QueryTriggerSynchrone_Query];

ALTER TABLE [dbo].[Query] WITH NOCHECK CHECK CONSTRAINT [FK_Query_QueryGroup];

ALTER TABLE [dbo].[Query] WITH NOCHECK CHECK CONSTRAINT [FK_Query_Rapport];

ALTER TABLE [dbo].[Query] WITH NOCHECK CHECK CONSTRAINT [FK_Query_Imputabilite];

ALTER TABLE [dbo].[CfgEvenementActionGroupRule] WITH NOCHECK CHECK CONSTRAINT [FK_CfgEvenementActionGroupRule_EvenementActionGroup];

ALTER TABLE [dbo].[CfgEvenementActionGroupRule] WITH NOCHECK CHECK CONSTRAINT [FK_CfgEvenementActionGroupRule_Rule];

ALTER TABLE [dbo].[CfgEvenementEtatTransitionGroupPermission] WITH NOCHECK CHECK CONSTRAINT [FK_CfgEvenementEtatTransitionGroupPermission_EtatTransition];

ALTER TABLE [dbo].[CfgEvenementEtatTransitionGroupPermission] WITH NOCHECK CHECK CONSTRAINT [FK_CfgEvenementEtatTransitionGroupPermission_Permission];

ALTER TABLE [dbo].[CfgEvenementSousCategorieGroupPermission] WITH NOCHECK CHECK CONSTRAINT [FK_CfgEvenementSousCategorieGroupPermission_EvenementSousCategorie];

ALTER TABLE [dbo].[CfgEvenementSousCategorieGroupPermission] WITH NOCHECK CHECK CONSTRAINT [FK_CfgEvenementSousCategorieGroupPermission_Permission];

ALTER TABLE [dbo].[CfgRessourceSousClasseGroupPermission] WITH NOCHECK CHECK CONSTRAINT [FK_CfgRessourceSousClasseGroupPermission_Permission];

ALTER TABLE [dbo].[CfgRessourceSousClasseGroupPermission] WITH NOCHECK CHECK CONSTRAINT [FK_CfgRessourceSousClasseGroupPermission_RessourceSousClasse];

ALTER TABLE [dbo].[EvenementAction] WITH NOCHECK CHECK CONSTRAINT [FK_EvenementAction_Etat];

ALTER TABLE [dbo].[EvenementAction] WITH NOCHECK CHECK CONSTRAINT [FK_EvenementAction_EvenementActionGroup];

ALTER TABLE [dbo].[EvenementAction] WITH NOCHECK CHECK CONSTRAINT [FK_EvenementAction_FermetureRaison];

ALTER TABLE [dbo].[EvenementActionRealise] WITH NOCHECK CHECK CONSTRAINT [FK_EvenementActionRealise_EvenementAction];

ALTER TABLE [dbo].[EvenementActionRealise] WITH NOCHECK CHECK CONSTRAINT [FK_EvenementActionRealise_EvenementListe];

ALTER TABLE [dbo].[EvenementEtat] WITH NOCHECK CHECK CONSTRAINT [FK_EvenementEtat_ActionHistoDefaut];

ALTER TABLE [dbo].[EvenementEtat] WITH NOCHECK CHECK CONSTRAINT [FK_EvenementEtat_ActionHistoOnChange];

ALTER TABLE [dbo].[EvenementEtat] WITH NOCHECK CHECK CONSTRAINT [FK_EvenementEtat_constEvenementEtat];

ALTER TABLE [dbo].[EvenementEtatTransition] WITH NOCHECK CHECK CONSTRAINT [FK_EvenementEtatTransition_EvenementEtatDest];

ALTER TABLE [dbo].[EvenementEtatTransition] WITH NOCHECK CHECK CONSTRAINT [FK_EvenementEtatTransition_EvenementEtatOrg];

ALTER TABLE [dbo].[QueryTriggerAsynchoneAccuse] WITH NOCHECK CHECK CONSTRAINT [FK_QueryTriggerAsynchoneAccuse_QueryTriggerAsynchoneHistorique];

ALTER TABLE [dbo].[CfgEvenementMaintenanceEmplacementRessource] WITH NOCHECK CHECK CONSTRAINT [FK_CfgEvenementMaintenanceEmplacementRessource_RessourceCategorieGroupe];

ALTER TABLE [dbo].[CfgEvenementMaintenanceOrganeRessource] WITH NOCHECK CHECK CONSTRAINT [FK_CfgEvenementMaintenanceOrganeRessource_RessourceCategorieGroupe];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK CHECK CONSTRAINT [FK_CfgGeneralMainCourante_CategorieClient];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK CHECK CONSTRAINT [FK_CfgGeneralMainCourante_ClientClasse];

ALTER TABLE [dbo].[CfgGeneralMainCourante] WITH NOCHECK CHECK CONSTRAINT [FK_CfgGeneralMainCourante_SousClasseClient];

ALTER TABLE [dbo].[MaterielDetailCategorie] WITH NOCHECK CHECK CONSTRAINT [FK_MaterielDetailCategorie_RessourceCategorieGroupe];


GO
PRINT N'Mise à jour terminée.';


GO

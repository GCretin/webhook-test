USE [s2002m401]

---------------------RLA 2.2.0-----------------------

---------------------Ev�nement �tat -----------------------
GO
INSERT INTO [dbo].[EvenementEtat]([nom],[rank],[id_evenementEtat_constEvenementEtat],[id_evenementEtat_evenementActionHistorique_defaut],[id_evenementEtat_evenementActionHistorique_onChange])
     VALUES ('Ouvert',10,1,3,1)
GO
GO
INSERT INTO [dbo].[EvenementEtat]([nom],[rank],[id_evenementEtat_constEvenementEtat],[id_evenementEtat_evenementActionHistorique_defaut],[id_evenementEtat_evenementActionHistorique_onChange])
     VALUES ('Ferm�',90,3,3,2)
GO
GO
INSERT INTO [dbo].[EvenementEtat]([nom],[rank],[id_evenementEtat_constEvenementEtat],[id_evenementEtat_evenementActionHistorique_defaut],[id_evenementEtat_evenementActionHistorique_onChange])
     VALUES ('Annul�',100,4,3,5)
GO
GO
INSERT INTO [dbo].[EvenementEtat]([nom],[rank],[id_evenementEtat_constEvenementEtat],[id_evenementEtat_evenementActionHistorique_defaut],[id_evenementEtat_evenementActionHistorique_onChange])
     VALUES ('En cours',50,2,3,2)
GO
GO
INSERT INTO [dbo].[EvenementEtat]([nom],[rank],[id_evenementEtat_constEvenementEtat],[id_evenementEtat_evenementActionHistorique_defaut],[id_evenementEtat_evenementActionHistorique_onChange])
     VALUES ('Brouillon',5,5,3,3)
GO
GO
INSERT INTO [dbo].[EvenementEtat]([nom],[rank],[id_evenementEtat_constEvenementEtat],[id_evenementEtat_evenementActionHistorique_defaut],[id_evenementEtat_evenementActionHistorique_onChange])
     VALUES ('Dupliqu�',13,1,3,4)
GO

---------------------Ressource cat�gories groupes -----------------------
GO
INSERT INTO [dbo].[RessourceCategorieGroupe]([nom],[id_reseau])
     VALUES('Voie',1)
GO
GO
INSERT INTO [dbo].[RessourceCategorieGroupe]([nom],[id_reseau])
     VALUES('Station voyageur tram',1)
GO
GO
INSERT INTO [dbo].[RessourceCategorieGroupe]([nom],[id_reseau])
     VALUES('LAC',1)
GO
GO
INSERT INTO [dbo].[RessourceCategorieGroupe]([nom],[id_reseau])
     VALUES('SSR',1)
GO
GO
INSERT INTO [dbo].[RessourceCategorieGroupe]([nom],[id_reseau])
     VALUES('SIG Ferroviaire',1)
GO
GO
INSERT INTO [dbo].[RessourceCategorieGroupe]([nom],[id_reseau])
     VALUES('SIG Routi�re',1)
GO
GO
INSERT INTO [dbo].[RessourceCategorieGroupe]([nom],[id_reseau])
     VALUES('Station voyageur bus',1)
GO
GO
INSERT INTO [dbo].[RessourceCategorieGroupe]([nom],[id_reseau])
     VALUES('Batiments',1)
GO
GO
INSERT INTO [dbo].[RessourceCategorieGroupe]([nom],[id_reseau])
     VALUES('Autres',1)
GO
GO
INSERT INTO [dbo].[RessourceCategorieGroupe]([nom],[id_reseau])
     VALUES('RH',1)
GO
GO
INSERT INTO [dbo].[RessourceCategorieGroupe]([nom],[id_reseau])
     VALUES('Points hauts',1)
GO
GO
INSERT INTO [dbo].[RessourceCategorieGroupe]([nom],[id_reseau])
     VALUES('Bus',1)
GO
GO
INSERT INTO [dbo].[RessourceCategorieGroupe]([nom],[id_reseau])
     VALUES('Tramway T1',1)
GO
GO
INSERT INTO [dbo].[RessourceCategorieGroupe]([nom],[id_reseau])
     VALUES('Tramway T2',1)
GO
GO
INSERT INTO [dbo].[RessourceCategorieGroupe]([nom],[id_reseau])
     VALUES('D�p�t Bus',1)
GO
GO
INSERT INTO [dbo].[RessourceCategorieGroupe]([nom],[id_reseau])
     VALUES('D�p�t Tram',1)
GO
GO
INSERT INTO [dbo].[RessourceCategorieGroupe]([nom],[id_reseau])
     VALUES('P+R',1)
GO
GO
INSERT INTO [dbo].[RessourceCategorieGroupe]([nom],[id_reseau])
     VALUES('Parking bus',1)
GO

GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 1
 WHERE [id_materielDetailCategorie]=10000
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 2
 WHERE [id_materielDetailCategorie]=10001
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 3
 WHERE [id_materielDetailCategorie]=10002
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 4
 WHERE [id_materielDetailCategorie]=10003
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 5
 WHERE [id_materielDetailCategorie]=10004
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 6
 WHERE [id_materielDetailCategorie]=10005
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 7
 WHERE [id_materielDetailCategorie]=10006
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 15
 WHERE [id_materielDetailCategorie]=10007
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 16
 WHERE [id_materielDetailCategorie]=10008
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 17
 WHERE [id_materielDetailCategorie]=10009
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 18
 WHERE [id_materielDetailCategorie]=10010
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 8
 WHERE [id_materielDetailCategorie]=10011
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 9
 WHERE [id_materielDetailCategorie]=10012
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 10
 WHERE [id_materielDetailCategorie]=10013
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 10
 WHERE [id_materielDetailCategorie]=10014
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 10
 WHERE [id_materielDetailCategorie]=10015
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 11
 WHERE [id_materielDetailCategorie]=10016
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 12
 WHERE [id_materielDetailCategorie]=10017
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 12
 WHERE [id_materielDetailCategorie]=10018
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 12
 WHERE [id_materielDetailCategorie]=10019
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 12
 WHERE [id_materielDetailCategorie]=10020
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 12
 WHERE [id_materielDetailCategorie]=10021
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 12
 WHERE [id_materielDetailCategorie]=10022
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 12
 WHERE [id_materielDetailCategorie]=10023
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 12
 WHERE [id_materielDetailCategorie]=10024
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 12
 WHERE [id_materielDetailCategorie]=10025
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 12
 WHERE [id_materielDetailCategorie]=10026
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 12
 WHERE [id_materielDetailCategorie]=10027
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 12
 WHERE [id_materielDetailCategorie]=10028
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 12
 WHERE [id_materielDetailCategorie]=10029
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 12
 WHERE [id_materielDetailCategorie]=10030
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 12
 WHERE [id_materielDetailCategorie]=10031
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 12
 WHERE [id_materielDetailCategorie]=10032
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 12
 WHERE [id_materielDetailCategorie]=10033
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 12
 WHERE [id_materielDetailCategorie]=10034
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 12
 WHERE [id_materielDetailCategorie]=10035
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 12
 WHERE [id_materielDetailCategorie]=10037
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 13
 WHERE [id_materielDetailCategorie]=10036
GO
GO
UPDATE [dbo].[MaterielDetailCategorie]
   SET [id_materielDetailCategorie_ressourceCategorieGroupe] = 14
 WHERE [id_materielDetailCategorie]=10038
GO

GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 13
 WHERE [id_cfgEvenementMaintenanceRessource] =1
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 13
 WHERE [id_cfgEvenementMaintenanceRessource] =2
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 13
 WHERE [id_cfgEvenementMaintenanceRessource] =3
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 13
 WHERE [id_cfgEvenementMaintenanceRessource] =7
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 13
 WHERE [id_cfgEvenementMaintenanceRessource] =9
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 14
 WHERE [id_cfgEvenementMaintenanceRessource] =167
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 14
 WHERE [id_cfgEvenementMaintenanceRessource] =168
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 14
 WHERE [id_cfgEvenementMaintenanceRessource] =169
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 14
 WHERE [id_cfgEvenementMaintenanceRessource] =170
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 14
 WHERE [id_cfgEvenementMaintenanceRessource] =171
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 12
 WHERE [id_cfgEvenementMaintenanceRessource] =118
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 12
 WHERE [id_cfgEvenementMaintenanceRessource] =160
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 12
 WHERE [id_cfgEvenementMaintenanceRessource] =121
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 12
 WHERE [id_cfgEvenementMaintenanceRessource] =164
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 13
 WHERE [id_cfgEvenementMaintenanceRessource] =124
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 14
 WHERE [id_cfgEvenementMaintenanceRessource] =172
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 14
 WHERE [id_cfgEvenementMaintenanceRessource] =173
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 13
 WHERE [id_cfgEvenementMaintenanceRessource] =125
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 13
 WHERE [id_cfgEvenementMaintenanceRessource] =126
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 14
 WHERE [id_cfgEvenementMaintenanceRessource] =174
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 1
 WHERE [id_cfgEvenementMaintenanceRessource] =127
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] =2 
 WHERE [id_cfgEvenementMaintenanceRessource] =128
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 3
 WHERE [id_cfgEvenementMaintenanceRessource] =129
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 4
 WHERE [id_cfgEvenementMaintenanceRessource] =130
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 5
 WHERE [id_cfgEvenementMaintenanceRessource] =131
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 6
 WHERE [id_cfgEvenementMaintenanceRessource] =133
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 12
 WHERE [id_cfgEvenementMaintenanceRessource] =134
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 12
 WHERE [id_cfgEvenementMaintenanceRessource] =161
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 12
 WHERE [id_cfgEvenementMaintenanceRessource] =74
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 7
 WHERE [id_cfgEvenementMaintenanceRessource] =149
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 15
 WHERE [id_cfgEvenementMaintenanceRessource] =150
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 16
 WHERE [id_cfgEvenementMaintenanceRessource] =151
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 17
 WHERE [id_cfgEvenementMaintenanceRessource] =152
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 18
 WHERE [id_cfgEvenementMaintenanceRessource] =153
GO
GO
UPDATE [dbo].[CfgEvenementMaintenanceOrganeRessource]
   SET [id_cfgEvenementMaintenanceRessource_ressourceCategorieGroupe] = 11
 WHERE [id_cfgEvenementMaintenanceRessource] =166
GO

---------------------Coloration-----------------------


GO
UPDATE [dbo].[Rules]
   SET [id_rules_coloration] = 9
 WHERE id_rules= 35
GO
GO
UPDATE [dbo].[Rules]
   SET [id_rules_coloration] = 8
 WHERE id_rules= 36
GO
GO
UPDATE [dbo].[Rules]
   SET [id_rules_coloration] = 12
 WHERE id_rules= 37
GO
GO
UPDATE [dbo].[Rules]
   SET [id_rules_coloration] = 4
 WHERE id_rules= 38
GO
GO
UPDATE [dbo].[Rules]
   SET [id_rules_coloration] = 10
 WHERE id_rules= 39
GO

---------------------Section � cacher-----------------------

INSERT INTO [dbo].[CfgEvenementSectionRule]
           ([id_cfgEvenementSectionRule_constSection]
           ,[id_cfgEvenementSectionRule_constState]
           ,[id_cfgEvenementSectionRule_constRessourceFamille]
           ,[id_cfgEvenementSectionRule_constEvenementFamille]
           ,[id_cfgEvenementSectionRule_constTerminal]
           ,[id_cfgEvenementSectionRule_constModeSaisie]
           ,[id_cfgEvenementSectionRule_reseaus101m401]
           ,[isDefaut])
     VALUES
           (19
           ,2
           ,null
           ,null
           ,null
           ,null
           ,1
           ,0)
GO
GO

INSERT INTO [dbo].[CfgEvenementSectionRule]
           ([id_cfgEvenementSectionRule_constSection]
           ,[id_cfgEvenementSectionRule_constState]
           ,[id_cfgEvenementSectionRule_constRessourceFamille]
           ,[id_cfgEvenementSectionRule_constEvenementFamille]
           ,[id_cfgEvenementSectionRule_constTerminal]
           ,[id_cfgEvenementSectionRule_constModeSaisie]
           ,[id_cfgEvenementSectionRule_reseaus101m401]
           ,[isDefaut])
     VALUES
           (18
           ,2
           ,null
           ,null
           ,null
           ,null
           ,1
           ,0)
GO
INSERT INTO [dbo].[CfgEvenementSectionRule]
           ([id_cfgEvenementSectionRule_constSection]
           ,[id_cfgEvenementSectionRule_constState]
           ,[id_cfgEvenementSectionRule_constRessourceFamille]
           ,[id_cfgEvenementSectionRule_constEvenementFamille]
           ,[id_cfgEvenementSectionRule_constTerminal]
           ,[id_cfgEvenementSectionRule_constModeSaisie]
           ,[id_cfgEvenementSectionRule_reseaus101m401]
           ,[isDefaut])
     VALUES
           (20
           ,2
           ,null
           ,null
           ,null
           ,null
           ,1
           ,0)
GO

---------------------Symptome-----------------------
GO
UPDATE [dbo].[EvenementMaintenanceSymptome]
   SET [nom] = 'Contacteur HS'
 WHERE [nom] LIKE '%Contacteur HS%'
GO
﻿/*
Script de déploiement pour s2002m401

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "s2002m401"
:setvar DefaultFilePrefix "s2002m401"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.DEVSQLSP\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.DEVSQLSP\MSSQL\DATA\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Suppression de [dbo].[DF_ConstQueryCriteria_isEligibleOnTheFlight]...';


GO
ALTER TABLE [dbo].[ConstQueryCriteria] DROP CONSTRAINT [DF_ConstQueryCriteria_isEligibleOnTheFlight];


GO
PRINT N'Suppression de [dbo].[FK_LnkQueryQueryCriteria_QueryCriteia]...';


GO
ALTER TABLE [dbo].[QueryCriteriaApplied] DROP CONSTRAINT [FK_LnkQueryQueryCriteria_QueryCriteia];


GO
PRINT N'Suppression de [dbo].[FK_CfgQueryDataSourceQueryCriteria_QueryCriteria]...';


GO
ALTER TABLE [dbo].[CfgQueryDataSourceQueryCriteria] DROP CONSTRAINT [FK_CfgQueryDataSourceQueryCriteria_QueryCriteria];


GO
PRINT N'Suppression de [dbo].[FK_ConstQueryCriteria_ConstQueryCriteriaGroup]...';


GO
ALTER TABLE [dbo].[ConstQueryCriteria] DROP CONSTRAINT [FK_ConstQueryCriteria_ConstQueryCriteriaGroup];


GO
PRINT N'Modification de [dbo].[CfgPageAnalyse]...';


GO
ALTER TABLE [dbo].[CfgPageAnalyse]
    ADD [withLegend]         INT            CONSTRAINT [DF_CfgPageAnalyse_withLegend] DEFAULT ((0)) NOT NULL,
        [legendPositionTop]  INT            CONSTRAINT [DF_CfgPageAnalyse_legendPositionTop] DEFAULT ((0)) NOT NULL,
        [legendPositionLeft] INT            CONSTRAINT [DF_CfgPageAnalyse_legendPositionLeft] DEFAULT ((0)) NOT NULL,
        [legendHeight]       INT            CONSTRAINT [DF_CfgPageAnalyse_legendHeight] DEFAULT ((0)) NOT NULL,
        [legendWidth]        INT            CONSTRAINT [DF_CfgPageAnalyse_legendWidth] DEFAULT ((0)) NOT NULL,
        [legend]             NVARCHAR (MAX) CONSTRAINT [DF_CfgPageAnalyse_legend] DEFAULT ((0)) NOT NULL,
        [orientationLabelX]  INT            CONSTRAINT [DF_CfgPageAnalyse_orientationLabelX] DEFAULT ((0)) NOT NULL,
        [colorChart]         NVARCHAR (50)  CONSTRAINT [DF_CfgPageAnalyse_colorChart] DEFAULT ((0)) NOT NULL;


GO
PRINT N'Modification de [dbo].[ConstAnalyseType]...';


GO
ALTER TABLE [dbo].[ConstAnalyseType]
    ADD [id_constAnalyseType_constAnalyseTypeGroup] INT NULL;


GO
PRINT N'Modification de [dbo].[ConstEvenementExploitConseq]...';


GO
ALTER TABLE [dbo].[ConstEvenementExploitConseq]
    ADD [with_observations]  INT            CONSTRAINT [DF_ConstEvenementExploitConseq_with_observations] DEFAULT ((0)) NOT NULL,
        [label_observations] NVARCHAR (MAX) CONSTRAINT [DF_ConstEvenementExploitConseq_label_observations] DEFAULT (N'LABEL_A_COMPLETER') NOT NULL;


GO
PRINT N'Début de la régénération de la table [dbo].[ConstQueryCriteria]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_ConstQueryCriteria] (
    [id_queryCriteria]                    INT            IDENTITY (1, 1) NOT NULL,
    [dataType]                            VARCHAR (50)   NOT NULL,
    [libelle]                             NVARCHAR (50)  NOT NULL,
    [id_queryCriteria_queryCriteriaGroup] INT            NOT NULL,
    [code]                                NVARCHAR (50)  NOT NULL,
    [isEligibleOnTheFlight]               INT            CONSTRAINT [DF_ConstQueryCriteria_isEligibleOnTheFlight] DEFAULT ((0)) NOT NULL,
    [isEligibleAdvancedMode]              INT            CONSTRAINT [DF_ConstQueryCriteria_isEligibleAdvancedMode] DEFAULT ((0)) NOT NULL,
    [description]                         NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK__QueryCri__14B6E3F24B32558B1] PRIMARY KEY CLUSTERED ([id_queryCriteria] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[ConstQueryCriteria])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_ConstQueryCriteria] ON;
        INSERT INTO [dbo].[tmp_ms_xx_ConstQueryCriteria] ([id_queryCriteria], [dataType], [libelle], [id_queryCriteria_queryCriteriaGroup], [code], [isEligibleOnTheFlight], [description])
        SELECT   [id_queryCriteria],
                 [dataType],
                 [libelle],
                 [id_queryCriteria_queryCriteriaGroup],
                 [code],
                 [isEligibleOnTheFlight],
                 [description]
        FROM     [dbo].[ConstQueryCriteria]
        ORDER BY [id_queryCriteria] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_ConstQueryCriteria] OFF;
    END

DROP TABLE [dbo].[ConstQueryCriteria];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_ConstQueryCriteria]', N'ConstQueryCriteria';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK__QueryCri__14B6E3F24B32558B1]', N'PK__QueryCri__14B6E3F24B32558B', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Modification de [dbo].[EvenementExploitationConseq]...';


GO
ALTER TABLE [dbo].[EvenementExploitationConseq]
    ADD [observation] NVARCHAR (MAX) NULL;


GO
PRINT N'Création de [dbo].[ConstAnalyseTypeGroup]...';


GO
CREATE TABLE [dbo].[ConstAnalyseTypeGroup] (
    [id_constAnalyseTypeGroup] INT            IDENTITY (1, 1) NOT NULL,
    [nom]                      NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_ConstAnalyseTypeGroup] PRIMARY KEY CLUSTERED ([id_constAnalyseTypeGroup] ASC)
);


GO
PRINT N'Création de [dbo].[LnkQueryTriggerAsynchroneEvenementSection]...';


GO
CREATE TABLE [dbo].[LnkQueryTriggerAsynchroneEvenementSection] (
    [id_lnkQueryTriggerAsynchroneEvenementSection]         INT IDENTITY (1, 1) NOT NULL,
    [id_lnkQueryTriggerAsynchroneEvenementSection_trigger] INT NOT NULL,
    [id_lnkQueryTriggerAsynchroneEvenementSection_section] INT NOT NULL,
    CONSTRAINT [PK_LnkQueryTriggerAsynchroneEvenementSection] PRIMARY KEY CLUSTERED ([id_lnkQueryTriggerAsynchroneEvenementSection] ASC)
);


GO
PRINT N'Création de [dbo].[QueryTriggerAsynchoneHistorique]...';


GO
CREATE TABLE [dbo].[QueryTriggerAsynchoneHistorique] (
    [id_queryDeclencheurAsynchoneHistorique]                           INT      IDENTITY (1, 1) NOT NULL,
    [id_queryDeclencheurAsynchoneHistorique_queryDeclencheurAsynchone] INT      NOT NULL,
    [date]                                                             DATE     NOT NULL,
    [heure]                                                            TIME (7) NOT NULL,
    [id_queryDeclencheurAsynchoneHistorique_alea]                      INT      NOT NULL,
    CONSTRAINT [PK_QueryDeclencheurAsynchoneHistorique] PRIMARY KEY CLUSTERED ([id_queryDeclencheurAsynchoneHistorique] ASC)
);


GO
PRINT N'Création de [dbo].[QueryTriggerAsynchrone]...';


GO
CREATE TABLE [dbo].[QueryTriggerAsynchrone] (
    [id_QueryTriggerAsynchrone]                       INT IDENTITY (1, 1) NOT NULL,
    [id_QueryTriggerAsynchrone_query]                 INT NOT NULL,
    [id_QueryTriggerAsynchrone_diffusionLists101m401] INT NOT NULL,
    [id_QueryTriggerAsynchrone_com]                   INT NOT NULL,
    CONSTRAINT [PK_QueryTriggerAsynchrone] PRIMARY KEY CLUSTERED ([id_QueryTriggerAsynchrone] ASC)
);


GO
PRINT N'Création de [dbo].[FK_LnkQueryQueryCriteria_QueryCriteia]...';


GO
ALTER TABLE [dbo].[QueryCriteriaApplied] WITH NOCHECK
    ADD CONSTRAINT [FK_LnkQueryQueryCriteria_QueryCriteia] FOREIGN KEY ([id_queryCriteriaApplied_queryCriteria]) REFERENCES [dbo].[ConstQueryCriteria] ([id_queryCriteria]);


GO
PRINT N'Création de [dbo].[FK_CfgQueryDataSourceQueryCriteria_QueryCriteria]...';


GO
ALTER TABLE [dbo].[CfgQueryDataSourceQueryCriteria] WITH NOCHECK
    ADD CONSTRAINT [FK_CfgQueryDataSourceQueryCriteria_QueryCriteria] FOREIGN KEY ([id_cfgQueryDataSourceQueryCriteria_queryCriteria]) REFERENCES [dbo].[ConstQueryCriteria] ([id_queryCriteria]);


GO
PRINT N'Création de [dbo].[FK_ConstQueryCriteria_ConstQueryCriteriaGroup]...';


GO
ALTER TABLE [dbo].[ConstQueryCriteria] WITH NOCHECK
    ADD CONSTRAINT [FK_ConstQueryCriteria_ConstQueryCriteriaGroup] FOREIGN KEY ([id_queryCriteria_queryCriteriaGroup]) REFERENCES [dbo].[ConstQueryCriteriaGroup] ([id_constQueryCriteriaGroup]);


GO
PRINT N'Création de [dbo].[FK_LnkQueryTriggerAsynchroneEvenementSection_section]...';


GO
ALTER TABLE [dbo].[LnkQueryTriggerAsynchroneEvenementSection] WITH NOCHECK
    ADD CONSTRAINT [FK_LnkQueryTriggerAsynchroneEvenementSection_section] FOREIGN KEY ([id_lnkQueryTriggerAsynchroneEvenementSection_section]) REFERENCES [dbo].[ConstEvenementSection] ([id_constEvenementSection]);


GO
PRINT N'Création de [dbo].[FK_LnkQueryTriggerAsynchroneEvenementSection_trigger]...';


GO
ALTER TABLE [dbo].[LnkQueryTriggerAsynchroneEvenementSection] WITH NOCHECK
    ADD CONSTRAINT [FK_LnkQueryTriggerAsynchroneEvenementSection_trigger] FOREIGN KEY ([id_lnkQueryTriggerAsynchroneEvenementSection_trigger]) REFERENCES [dbo].[QueryTriggerAsynchrone] ([id_QueryTriggerAsynchrone]);


GO
PRINT N'Création de [dbo].[FK_QueryTriggerAsynchoneHistorique_Alea]...';


GO
ALTER TABLE [dbo].[QueryTriggerAsynchoneHistorique] WITH NOCHECK
    ADD CONSTRAINT [FK_QueryTriggerAsynchoneHistorique_Alea] FOREIGN KEY ([id_queryDeclencheurAsynchoneHistorique_alea]) REFERENCES [dbo].[AleaListe] ([id_aleaListe]);


GO
PRINT N'Création de [dbo].[FK_QueryTriggerAsynchoneHistorique_QueryTriggerAsynchone]...';


GO
ALTER TABLE [dbo].[QueryTriggerAsynchoneHistorique] WITH NOCHECK
    ADD CONSTRAINT [FK_QueryTriggerAsynchoneHistorique_QueryTriggerAsynchone] FOREIGN KEY ([id_queryDeclencheurAsynchoneHistorique_queryDeclencheurAsynchone]) REFERENCES [dbo].[QueryTriggerAsynchrone] ([id_QueryTriggerAsynchrone]);


GO
PRINT N'Création de [dbo].[FK_QueryTriggerAsynchrone_com]...';


GO
ALTER TABLE [dbo].[QueryTriggerAsynchrone] WITH NOCHECK
    ADD CONSTRAINT [FK_QueryTriggerAsynchrone_com] FOREIGN KEY ([id_QueryTriggerAsynchrone_com]) REFERENCES [dbo].[COM] ([id_aleaCOM]);


GO
PRINT N'Création de [dbo].[FK_QueryTriggerAsynchrone_Query]...';


GO
ALTER TABLE [dbo].[QueryTriggerAsynchrone] WITH NOCHECK
    ADD CONSTRAINT [FK_QueryTriggerAsynchrone_Query] FOREIGN KEY ([id_QueryTriggerAsynchrone_query]) REFERENCES [dbo].[Query] ([id_query]);


GO
PRINT N'Création de [dbo].[FK_ConstAnalyseType_ConstAnalyseTypeGroup]...';


GO
ALTER TABLE [dbo].[ConstAnalyseType] WITH NOCHECK
    ADD CONSTRAINT [FK_ConstAnalyseType_ConstAnalyseTypeGroup] FOREIGN KEY ([id_constAnalyseType_constAnalyseTypeGroup]) REFERENCES [dbo].[ConstAnalyseTypeGroup] ([id_constAnalyseTypeGroup]);


GO
PRINT N'Vérification de données existantes par rapport aux nouvelles contraintes';


GO
USE [$(DatabaseName)];


GO
ALTER TABLE [dbo].[QueryCriteriaApplied] WITH CHECK CHECK CONSTRAINT [FK_LnkQueryQueryCriteria_QueryCriteia];

ALTER TABLE [dbo].[CfgQueryDataSourceQueryCriteria] WITH CHECK CHECK CONSTRAINT [FK_CfgQueryDataSourceQueryCriteria_QueryCriteria];

ALTER TABLE [dbo].[ConstQueryCriteria] WITH CHECK CHECK CONSTRAINT [FK_ConstQueryCriteria_ConstQueryCriteriaGroup];

ALTER TABLE [dbo].[LnkQueryTriggerAsynchroneEvenementSection] WITH CHECK CHECK CONSTRAINT [FK_LnkQueryTriggerAsynchroneEvenementSection_section];

ALTER TABLE [dbo].[LnkQueryTriggerAsynchroneEvenementSection] WITH CHECK CHECK CONSTRAINT [FK_LnkQueryTriggerAsynchroneEvenementSection_trigger];

ALTER TABLE [dbo].[QueryTriggerAsynchoneHistorique] WITH CHECK CHECK CONSTRAINT [FK_QueryTriggerAsynchoneHistorique_Alea];

ALTER TABLE [dbo].[QueryTriggerAsynchoneHistorique] WITH CHECK CHECK CONSTRAINT [FK_QueryTriggerAsynchoneHistorique_QueryTriggerAsynchone];

ALTER TABLE [dbo].[QueryTriggerAsynchrone] WITH CHECK CHECK CONSTRAINT [FK_QueryTriggerAsynchrone_com];

ALTER TABLE [dbo].[QueryTriggerAsynchrone] WITH CHECK CHECK CONSTRAINT [FK_QueryTriggerAsynchrone_Query];

ALTER TABLE [dbo].[ConstAnalyseType] WITH CHECK CHECK CONSTRAINT [FK_ConstAnalyseType_ConstAnalyseTypeGroup];


GO
PRINT N'Mise à jour terminée.';


GO

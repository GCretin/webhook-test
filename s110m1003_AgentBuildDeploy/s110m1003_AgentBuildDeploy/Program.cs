﻿using Newtonsoft.Json;
using s110m1003_AgentBuildDeploy.classes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace s110m1003_AgentBuildDeploy
{
    class Program
    {
        static void Main(string[] args)
        {
            string branch = null;
            string publishFolder = Environment.GetEnvironmentVariable("FILEXIS_ROOT", EnvironmentVariableTarget.Machine);
            string repoRootPath = Environment.GetEnvironmentVariable("BEE_REPO_ROOT", EnvironmentVariableTarget.Machine);
            string binariesTMP = null;
            int index;
            string versionToUpdateString = null;
            int versionToUpdateInt;
            bool doTest = false;
            bool doPublish = false;
            bool doDeploy = false;
            int? modulIncludeIndex = null;
            int? modulExcludeIndex = null;

            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("[HELP] Modules disponibles : ");
            string[] lines = File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"static\ModulList.build"));
            foreach (string line in lines)
            {
                string[] lineTab = line.Split(';');
                if (lineTab.Length == 4) Console.Write(" " + lineTab[0] + "-" + lineTab[3]);
            }

            Console.WriteLine("\n\n[HELP] .exe -distant (-version revision/build/mineure/majeure) (-test) (-publish) (-deploy) (-moduleInclude codeprojet1 codeprojet2...) (-moduleExclude codeprojet1 codeprojet2...)");
            Console.WriteLine("[HELP] .exe -local {branch:actuelle} {version:revision} (-test) (-publish) (-deploy) (-moduleInclude codeprojet1 codeprojet2...) (-moduleExclude codeprojet1 codeprojet2...)\n");
            Console.ResetColor();

            foreach (string arg in args)
            {
                switch (arg)
                {
                    case "-local":
                        binariesTMP = "_BINAIRIES_TMP_LOCAL";
                        break;

                    case "-distant":
                        binariesTMP = "_BINAIRIES_TMP";
                        break;

                    case "-branch":
                        index = Array.IndexOf(args, "-branch") + 1;
                        branch = args[index].Replace("origin/", "");
                        break;

                    case "-version":
                        index = Array.IndexOf(args, "-version") + 1;
                        versionToUpdateString = args[index];
                        if (versionToUpdateString != "revision" || versionToUpdateString != "build" || versionToUpdateString != "mineure" || versionToUpdateString != "majeure")
                        {
                            Console.BackgroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine("[ERROR] La version '" + versionToUpdateString + "' est invalide.");
                            Console.ResetColor();
                        }
                        break;
                    case "-test":
                        doTest = true;
                        break;
                    case "-publish":
                        doPublish = true;
                        break;
                    case "-deploy":
                        doDeploy = true;
                        break;
                    case "-moduleInclude":
                        modulIncludeIndex = Array.IndexOf(args, "-moduleInclude") + 1;
                        break;
                    case "-moduleExclude":
                        modulExcludeIndex = Array.IndexOf(args, "-moduleExclude") + 1;
                        break;
                }
            }

            if (String.IsNullOrEmpty(publishFolder) || String.IsNullOrEmpty(repoRootPath))
            {
                Console.BackgroundColor = ConsoleColor.DarkRed;
                if (String.IsNullOrEmpty(publishFolder)) Console.WriteLine("[ERROR] La variable d'environnement FILEXIS_ROOT n'est pas definie.");
                if (String.IsNullOrEmpty(repoRootPath)) Console.WriteLine("[ERROR] La variable d'environnement BEE_REPO_ROOT n'est pas definie.");

                Console.ResetColor();

                return;
            }

            if (branch == null) branch = Common.GetGitBranch();
            if (binariesTMP == null)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("[WARNING] Argument -local/-distant manquant.");
                Console.WriteLine("[WARNING] Local pris par défaut.\n");

                Console.ResetColor();

                binariesTMP = "_BINAIRIES_TMP_LOC";
            }
            if (versionToUpdateString == null && binariesTMP == "_BINAIRIES_TMP")
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("[WARNING] Exécution distante.");
                Console.WriteLine("[WARNING] Version build mise à jour par défaut.\n");

                Console.ResetColor();

                versionToUpdateString = "build";
            }
            if (binariesTMP == "_BINAIRIES_TMP_LOC")
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("[WARNING] Exécution locale.");
                Console.WriteLine("[WARNING] Version revision mise à jour.\n");

                Console.ResetColor();

                versionToUpdateString = "revision";
            }
            //doTest = true;
            //doPublish = true;
            //doDeploy = true;

            List<Project> myProjects = new List<Project>();
            if (modulIncludeIndex.HasValue && modulExcludeIndex.HasValue)
            {
                List<string> includedProjects = new List<string>();
                while (modulIncludeIndex.Value < args.Length && !args[modulIncludeIndex.Value].StartsWith("-"))
                {
                    includedProjects.Add(args[modulIncludeIndex.Value]);
                    modulIncludeIndex += 1;
                }
                List<string> excludedProjects = new List<string>();
                while (modulExcludeIndex.Value < args.Length && !args[modulExcludeIndex.Value].StartsWith("-"))
                {
                    excludedProjects.Add(args[modulExcludeIndex.Value]);
                    modulExcludeIndex += 1;
                }

                foreach (string line in lines)
                {
                    string[] lineTab = line.Split(';');
                    if ((includedProjects.Contains(lineTab[0]) && !excludedProjects.Contains(lineTab[0])) || lineTab[0] == "s104") myProjects = addToProject(lineTab, myProjects);
                }
            }
            else if (modulIncludeIndex.HasValue)
            {
                List<string> includedProjects = new List<string>();
                while (modulIncludeIndex.Value < args.Length && !args[modulIncludeIndex.Value].StartsWith("-"))
                {
                    includedProjects.Add(args[modulIncludeIndex.Value]);
                    modulIncludeIndex += 1;
                }

                foreach (string line in lines)
                {
                    string[] lineTab = line.Split(';');
                    if (includedProjects.Contains(lineTab[0]) || lineTab[0] == "s104") myProjects = addToProject(lineTab, myProjects);
                }
            }
            else if (modulExcludeIndex.HasValue)
            {
                List<string> excludedProjects = new List<string>();
                while (modulExcludeIndex.Value < args.Length && !args[modulExcludeIndex.Value].StartsWith("-"))
                {
                    excludedProjects.Add(args[modulExcludeIndex.Value]);
                    modulExcludeIndex += 1;
                }

                foreach (string line in lines)
                {
                    string[] lineTab = line.Split(';');
                    if ((!excludedProjects.Contains(lineTab[0]) && lineTab[0] == "s110m1003") || lineTab[0] == "s104") myProjects = addToProject(lineTab, myProjects);
                }
            }
            else
            {
                foreach (string line in lines)
                {
                    string[] lineTab = line.Split(';');
                    if (lineTab[0] == "s110m1003") continue;
                    myProjects = addToProject(lineTab, myProjects);
                }
            }

            VersionFile versionFile = new VersionFile();
            List<ProjectToJSON> allProjects = new List<ProjectToJSON>();
            foreach (string line in lines)
            {
                string[] lineTab = line.Split(';');
                allProjects.Add(new ProjectToJSON(lineTab[0], null));
            }

            string path = Path.Combine(repoRootPath, "beemo_applicatif_backend");
            string[] allFiles = Directory.GetFiles(path);
            Dictionary<string, Version> allVersionFilesDic = new Dictionary<string, Version>();

            foreach (string file in Directory.GetFiles(path))
            {
                if (file.Split('.')[1] == "version" || file.Split('.')[1] == "versionlocal")
                {
                    string version = file.Split('\\').Last().Split('.')[0].Split('_')[0];
                    allVersionFilesDic.Add(file, new Version(version.Replace("-", ".")));
                }
            }

            versionFile.modules = allProjects;

            var test = allVersionFilesDic.OrderByDescending(version => version.Value).ToDictionary(item => item.Key, item => item.Value);

            List<string> allVersionFiles = test.Keys.ToList();

            int z = 0;
            while (!versionFile.isComplete())
            {
                string versionFilePath = allVersionFiles[z];
                VersionFile oldVersionFile = JsonConvert.DeserializeObject<VersionFile>(File.ReadAllText(versionFilePath));

                foreach (ProjectToJSON projet in oldVersionFile.modules)
                {
                    ProjectToJSON actualProject = versionFile.modules.Find(module => module.uid == projet.uid);
                    if (actualProject.version == null) versionFile.modules.Find(module => module.uid == projet.uid).version = projet.version;
                }

                z++;
                if (z == allVersionFiles.Count) break;
            }

            versionFile.publishVersion = versionFile.modules.Find(module => module.uid == "s104").version;

            foreach(ProjectToJSON projet in versionFile.modules)
            {
                if (projet.version == null) projet.version = "0-0-0-0";
            }

            switch (versionToUpdateString)
            {
                case "revision":
                    versionToUpdateInt = 0;
                    break;
                case "build":
                    versionToUpdateInt = 1;
                    break;
                case "mineure":
                    versionToUpdateInt = 2;
                    break;
                case "majeure":
                    versionToUpdateInt = 3;
                    break;
                default:
                    versionToUpdateInt = 0;
                    break;
            }

            try
            {
                string version_s104 = "0-0-0-0";
                if (allProjects.Exists(project => project.uid == "s104")) version_s104 = versionFile.publishVersion;
                else
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("[ERROR] Projet s104 absent du fichier MoodulList.build");
                    Console.ResetColor();
                    return;
                }

                if (doPublish)
                {
                    string[] version_s104Tab = version_s104.Split('-');
                    version_s104Tab[3 - versionToUpdateInt] = (int.Parse(version_s104Tab[3 - versionToUpdateInt]) + 1).ToString();
                    version_s104 = version_s104Tab[0] + "-" + version_s104Tab[1] + "-" + version_s104Tab[2] + "-" + version_s104Tab[3];
                }

                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("[INFO] Paramètres utilisés :");
                Console.Write(((binariesTMP == "_BINAIRIES_TMP") ? "-distant" : "-local") + " -branch " + branch + " -version " + versionToUpdateString + (doTest ? " -test" : "") + (doPublish ? " -publish" : "") + (doDeploy ? " -deploy" : "") + (modulIncludeIndex.HasValue ? " -moduleInclude" : "") + (modulExcludeIndex.HasValue ? " -moduleExclude" : "") + " -modules ");
                if (modulIncludeIndex.HasValue || modulExcludeIndex.HasValue)
                {
                    foreach (Project project in myProjects) Console.Write(project.uid + " ");
                }
                else Console.Write("*");
                Console.WriteLine("\n");
                Console.ResetColor();

                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("[INFO] Version de la s110m1003 : " + versionFile.modules.Find(module => module.uid == "s110m1003").version);
                Console.ResetColor();

                Console.WriteLine("_____________________________________________________________________________________________________");
                Console.WriteLine("                                             TEST");
                Console.WriteLine("_____________________________________________________________________________________________________\n");

                if (doTest) Tester.StartTester(publishFolder, repoRootPath, binariesTMP, branch, version_s104);
                else
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("[INFO] Pas de tests demandés.");
                    Console.ResetColor();
                }

                Console.WriteLine("_____________________________________________________________________________________________________");
                Console.WriteLine("                                          COMPILATION");
                Console.WriteLine("_____________________________________________________________________________________________________\n");

                myProjects = Compiler.StartCompiler(myProjects, publishFolder, repoRootPath, binariesTMP, branch, version_s104);

                List<Project> compiledProjects = new List<Project>();
                List<Project> failedCompileProject = new List<Project>();

                foreach (Project project in myProjects)
                {
                    if (project.builded) compiledProjects.Add(project);
                    else failedCompileProject.Add(project);
                }

                if (compiledProjects.Count == 0) Console.ForegroundColor = ConsoleColor.White;
                else Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.Write("[COMPILE_RESULT_OK] " + compiledProjects.Count);
                foreach(Project project in compiledProjects)
                {
                    if (compiledProjects[0] == project) Console.Write(" [");
                    else Console.Write(", ");
                    Console.Write(project.uid);
                    if (compiledProjects[compiledProjects.Count - 1] == project) Console.Write("]");
                }
                Console.WriteLine();
                Console.ResetColor();

                if (failedCompileProject.Count != 0) Console.ForegroundColor = ConsoleColor.DarkRed;

                Console.Write("[COMPILE_RESULT_ERROR] " + failedCompileProject.Count);
                foreach (Project project in failedCompileProject)
                {
                    if (failedCompileProject[0] == project) Console.Write(" [");
                    else Console.Write(", ");
                    Console.Write(project.uid);
                    if (failedCompileProject[failedCompileProject.Count - 1] == project) Console.Write("]");
                }
                Console.WriteLine();
                Console.ResetColor();

                Console.WriteLine("_____________________________________________________________________________________________________");
                Console.WriteLine("                                          PUBLICATION");
                Console.WriteLine("_____________________________________________________________________________________________________\n");

                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("[INFO] Chemin de publication : " + Path.Combine(publishFolder, binariesTMP, branch));
                Console.ResetColor();

                if (!doPublish)
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("[INFO] Pas de publication demandée.");
                    Console.ResetColor();
                }

                if (doPublish)
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("[INFO] Version de publication : " + version_s104 + "\n");
                    Console.ResetColor();

                    foreach (Project project in myProjects)
                    {
                        if (!project.builded)
                        {
                            doPublish = false;

                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("[WARNING] Pas de publication.");
                            Console.ResetColor();

                            break;
                        }
                    }
                }

                if (doPublish)
                {
                    myProjects = Publisher.StartPublisher(myProjects, publishFolder, repoRootPath, binariesTMP, branch, versionToUpdateInt, version_s104, versionFile);
                    

                    List<Project> publishedProjects = new List<Project>();
                    List<Project> failedPublishProject = new List<Project>();

                    foreach (Project project in myProjects)
                    {
                        if (project.builded) publishedProjects.Add(project);
                        else failedPublishProject.Add(project);
                    }

                    string result;
                    string strCmdPublishAll = "powershell.exe -noprofile -executionPolicy unrestricted .\\BEECONF_PUBLISH_LOCAL.ps1";
                    result = Common.ExecuteCommandSync(strCmdPublishAll, false, "", "", Path.Combine(repoRootPath, "beemo_applicatif_backend", "PowerShellScripts"));
                    if (result != "failure")
                    {
                        Console.WriteLine(result);
                        publishedProjects.Add(new Project("BEECONF_PUBLISH_LOCAL.ps1"));
                    }
                    else
                    {
                        failedPublishProject.Add(new Project("BEECONF_PUBLISH_LOCAL.ps1"));
                    }
                    strCmdPublishAll = "powershell.exe -noprofile -executionPolicy unrestricted .\\TOOLS_PUBLISH_LOCAL.ps1";
                    result = Common.ExecuteCommandSync(strCmdPublishAll, false, "", "", Path.Combine(repoRootPath, "beemo_applicatif_backend", "PowerShellScripts"));
                    if (result != "failure")
                    {
                        Console.WriteLine(result);
                        publishedProjects.Add(new Project("TOOLS_PUBLISH_LOCAL.ps1"));
                    }
                    else
                    {
                        failedPublishProject.Add(new Project("TOOLS_PUBLISH_LOCAL.ps1"));
                    }

                    if (publishedProjects.Count == 0) Console.ForegroundColor = ConsoleColor.White;
                    else Console.ForegroundColor = ConsoleColor.DarkGreen;

                    Console.Write("[PUBLISH_RESULT_OK] " + publishedProjects.Count);
                    foreach (Project project in publishedProjects)
                    {
                        if (publishedProjects[0] == project) Console.Write(" [");
                        else Console.Write(", ");
                        Console.Write(project.uid);
                        if (publishedProjects[publishedProjects.Count - 1] == project) Console.Write("]");
                    }
                    Console.WriteLine();
                    Console.ResetColor();

                    if (failedPublishProject.Count != 0) Console.ForegroundColor = ConsoleColor.DarkRed;

                    Console.Write("[PUBLISH_RESULT_ERROR] " + failedPublishProject.Count);
                    foreach (Project project in failedPublishProject)
                    {
                        if (failedPublishProject[0] == project) Console.Write(" [");
                        else Console.Write(", ");
                        Console.Write(project.uid);
                        if (failedPublishProject[failedPublishProject.Count - 1] == project) Console.Write("]");
                    }
                    Console.WriteLine("\n");
                    Console.ResetColor();
                }

                Console.WriteLine("_____________________________________________________________________________________________________");
                Console.WriteLine("                                             DEPLOY");
                Console.WriteLine("_____________________________________________________________________________________________________\n");

                //if (doDeploy) Deployer.StartDeploy();
                //else
                //{
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("[INFO] Pas de déploiement actuellement.");
                    //Console.ResetColor();
                //}

                Console.ResetColor();
            }
            catch (Exception ex)
            {
                // Build raté
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("[ERROR] " + ex.Message);
                Console.ResetColor();
            }
        }

        public static List<Project> addToProject(string[] lineTab, List<Project> myProjects)
        {
            ProjectType type;
            switch (lineTab[3])
            {
                case "WCF":
                    type = ProjectType.WCF;
                    break;
                case "WEB":
                    type = ProjectType.WEB;
                    break;
                case "NODE":
                    type = ProjectType.NODE;
                    break;
                case "ASPMVC":
                    type = ProjectType.ASPMVC;
                    break;
                default:
                    type = ProjectType.WCF;
                    break;
            }
            myProjects.Add(new Project(lineTab[0], lineTab[1], lineTab[2], type));
            return myProjects;
        }
    }
}
﻿using Newtonsoft.Json;
using s110m1003_AgentBuildDeploy.classes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s110m1003_AgentBuildDeploy
{
    static class Publisher
    {
        static public List<Project> StartPublisher(List<Project> myProjects, string publishFolder, string repoRootPath, string binariesTMP, string branch, int versionToUpdate, string version_s104, VersionFile versionFile)
        {
            foreach (Project project in myProjects)
            {
                if (project.uid == "s110m1003") continue;

                string projectSlnName = project.projectSlnName;

                string projectSlnNameToPrint = projectSlnName;
                while (projectSlnNameToPrint.Length < 40) projectSlnNameToPrint += " ";

                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("[INFO] " + projectSlnNameToPrint);

                Common.WriteInLog("**********" + projectSlnName + "**********\n", "publishinfo");

                if (project.builded)
                {
                    string uid = project.uid;
                    string csProjName = project.csProjName;
                    ProjectType type = project.type;
                    bool found = false;
                    string projectSlnPath = "";

                    string[] pathTab = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName).Split('\\');
                    string frontendPath = Path.Combine(repoRootPath, "beemo_frontend");
                    string backendPath = Path.Combine(repoRootPath, "beemo_applicatif_backend");

                    if (type == ProjectType.WCF || type == ProjectType.NODE || type == ProjectType.ASPMVC)
                    {
                        foreach (string directory in Directory.GetDirectories(backendPath))
                        {
                            foreach (string file in Directory.EnumerateFiles(directory))
                            {
                                string[] filePathTab = file.Split('\\');
                                if (filePathTab[filePathTab.Length - 1] == projectSlnName)
                                {
                                    found = true;
                                    projectSlnPath = directory + "\\" + projectSlnName;
                                    break;
                                }
                            }
                            if (found)
                            {
                                break;
                            }
                        }
                    }
                    else if (type == ProjectType.WEB)
                    {
                        foreach (string directory in Directory.GetDirectories(frontendPath))
                        {
                            foreach (string file in Directory.EnumerateFiles(directory))
                            {
                                string[] filePathTab = file.Split('\\');
                                if (filePathTab[filePathTab.Length - 1] == projectSlnName)
                                {
                                    found = true;
                                    projectSlnPath = directory + "\\" + projectSlnName;
                                    break;
                                }
                            }
                            if (found)
                            {
                                break;
                            }
                        }
                    }

                    project.published = MoveFile(project, projectSlnPath, publishFolder, type, branch, binariesTMP, versionToUpdate, repoRootPath, versionFile).published;
                }
                else
                {
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("   Erreur EC001   ");
                    Console.ResetColor();
                    Common.WriteInLog("Erreur EC001" + "\n\n", "publishinfo");

                }

                Console.WriteLine("");
            }

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("[INFO] Copie des projets de '" + Path.Combine(repoRootPath, "beemo_applicatif_backend", "s110m1003_AgentBuildDeploy", "s110m1003_AgentBuildDeploy", "StaticPublish") + "'");
            Console.ResetColor();
            try
            {
                foreach (string directory in Directory.EnumerateDirectories(Path.Combine(repoRootPath, "beemo_applicatif_backend", "s110m1003_AgentBuildDeploy", "s110m1003_AgentBuildDeploy", "StaticPublish")))
                {
                    DirectoryCopy(directory, Path.Combine(publishFolder, binariesTMP, branch, "BIN", directory.Split('\\').Last()), true);
                }
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("[SUCCES] Copie réussie.");
                Console.ResetColor();
            }
            catch(Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("[ERREUR] Copie échouée.");
                Console.ResetColor();
            }

            Console.WriteLine();

            List<string> infoFiles = new List<string>();

            foreach (string file in Directory.EnumerateFiles(Path.Combine(publishFolder, binariesTMP, branch)))
            {
                string[] fileTab = file.Split('.');
                if (fileTab[fileTab.Length - 1] == "publishinfo") infoFiles.Add(file);
            }

            if (infoFiles.Count() > 9) File.Delete(infoFiles[0]);

            string from = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "temp.publishinfo");
            string to = Path.Combine(publishFolder, binariesTMP, branch, version_s104 + "_" + DateTime.Now.ToString("yyMMddHHmmss")) + ".publishinfo";
            File.Move(from, to);

            from = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"static\ServiceManager.bat");
            to = Path.Combine(publishFolder, binariesTMP, branch, "ServiceManager.bat");
            File.Copy(from, to, true);

            UpdateVersion(null, ProjectType.WCF, versionToUpdate, versionFile, "s110m1003");

            publishVersionFile(versionFile, Path.Combine(repoRootPath, "beemo_applicatif_backend"), 10, binariesTMP);
            //publishVersionFile(versionFile, Path.Combine(repoRootPath, "beemo_frontend"), 10, binariesTMP);
            publishVersionFile(versionFile, Path.Combine(publishFolder, binariesTMP, branch), 10, binariesTMP);

            foreach(Project projet in myProjects)
            {
                if (projet.type != ProjectType.WCF)
                {
                    string path = Path.Combine(publishFolder, binariesTMP, branch, "BIN", projet.uid, "Application Files", projet.csProjName.Replace(".csproj", "_")) + versionFile.modules.Find(module => module.uid == projet.uid).version;
                    publishVersionFile(versionFile, path, 1, binariesTMP);
                }
            }

            return myProjects;
        }

        static private void publishVersionFile(VersionFile versionFile, string path, int maxFiles, string binariesTMP)
        {
            List<ProjectToJSON> projectsToJson = new List<ProjectToJSON>();

            string version_s104 = versionFile.modules.Find(module => module.uid == "s104").version;

            versionFile.publishVersion = versionFile.modules.Find(project => project.uid == "s104").version;
            versionFile.date = DateTime.Now.ToString("yyMMddHHmmss");

            string jsonFile = JsonConvert.SerializeObject(versionFile, Formatting.Indented);

            List<string> versionFiles = new List<string>();
            foreach (string file in Directory.GetFiles(path))
            {
                if (file.Split('.')[1] == "version" || file.Split('.')[1] == "versionlocal") versionFiles.Add(file);
            }

            if (versionFiles.Count == maxFiles) File.Delete(versionFiles[0]);

            string versionExtension;

            if (binariesTMP == "_BINAIRIES_TMP") versionExtension = "version";
            else versionExtension = "versionlocal";

            Common.WriteInLog(jsonFile, versionExtension);

            string from = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "temp." + versionExtension);
            string to = Path.Combine(path, version_s104 + "_" + versionFile.date + "." + versionExtension);
            File.Move(from, to);
        }

        static private Project MoveFile(Project project, string projectSlnPath, string publishFolder, ProjectType type, string branch, string binariesTMP, int versionToUpdate, string repoRootPath, VersionFile versionFile)
        {
            string[] pathTab = projectSlnPath.Split('\\');
            string projectPath = "";
            string projectSlnName = pathTab[pathTab.Length - 1];
            for (int z = 0; z < pathTab.Length - 1; z++)
            {
                projectPath += pathTab[z] + "\\";
            }

            string startProjectPath = null;
            bool found = false;

            if (type == ProjectType.WCF || type == ProjectType.ASPMVC)
            {
                foreach (string directory in Directory.EnumerateDirectories(projectPath))
                {
                    foreach (string file in Directory.EnumerateFiles(directory))
                    {
                        string[] filePathTab = file.Split('\\');
                        if (filePathTab[filePathTab.Length - 1] == project.csProjName)
                        {
                            found = true;
                        }
                    }
                    if (found)
                    {
                        startProjectPath = directory;
                        break;
                    }
                }
            }
            else if (type == ProjectType.WEB)
            {
                startProjectPath = projectPath.Substring(0, projectPath.Length - 1);
            }
            else if (type == ProjectType.NODE)
            {
                startProjectPath = projectSlnPath.Replace(".sln", "");
            }

            string newPath = Path.Combine(publishFolder, binariesTMP, branch, "BIN", project.uid, "Application Files");

            string version;
            string versionPath;
            if (type == ProjectType.WCF) versionPath = Path.Combine(startProjectPath, project.csProjName);
            else if (type == ProjectType.ASPMVC) versionPath = Path.Combine(startProjectPath, "Web.config");
            else if (type == ProjectType.WEB) versionPath = Path.Combine(startProjectPath, "js_shared", "configFile.js");
            else versionPath = Path.Combine(startProjectPath, "app.js");
            try
            {
                versionFile = UpdateVersion(versionPath, type, versionToUpdate, versionFile, project.uid);
                if (type == ProjectType.ASPMVC) versionFile = UpdateVersion(Path.Combine(startProjectPath, "bin", "Release", "Publish", "Web.config"), type, versionToUpdate, versionFile, project.uid) ;
                version = versionFile.modules.Find(module => module.uid == project.uid).version;

                string versionToPrint = version;
                while (versionToPrint.Length < 15) versionToPrint += " ";

                string[] versionToPrintTab = versionToPrint.Split('-');

                switch (versionToUpdate)
                {
                    case 0:
                        Console.Write(versionToPrintTab[0] + "_" + versionToPrintTab[1] + "_" + versionToPrintTab[2] + "_");
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                        Console.Write(versionToPrintTab[3]);
                        Console.ResetColor();
                        break;
                    case 1:
                        Console.Write(versionToPrintTab[0] + "_" + versionToPrintTab[1] + "_");
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                        Console.Write(versionToPrintTab[2]);
                        Console.ResetColor();
                        Console.Write("_" + versionToPrintTab[3]);
                        break;
                    case 2:
                        Console.Write(versionToPrintTab[0] + "_");
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                        Console.Write(versionToPrintTab[1]);
                        Console.ResetColor();
                        Console.Write("_" + versionToPrintTab[2] + "_" + versionToPrintTab[3]);
                        break;
                    case 3:
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                        Console.Write(versionToPrintTab[0]);
                        Console.ResetColor();
                        Console.Write("_" + versionToPrintTab[1] + "_" + versionToPrintTab[2] + "_" + versionToPrintTab[3]);
                        break;
                }

                Console.WriteLine();

                if (type == ProjectType.WCF) startProjectPath = Path.Combine(startProjectPath, "bin", "Release");
                else if (type == ProjectType.ASPMVC) startProjectPath = Path.Combine(startProjectPath, "bin", "Release", "Publish");


                if (!Directory.Exists(newPath)) Directory.CreateDirectory(newPath);
                DirectoryCopy(startProjectPath, Path.Combine(newPath, project.csProjName.Replace(".csproj", "") + "_" + version), true);

                if (project.uid == "s110m1801")
                {
                    string serviceManagerPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"static\ServiceManager.bat");
                    string returnText = "";
                    string[] lines = File.ReadAllLines(serviceManagerPath);

                    foreach (string line in lines)
                    {
                        if (line.StartsWith("cd ")) returnText += "cd \".\\BIN\\" + project.uid + @"\Application Files\" + project.csProjName.Replace(".csproj", "") + "_" + version + "\"";
                        else returnText += line;
                        returnText += "\n";
                    }

                    File.WriteAllText(serviceManagerPath, returnText);
                }

                if (type == ProjectType.NODE) executeNode(Path.Combine(newPath, projectSlnName.Replace(".sln", "") + "_" + version));

                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("      Succès      ");
                Console.ResetColor();
                Common.WriteInLog("Succès" + "\n\n", "publishinfo");
                project.published = true;
            }
            catch (Exception ex)
            {
                Console.BackgroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("      Erreur      ");
                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("[ERROR] " + ex.Message);
                Console.ResetColor();
                project.published = false;
            }

            try
            {
                List<string> myDirectories = new List<string>();
                List<string> myLongerDirectories = new List<string>();
                foreach (string directory in Directory.GetDirectories(newPath))
                {
                    if (myDirectories.Count == 0) myDirectories.Add(directory);
                    else if (directory.Length < myDirectories[0].Length)
                    {
                        foreach (string dir in myDirectories)
                        {
                            myLongerDirectories.Add(dir);
                        }
                        myDirectories.Clear();
                        myDirectories.Add(directory);
                    }
                    else if (directory.Length == myDirectories[0].Length) myDirectories.Add(directory);
                    else if (directory.Length > myDirectories[0].Length) myLongerDirectories.Add(directory);
                }
                myDirectories.Sort();
                myLongerDirectories.Sort();
                while (myDirectories.Count + myLongerDirectories.Count > 3)
                {
                    if (myDirectories.Count > 0)
                    {
                        Directory.Delete(myDirectories[0], true);
                        myDirectories.Remove(myDirectories[0]);
                    }
                    else
                    {
                        Directory.Delete(myLongerDirectories[0], true);
                        myLongerDirectories.Remove(myLongerDirectories[0]);
                    }
                }
            }
            catch (Exception ex)
            {
                //Chemin de publication invalide
            }

            return project;
        }

        static private void executeNode(string path)
        {
            Common.ExecuteCommandSync("npm install", false, "", "", path);
        }

        static private void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (Directory.Exists(destDirName))
            {
                Directory.Delete(destDirName, true);
            }
            Directory.CreateDirectory(destDirName);
            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

        static private VersionFile UpdateVersion(string path, ProjectType type, int versionToUpdate, VersionFile versionFile, string uid)
        {
            if (type == ProjectType.WEB)
            {
                char sep = ' ';
                try
                {
                    string[] lines = File.ReadAllLines(path);
                    for (int z = 0; z < lines.Length; z++)
                    {
                        string[] lineTab = lines[z].Split(sep);
                        int version = 0;
                        switch (versionToUpdate)
                        {
                            case 0:
                                if (lineTab[0] == "VERSION_REVISION")
                                {
                                    version = int.Parse(lineTab[2].Replace("\"", "").Replace(";", ""));
                                    lines[z] = lineTab[0] + " " + lineTab[1] + " \"" + (version + 1) + "\";";
                                }
                                break;
                            case 1:
                                if (lineTab[0] == "VERSION_BUILD")
                                {
                                    version = int.Parse(lineTab[2].Replace("\"", "").Replace(";", ""));
                                    lines[z] = lineTab[0] + " " + lineTab[1] + " \"" + (version + 1) + "\";";
                                }
                                else if (lineTab[0] == "VERSION_REVISION")
                                {
                                    lines[z] = lineTab[0] + " " + lineTab[1] + " \"0\";";
                                }
                                break;
                            case 2:
                                if (lineTab[0] == "VERSION_MINEUR")
                                {
                                    version = int.Parse(lineTab[2].Replace("\"", "").Replace(";", ""));
                                    lines[z] = lineTab[0] + " " + lineTab[1] + " \"" + (version + 1) + "\";";
                                }
                                else if (lineTab[0] == "VERSION_REVISION" || lineTab[0] == "VERSION_BUILD")
                                {
                                    lines[z] = lineTab[0] + " " + lineTab[1] + " \"0\";";
                                }
                                break;
                            case 3:
                                if (lineTab[0] == "VERSION_MAJEUR")
                                {
                                    version = int.Parse(lineTab[2].Replace("\"", "").Replace(";", ""));
                                    lines[z] = lineTab[0] + " " + lineTab[1] + " \"" + (version + 1) + "\";";
                                }
                                else if (lineTab[0] == "VERSION_REVISION" || lineTab[0] == "VERSION_BUILD" || lineTab[0] == "VERSION_MINEUR")
                                {
                                    lines[z] = lineTab[0] + " " + lineTab[1] + " \"0\";";
                                }
                                break;
                        }
                    }
                    File.WriteAllLines(path, lines);
                }
                catch (Exception ex)
                {
                    //Fichier non trouvé
                }
            }
            else if (type == ProjectType.NODE)
            {
                char sep = ' ';
                try
                {
                    string[] lines = File.ReadAllLines(path);
                    for (int z = 0; z < lines.Length; z++)
                    {
                        string[] firstSep = lines[z].Split('=');
                        string[] lineTab = lines[z].Replace("  ", "").Split(sep);
                        if (lineTab.Length > 1 && lineTab[1] == "_version")
                        {
                            string[] version = lineTab[3].Replace("\"", "").Replace(";", "").Split('.');
                            switch (versionToUpdate)
                            {
                                case 0:
                                    lines[z] = firstSep[0] + "= \"" + version[0] + "." + version[1] + "." + version[2] + "." + (int.Parse(version[3]) + 1) + "\";";
                                    break;
                                case 1:
                                    lines[z] = firstSep[0] + "= \"" + version[0] + "." + version[1] + "." + (int.Parse(version[2]) + 1) + ".0\";";
                                    break;
                                case 2:
                                    lines[z] = firstSep[0] + "= \"" + version[0] + "." + (int.Parse(version[1]) + 1) + ".0.0\";";
                                    break;
                                case 3:
                                    lines[z] = firstSep[0] + "= \"" + (int.Parse(version[0]) + 1) + ".0.0.0\";";
                                    break;
                            }
                        }
                    }
                    File.WriteAllLines(path, lines);
                }
                catch (Exception ex)
                {
                    //Fichier non trouvé
                }
            }
            else if (type == ProjectType.ASPMVC)
            {
                try
                {
                    string[] lines = File.ReadAllLines(path);
                    for (int z = 0; z < lines.Length; z++)
                    {
                        string[] lineTab = lines[z].Split('\"');
                        if (lineTab.Length > 1 && lineTab[1] == "version")
                        {
                            Version version;
                            Version.TryParse(lineTab[3], out version);

                            switch (versionToUpdate)
                            {
                                case 0:
                                    version = new Version(version.Major, version.Minor, version.Build, version.Revision + 1);
                                    break;
                                case 1:
                                    version = new Version(version.Major, version.Minor, version.Build + 1, 0);
                                    break;
                                case 2:
                                    version = new Version(version.Major, version.Minor+ 1, 0, 0);
                                    break;
                                case 3:
                                    version = new Version(version.Major + 1, 0, 0, 0);
                                    break;
                            }

                            lines[z] = lineTab[0] + "\"" + lineTab[1] + "\"" + lineTab[2] + "\"" + version.Major.ToString() + "." + version.Minor.ToString() + "." + version.Build.ToString() + "." + version.Revision.ToString() + "\" />";
                        }
                    }
                    File.WriteAllLines(path, lines);
                }
                catch (Exception ex)
                {
                    //Fichier non trouvé
                }
            }

            string versionFromVersionFile = versionFile.modules.Find(module => module.uid == uid).version;
            string[] versionFromVersionFileTab = versionFromVersionFile.Split('-');
            versionFromVersionFileTab[versionFromVersionFileTab.Length - versionToUpdate - 1] = (int.Parse(versionFromVersionFileTab[versionFromVersionFileTab.Length - versionToUpdate - 1]) + 1).ToString();

            versionFile.modules.Find(module => module.uid == uid).version = versionFromVersionFileTab[0] + "-" + versionFromVersionFileTab[1] + "-" + versionFromVersionFileTab[2] + "-" + versionFromVersionFileTab[3];

            return versionFile;
        }

    }
}

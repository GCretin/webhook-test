﻿using s110m1003_AgentBuildDeploy.classes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s110m1003_AgentBuildDeploy
{
    static class Compiler
    {
        static public List<Project> StartCompiler(List<Project> myProjects, string publishFolder, string repoRootPath, string binariesTMP, string branch, string version_s104)
        {
            try
            {
                foreach (Project project in myProjects)
                {
                    string uid = project.uid;
                    string projectSlnName = project.projectSlnName;
                    ProjectType type = project.type;
                    bool found = false;
                    string projectSlnPath = "";

                    //string projectSlnNameToPrint = projectSlnName;
                    //while (projectSlnNameToPrint.Length < 40) projectSlnNameToPrint += " ";

                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("[INFO] " + projectSlnName);

                    if (type == ProjectType.WCF || type == ProjectType.ASPMVC)
                    {
                        Common.WriteInLog("**********" + projectSlnName + "**********\n", "buildinfo");

                        string backendPath = Path.Combine(repoRootPath, "beemo_applicatif_backend");

                        foreach (string directory in Directory.GetDirectories(backendPath))
                        {
                            foreach (string file in Directory.EnumerateFiles(directory))
                            {
                                string[] filePathTab = file.Split('\\');
                                if (filePathTab[filePathTab.Length - 1] == projectSlnName)
                                {
                                    found = true;
                                    projectSlnPath = directory + "\\" + projectSlnName;
                                    break;
                                }
                            }
                            if (found)
                            {
                                break;
                            }
                        }

                        if (Compile(projectSlnPath, publishFolder, binariesTMP, type))
                        {
                            project.builded = true;
                        }
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.DarkBlue;
                        Console.WriteLine("      Ignoré      ");
                        Console.ResetColor();
                    }
                    Console.WriteLine("");
                }

                if (!Directory.Exists(Path.Combine(publishFolder, binariesTMP)) || !Directory.Exists(Path.Combine(publishFolder, binariesTMP, branch)))
                {
                    Directory.CreateDirectory(Path.Combine(publishFolder, binariesTMP, branch));
                }

                List<string> infoFiles = new List<string>();

                foreach (string file in Directory.EnumerateFiles(Path.Combine(publishFolder, binariesTMP, branch)))
                {
                    string[] fileTab = file.Split('.');
                    if (fileTab[fileTab.Length - 1] == "buildinfo") infoFiles.Add(file);
                }

                if (infoFiles.Count() > 9) File.Delete(infoFiles[0]);

                string from = Path.Combine(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), "temp") + ".buildinfo";
                string to = Path.Combine(publishFolder, binariesTMP, branch, version_s104 + "_" + DateTime.Now.ToString("yyMMddHHmmss")) + ".buildinfo";

                File.Move(from, to);

                return myProjects;
            }
            catch (Exception ex)
            {
                // Build raté
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        static private bool Compile(string projectSlnPath, string publishFolder, string binariesTMP, ProjectType type)  
        {
            string strCmdRestore = "dotnet restore " + projectSlnPath + " --no-dependencies -r win-x64";

            Common.ExecuteCommandSync(strCmdRestore, false, "", "");
            string strCmdBuild;
            string msbuild = "MsBuildPath";
            string msbuildPath = null;

            for (int z = 1; z < 5; z++)
            {
                msbuildPath = ConfigurationManager.AppSettings.Get(msbuild + z);

                if (msbuildPath != "" && File.Exists(msbuildPath))
                {
                    break;
                }
            }

            if (!File.Exists(msbuildPath))
            {
                Console.BackgroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Erreur lors de la tentative d'accès au MSBuild");
                Console.ResetColor();

                return false;
            }

            if (type == ProjectType.WCF) strCmdBuild = "\"" + msbuildPath + "\" " + projectSlnPath + " -p:Configuration=Release";
            else strCmdBuild = "\"" + msbuildPath + "\" " + projectSlnPath + " /p:DeployOnBuild=true /p:PublishProfile=s110m1003_PublishProfile";

            return (Common.ExecuteCommandSync(strCmdBuild, true, publishFolder, binariesTMP) == "success");
        }
    } 
}

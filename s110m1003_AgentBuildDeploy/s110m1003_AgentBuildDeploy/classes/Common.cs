﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s110m1003_AgentBuildDeploy.classes
{
    static class Common
    {
        static public string ExecuteCommandSync(object command, bool compile, string publishFolder, string binariesTMP, string startPath = null)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("[INFO] " + command + (startPath != null ? " depuis " + startPath : ""));
            Console.ResetColor();

            try
            {
                string branch = GetGitBranch();
                ProcessStartInfo procStartInfo = new ProcessStartInfo("cmd.exe", "/c " + command)
                {
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    WindowStyle = ProcessWindowStyle.Normal
                };

                if (!String.IsNullOrEmpty(startPath)) procStartInfo.WorkingDirectory = startPath;

                Process proc = Process.Start(procStartInfo);

                if (compile)
                {
                    string line;
                    string result = "";
                    Dictionary<string, string> errors = new Dictionary<string, string>();
                    bool success = false;
                    while (!proc.StandardOutput.EndOfStream)
                    {
                        line = proc.StandardOutput.ReadLine();
                        result += line + "\n";
                        if (line.Contains(" error "))
                        {
                            string[] errorTab = line.Split(':');
                            if (!errors.ContainsKey((errorTab[0] + errorTab[1]).Replace(" ", ""))) errors.Add((errorTab[0] + errorTab[1]).Replace(" ", ""), line);
                        }
                        if (line.Contains("La génération a réussi."))
                        {
                            success = true;
                        }
                    }

                    if (proc.ExitCode == 1)
                    {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("[ERREUR] Erreur lors de l'éxécution de la commande : " + command);
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                        Console.WriteLine("[SUCCES] Commande exécutée.");
                        Console.ResetColor();
                    }

                    if (success)
                    {
                        Console.BackgroundColor = ConsoleColor.DarkGreen;
                        Console.WriteLine("      Succès      ");
                        WriteInLog("Generation réussie" + "\n\n", "buildinfo");
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("      Erreur      ");
                        Console.ResetColor();
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine(procStartInfo.Arguments);
                        Console.ResetColor();
                        if (errors.Count == 0)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkYellow;
                            Console.WriteLine("[WARNING] Aucun résultat de compilation.");
                            Console.ResetColor();
                        }
                        foreach (KeyValuePair<string, string> keyValue in errors)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine("[ERROR] " + keyValue.Value);
                            Console.ResetColor();
                        }
                        string errorsString = "";
                        foreach (KeyValuePair<string, string> error in errors) errorsString += error.Value + "\n";
                        WriteInLog(errorsString + "\n", "buildinfo");
                        Console.ResetColor();

                        WriteInLog(result, "buildinfo");
                        return "failure";
                    }
                    Console.ResetColor();

                    return "success";
                }
                else
                {
                    string result = proc.StandardOutput.ReadToEnd();

                    if (proc.ExitCode == 1)
                    {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("[ERROR] Erreur lors de l'éxécution de la commande : " + command);
                        Console.ResetColor();
                        return "failure";
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                        Console.WriteLine("[SUCCES] Commande exécutée.");
                        Console.ResetColor();
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("[ERROR] Erreur lors de l'éxécution de la commande : " + command + " : " + ex.Message);
                Console.ResetColor();
                return "failure";
            }
        }

        static public string GetGitBranch()
        {
            try
            {
                string repoRootPath = Environment.GetEnvironmentVariable("BEE_REPO_ROOT", EnvironmentVariableTarget.Machine);
                string text = File.ReadAllText(Path.Combine(repoRootPath, "beemo_applicatif_backend\\.git\\HEAD"));
                string[] branchTab = text.Split('/');
                string branch = "";
                for (int z = 2; z < branchTab.Length; z++)
                {
                    branch += branchTab[z];
                    if (z < branchTab.Length - 1) branch += "_";
                }
                return branch.Replace("\n","");
            }
            catch (Exception ex)
            {
                // Ne peut pas accéder au .git/HEADER
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("[ERROR] Récupération de la branche impossible : " + ex.Message);
                return null;
            }
        }

        static public void WriteInLog(string text, string extension)
        {
            string tempFilePath = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + "\\temp." + extension;
            string savedText = "";
            if (File.Exists(tempFilePath)) savedText += File.ReadAllText(tempFilePath);
            text = savedText + text;
            File.WriteAllText(tempFilePath, text);
        }
    }
}

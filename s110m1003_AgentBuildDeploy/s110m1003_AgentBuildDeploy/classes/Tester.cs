﻿using s110m1003_AgentBuildDeploy.classes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s110m1003_AgentBuildDeploy
{
    static class Tester
    {
        static public void StartTester(string publishFolder, string repoRootPath, string binariesTMP, string branch, string version_s104)
        {
            string backendPath = Path.Combine(repoRootPath, "beemo_applicatif_backend");
            string frontendPath = Path.Combine(repoRootPath, "beemo_frontend");

            string[] testCsproj = File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"static\ModulList.test"));

            foreach (string directory in Directory.GetDirectories(backendPath))
            {
                foreach (string subDirectory in Directory.GetDirectories(directory))
                {
                    string myFile = null;
                    foreach (string file in Directory.GetFiles(subDirectory))
                    {
                        if (file.Split('.')[1] == "csproj")
                        {
                            int fileLength = file.Split('\\').Length;
                            myFile = file.Split('\\')[fileLength - 1];
                            break;
                        }
                    }
                    if (Array.Exists(testCsproj, element => element == myFile))
                    {
                        string strCmdTest = "cd " + subDirectory + " && dotnet test " + myFile;

                        Console.WriteLine("TEST \n" + strCmdTest + "\n");

                        string result = Common.ExecuteCommandSync(strCmdTest, false, "", "");
                        Common.WriteInLog(result, ".testinfo");

                        string[] resultTab = result.Split('\n');
                        string resultToDisplay = "";

                        foreach (string line in resultTab)
                        {
                            if (line.Contains("Nombre total de tests"))
                            {
                                int index = Array.IndexOf(resultTab, line);
                                for (int z = index; z < resultTab.Length; z++)
                                {
                                    resultToDisplay += resultTab[z];
                                }
                                break;
                            }
                        }
                        Console.WriteLine(resultToDisplay);
                        Console.WriteLine("DONE");
                    }
                }
            }

            foreach (string directory in Directory.GetDirectories(frontendPath))
            {
                foreach (string subDirectory in Directory.GetDirectories(directory))
                {
                    string myFile = null;
                    foreach (string file in Directory.GetFiles(subDirectory))
                    {
                        if (file.Split('.')[1] == "csproj")
                        {
                            int fileLength = file.Split('\\').Length;
                            myFile = file.Split('\\')[fileLength - 1];
                            break;
                        }
                    }
                    if (Array.Exists(testCsproj, element => element == myFile))
                    {
                        string strCmdTest = "cd " + subDirectory + " && dotnet test " + myFile;

                        Console.WriteLine("Test de " + myFile + "");

                        string result = Common.ExecuteCommandSync(strCmdTest, false, "", "");

                        if (!Directory.Exists(Path.Combine(publishFolder, binariesTMP)) || !Directory.Exists(Path.Combine(publishFolder, binariesTMP, branch)))
                        {
                            Directory.CreateDirectory(Path.Combine(publishFolder, binariesTMP, branch));
                        }

                        Common.WriteInLog(result, "testinfo");

                        string[] resultTab = result.Split('\n');

                        foreach (string line in resultTab)
                        {
                            if (line.Contains("X "))
                            {
                                Console.BackgroundColor = ConsoleColor.DarkRed;
                                Console.WriteLine(line);
                                Console.ResetColor();
                            }
                            else if (line.Contains("V "))
                            {
                                Console.BackgroundColor = ConsoleColor.DarkGreen;
                                Console.ForegroundColor = ConsoleColor.Black;
                                Console.WriteLine(line);
                                Console.ResetColor();
                            }
                            else if (line.Contains("Nombre total de tests"))
                            {
                                Console.WriteLine("");
                                int index = Array.IndexOf(resultTab, line);
                                for (int z = index ; z < resultTab.Length; z++)
                                {
                                    Console.WriteLine(resultTab[z]);
                                }
                                break;
                            }                            
                        }
                        Console.WriteLine("DONE \n");
                    }
                }
            }

            List<string> infoFiles = new List<string>();

            foreach (string file in Directory.EnumerateFiles(Path.Combine(publishFolder, binariesTMP, branch)))
            {
                string[] fileTab = file.Split('.');
                if (fileTab[fileTab.Length - 1] == "testinfo") infoFiles.Add(file);
            }

            if (infoFiles.Count() > 9) File.Delete(infoFiles[0]);

            string from = Path.Combine(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), "temp.testinfo");
            string to = Path.Combine(publishFolder, binariesTMP, branch, version_s104 + "_" + DateTime.Now.ToString("yyMMddHHmmss") + ".testinfo");

            File.Move(from, to);
        }
    }
}

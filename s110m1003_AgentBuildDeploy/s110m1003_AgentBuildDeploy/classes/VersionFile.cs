﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s110m1003_AgentBuildDeploy.classes
{
    class VersionFile
    {
        public string publishVersion;
        public string date;
        public List<ProjectToJSON> modules;

        public VersionFile(string date, List<ProjectToJSON> modules)
        {
            this.date = date;
            this.modules = modules;
        }
        public VersionFile()
        {
        }

        public bool isComplete()
        {
            foreach (ProjectToJSON projet in this.modules)
            {
                if (projet.version == null) return false;
            }
            return true;
        }
    }
}

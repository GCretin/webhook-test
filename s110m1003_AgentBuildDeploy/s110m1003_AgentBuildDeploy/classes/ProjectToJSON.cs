﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s110m1003_AgentBuildDeploy.classes
{
    class ProjectToJSON
    {
        public string uid;
        public string version;

        public ProjectToJSON(string uid, string version)
        {
            this.uid = uid;
            this.version = version;
        }
    }
}

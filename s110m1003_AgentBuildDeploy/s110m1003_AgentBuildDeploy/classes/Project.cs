﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s110m1003_AgentBuildDeploy
{
    enum ProjectType 
    {
        WCF,
        WEB,
        NODE,
        ASPMVC
    };

    class Project
    {
        public string uid;
        public string projectSlnName;
        public string csProjName;
        public ProjectType type;
        public bool builded;
        public bool published;

        public Project(string uid, string projectSlnName, string csProjName, ProjectType type)
        {
            this.uid = uid;
            this.projectSlnName = projectSlnName;
            this.csProjName = csProjName;
            this.type = type;
            if (type == ProjectType.WCF || type == ProjectType.ASPMVC) builded = false;
            else builded = true;
            this.published = false;
        }

        public Project(string uid)
        {
            this.uid = uid;
        }
    }
}
